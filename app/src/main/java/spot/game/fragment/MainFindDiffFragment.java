package spot.game.fragment;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.res.AssetManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ScreenUtils;
import com.blankj.utilcode.util.ThreadUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.blankj.utilcode.util.VibrateUtils;

import net.goal.differences.BuildConfig;
import net.goal.differences.R;
import net.goal.differences.databinding.GameSpotFragmentMainFindDiffLayoutZcBinding;

import java.util.ArrayList;
import java.util.List;

import razerdp.basepopup.BasePopupWindow;
import spot.differences.gola.GolaAThisCorrectAnswerWindowSSSof;
import spot.differences.gola.GolaNewUserWindowSSSof;
import spot.differences.gola.GolaUsCollectPieceWindowSSSOF;
import spot.game.constant.ZeusConfig;
import spot.game.manager.ADManager;
import spot.game.manager.ConfigManager;
import spot.game.manager.ContextManager;
import spot.game.manager.GameManager;
import spot.game.manager.TrackManager;
import spot.game.model.JLogExtBean;
import spot.game.model.callModel.ProfitRequest;
import spot.game.model.callModel.ZeusEvent;
import spot.game.model.backModel.ResultAnswerResponse;
import spot.game.util.AnimationUtils;
import spot.game.util.MineDataUtils;
import spot.game.util.ViewUtils;
import spot.game.view.gameFindFileDiffFile.GameFindFileDiffTargetModel;
import spot.game.view.gameFindFileDiffFile.GameFindFileDiffViewFile;
import spot.game.view.gameFindFileDiffFile.GameFindFileViewFile;

/**
 * 答题Fragment
 */
public class MainFindDiffFragment extends BaseFragment implements View.OnClickListener {

    //    //兑换手机
//    @BindView(R.id.rl_game_spot_fragment_find_diff_phone_task)
//    RelativeLayout rlPhoneTask;
//    //宝藏图鉴
//    @BindView(R.id.rl_game_spot_fragment_find_diff_treasure_task)
//    RelativeLayout rlTreasureTask;
//    //标题
//    @BindView(R.id.tv_game_spot_fragment_find_diff_storage_title)
//    TextView tvStorageTitle;
//    //提示
//    @BindView(R.id.tv_game_spot_find_diff_zone_tips_dot)
//    TextView tvTipsDot;
//    @BindView(R.id.iv_game_spot_find_diff_zone_ad_dot)
//    ImageView ivAdDot;
//    @BindView(R.id.tv_game_spot_find_diff_zone_tips)
//    TextView tvTips;
//    @BindView(R.id.tv_game_spot_fragment_find_diff_header_desc)
//    TextView tvCollectTitleDesc;
//    @BindView(R.id.tv_game_spot_fragment_find_diff_treasure_task_desc)
//    TextView tvTreasureTitleDesc;
//    @BindView(R.id.tv_game_spot_fragment_find_diff_header_money_desc)
//    TextView tvTreasureTitleProgress;
//    //@BindView(R.id.rl_game_spot_find_diff_zone_tips)
//    //RelativeLayout rlFindDiffZoneTips;
//    //问题区域
//    @BindView(R.id.fdv_game_spot_find_diff_content)
//    GameFindFileDiffViewFile mFindDiffView;
//    //
//    @BindView(R.id.iv_game_spot_fragment_find_diff_first_guide)
//    ImageView quizFirstGuide;
//    @BindView(R.id.pb_game_spot_fragment_find_diff_phone_task_bg_anim_2)
//    View pbCollectPiece;
//    @BindView(R.id.pb_game_spot_fragment_find_diff_treasure_task_bg_anim_2)
//    View pbTreasure;
//    @BindView(R.id.pb_game_spot_fragment_find_dif_withdraw_progress)
//    ProgressBar pbWithdraw;
//    @BindView(R.id.tv_game_spot_fragment_find_dif_withdraw_progress)
//    TextView tvWithdraw;
//    @BindView(R.id.iv_game_spot_find_diff_withdraw_progress_reward)
//    ImageView ivWithdrawIcon;

    private GameSpotFragmentMainFindDiffLayoutZcBinding mBinding;
    
    
    static final int MSG_LOAD_QUESTION = BuildConfig.VERSION_CODE;

    private int questionNo; // 当前通过第几题
    private MediaPlayer mBgPlayer;// 图片背景音乐
    private List<GameFindFileDiffTargetModel> questionLists;
    private GameFindFileDiffTargetModel currentQuestionInfo;

    private boolean isVisible = false;
    private AssetManager mAssetManager;

    /**
     * 视图是否已经初初始化
     */
    protected boolean isInit = false;
    protected boolean isLoad = false;
    private int guideTimes; // 提示次数
    GolaUsCollectPieceWindowSSSOF mCollectPieceWindow;
    GolaAThisCorrectAnswerWindowSSSof mCorrectAnswerWindow;

    private CountDownTimer notClickCountDownTimer;


    public static MainFindDiffFragment getInstance() {
        return new MainFindDiffFragment();
    }

    Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_LOAD_QUESTION://
                    if (!getUserVisibleHint()) {
                        break;
                    }
                    LogUtils.e("MSG_LOAD_QUESTION");
                    questionLists = MineDataUtils.getInstance().getQuestionLists();
                    //getDelayLoadingDismiss();
                    if (questionLists != null && questionNo >= 0 && 0 < questionLists.size()) {
                        currentQuestionInfo = questionLists.get(questionNo % questionLists.size());
                        updateQuestionInfo(false);
                    }
                    break;
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = GameSpotFragmentMainFindDiffLayoutZcBinding.inflate(inflater);
        isInit = true;
        /**初始化的时候去加载数据**/
        isCanLoadData();
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getContext() != null) {
            mAssetManager = getContext().getAssets();
        }
        //
        initView();
    }

    private void isCanLoadData() {
        if (!isInit) {
            return;
        }
        if (getUserVisibleHint()) {
            lazyLoad();
            isLoad = true;
        } else {
            if (isLoad) {
                stopLoad();
            }
        }
    }

    public void lazyLoad() {
        if (ConfigManager.getIsNew()) {
            GolaNewUserWindowSSSof.showNewUserWindow(getActivity());
            ConfigManager.setIsNew(false);
        }

        loadQuestionList();

        questionNo = ConfigManager.getLocalLevel();
        guideTimes = ConfigManager.getFindDiffTipsGuideCount();
    }

    /**
     * 当视图已经对用户不可见并且加载过数据，如果需要在切换到其他页面时停止加载数据，可以调用此方法
     */
    protected void stopLoad() {

    }

    private void initView() {

        int screen_height = ScreenUtils.getScreenHeight();
        if (screen_height > 1920){
            mBinding.topDivideLayoutView.setVisibility(View.VISIBLE);
        }else{
            mBinding.topDivideLayoutView.setVisibility(View.GONE);
        }

        //, float defaultSp, int startChangLength, float maxSP, float minSP, float stepRatio
        ViewUtils.autoResizeText(mBinding.tvGameSpotFragmentFindDiffHeaderDesc, ContextManager.getContext().getString(R.string.game_spot_collect_piece_pthone), 12, 8, 20, 8, 1);
        ViewUtils.autoResizeText(mBinding.tvGameSpotFragmentFindDiffTreasureTaskDesc, ContextManager.getContext().getString(R.string.game_spot_collect), 12, 8, 20, 8, 1);
        //
        guideTimes = ConfigManager.getFindDiffTipsGuideCount();
        setTipsDot(guideTimes);
        //
        setTreasureProgress();
        //
        mBinding.rlGameSpotFragmentFindDiffTreasureTask.setOnClickListener(this);
        mBinding.rlGameSpotFragmentFindDiffPhoneTask.setOnClickListener(this);
        mBinding.tvGameSpotFindDiffZoneTips.setOnClickListener(this);
        mBinding.pbGameSpotFragmentFindDifWithdrawProgress.setOnClickListener(this);
        mBinding.ivGameSpotFindDiffWithdrawProgressReward.setOnClickListener(this);
        //
        AnimationUtils.playRotationAnimation(mBinding.pbGameSpotFragmentFindDiffPhoneTaskBgAnim2, 3_000);
        AnimationUtils.playRotationAnimation(mBinding.pbGameSpotFragmentFindDiffTreasureTaskBgAnim2, 3_000);
        //
        setWithdrawEnableProgress();

    }

    private void setWithdrawEnableProgress() {
        int needLevel = ConfigManager.getNeedLevel();
        int localLevel = ConfigManager.getLocalLevel();
        if (localLevel >= needLevel) {
            mBinding.tvGameSpotFragmentFindDifWithdrawProgress.setText(needLevel + "/" + needLevel);
        } else {
            mBinding.tvGameSpotFragmentFindDifWithdrawProgress.setText(localLevel + "/" + needLevel);
        }
        if (localLevel > 0 && localLevel < needLevel * 0.1) {
            localLevel = (int) (needLevel * 0.1);
        }

        mBinding.pbGameSpotFragmentFindDifWithdrawProgress.setMax(needLevel);
        mBinding.pbGameSpotFragmentFindDifWithdrawProgress.setProgress(localLevel);
        if (localLevel >= needLevel) {
            mBinding.ivGameSpotFindDiffWithdrawProgressReward.setImageResource(R.mipmap.game_spot_icon_find_diff_withdraw_progress_reward);
        } else {
            mBinding.ivGameSpotFindDiffWithdrawProgressReward.setImageResource(R.mipmap.game_spot_icon_find_diff_withdraw_progress_reward_gray);
        }
    }

    private void setTreasureProgress() {
        mBinding.tvGameSpotFragmentFindDiffHeaderMoneyDesc.setText(ConfigManager.getTreasureCompleteCount() + "/" + ConfigManager.getTreasureTotalCount());
    }

    private void setTipsDot(int guideTimes) {
        if (guideTimes < 1) {
            mBinding.tvGameSpotFindDiffZoneTipsDot.setVisibility(View.GONE);
            mBinding.ivGameSpotFindDiffZoneAdDot.setVisibility(View.VISIBLE);
        } else {
            mBinding.ivGameSpotFindDiffZoneAdDot.setVisibility(View.GONE);
            mBinding.tvGameSpotFindDiffZoneTipsDot.setVisibility(View.VISIBLE);
            mBinding.tvGameSpotFindDiffZoneTipsDot.setText(String.valueOf(guideTimes));
        }
    }



    @Override
    public void onClick(View view) {
        GameManager.newClick();
        switch (view.getId()) {
            case R.id.rl_game_spot_fragment_find_diff_phone_task:
                TrackManager.sendTrack(JLogExtBean.event(TrackManager.iphone_click).setIntvalue(questionNo));
                showCollectPieceWindow();
                break;
            case R.id.rl_game_spot_fragment_find_diff_treasure_task:
                TrackManager.sendTrack(JLogExtBean.event(TrackManager.treasure_click).setIntvalue(questionNo));
                GameManager.intentToTreasurePage(getContext());
                break;
            case R.id.pb_game_spot_fragment_find_dif_withdraw_progress:
            case R.id.iv_game_spot_find_diff_withdraw_progress_reward:
                GameManager.intentToMine(getContext());
                break;
            case R.id.tv_game_spot_find_diff_zone_tips:
                TrackManager.sendTrack(JLogExtBean.event(TrackManager.prompt_click).setIntvalue(questionNo));
                resetIvGuide();
                cancelNotClickCountDownTimer();
                if (guideTimes <= 0) {
                    showRequestAddTipsDialog();
                } else {
                    mBinding.fdvGameSpotFindDiffContent.guide(new GameFindFileDiffViewFile.GuideListener() {
                        @Override
                        public void guideSuccess() {
                            guideTimes--;
                            setTipsDot(guideTimes);
                            ConfigManager.setFindDiffTipsGuideCount(guideTimes);
                        }

                        @Override
                        public void guideFail(String msg) {

                        }
                    });
                }
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getUserVisibleHint()) {
            isVisible = true;
            //setPlayWhenReady(true);

            if (notClickCountDownTimer != null) {
                notClickCountDownTimer.start();
            }
        } else {
            isVisible = false;
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isCanLoadData();
        if (isVisibleToUser) {
            isVisible = true;
            //setPlayWhenReady(true);

            if (notClickCountDownTimer != null) {
                notClickCountDownTimer.start();
            }
        } else {
            cancelNotClickCountDownTimer();

            isVisible = false;
            //setPlayWhenReady(false);
            //playBgMusic(false);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        isVisible = false;
        //setPlayWhenReady(false);
        //playBgMusic(false);

        cancelNotClickCountDownTimer();
    }

    @Override
    public void onPause() {
        super.onPause();
        isVisible = false;
        //playBgMusic(false);

        cancelNotClickCountDownTimer();
    }

    /**
     * 获取问题列表
     */
    private void loadQuestionList() {
        //getDelayLoadingShow();
        if (questionLists == null) {
            questionLists = new ArrayList<>();
        }
        MineDataUtils.getInstance().loadCorrectZone(mHandler,getContext(),MSG_LOAD_QUESTION);
    }

    /**
     * 更新题目信息
     */
    public void updateQuestionInfo() {
        updateQuestionInfo(true);
    }

    public void updateQuestionInfo(boolean isSwitchQuestionAnimate) {
        TrackManager.sendTrack(JLogExtBean.event("updateQuestionInfo").setIntvalue(questionNo));
        //
        resetIvGuide();
        cancelNotClickCountDownTimer();
        setWithdrawEnableProgress();
        if (questionLists == null || questionLists.isEmpty()) {

            //playBgMusic();

            loadQuestionList();
            return;
        }

//        mBinding.fdvGameSpotFindDiffContent.tvGameSpotFragmentFindDiffStorageTitle.setText(Html.fromHtml(ContextManager.getContext().getString(R.string.game_spot_level, " " + (questionNo + 1))));
        GameFindFileDiffTargetModel oldFindDiffTargetZone = questionLists.get((questionNo + questionLists.size() - 1) % questionLists.size());

        currentQuestionInfo = questionLists.get(questionNo % questionLists.size());

        //问题和答案
        GameFindFileDiffTargetModel findDiffTargetZone = currentQuestionInfo;

        //mAnswerBtnAnim.start();
        //playBgMusic();

        if (findDiffTargetZone != null) {
            //初始化关卡数据
            mBinding.fdvGameSpotFindDiffContent.initData(questionNo + 1, findDiffTargetZone.getTarget_location());
            //关卡监听
            mBinding.fdvGameSpotFindDiffContent.setFindDiffListener(new MyFindDiffListener());
            //关卡切换
            mBinding.fdvGameSpotFindDiffContent.update(findDiffTargetZone, oldFindDiffTargetZone, isSwitchQuestionAnimate);
            //宝藏
            final int treasureCompleteCount = ConfigManager.getTreasureCompleteCount();
            if (MineDataUtils.treasureConfigList.get(treasureCompleteCount) <= questionNo + 1) {
                mBinding.fdvGameSpotFindDiffContent.checkTreasureView(MainTreasureFragment.getTreasureUri(MainTreasureFragment.SMALL_TREASURE_URI, MineDataUtils.treasureItemConfigList.get(treasureCompleteCount)), mBinding.rlGameSpotFragmentFindDiffTreasureTask, new Runnable() {
                    @Override
                    public void run() {
                        TrackManager.sendTrack(JLogExtBean.event("checkTreasureView").setIntvalue(questionNo));
                        playRawMusic(R.raw.game_spot_find_treasure);
                        ConfigManager.setTreasureCompleteCount(ConfigManager.getTreasureCompleteCount() + 1);
                        setTreasureProgress();
                    }
                });
            }
            if (questionNo == 0) {
                // 播放点击引导动画
                mBinding.fdvGameSpotFindDiffContent.guide(new GameFindFileDiffViewFile.GuideListener() {
                    @Override
                    public void guideSuccess() {

                    }

                    @Override
                    public void guideFail(String msg) {

                    }
                });

            }

            startNotClickCountDownTimer(mBinding.ivGameSpotFragmentFindDiffFirstGuide);
        }
    }

    //开始计时
    private void startNotClickCountDownTimer(ImageView ivRight) {
        if (ivRight == null) {
            return;
        }
        notClickCountDownTimer = new CountDownTimer(15000, 1000L) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                ivRight.setVisibility(View.VISIBLE);
                AnimatorSet animatorSet = new AnimatorSet();
                ObjectAnimator anim1 = ObjectAnimator.ofFloat(ivRight, "scaleX", 1, 1.05F, 0.95F, 1.1F, 0.9F, 1);
                anim1.setRepeatCount(-1);
                anim1.setDuration(2000);
                ObjectAnimator anim2 = ObjectAnimator.ofFloat(ivRight, "scaleY", 1, 1.05F, 0.95F, 1.1F, 0.9F, 1);
                anim2.setRepeatCount(-1);
                anim2.setDuration(2000);
                animatorSet.playTogether(anim1, anim2);
                animatorSet.start();
                notClickCountDownTimer = null;
            }
        };
        notClickCountDownTimer.start();
    }

    //取消长时间未点击计时
    private void cancelNotClickCountDownTimer() {
        if (notClickCountDownTimer != null) {
            notClickCountDownTimer.cancel();
        }
    }

    //重置长时间未点击引导
    public void resetIvGuide() {
        mBinding.ivGameSpotFragmentFindDiffFirstGuide.setVisibility(View.INVISIBLE);
    }

    private void checkAnswer() {
        ZeusEvent zeusEvent = new ZeusEvent();
        zeusEvent.setEvent(ZeusConfig.ext_pass);
        TrackManager.sendZeusEvent(zeusEvent);
        //
        if (questionLists == null || questionLists.size() == 0) return;
        chekAnswerResult();
    }


    /**
     * 根据答题结果显示弹框
     */
    private void chekAnswerResult() {
        //
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // 下一题
                updateQuestionInfo();
            }
        }, 600);
        //询问
        if (mCorrectAnswerWindow != null) {
            mCorrectAnswerWindow.dismiss();
        }
        mCorrectAnswerWindow = new GolaAThisCorrectAnswerWindowSSSof(getContext());
        ResultAnswerResponse response = new ResultAnswerResponse();
        response.setAwMoney(GameManager.getPassReward(true));
        mCorrectAnswerWindow.setParam(response, new GolaAThisCorrectAnswerWindowSSSof.CorrectListener() {
            @Override
            public void showAd() {
                if (mCorrectAnswerWindow != null) {
                    mCorrectAnswerWindow.dismiss();
                }
                TrackManager.sendTrack(JLogExtBean.event(TrackManager.level_double).setIntvalue(questionNo));
                viewAdVideoToMoreReward(ProfitRequest.DOUBLE_REWARD, ProfitRequest.ANSWER_REWARD);
            }

            @Override
            public void close() {
                TrackManager.sendTrack(JLogExtBean.event("mCorrectAnswerWindow", "close").setIntvalue(questionNo));
                if (mCorrectAnswerWindow != null) {
                    mCorrectAnswerWindow.dismiss();
                }
                //强弹
                if (GameManager.getLevelAd(questionNo)) {
                    viewAdVideoToMoreReward(true, ProfitRequest.DOUBLE_REWARD, ProfitRequest.ANSWER_REWARD);
                } else {
                    resultAnswerProfit(ProfitRequest.ANSWER_REWARD);
                }
            }
        });
        mCorrectAnswerWindow.showPopupWindow();
//        if (questionNo == 1) {
//            TrackManager.sendTrack(JLogExtBean.event(TrackManager.novice_click).setIntvalue(questionNo));
//        }
    }


    private void resultAnswerProfit(String type) {
        if (TextUtils.isEmpty(type)) {
            return;
        }
        //提示道具个数
        if (ProfitRequest.TIPS_REWARD.equalsIgnoreCase(type)) {
            ConfigManager.setFindDiffTipsGuideCount(3);
            guideTimes = 3;
            setTipsDot(guideTimes);
            return;
        }
        //钞票奖励
        Activity activity = null;
        try {
            double reward = 0d;
            activity = getSafeActivity();
            switch (type) {
                case ProfitRequest.ANSWER_REWARD:
                    reward = GameManager.getPassReward(false);
                    break;
                case ProfitRequest.DOUBLE_REWARD:
                    reward = GameManager.getPassReward(false) * 2;
                    break;
            }
            GameManager.showRewardToast(reward, activity);
        } catch (Exception e) {
            checkActivityLive();
        }
    }


    public void playRawMusic(int rawResId) {
        ThreadUtils.executeByIo(new ThreadUtils.SimpleTask<Object>() {
            @Override
            public Object doInBackground() throws Throwable {
                try {
                    releasePlayRawMusic();
                    //
                    mBgPlayer = MediaPlayer.create(getContext(), rawResId);
                    mBgPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                        @Override
                        public boolean onError(MediaPlayer mediaPlayer2, int i, int i1) {
                            if (mBgPlayer != null) {
                                mBgPlayer.reset();
                            }
                            return false;
                        }
                    });
                    mBgPlayer.start();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            public void onSuccess(Object result) {

            }

        });
    }

    private void releasePlayRawMusic() {
        if (mBgPlayer != null) {
            mBgPlayer.pause();
            mBgPlayer.reset();
            mBgPlayer.release();
        }
    }


    private void showRequestAddTipsDialog() {
        viewAdVideoToMoreReward(ProfitRequest.TIPS_REWARD, "");
        //if (mRequestAddTipsDialog == null && getActivity() != null) {
        //    mRequestAddTipsDialog = new RequestAddTipsDialog(getActivity(), new View.OnClickListener() {
        //        @Override
        //        public void onClick(View v) {
        //            // 播广告
        //            String adCode = RewardADVideoUtils.checkBestLocationCode(getActivity(), Constants.AD_LOCATION_OBTIVE_REWARDVIDEO);
        //            RewardADVideoUtils.playPreVideoAd(getActivity(), adCode, new ADVideoActionListener() {
        //                @Override
        //                public void videoAdSuccess(String requestId, XzAdCallbackModel xzAdCallbackModel) {
        //                    guideTimes = 3;
        //                     setTipsDot(guideTimes);
        //                    UtilsSharePre.setPreferenceInt(getContext(), Constants.SP_GUIDE_TIMES, guideTimes);
        //                }
        //
        //                @Override
        //                public void videoAdFail() {
        //                    ToastUtil.showTextToast(getContext(), "广告播放失败，请5秒后再试");
        //                }
        //
        //                @Override
        //                public void videoCancel(XzAdCallbackModel xzAdCallbackModel) {
        //                    ToastUtil.showTextToast(getContext(), "请观看完整广告，可获得提示");
        //                }
        //
        //                @Override
        //                public void videoControlError() {
        //                    ToastUtil.showTextToast(getContext(), "广告播放失败，请5秒后再试");
        //                }
        //
        //                @Override
        //                public void videoClose() {
        //                    ToastUtil.showBgToast(getContext(), R.layout.layout_reward_tips_zc);
        //                }
        //            });
        //        }
        //    });
        //}
        //mRequestAddTipsDialog.show();
    }

    private class MyFindDiffListener implements GameFindFileDiffViewFile.FindDiffListener {
        @Override
        public void findCorrect(MotionEvent event, GameFindFileViewFile diffZoneView) {
            playRawMusic(R.raw.game_spot_find_diff);
        }

        @Override
        public void findWrong(MotionEvent event, GameFindFileViewFile diffZoneView) {
            VibrateUtils.vibrate(50);
        }

        @Override
        public void finish() {
            //if (BuildConfig.DEBUG) {
            //    questionNo = new Random().nextInt(300);
            //}
            questionNo++;
            LogUtils.e("questionNo=" + questionNo);
            ConfigManager.setLocalLevel(questionNo);
            TrackManager.sendTrack(JLogExtBean.event(TrackManager.level).setIntvalue(questionNo));
            checkAnswer();
        }
    }


    /**
     * 看激励视频
     */
    private void viewAdVideoToMoreReward(String hiddenRewardType, String errorRewardType) {
        viewAdVideoToMoreReward(false, hiddenRewardType, errorRewardType);
    }

    private void viewAdVideoToMoreReward(boolean isForce, String hiddenRewardType, String errorRewardType) {
        ADManager.ADShowListener listener = new ADManager.ADShowListener() {

            @Override
            public void onAdHidden() {
                resultAnswerProfit(hiddenRewardType);
            }

            @Override
            public void onAdDisplayFailed() {
                ToastUtils.showShort(getContext().getResources().getString(R.string.game_spot_ad_failed));
                resultAnswerProfit(errorRewardType);
            }
        };
        if (isForce) {
            ADManager.INSTANCE().showInterstitial(listener);
        } else {
            ADManager.INSTANCE().showMaxVideoAd(listener);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (mCollectPieceWindow != null) {
            mCollectPieceWindow.dismiss();
            mCollectPieceWindow = null;
        }
        if (mBinding.fdvGameSpotFindDiffContent != null) {
            mBinding.fdvGameSpotFindDiffContent.removeAllViews();
//            mBinding.fdvGameSpotFindDiffContent = null;
        }
    }

    private void showCollectPieceWindow() {
        if (mCollectPieceWindow != null) {
            if (!mCollectPieceWindow.isShowing()) {
                return;
            } else {
                mCollectPieceWindow.dismiss();
                mCollectPieceWindow = null;
            }
        }
        try {
            mCollectPieceWindow = new GolaUsCollectPieceWindowSSSOF(getSafeActivity());
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        mCollectPieceWindow.setOnDismissListener(new BasePopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                mCollectPieceWindow = null;
            }
        });
        mCollectPieceWindow.showPopupWindow();
    }

}
