package spot.game.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.blankj.utilcode.util.ActivityUtils;

import spot.game.manager.ContextManager;
import spot.game.manager.GameManager;

/**
 * @Class: BaseFragment
 * @author: 许聪云
 * @E-mail: xucongyun@gmail.com
 * @Date: 2021/10/12
 * @Description:
 */
public class BaseFragment extends Fragment {

    Context mContext;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    /**
     * onDestroyView中进行解绑操作
     */
    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @Nullable
    @Override
    public Context getContext() {
        try {
            Context context = super.getContext();
            if (context != null) {
                return context;
            }
            if (mContext != null) {
                return mContext;
            }
            //可能引起内存泄漏，所以需要及时释放对象
            Activity activity = ContextManager.getActivity();
            if (ActivityUtils.isActivityAlive(activity)) {
                return activity;
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return ContextManager.getContext();
    }


    protected Activity getSafeActivity() throws Exception {
        Activity activity = getActivity();
        Context context = getContext();
        if (context != null && context instanceof Activity) {
            activity = (Activity) context;
        }
        if(!ActivityUtils.isActivityAlive(activity)){
            throw new Exception("fragment not attach activity");
        }
        return activity;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }


    /**
     * 检测Activity是否正常存活
     * 通常在一些回调或者常用方法里检查
     * * @return
     */
    protected boolean checkActivityLive() {
        if (getActivity() != null && !getActivity().isFinishing() && !getActivity().isDestroyed()) {
            return true;
        }
        GameManager.restartApp(getContext());
        return false;
    }
}
