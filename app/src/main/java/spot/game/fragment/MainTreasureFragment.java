package spot.game.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;

import com.blankj.utilcode.util.ThreadUtils;
import com.blankj.utilcode.util.ToastUtils;

import net.goal.differences.BuildConfig;
import net.goal.differences.R;
import net.goal.differences.databinding.GameSpotFragmentTreasureBinding;

import java.util.ArrayList;
import java.util.List;

import spot.game.adapter.TreasureAdapter;
import spot.game.manager.BalanceManager;
import spot.game.manager.ConfigManager;
import spot.game.manager.ContextManager;
import spot.game.manager.GameManager;
import spot.game.manager.TrackManager;
import spot.game.model.JLogExtBean;
import spot.game.model.TreasureBean;
import spot.game.util.MineDataUtils;
import spot.game.util.ViewUtils;

public class MainTreasureFragment extends BaseFragment implements View.OnClickListener {

    private static final String BASE_TREASURE_URI = "file:///android_asset/spot_gola_treasure/";
    public static final String SMALL_TREASURE_URI = BASE_TREASURE_URI + "spot_gola_treasure_s/spot_gola_treasures";
    public static final String NORMAL_TREASURE_URI = BASE_TREASURE_URI + "spot_gola_treasure/spot_gola_treasure";
    public static final String UNLOCK_TREASURE_URI = BASE_TREASURE_URI + "spot_gola_treasure_gray/spot_gola_treasureh";
    //
    private static final int MSG_LOAD_LIST = BuildConfig.VERSION_CODE;
    
    private GameSpotFragmentTreasureBinding mBinding;

    TreasureAdapter mTreasureAdapter;
    List<TreasureBean> mTreasureBeanList;

    /**
     * 视图是否已经初初始化
     */
    protected boolean isInit = false;
    protected boolean isLoad = false;

    public static MainTreasureFragment getInstance() {
        return new MainTreasureFragment();
    }

    Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_LOAD_LIST://
                    setTreasureProgress();
                    if (mTreasureBeanList != null && !mTreasureBeanList.isEmpty()) {
                        mTreasureAdapter.setData(mTreasureBeanList);
                        mTreasureAdapter.notifyDataSetChanged();
                    }
                    break;
            }
        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = GameSpotFragmentTreasureBinding.inflate(inflater);
        isInit = true;
        /**初始化的时候去加载数据**/
        isCanLoadData();
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //
        initView();
    }

    private void isCanLoadData() {
        if (!isInit) {
            return;
        }
        if (getUserVisibleHint()) {
            lazyLoad();
            isLoad = true;
        } else {
            if (isLoad) {
                stopLoad();
            }
        }
    }

    protected void lazyLoad() {
        loadTreasureList();
    }


    private void loadTreasureList() {
        ThreadUtils.executeByIo(new ThreadUtils.SimpleTask<Object>() {
            @Override
            public Object doInBackground() throws Throwable {
                try {
                    int treasureCompleteCount = ConfigManager.getTreasureCompleteCount();
                    int totalCount = ConfigManager.getTreasureTotalCount();
                    if (mTreasureBeanList == null) {
                        mTreasureBeanList = new ArrayList<>(totalCount);
                    }
                    mTreasureBeanList.clear();
                    List<Integer> completeTreasureItemConfigList = null;
                    if (treasureCompleteCount > 0) {
                        completeTreasureItemConfigList = MineDataUtils.treasureItemConfigList.subList(0, treasureCompleteCount);
                    }
                    //从1开始
                    for (int i = 1; i <= totalCount; i++) {
                        //重置
                        TreasureBean bean = new TreasureBean();
                        if (completeTreasureItemConfigList != null && completeTreasureItemConfigList.contains(i)) {//已完成
                            bean.setIconUrl(getTreasureUri(NORMAL_TREASURE_URI, i));
                        } else {//未完成
                            bean.setIconUrl(getTreasureUri(UNLOCK_TREASURE_URI, i));
                        }
                        try {
                            ////检验以及预加载，占内存
                            //Glide.with(getContext()).load(bean.getIconUrl()).preload();
                            mTreasureBeanList.add(bean);
                        } catch (Throwable e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Throwable e) {
                    e.printStackTrace();
                } finally {
                    mHandler.sendEmptyMessage(MSG_LOAD_LIST);
                }
                return null;
            }

            @Override
            public void onSuccess(Object result) {

            }
        });
    }

    /**
     * 当视图已经对用户不可见并且加载过数据，如果需要在切换到其他页面时停止加载数据，可以调用此方法
     */
    protected void stopLoad() {

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isCanLoadData();
    }

    private void initView() {
        mBinding.rvGameSpotFragmentTrasureList.setLayoutManager(new GridLayoutManager(getContext(), 3));
        mTreasureAdapter = new TreasureAdapter();
        mBinding.rvGameSpotFragmentTrasureList.setAdapter(mTreasureAdapter);
        //
        Spanned text = Html.fromHtml(ContextManager.getContext().getString(R.string.game_spot_resgatar) + "<br>" + ConfigManager.getCurrency() + BalanceManager.getFormatBalance(2000 * ConfigManager.getBalanceRate()));
        ViewUtils.autoResizeText(mBinding.tvGameSpotFragmentTrasureSubmitBtn, text, 15, 19, 32, 11, 1);
        //
        mBinding.ivGameSpotFragmentTreasureBackBtn.setOnClickListener(this);
        mBinding.tvGameSpotFragmentTrasureSubmitBtn.setOnClickListener(this);
        //
        setTreasureProgress();
    }

    private void setTreasureProgress() {
        //
        int progress = ConfigManager.getTreasureCompleteCount();
        int totalCount = ConfigManager.getTreasureTotalCount();
        String percentProgress = (progress * 100 / totalCount) + "%";
        mBinding.tvGameSpotFragmentTrasureProgressContent.setText(percentProgress);
        mBinding.tvGameSpotFragmentTrasureDesc.setText(Html.fromHtml(ContextManager.getContext().getString(R.string.game_spot_progress_percent, percentProgress)));
        if (progress > 0 && progress < totalCount * 0.1) {
            progress = (int) (totalCount * 0.1);
        }
        mBinding.pbGameSpotFragmentTrasureProgress.setProgress(progress);
        mBinding.pbGameSpotFragmentTrasureProgress.setMax(totalCount);
    }


    @Override
    public void onClick(View v) {
        GameManager.newClick();
        switch (v.getId()) {
            case R.id.iv_game_spot_fragment_treasure_back_btn:
                GameManager.toHome();
                break;
            case R.id.tv_game_spot_fragment_trasure_submit_btn:
                TrackManager.sendTrack(JLogExtBean.event("treasure_submit").setIntvalue(ConfigManager.getTreasureCompleteCount()));
                ToastUtils.showShort(Html.fromHtml(ContextManager.getContext().getString(R.string.game_spot_collect_treasure_submit_btn, "" + ConfigManager.getTreasureCompleteCount())));
                break;
        }
    }


    /**
     * 返回固定路劲uri
     *
     * @param type
     * @param index 从1开始
     * @return
     */
    public static String getTreasureUri(String type, int index) {
        return type + index + ".png";
    }
}
