package spot.game.application;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.webkit.WebView;

import com.blankj.utilcode.util.ProcessUtils;

import net.goal.differences.BuildConfig;
import net.goal.differences.R;

import spot.game.callback.AliveActivityLifecycleCallbacks;
import spot.game.constant.GameConfig;
import spot.game.manager.ADManager;
import spot.game.manager.othersdk.AdJustManager;
import spot.game.manager.ConfigManager;
import spot.game.manager.ContextManager;
import spot.game.manager.GameManager;
import spot.game.manager.TrackManager;
import spot.game.manager.othersdk.FaceBookManager;
import spot.game.model.DefaultConfigBean;
import spot.game.model.JLogExtBean;

public class GameApplication extends Application {

    static GameApplication mGameApplication;
    public static boolean isAppForeground;

    public static Application getIns() {
        return mGameApplication;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        mGameApplication = this;
        String procName = ProcessUtils.getCurrentProcessName();
        if (procName != null && !procName.equals(getPackageName())) {
            if (Build.VERSION.SDK_INT >= 28) {
                WebView.setDataDirectorySuffix(ProcessUtils.getCurrentProcessName());
            }
        }
        if (ProcessUtils.isMainProcess() && !GameConfig.isWhite) {
            //
            DefaultConfigBean config = new DefaultConfigBean.Builder().appCode(BuildConfig.APP_CODE)
                    .appName(getResources().getResourceName(R.string.game_spot_app_name))
                    .appVersionCode(BuildConfig.VERSION_CODE)
                    .appVersionName(BuildConfig.VERSION_NAME)
                    .channel("Google")
                    .debug(BuildConfig.DEBUG).build();
            GameManager.preInit(this, config);
            if (ConfigManager.AdJustEnable()) {
                FaceBookManager.initFacebookSdk();
                AdJustManager.INSTANCE().init(this);
            }
            //
            registerActivityLifecycleCallbacks(new ContextManager());
            //
            AliveActivityLifecycleCallbacks callback = new AliveActivityLifecycleCallbacks();
            callback.register(new AliveActivityLifecycleCallbacks.ActivityAllPauseListener() {
                @Override
                public void allActivityPaused() {
                    isAppForeground = false;
                    TrackManager.sendTrack(JLogExtBean.event(TrackManager.appclose));
                    GameManager.stopMedia();
                }

                @Override
                public void hadAliveActivity(int count, Activity activity) {
                    //回到前台拉一下广告
                    isAppForeground = true;
                    if (count == 1) {
                        ADManager.INSTANCE().loadAd();
                    }
                    //
                    if (GameManager.isAppActivity(activity)) {
                        GameManager.initBGMMedia();
                    } else {
                        GameManager.stopMedia();
                    }
                }
            });
            registerActivityLifecycleCallbacks(callback);
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        GameManager.attachBaseContext(this);
        mGameApplication = this;
    }


}
