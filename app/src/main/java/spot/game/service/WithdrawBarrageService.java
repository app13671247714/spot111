package spot.game.service;

import android.app.Activity;
import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.ThreadUtils;
import com.bumptech.glide.Glide;

import net.goal.differences.BuildConfig;

import java.util.Locale;
import java.util.Random;

import spot.differences.gola.GolaBannerTopBarrageWindowSSSof;
import spot.game.manager.ConfigManager;
import spot.game.manager.ContextManager;
import spot.game.manager.GameManager;
import spot.game.model.WithdrawBarrageList;

public class WithdrawBarrageService extends Service {

    final String mFilePath = "file:///android_asset/spot_gola_barrage/spot_gola_barrage_";
    final String mImageName = "pot_gola_barrage_";
    final int MSG_ROLLING = BuildConfig.VERSION_CODE + 2023;
    int mIndex = 0;
    boolean isStart;
    ThreadUtils.SimpleTask<Object> mTask;
    GolaBannerTopBarrageWindowSSSof mWithdrawBarrageWindow;

    WithdrawBarrageList mList = null;
    //
    Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_ROLLING:
                    mHandler.removeMessages(MSG_ROLLING);
                    if (!isStart) {
                        return;
                    }
                    String country = ConfigManager.getCountry().trim().toLowerCase(Locale.ROOT);
                    if (mList != null && mList.getList() != null && country.equalsIgnoreCase(mList.getCountry())) {
                        mIndex = (mIndex + 1) % mList.getList().size();
                        start(mList.getList().get(mIndex));
                        //
                        Random random = new Random();
                        mHandler.sendEmptyMessageDelayed(MSG_ROLLING, (random.nextInt(10) + 20) * 1000);
                    } else {
                        initData();
                    }
                    break;
            }
        }
    };

    private void initData() {
        String country = ConfigManager.getCountry().trim().toLowerCase(Locale.ROOT);
        if (mList != null && mList.getList() != null && country.equalsIgnoreCase(mList.getCountry())) {
            return;
        }
        mTask = new ThreadUtils.SimpleTask<Object>() {

            @Override
            public Object doInBackground() throws Throwable {
                try {
                    String listPath = "us";
                    String headIconPrefix = "br_";
                    String headIconPath = "br";
                    int maxMoney = 101;
                    int minMoney = 50;
                    switch (country) {
                        case "br":
                            listPath = "br";
                            break;
                        case "id":
                            listPath = "id";
                            headIconPrefix = "id_";
                            headIconPath = "id";
                            maxMoney = 100000;
                            minMoney = 50000;
                            break;
                        default:
                            listPath = "us";
                            break;
                    }

                    try {
                        String json = GameManager.loadJSONFromAsset(ContextManager.getContext(), "spot_gola_barrage/spot_gola_" + listPath + ".json");
                        mList = GsonUtils.fromJson(json, WithdrawBarrageList.class);
                        mList.setCountry(country);
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                    Random random = new Random();
                    if (mList != null && mList.getList() != null) {
                        for (int i = 0; i < mList.getList().size(); i++) {
                            WithdrawBarrageList.SpotGolaBarrageBean bean = mList.getList().get(i);
                            if (bean == null) {
                                continue;
                            }
                            try {
                                if (!listPath.equalsIgnoreCase("us")) {
                                    bean.setMoney(ConfigManager.getCurrency() + (random.nextInt(maxMoney - minMoney) + minMoney));
                                }
                                bean.setHeadIcon(Glide.with(ContextManager.getContext()).load(mFilePath + headIconPath + "/" + mImageName + headIconPrefix + bean.getIndex() + ".png").submit().get());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } catch (Throwable e) {
                    e.printStackTrace();
                } finally {
                    if (isStart) {
                        mHandler.sendEmptyMessage(MSG_ROLLING);
                    }
                }
                return null;
            }

            @Override
            public void onSuccess(Object result) {

            }
        };
        ThreadUtils.executeByIo(mTask);
    }

    public void start(WithdrawBarrageList.SpotGolaBarrageBean bean) {
        if (bean == null || bean.getHeadIcon() == null) {
            initData();
            return;
        }
        try {
            Activity activity = ContextManager.getActivity();
            if (mWithdrawBarrageWindow != null) {
                mWithdrawBarrageWindow.dismiss();
                mWithdrawBarrageWindow = null;
            }
            mWithdrawBarrageWindow = new GolaBannerTopBarrageWindowSSSof(activity);
            mWithdrawBarrageWindow.start(bean);
            mWithdrawBarrageWindow.showPopupWindow();
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (mWithdrawBarrageWindow != null) {
                        mWithdrawBarrageWindow.dismiss();
                    }
                }
            }, 5_000);
        } catch (Throwable e) {
            e.printStackTrace();
            mHandler.sendEmptyMessageDelayed(MSG_ROLLING, 1_000);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        isStart = true;
        initData();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mTask != null) {
            mTask.onCancel();
            mTask = null;
        }
        mHandler.removeMessages(MSG_ROLLING);
        mHandler.removeCallbacks(null);
        if (mWithdrawBarrageWindow != null) {
            mWithdrawBarrageWindow.dismiss();
            mWithdrawBarrageWindow = null;
        }
        isStart = false;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}