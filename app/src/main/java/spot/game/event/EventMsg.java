package spot.game.event;

public class EventMsg {
    int code;
    int arg1;
    double arg2;
    String msg;

    private EventMsg(Builder builder) {
        this.code = builder.code;
        this.arg1 = builder.arg1;
        this.arg2 = builder.arg2;
        this.msg = builder.msg;
    }

    public EventMsg() {
    }


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getArg1() {
        return arg1;
    }

    public void setArg1(int arg1) {
        this.arg1 = arg1;
    }

    public double getArg2() {
        return arg2;
    }

    public void setArg2(double arg2) {
        this.arg2 = arg2;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }


    public static class Builder {
        int code;
        int arg1;
        double arg2;
        String msg;


        public Builder setCode(int code) {
            this.code = code;
            return this;
        }


        public Builder setArg1(int arg1) {
            this.arg1 = arg1;
            return this;
        }


        public Builder setArg2(double arg2) {
            this.arg2 = arg2;
            return this;
        }


        public Builder setMsg(String msg) {
            this.msg = msg;
            return this;
        }

        public EventMsg build() {
            return new EventMsg(this);
        }
    }

    public static Builder event(int code) {
        return new Builder().setCode(code);
    }

    public static Builder event(int code, int arg1) {
        return new Builder().setCode(code).setArg1(arg1);
    }

    public static Builder event(int code, double arg2) {
        return new Builder().setCode(code).setArg2(arg2);
    }

    public static Builder event(int code, String msg) {
        return new Builder().setCode(code).setMsg(msg);
    }
}
