package spot.game.event;

import net.goal.differences.BuildConfig;

public class EventConstant {
    public static final int CODE_EVENT_RESET_ACTIVITY = 0x123 + BuildConfig.VERSION_CODE;
    public static final int CODE_EVENT_MAIN_TO_TAB= CODE_EVENT_RESET_ACTIVITY + 1;
    public static final int CODE_EVENT_REWARD_ACCOUNT = CODE_EVENT_MAIN_TO_TAB + 1;
    public static final int CODE_EVENT_REFRESH_WHOLE_THEME_BG = CODE_EVENT_REWARD_ACCOUNT + 1;
    public static final int CODE_EVENT_WINDOW_HAS_SHOWING = CODE_EVENT_REFRESH_WHOLE_THEME_BG + 1;
    public static final int CODE_EVENT_WINDOW_ALL_DISMISS = CODE_EVENT_WINDOW_HAS_SHOWING + 1;
    //public static final int CODE_EVENT_RESTART_GAME = CODE_EVENT_WINDOW_ALL_DISMISS + 1;
}
