package spot.game.popupwindow;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ClickUtils;

import net.goal.differences.R;
import net.goal.differences.databinding.GameSpotPopWithdrawalDouubleCheckLayoutBinding;

import spot.game.manager.GameManager;


/**
 * 假提现 二次确认
 */
public class WithdrawalCashDoubleCheckDialog extends Dialog {

    private GameSpotPopWithdrawalDouubleCheckLayoutBinding mBinding;


//    private ChangeStateListener mListener;
    public WithdrawalCashDoubleCheckDialog(@NonNull Context context, DialogButtonClickListener listener) {
        super(context, R.style.game_spot_NormalDialog);
        mBinding = GameSpotPopWithdrawalDouubleCheckLayoutBinding.inflate(LayoutInflater.from(context));
        setContentView(mBinding.getRoot());

//        mListener = listener;
        initView();
    }

    private void initView() {
        ClickUtils.applyPressedViewScale(mBinding.ivWithdrawPopClose,mBinding.tvWithdrawInfoPopConfirm);

        mBinding.ivWithdrawPopClose.setOnClickListener(view -> dismiss());

        mBinding.tvWithdrawInfoPopConfirm.setOnClickListener(view -> {
//            if (mListener!=null) {
//                        mListener.onChange(true);
//                    }
                dismiss();
        });
    }

    @Override
    public void dismiss() {
        super.dismiss();
        GameManager.newClick();
    }
}
