package spot.game.popupwindow;

import android.app.Activity;
import android.app.Dialog;
import android.view.LayoutInflater;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ClickUtils;

import net.goal.differences.R;
import net.goal.differences.databinding.GameSpotPopWithdrawalCancellationLayoutBinding;

import spot.game.manager.GameManager;
import spot.game.util.GameTaskUtils;


/**
 * 假提现 取消弹窗
 */
public class WithdrawalCancellationDialog extends Dialog {
    private GameSpotPopWithdrawalCancellationLayoutBinding mBinding;
    private DialogButtonClickListener mListener;

    public WithdrawalCancellationDialog(@NonNull Activity context, DialogButtonClickListener listener) {
        super(context, R.style.game_spot_NormalDialog);
        mBinding = GameSpotPopWithdrawalCancellationLayoutBinding.inflate(LayoutInflater.from(context));
        setContentView(mBinding.getRoot());
        mListener = listener;

        mBinding.ivWithdrawPopClose.setOnClickListener(view -> {
            if (mListener != null) {
                mListener.onClose();
            }
            dismiss();
        });
        mBinding.tvWithdrawInfoPopConfirm.setOnClickListener(view -> {
            GameTaskUtils.resetTaskState();
            if (mListener != null) {
                mListener.onConfirm();
            }
            dismiss();
        });

        ClickUtils.applyPressedViewScale(mBinding.ivWithdrawPopClose, mBinding.tvWithdrawInfoPopConfirm);
    }

    @Override
    public void dismiss() {
        super.dismiss();
        GameManager.newClick();
    }
}
