package spot.game.popupwindow;

import android.app.Activity;
import android.app.Dialog;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ClickUtils;

import net.goal.differences.R;
import net.goal.differences.databinding.GameSpotPopWithdrawalProgressLayoutBinding;

import spot.game.manager.BalanceManager;
import spot.game.manager.ConfigManager;
import spot.game.manager.ContextManager;
import spot.game.manager.GameManager;
import spot.game.model.WithdrawStateModel;
import spot.game.util.GameTaskUtils;


/**
 * 假提现 进度弹窗
 */
public class WithdrawalProgressDialog extends Dialog {

    private WithdrawStateModel mWithdrawStateModel;
    private GameSpotPopWithdrawalProgressLayoutBinding mBinding;

    public WithdrawalProgressDialog(@NonNull Activity context) {
        super(context, R.style.game_spot_NormalDialog);
        mBinding = GameSpotPopWithdrawalProgressLayoutBinding.inflate(LayoutInflater.from(context));
        setContentView(mBinding.getRoot());

        mWithdrawStateModel = GameTaskUtils.getWithdrawStateModel();
        initView();
        initData();
    }

    private void initView() {

        View.OnClickListener clickListener= new ClickUtils.OnDebouncingClickListener(500){
            @Override
            public void onDebouncingClick(View v) {
                switch (v.getId()) {
                    case R.id.iv_pop_close:
                        dismiss();
                        break;
                    case R.id.tv_pop_confirm:
                        dismiss();
                }
            }
        };

        mBinding.ivPopClose.setOnClickListener(clickListener);
        mBinding.tvPopConfirm.setOnClickListener(clickListener);

        ClickUtils.applyPressedViewScale(mBinding.ivPopClose, mBinding.tvPopConfirm);
    }

    private void initData(){
        String balanceChange = ConfigManager.getCurrency() + BalanceManager.getFormatBalance(mWithdrawStateModel.getWithdrawCashNmu());
        mBinding.tvBalance.setText(balanceChange);

        WithdrawStateModel model = GameTaskUtils.getWithdrawStateModel();

        int current = model.getCompleteDay();
        int requirement = model.getWithdrawCashPlayAdDay();

        if (mWithdrawStateModel.getWithdrawCashProgress() == 2){

            int adPlayCount = model.getPlayAdDayCount();
            int withdrawCashPlayAdDayCount = model.getWithdrawCashPlayAdDayCount();

            //签到几天是完成任务提示
            mBinding.tvTaskHint.setText(Html.fromHtml(ContextManager.getContext()
                    .getString(R.string.game_spot_check_in_for_3_days_to_complete_the_withdrawal_review, requirement+"")));
            //当日视频个观看数量
            mBinding.tvTaskHintTwo.setText(Html.fromHtml(ContextManager.getContext()
                    .getString(R.string.game_spot_watch_20_videos_to_oomplete_todsy_s_check_in_10_20,
                            withdrawCashPlayAdDayCount+"","("+adPlayCount+"/"+withdrawCashPlayAdDayCount+")")));
            //签到了几天
            mBinding.tvUserTaskProgress.setText(Html.fromHtml(ContextManager.getContext()
                    .getString(R.string.game_spot_you_have_checked_in_for_1_days, current+"")));
        }else {
            current = ConfigManager.getLocalLevel();
            requirement = model.getWithdrawCashLevel();

            //通关任务提示
            mBinding.tvTaskHint.setText(Html.fromHtml(ContextManager.getContext().getString(R.string.game_spot_complete_level_100, requirement+"")));
            //当日视频个观看数量
            mBinding.tvTaskHintTwo.setVisibility(View.GONE);
            //通关任务完成多少
            mBinding.tvUserTaskProgress.setText(Html.fromHtml(ContextManager.getContext().getString(R.string.game_spot_you_have_completed_level, current+"")));
        }

        //任务进度
        mBinding.tvTaskProgress.setText(current+"/"+requirement);
        mBinding.withdrawProgressBar.setProgress(current);
        mBinding.withdrawProgressBar.setMax(requirement);
    }

    @Override
    public void dismiss() {
        super.dismiss();
        GameManager.newClick();
    }
}
