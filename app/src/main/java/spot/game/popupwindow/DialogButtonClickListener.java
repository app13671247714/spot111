package spot.game.popupwindow;

public interface DialogButtonClickListener {
    void onClose();
    void onConfirm();
}
