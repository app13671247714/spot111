package spot.game.popupwindow;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.TimeUtils;

import net.goal.differences.R;
import net.goal.differences.databinding.GameSpotPopWithdrawalCheckLayoutBinding;

import spot.game.manager.BalanceManager;
import spot.game.manager.ConfigManager;
import spot.game.manager.GameManager;
import spot.game.manager.IconSelectManager;
import spot.game.model.WithdrawStateModel;
import spot.game.util.GameTaskUtils;


/**
 * 假提现 订单确认
 */
public class WithdrawalCashDialog extends Dialog {

    private GameSpotPopWithdrawalCheckLayoutBinding mBinding;
    private WithdrawStateModel mWithdrawStateModel;
    private DialogButtonClickListener mListener;

    public WithdrawalCashDialog(@NonNull Context context, DialogButtonClickListener listener) {
        super(context, R.style.game_spot_NormalDialog);
        mBinding = GameSpotPopWithdrawalCheckLayoutBinding.inflate(LayoutInflater.from(context));
        setContentView(mBinding.getRoot());

        this.mListener = listener;
        mWithdrawStateModel = GameTaskUtils.getWithdrawStateModel();
        initView();
    }

    private void initView() {
        ClickUtils.applyPressedViewScale(mBinding.ivWithdrawPopClose, mBinding.tvWithdrawInfoPopConfirm);
        mBinding.ivWithdrawPopClose.setOnClickListener(v -> {
            if (mListener != null) {
                mListener.onClose();
            }
            dismiss();
        });
        mBinding.tvWithdrawInfoPopConfirm.setOnClickListener(v -> {
            if (mListener != null) {
                mListener.onConfirm();
            }
            dismiss();
        });

        mBinding.ivBalanceImg.setImageResource(IconSelectManager.getManyRewardIcon());

        String balanceChange = ConfigManager.getCurrency() + BalanceManager.getFormatBalance(mWithdrawStateModel.getWithdrawCashNmu());
        mBinding.tvTotalBalance.setText(balanceChange);

        mBinding.tvOrder.setText(mWithdrawStateModel.getWithdrawCashOrder());
        mBinding.tvAccount.setText(mWithdrawStateModel.getWithdrawCashAccount());

        //需要转换成日期
        mBinding.tvDate.setText(TimeUtils.millis2String(mWithdrawStateModel.getWithdrawCashHour(), "dd/MM/yyyy"));

        mBinding.ivWithdrawalMethod.setImageResource(IconSelectManager.getChannelResIdNew(mWithdrawStateModel.getWithdrawCashBankName()));
    }

    @Override
    public void dismiss() {
        super.dismiss();
        GameManager.newClick();
    }

    public interface ChangeStateListener {
        void onChange(boolean isSubmit);
    }
}
