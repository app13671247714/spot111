package spot.game.ad.strategy;


import java.util.concurrent.TimeUnit;

import spot.game.ad.ILoadStrategy;
import spot.game.constant.BusinessFieldName;
import spot.game.manager.TrackManager;
import spot.game.model.TopOnPlacement;

/**
 * 普通广告策略，持久化某些次数、间隔
 */
public class SimpleLoadStrategy implements ILoadStrategy {
    TopOnPlacement topOnPlacement;
    /**
     * 加载失败重试，每次重试之间间隔时间约束
     */
    private int retryAttempt = 0;//重试次数,播放成功重置
    private long lastAdLoadMillis = 0;//上一次
    /**
     * 最大、最小计算索引，用于时间间隔计算
     */
    int minRetryCalculateCount = 3;
    int maxRetryCalculateCount = 6;
    /**
     * 最大连续重试次数， 达到之后均为某个值
     */
    int continuousCalculateCount = 7;
    int continuousMaxRetryAttempt = 20;
    /**
     * n次连续重试进行间隔时间检查，如1小时最多8次，可能持久化
     */
    int checkStepCount = 0;//加载次数，不重置
    int checkStepIndex = 100000;//检查点索引
    long checkStepMillis = 60 * 60 * 1000;//每隔小时
    long lastCheckStepMillis = 0;//上次检查点时间

    public SimpleLoadStrategy(TopOnPlacement topOnPlacement) {
        if (topOnPlacement == null) {
            throw new NullPointerException("topOnPlacement == null");
        }
        this.topOnPlacement = topOnPlacement;
    }


    public void setMinRetryCalculateCount(int minRetryCalculateCount) {
        this.minRetryCalculateCount = minRetryCalculateCount;
    }

    public void setMaxRetryCalculateCount(int maxRetryCalculateCount) {
        this.maxRetryCalculateCount = maxRetryCalculateCount;
    }

    public void setContinuousCalculateCount(int continuousCalculateCount) {
        this.continuousCalculateCount = continuousCalculateCount;
    }

    public void setContinuousMaxRetryAttempt(int continuousMaxRetryAttempt) {
        this.continuousMaxRetryAttempt = continuousMaxRetryAttempt;
    }


    public void setCheckStepIndex(int checkStepIndex) {
        this.checkStepIndex = checkStepIndex;
    }

    public void setCheckStepMillis(long checkStepMillis) {
        this.checkStepMillis = checkStepMillis;
    }


    @Override
    public long retryLoadMillis() {
        long waitCheckStepMillis = 0;
        long waitContinuousMaxRetryMillis = 0;
        long waitNormalRetryMillis;
        //n次连续重试进行间隔时间检查，如1小时最多8次
        if (checkStepCount > 0 && checkStepCount % checkStepIndex == 0) {
            waitCheckStepMillis = checkStepMillis - System.currentTimeMillis() + lastCheckStepMillis;
        }
        //连续加载n次则之后均为某个值
        if (retryAttempt > continuousMaxRetryAttempt) {
            waitContinuousMaxRetryMillis = getWaitAdLoadingMillis(strategyDelayMillis(continuousCalculateCount), lastAdLoadMillis);
        }
        //普通失败重试加载间隔
        waitNormalRetryMillis = getWaitAdLoadingMillis(getLoadAdDelayMillis(retryAttempt), lastAdLoadMillis);
        //取最大值
        long max = Math.max(waitNormalRetryMillis, Math.max(waitCheckStepMillis, waitContinuousMaxRetryMillis));
        if (max > 0) {
            TrackManager.sendTrack(BusinessFieldName.preload_wait, topOnPlacement.getPlacementId(), "SimpleLoad retryLoadMillis, retryCount=" + retryAttempt + ", 2n=" + waitNormalRetryMillis + ",  gap=" + waitCheckStepMillis + ", cycle=" + waitContinuousMaxRetryMillis);
        }
        return max;
    }

    @Override
    public void starLoad() {
        retryAttempt++;
        lastAdLoadMillis = System.currentTimeMillis();
        //
        if (checkStepCount > 0 && checkStepCount % checkStepIndex == 0) {
            if (System.currentTimeMillis() - lastCheckStepMillis > checkStepMillis) {
                //在本方法中赋值，则本方法不可随意调用
                lastCheckStepMillis = System.currentTimeMillis();
            }
        }
        if (lastCheckStepMillis <= 0) {
            lastCheckStepMillis = System.currentTimeMillis();
        }
        //
        checkStepCount++;
    }

    @Override
    public void loadSuccess() {
        retryAttempt = 0;
        lastAdLoadMillis = 0;
    }

    @Override
    public String getMsg() {
        return ", retry_load_count=" + retryAttempt;
    }

    private long getWaitAdLoadingMillis(long shouldWaitMillis, long lastAdLoadMillis) {
        return shouldWaitMillis - System.currentTimeMillis() + lastAdLoadMillis;
    }

    private long getLoadAdDelayMillis(int retryAttempt) {
        return strategyDelayMillis(Math.min(maxRetryCalculateCount, retryAttempt + minRetryCalculateCount));
    }

    protected long strategyDelayMillis(int retryAttempt) {
        return TimeUnit.SECONDS.toMillis((long) Math.pow(2, retryAttempt));
    }
}
