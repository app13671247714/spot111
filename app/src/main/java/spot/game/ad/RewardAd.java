package spot.game.ad;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import androidx.annotation.NonNull;

import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxError;
import com.applovin.mediation.MaxReward;
import com.applovin.mediation.MaxRewardedAdListener;
import com.applovin.mediation.ads.MaxRewardedAd;
import com.applovin.sdk.AppLovinSdk;
import com.blankj.utilcode.util.AppUtils;

import net.goal.differences.BuildConfig;

import java.util.concurrent.atomic.AtomicBoolean;

import spot.game.application.GameApplication;
import spot.game.constant.BusinessFieldName;
import spot.game.constant.ZeusConfig;
import spot.game.manager.ADManager;
import spot.game.manager.ContextManager;
import spot.game.manager.GameManager;
import spot.game.manager.TrackManager;
import spot.game.model.JLogExtBean;
import spot.game.model.TopOnPlacement;
import spot.game.model.callModel.ReportRequest;

/**
 * 激励广告封装，使用策略控制加载。展示
 */
public class RewardAd {
    final int MSG_REWARD_PRELOAD = BuildConfig.VERSION_CODE;
    private ILoadStrategy mLoadStrategy;
    private IShowStrategy mIShowStrategy;
    private MaxAd mLoadMaxAd;
    private MaxRewardedAd mMaxRewardedAd;
    private TopOnPlacement topOnPlacement;
    private MaxRewardedAdListener mMaxRewardedAdListener;
    private AtomicBoolean isLoading;
    private AtomicBoolean isShowing;
    private String locationCode;
    Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_REWARD_PRELOAD:
                    load();
                    break;
            }
        }
    };

    public RewardAd(String locationCode, TopOnPlacement topOnPlacement) {
        this.locationCode = locationCode;
        this.topOnPlacement = topOnPlacement;
    }

    public RewardAd(TopOnPlacement topOnPlacement, IShowStrategy mIShowStrategy, ILoadStrategy mLoadStrategy) {
        this.mLoadStrategy = mLoadStrategy;
        this.topOnPlacement = topOnPlacement;
        this.mIShowStrategy = mIShowStrategy;
    }

    public void setTopOnPlacement(TopOnPlacement topOnPlacement) {
        this.topOnPlacement = topOnPlacement;
    }

    public void setMaxRewardedAdListener(MaxRewardedAdListener mMaxAdListener) {
        this.mMaxRewardedAdListener = mMaxAdListener;
    }

    public void setLoadStrategy(ILoadStrategy loadStrategy) {
        mLoadStrategy = loadStrategy;
    }

    public void setIShowStrategy(IShowStrategy mIShowStrategy) {
        this.mIShowStrategy = mIShowStrategy;
    }

    public MaxAd getLoadMaxAd() {
        return mLoadMaxAd;
    }

    public TopOnPlacement getTopOnPlacement() {
        return topOnPlacement;
    }

    public ILoadStrategy getLoadStrategy() {
        return mLoadStrategy;
    }

    public IShowStrategy getShowStrategy() {
        return mIShowStrategy;
    }

    public MaxRewardedAd getMaxRewardedAd() {
        return mMaxRewardedAd;
    }

    public void load() {
        mHandler.removeMessages(MSG_REWARD_PRELOAD);
        if (isLoading == null) {
            isLoading = new AtomicBoolean();
        }
        try {
            if (isLoading.get()) {
                return;
            }
            if (topOnPlacement == null) {
                TrackManager.sendTrack(TrackManager.ad_error, locationCode, "topOnPlacement == null");
                return;
            }
            isLoading.set(true);
            String placementId = topOnPlacement.getPlacementId();
            //isReady
            if (mMaxRewardedAd != null && mMaxRewardedAd.isReady()) {
                TrackManager.sendTrack(BusinessFieldName.preload_ban, locationCode, placementId + ", isReady");
                isLoading.set(false);
                return;
            }
            //
            long waitAdLoadingMillis = 0;
            if (mIShowStrategy != null) {
                waitAdLoadingMillis = mIShowStrategy.retryLoadMillis();
                if (mIShowStrategy.alwaysRefuseShow()) {
                    TrackManager.sendTrack(BusinessFieldName.preload_ban, locationCode, placementId + ", alwaysRefuseShow=" + mIShowStrategy.alwaysRefuseShow());
                    isLoading.set(false);
                    return;
                }
            }
            if (mLoadStrategy != null) {
                waitAdLoadingMillis = Math.max(waitAdLoadingMillis, mLoadStrategy.retryLoadMillis());
                if (mLoadStrategy.alwaysRefuseLoad()) {
                    TrackManager.sendTrack(BusinessFieldName.preload_ban, locationCode, placementId + ", alwaysRefuseLoad=" + mLoadStrategy.alwaysRefuseLoad());
                    isLoading.set(false);
                    return;
                }
            }
            if (waitAdLoadingMillis > 0) {
                //TrackManager.sendTrack(TrackManager.ad_error, "loadRewardVideo", placementId + ", waitAdLoadingMillis=" + waitAdLoadingMillis);
                mHandler.removeMessages(MSG_REWARD_PRELOAD);
                mHandler.sendEmptyMessageDelayed(MSG_REWARD_PRELOAD, waitAdLoadingMillis);
                isLoading.set(false);
                return;
            }
            Activity activity = null;
            try {
                activity = ContextManager.getActivity();
            } catch (Throwable e) {
                e.printStackTrace();
                //后台报空则不重试
                boolean isAppForeground = GameApplication.isAppForeground || AppUtils.isAppForeground();
                if (isAppForeground) {
                    mHandler.sendEmptyMessageDelayed(MSG_REWARD_PRELOAD, 2_000L);
                }
                TrackManager.sendTrack(TrackManager.ad_error, "loadRewardVideo activity is null", placementId + ",isAppForeground=" + isAppForeground);
                isLoading.set(false);
                return;
            }
            AppLovinSdk appLovinSdk = GameManager.getAppLovinSdk();
            if (appLovinSdk == null) {
                TrackManager.sendTrack(TrackManager.ad_error, "loadRewardVideo AppLovinSdk is null", placementId);
                isLoading.set(false);
                return;
            }
            //
            if (topOnPlacement != null) {
                String msg = "";
                if (mIShowStrategy != null) {
                    msg += mIShowStrategy.getMsg();
                }
                if (mLoadStrategy != null) {
                    msg += mLoadStrategy.getMsg();
                }
                mMaxRewardedAd = MaxRewardedAd.getInstance(topOnPlacement.getPlacementId(), appLovinSdk, activity);
                if (mMaxRewardedAd != null) {
                    mMaxRewardedAd.setListener(new MaxRewardedAdListener() {
                        @Override
                        public void onUserRewarded(MaxAd maxAd, MaxReward maxReward) {
                            if (mMaxRewardedAdListener != null) {
                                mMaxRewardedAdListener.onUserRewarded(maxAd, maxReward);
                            }
                        }

                        @Override
                        public void onRewardedVideoStarted(MaxAd maxAd) {
                            if (mMaxRewardedAdListener != null) {
                                mMaxRewardedAdListener.onRewardedVideoStarted(maxAd);
                            }
                        }

                        @Override
                        public void onRewardedVideoCompleted(MaxAd maxAd) {
                            if (mMaxRewardedAdListener != null) {
                                mMaxRewardedAdListener.onRewardedVideoCompleted(maxAd);
                            }
                        }

                        @Override
                        public void onAdLoaded(MaxAd maxAd) {
                            mLoadMaxAd = maxAd;
                            if (mLoadStrategy != null) {
                                mLoadStrategy.loadSuccess();
                            }
                            if (mIShowStrategy != null) {
                                mIShowStrategy.loadSuccess();
                            }
                            if (mMaxRewardedAdListener != null) {
                                mMaxRewardedAdListener.onAdLoaded(maxAd);
                            }
                        }

                        @Override
                        public void onAdDisplayed(MaxAd maxAd) {
                            if (mLoadStrategy != null) {
                                mLoadStrategy.showSuccess();
                            }
                            if (mIShowStrategy != null) {
                                mIShowStrategy.showSuccess();
                            }
                            if (mMaxRewardedAdListener != null) {
                                mMaxRewardedAdListener.onAdDisplayed(maxAd);
                            }
                        }

                        @Override
                        public void onAdHidden(MaxAd maxAd) {
                            if (mMaxRewardedAdListener != null) {
                                mMaxRewardedAdListener.onAdHidden(maxAd);
                            }
                            mHandler.sendEmptyMessage(MSG_REWARD_PRELOAD);
                        }

                        @Override
                        public void onAdClicked(MaxAd maxAd) {
                            if (mMaxRewardedAdListener != null) {
                                mMaxRewardedAdListener.onAdClicked(maxAd);
                            }
                        }

                        @Override
                        public void onAdLoadFailed(String s, MaxError maxError) {
                            if (mLoadStrategy != null) {
                                mLoadStrategy.loadFailed();

                            }
                            if (mIShowStrategy != null) {
                                mIShowStrategy.loadFailed();
                            }
                            if (mMaxRewardedAdListener != null) {
                                mMaxRewardedAdListener.onAdLoadFailed(s, maxError);
                            }
                            if (mLoadStrategy == null || !mLoadStrategy.refuseLoadWhenLoadFailed()) {
                                mHandler.sendEmptyMessage(MSG_REWARD_PRELOAD);
                            }
                        }

                        @Override
                        public void onAdDisplayFailed(MaxAd maxAd, MaxError maxError) {
                            if (mLoadStrategy != null) {
                                mLoadStrategy.showFailed();
                            }
                            if (mIShowStrategy != null) {
                                mIShowStrategy.showFailed();
                            }
                            if (mMaxRewardedAdListener != null) {
                                mMaxRewardedAdListener.onAdDisplayFailed(maxAd, maxError);
                            }
                            mHandler.sendEmptyMessage(MSG_REWARD_PRELOAD);
                        }
                    });
                    if (mLoadStrategy != null) {
                        mLoadStrategy.starLoad();
                    }
                    if (mIShowStrategy != null) {
                        mIShowStrategy.starLoad();
                    }
                    TrackManager.sendTrack(JLogExtBean.event(TrackManager.ad_preload, locationCode + "," + topOnPlacement.getPlacementId()).setStrvalue(locationCode));
                    mMaxRewardedAd.loadAd();
                } else {
                    TrackManager.sendTrack(TrackManager.ad_error, locationCode, "loadRewardVideo topOnPlacement is null");
                }
            } else {
                TrackManager.sendTrack(TrackManager.ad_error, "loadRewardVideo", "TopOnPlacement is empty");
            }
        } catch (Throwable e) {
            e.printStackTrace();
        } finally {
            isLoading.set(false);
        }
    }

    public boolean show() {
        return show(null);
    }

    public boolean show(MaxRewardedAdListener maxAdListener) {
        if (isShowing == null) {
            isShowing = new AtomicBoolean();
        }
        if (isShowing.get()) {
            return false;
        }
        if (topOnPlacement == null) {
            TrackManager.sendTrack(TrackManager.ad_error, "showReward", "topOnPlacement == null");
            return false;
        }
        String placementId = topOnPlacement.getPlacementId();
        isShowing.set(true);
        boolean result = false;
        try {
            if (mIShowStrategy != null) {
                long retryShowMillis = mIShowStrategy.retryShowMillis();
                if (retryShowMillis > 0 || mIShowStrategy.alwaysRefuseShow()) {
                    TrackManager.sendTrack(BusinessFieldName.show_wait, "showReward", placementId + ", retryShowMillis=" + retryShowMillis + ", " + mIShowStrategy.alwaysRefuseShow());
                    isShowing.set(false);
                    return false;
                }
            }
            if (mMaxRewardedAd == null) {
                load();
                TrackManager.sendTrack(TrackManager.ad_error, "showReward_error", BusinessFieldName.REWARD + ": maxRewardedAd is null");
            } else {
                if (mMaxRewardedAd.isReady()) {
                    if (maxAdListener == null) {
                        maxAdListener = mMaxRewardedAdListener;
                    }
                    MaxRewardedAdListener finalMaxAdListener = maxAdListener;
                    mMaxRewardedAd.setListener(new MaxRewardedAdListener() {
                        @Override
                        public void onUserRewarded(MaxAd maxAd, MaxReward maxReward) {

                            if (finalMaxAdListener != null) {
                                finalMaxAdListener.onUserRewarded(maxAd, maxReward);
                            }
                        }

                        @Override
                        public void onRewardedVideoStarted(MaxAd maxAd) {
                            if (finalMaxAdListener != null) {
                                finalMaxAdListener.onRewardedVideoStarted(maxAd);
                            }
                        }

                        @Override
                        public void onRewardedVideoCompleted(MaxAd maxAd) {
                            if (finalMaxAdListener != null) {
                                finalMaxAdListener.onRewardedVideoCompleted(maxAd);
                            }
                        }

                        @Override
                        public void onAdLoaded(MaxAd maxAd) {
                            mLoadMaxAd = maxAd;
                            if (mLoadStrategy != null) {
                                mLoadStrategy.loadSuccess();
                            }
                            if (mIShowStrategy != null) {
                                mIShowStrategy.loadSuccess();
                            }
                        }

                        @Override
                        public void onAdDisplayed(MaxAd maxAd) {
                            if (mLoadStrategy != null) {
                                mLoadStrategy.showSuccess();
                            }
                            if (mIShowStrategy != null) {
                                mIShowStrategy.showSuccess();
                            }
                            if (finalMaxAdListener != null) {
                                finalMaxAdListener.onAdDisplayed(maxAd);
                            }
                        }

                        @Override
                        public void onAdHidden(MaxAd maxAd) {
                            if (finalMaxAdListener != null) {
                                finalMaxAdListener.onAdHidden(maxAd);
                            }
                            mHandler.sendEmptyMessage(MSG_REWARD_PRELOAD);
                        }

                        @Override
                        public void onAdClicked(MaxAd maxAd) {
                            if (finalMaxAdListener != null) {
                                finalMaxAdListener.onAdClicked(maxAd);
                            }
                        }

                        @Override
                        public void onAdLoadFailed(String s, MaxError maxError) {
                            if (mLoadStrategy != null) {
                                mLoadStrategy.loadFailed();
                            }
                            if (mIShowStrategy != null) {
                                mIShowStrategy.loadFailed();
                            }
                            if (finalMaxAdListener != null) {
                                finalMaxAdListener.onAdLoadFailed(s, maxError);
                            }
                            if (mLoadStrategy == null || !mLoadStrategy.refuseLoadWhenLoadFailed()) {
                                mHandler.sendEmptyMessage(MSG_REWARD_PRELOAD);
                            }
                        }

                        @Override
                        public void onAdDisplayFailed(MaxAd maxAd, MaxError maxError) {
                            if (mLoadStrategy != null) {
                                mLoadStrategy.showFailed();
                            }
                            if (mIShowStrategy != null) {
                                mIShowStrategy.showFailed();
                            }
                            if (finalMaxAdListener != null) {
                                finalMaxAdListener.onAdDisplayFailed(maxAd, maxError);
                            }
                            mHandler.sendEmptyMessage(MSG_REWARD_PRELOAD);
                        }
                    });
                    if (mLoadStrategy != null) {
                        mLoadStrategy.startShow();
                    }
                    if (mIShowStrategy != null) {
                        mIShowStrategy.startShow();
                    }
                    mMaxRewardedAd.showAd();
                    result = true;
                } else {
                    TrackManager.sendTrack(JLogExtBean.event(TrackManager.ad_loading, locationCode).setStrvalue(locationCode));
                    ReportRequest request = new ReportRequest();
                    request.setExtendParam(ADManager.INSTANCE().buildExtendParam(ZeusConfig.ad_loading, "ad_loading", topOnPlacement.getSourceType(), topOnPlacement.getId(), null));
                    //
                    request.setTarget(topOnPlacement.getTarget());
                    TrackManager.sendTrack(request);
                    load();
                }
            }
        } catch (Throwable e) {
            TrackManager.sendTrack(TrackManager.ad_error, "showReward", "" + e.getMessage());
        }
        isShowing.set(false);
        return result;
    }

    /**
     * 是否可加载
     */
    public boolean couldLoad() {
        if (topOnPlacement == null) {
            return false;
        }
        //
        long waitAdLoadingMillis = 0;
        if (mIShowStrategy != null) {
            waitAdLoadingMillis = mIShowStrategy.retryLoadMillis();
            if (mIShowStrategy.alwaysRefuseShow()) {
                return false;
            }
        }
        if (mLoadStrategy != null) {
            waitAdLoadingMillis = Math.max(waitAdLoadingMillis, mLoadStrategy.retryLoadMillis());
            if (mLoadStrategy.alwaysRefuseLoad()) {
                return false;
            }
        }
        return waitAdLoadingMillis <= 0;
    }

    /**
     * 是否可展示，常用于比价
     */
    public boolean couldShow() {
        if (topOnPlacement == null) {
            return false;
        }
        if (mIShowStrategy != null) {
            long retryShowMillis = mIShowStrategy.retryShowMillis();
            if (retryShowMillis > 0 || mIShowStrategy.alwaysRefuseShow()) {
                return false;
            }
        }
        return true;
    }
}
