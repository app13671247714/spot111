package spot.game.ad;

public interface ILoadStrategy {

    /**
     * 因为随时都可能调用， 所以在本方法中不可有影响后续逻辑的赋值操作，
     */
    long retryLoadMillis();

    /**
     * 不再允许加载
     */
    default boolean alwaysRefuseLoad() {
        return false;
    }

    /**
     * 不允许加载失败之后重加载
     */
    default boolean refuseLoadWhenLoadFailed() {
        return false;
    }

    /**
     *
     */
    default String getMsg() {
        return "";
    }

    /**
     * 以下均为策略点
     */
    default void starLoad() {
    }

    default void startShow() {
    }

    default void loadFailed() {
    }

    default void loadSuccess() {
    }

    default void showFailed() {
    }

    default void showSuccess() {
    }
}
