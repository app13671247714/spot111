package spot.game.ad;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import androidx.annotation.NonNull;

import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxError;
import com.applovin.mediation.ads.MaxInterstitialAd;
import com.applovin.sdk.AppLovinSdk;
import com.blankj.utilcode.util.AppUtils;

import net.goal.differences.BuildConfig;

import java.util.concurrent.atomic.AtomicBoolean;

import spot.game.application.GameApplication;
import spot.game.constant.BusinessFieldName;
import spot.game.constant.ZeusConfig;
import spot.game.manager.ADManager;
import spot.game.manager.ContextManager;
import spot.game.manager.GameManager;
import spot.game.manager.TrackManager;
import spot.game.model.JLogExtBean;
import spot.game.model.TopOnPlacement;
import spot.game.model.callModel.ReportRequest;

/**
 * 插屏广告封装，使用策略控制加载。展示
 */
public class InterstitialAd {
    private final int MSG_INTERSTITIAL_PRELOAD = 1 + BuildConfig.VERSION_CODE;
    private ILoadStrategy mLoadStrategy;
    private IShowStrategy mIShowStrategy;
    private MaxAd mLoadMaxAd;
    private MaxInterstitialAd mMaxInterstitialAd;
    private TopOnPlacement topOnPlacement;
    private MaxAdListener mMaxAdListener;
    private AtomicBoolean isLoading;
    private AtomicBoolean isShowing;
    private String locationCode;
    Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_INTERSTITIAL_PRELOAD:
                    load();
                    break;
            }
        }
    };

    public InterstitialAd(String locationCode, TopOnPlacement topOnPlacement) {
        this.locationCode = locationCode;
        this.topOnPlacement = topOnPlacement;
    }

    public InterstitialAd(TopOnPlacement topOnPlacement, IShowStrategy mIShowStrategy, ILoadStrategy mLoadStrategy) {
        this.mLoadStrategy = mLoadStrategy;
        this.topOnPlacement = topOnPlacement;
        this.mIShowStrategy = mIShowStrategy;
    }

    public void setTopOnPlacement(TopOnPlacement topOnPlacement) {
        this.topOnPlacement = topOnPlacement;
    }

    public void setMaxAdListener(MaxAdListener mMaxAdListener) {
        this.mMaxAdListener = mMaxAdListener;
    }

    public void setLoadStrategy(ILoadStrategy loadStrategy) {
        mLoadStrategy = loadStrategy;
    }

    public void setIShowStrategy(IShowStrategy mIShowStrategy) {
        this.mIShowStrategy = mIShowStrategy;
    }

    public ILoadStrategy getLoadStrategy() {
        return mLoadStrategy;
    }

    public IShowStrategy getShowStrategy() {
        return mIShowStrategy;
    }

    public MaxAd getLoadMaxAd() {
        return mLoadMaxAd;
    }

    public TopOnPlacement getTopOnPlacement() {
        return topOnPlacement;
    }

    public MaxInterstitialAd getMaxInterstitialAd() {
        return mMaxInterstitialAd;
    }

    public void load() {
        mHandler.removeMessages(MSG_INTERSTITIAL_PRELOAD);
        if (isLoading == null) {
            isLoading = new AtomicBoolean();
        }
        try {
            if (isLoading.get()) {
                return;
            }
            if (topOnPlacement == null) {
                TrackManager.sendTrack(TrackManager.ad_error, "loadInterstitial", "topOnPlacement == null");
                return;
            }
            isLoading.set(true);
            String placementId = topOnPlacement.getPlacementId();
            //isReady
            if (mMaxInterstitialAd != null && mMaxInterstitialAd.isReady()) {
                TrackManager.sendTrack(BusinessFieldName.preload_ban, locationCode, placementId + ", isReady");
                isLoading.set(false);
                return;
            }
            //
            long waitAdLoadingMillis = 0;
            if (mIShowStrategy != null) {
                waitAdLoadingMillis = mIShowStrategy.retryLoadMillis();
                if (mIShowStrategy.alwaysRefuseShow()) {
                    TrackManager.sendTrack(BusinessFieldName.preload_ban, locationCode, placementId + ", alwaysRefuseShow=" + mIShowStrategy.alwaysRefuseShow());
                    isLoading.set(false);
                    return;
                }
            }
            if (mLoadStrategy != null) {
                waitAdLoadingMillis = Math.max(waitAdLoadingMillis, mLoadStrategy.retryLoadMillis());
                if (mLoadStrategy.alwaysRefuseLoad()) {
                    TrackManager.sendTrack(BusinessFieldName.preload_ban, locationCode, placementId + ", alwaysRefuseLoad=" + mLoadStrategy.alwaysRefuseLoad());
                    isLoading.set(false);
                    return;
                }
            }
            if (waitAdLoadingMillis > 0) {
                //TrackManager.sendTrack(TrackManager.ad_error, "loadInterstitial", placementId + ", waitAdLoadingMillis=" + waitAdLoadingMillis);
                mHandler.removeMessages(MSG_INTERSTITIAL_PRELOAD);
                mHandler.sendEmptyMessageDelayed(MSG_INTERSTITIAL_PRELOAD, waitAdLoadingMillis);
                isLoading.set(false);
                return;
            }
            //
            Activity activity = null;
            try {
                activity = ContextManager.getActivity();
            } catch (Exception e) {
                e.printStackTrace();
                //后台报空则不重试
                boolean isAppForeground = GameApplication.isAppForeground || AppUtils.isAppForeground();
                if (isAppForeground) {
                    mHandler.sendEmptyMessageDelayed(MSG_INTERSTITIAL_PRELOAD, 2_000L);
                }
                TrackManager.sendTrack(TrackManager.ad_error, "loadInterstitial activity is null", placementId + ",isAppForeground=" + isAppForeground);
                isLoading.set(false);
                return;
            }
            AppLovinSdk appLovinSdk = GameManager.getAppLovinSdk();
            if (appLovinSdk == null) {
                TrackManager.sendTrack(TrackManager.ad_error, "loadInterstitial AppLovinSdk is null", placementId);
                isLoading.set(false);
                return;
            }
            //
            if (topOnPlacement != null) {
                String msg = "";
                if (mIShowStrategy != null) {
                    msg += mIShowStrategy.getMsg();
                }
                if (mLoadStrategy != null) {
                    msg += mLoadStrategy.getMsg();
                }
                mMaxInterstitialAd = new MaxInterstitialAd(topOnPlacement.getPlacementId(), appLovinSdk, activity);
                mMaxInterstitialAd.setListener(new MaxAdListener() {
                    @Override
                    public void onAdLoaded(MaxAd maxAd) {
                        mLoadMaxAd = maxAd;
                        if (mLoadStrategy != null) {
                            mLoadStrategy.loadSuccess();
                        }
                        if (mIShowStrategy != null) {
                            mIShowStrategy.loadSuccess();
                        }
                        if (mMaxAdListener != null) {
                            mMaxAdListener.onAdLoaded(maxAd);
                        }
                    }

                    @Override
                    public void onAdDisplayed(MaxAd maxAd) {
                        if (mLoadStrategy != null) {
                            mLoadStrategy.showSuccess();
                        }
                        if (mIShowStrategy != null) {
                            mIShowStrategy.showSuccess();
                        }
                        if (mMaxAdListener != null) {
                            mMaxAdListener.onAdDisplayed(maxAd);
                        }
                    }

                    @Override
                    public void onAdHidden(MaxAd maxAd) {

                        if (mMaxAdListener != null) {
                            mMaxAdListener.onAdHidden(maxAd);
                        }
                        mHandler.sendEmptyMessage(MSG_INTERSTITIAL_PRELOAD);
                    }

                    @Override
                    public void onAdClicked(MaxAd maxAd) {
                        if (mMaxAdListener != null) {
                            mMaxAdListener.onAdClicked(maxAd);
                        }
                    }

                    @Override
                    public void onAdLoadFailed(String s, MaxError maxError) {
                        if (mLoadStrategy != null) {
                            mLoadStrategy.loadFailed();
                        }
                        if (mIShowStrategy != null) {
                            mIShowStrategy.loadFailed();
                        }
                        if (mMaxAdListener != null) {
                            mMaxAdListener.onAdLoadFailed(s, maxError);
                        }
                        if (mLoadStrategy == null || !mLoadStrategy.refuseLoadWhenLoadFailed()) {
                            mHandler.sendEmptyMessage(MSG_INTERSTITIAL_PRELOAD);
                        }
                    }

                    @Override
                    public void onAdDisplayFailed(MaxAd maxAd, MaxError maxError) {
                        if (mLoadStrategy != null) {
                            mLoadStrategy.showFailed();
                        }
                        if (mIShowStrategy != null) {
                            mIShowStrategy.showFailed();
                        }
                        if (mMaxAdListener != null) {
                            mMaxAdListener.onAdDisplayFailed(maxAd, maxError);
                        }
                        mHandler.sendEmptyMessage(MSG_INTERSTITIAL_PRELOAD);
                    }
                });
                // Load the first ad
                if (mLoadStrategy != null) {
                    mLoadStrategy.starLoad();
                }
                if (mIShowStrategy != null) {
                    mIShowStrategy.starLoad();
                }
                TrackManager.sendTrack(JLogExtBean.event(TrackManager.ad_preload, locationCode + "," + topOnPlacement.getPlacementId()).setStrvalue(locationCode));
                mMaxInterstitialAd.loadAd();
            } else {
                TrackManager.sendTrack(TrackManager.ad_error, "inter", "TopOnPlacement is empty");
            }
        } catch (Throwable e) {
            e.printStackTrace();
        } finally {
            isLoading.set(false);
        }
    }

    public boolean show() {
        return show(null);
    }

    public boolean show(MaxAdListener maxAdListener) {
        if (isShowing == null) {
            isShowing = new AtomicBoolean();
        }
        if (isShowing.get() || topOnPlacement == null) {
            return false;
        }
        isShowing.set(true);
        //
        boolean result = false;
        try {
            String placementId = topOnPlacement.getPlacementId();
            if (mIShowStrategy != null) {
                long retryShowMillis = mIShowStrategy.retryShowMillis();
                if (retryShowMillis > 0 || mIShowStrategy.alwaysRefuseShow()) {
                    TrackManager.sendTrack(BusinessFieldName.show_wait, locationCode, placementId + ", retryShowMillis=" + retryShowMillis + ", " + mIShowStrategy.alwaysRefuseShow());
                    isShowing.set(false);
                    return false;
                }
            }
            if (mMaxInterstitialAd == null) {
                mHandler.sendEmptyMessage(MSG_INTERSTITIAL_PRELOAD);
                TrackManager.sendTrack(TrackManager.ad_error, locationCode, topOnPlacement.getId() + ": maxInterstitialAd is null");
            } else {
                if (mMaxInterstitialAd.isReady()) {
                    if (maxAdListener == null) {
                        maxAdListener = mMaxAdListener;
                    }
                    MaxAdListener finalMaxAdListener = maxAdListener;
                    mMaxInterstitialAd.setListener(new MaxAdListener() {
                        @Override
                        public void onAdLoaded(MaxAd maxAd) {
                            mLoadMaxAd = maxAd;
                            if (mLoadStrategy != null) {
                                mLoadStrategy.loadSuccess();
                            }
                            if (mIShowStrategy != null) {
                                mIShowStrategy.loadSuccess();
                            }
                            if (finalMaxAdListener != null) {
                                finalMaxAdListener.onAdLoaded(maxAd);
                            }
                        }

                        @Override
                        public void onAdDisplayed(MaxAd maxAd) {
                            if (mLoadStrategy != null) {
                                mLoadStrategy.showSuccess();
                            }
                            if (mIShowStrategy != null) {
                                mIShowStrategy.showSuccess();
                            }
                            if (finalMaxAdListener != null) {
                                finalMaxAdListener.onAdDisplayed(maxAd);
                            }
                        }

                        @Override
                        public void onAdHidden(MaxAd maxAd) {
                            if (finalMaxAdListener != null) {
                                finalMaxAdListener.onAdHidden(maxAd);
                            }
                            mHandler.sendEmptyMessageDelayed(MSG_INTERSTITIAL_PRELOAD, 5_000);
                        }

                        @Override
                        public void onAdClicked(MaxAd maxAd) {
                            if (finalMaxAdListener != null) {
                                finalMaxAdListener.onAdClicked(maxAd);
                            }
                        }

                        @Override
                        public void onAdLoadFailed(String s, MaxError maxError) {
                            if (mLoadStrategy != null) {
                                mLoadStrategy.loadFailed();
                            }
                            if (mIShowStrategy != null) {
                                mIShowStrategy.loadFailed();
                            }
                            if (finalMaxAdListener != null) {
                                finalMaxAdListener.onAdLoadFailed(s, maxError);
                            }
                            if (mLoadStrategy == null || !mLoadStrategy.refuseLoadWhenLoadFailed()) {
                                mHandler.sendEmptyMessage(MSG_INTERSTITIAL_PRELOAD);
                            }
                        }

                        @Override
                        public void onAdDisplayFailed(MaxAd maxAd, MaxError maxError) {
                            if (mLoadStrategy != null) {
                                mLoadStrategy.showFailed();
                            }
                            if (mIShowStrategy != null) {
                                mIShowStrategy.showFailed();
                            }
                            if (finalMaxAdListener != null) {
                                finalMaxAdListener.onAdDisplayFailed(maxAd, maxError);
                            }
                            mHandler.sendEmptyMessage(MSG_INTERSTITIAL_PRELOAD);
                        }
                    });
                    if (mLoadStrategy != null) {
                        mLoadStrategy.startShow();
                    }
                    if (mIShowStrategy != null) {
                        mIShowStrategy.startShow();
                    }
                    mMaxInterstitialAd.showAd();
                    result = true;
                } else {
                    mHandler.sendEmptyMessage(MSG_INTERSTITIAL_PRELOAD);
                    TrackManager.sendTrack(JLogExtBean.event(TrackManager.ad_loading, locationCode).setStrvalue(locationCode));

                    ReportRequest request = new ReportRequest();
                    request.setExtendParam(ADManager.INSTANCE().buildExtendParam(ZeusConfig.ad_loading, "ad_loading", topOnPlacement.getSourceType(), topOnPlacement.getId(), null));

                    request.setTarget(topOnPlacement.getTarget());
                    TrackManager.sendTrack(request);
                }
            }
        } catch (Throwable throwable) {
            TrackManager.sendTrack(TrackManager.ad_error, "showInterstitial", "" + throwable.getMessage());
        }
        isShowing.set(false);
        return result;
    }

    /**
     * 是否可加载
     */
    public boolean couldLoad() {
        if (topOnPlacement == null) {
            return false;
        }
        //
        long waitAdLoadingMillis = 0;
        if (mIShowStrategy != null) {
            waitAdLoadingMillis = mIShowStrategy.retryLoadMillis();
            if (mIShowStrategy.alwaysRefuseShow()) {
                return false;
            }
        }
        if (mLoadStrategy != null) {
            waitAdLoadingMillis = Math.max(waitAdLoadingMillis, mLoadStrategy.retryLoadMillis());
            if (mLoadStrategy.alwaysRefuseLoad()) {
                return false;
            }
        }
        return waitAdLoadingMillis <= 0;
    }

    /**
     * 是否可展示，常用于比价
     */
    public boolean couldShow() {
        if (topOnPlacement == null) {
            return false;
        }
        if (mIShowStrategy != null) {
            long retryShowMillis = mIShowStrategy.retryShowMillis();
            if (retryShowMillis > 0 || mIShowStrategy.alwaysRefuseShow()) {
                return false;
            }
        }
        return true;
    }

}
