package spot.game.ad;

public interface IShowStrategy {
    /**
     * 因为随时都可能调用， 所以在本方法中不可有影响后续逻辑的赋值操作，
     */
    long retryShowMillis();

    /**
     * 不再允许展示
     */
    default boolean alwaysRefuseShow() {
        return false;
    }

    /**
     * 展示后load时进行判断,通常用于防止展示后立即加载，而是等（展示间隔-预期加载时间）
     */
    default long retryLoadMillis() {
        return 0;
    }

    /**
     * 以下均为策略点
     */
    default String getMsg() {
        return "";
    }

    /**
     * 以下均为策略点
     */
    default void starLoad() {
    }

    default void startShow() {
    }

    default void loadFailed() {
    }

    default void loadSuccess() {
    }

    default void showFailed() {
    }

    default void showSuccess() {
    }

}
