package spot.game.manager;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.android.installreferrer.api.InstallReferrerClient;
import com.android.installreferrer.api.InstallReferrerStateListener;
import com.android.installreferrer.api.ReferrerDetails;
import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.SPStaticUtils;

import net.goal.differences.BuildConfig;

import spot.game.constant.BusinessFieldName;
import spot.game.constant.ZeusConfig;
import spot.game.model.callModel.AttributionRequest;
import spot.game.net.AbstractObserver;
import spot.game.util.AesGcmUtil;

public class InstallReferrerManager {
    InstallReferrerClient referrerClient;
    public static final int MSG_RETRY_UPLOAD_INSTALL_REFERRER = 0x123 + BuildConfig.VERSION_CODE;
    int retryCount = 0;
    long startMillis = 0;

    private volatile static InstallReferrerManager helper = null;

    public static InstallReferrerManager INSTANCE() {
        if (helper == null) {
            synchronized (InstallReferrerManager.class) {
                if (helper == null) {
                    helper = new InstallReferrerManager();
                }
            }
        }
        return helper;
    }

    Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_RETRY_UPLOAD_INSTALL_REFERRER:
                    mHandler.removeMessages(MSG_RETRY_UPLOAD_INSTALL_REFERRER);
                    if (retryCount < 3) {
                        retryCount++;
                        uploadInstallReferrer(null);
                    } else {
                        mHandler.removeCallbacks(null);
                    }
                    break;
            }
        }
    };

    public void uploadInstallReferrer() {
        try {
            if (referrerClient != null) {
                referrerClient.endConnection();
            }
            startMillis = System.currentTimeMillis();
            //TrackManager.sendTrack(TrackManager.installrefer, "start", "");
            referrerClient = InstallReferrerClient.newBuilder(ContextManager.getContext()).build();
            referrerClient.startConnection(new InstallReferrerStateListener() {
                @Override
                public void onInstallReferrerSetupFinished(int responseCode) {
                    try {
                        //TrackManager.sendTrack(TrackManager.installrefer, "onInstallReferrerSetupFinished", responseCode + ", stepMillis=" + (System.currentTimeMillis() - startMillis));
                        switch (responseCode) {
                            case InstallReferrerClient.InstallReferrerResponse.OK:
                                // Connection established.
                                ReferrerDetails response = referrerClient.getInstallReferrer();
                                if (response != null) {
                                    //
                                    String referrerUrl = response.getInstallReferrer();
                                    long referrerClickTime = response.getReferrerClickTimestampSeconds();
                                    long appInstallTime = response.getInstallBeginTimestampSeconds();
                                    boolean instantExperienceLaunched = response.getGooglePlayInstantParam();
                                    TrackManager.sendTrack(TrackManager.installrefer, "OK"
                                            , "referrerUrl=" + referrerUrl + ", referrerClickTime=" + referrerClickTime + ", appInstallTime=" + appInstallTime + ", instantExperienceLaunched=" + instantExperienceLaunched
                                    );
                                    uploadInstallReferrer(response);
                                } else {
                                    TrackManager.sendTrack(TrackManager.installrefer, "OK", "ReferrerDetails == null ");
                                }
                                break;
                            case InstallReferrerClient.InstallReferrerResponse.FEATURE_NOT_SUPPORTED:
                                // API not available on the current Play Store app.
                                break;
                            case InstallReferrerClient.InstallReferrerResponse.SERVICE_UNAVAILABLE:
                                // Connection couldn't be established.
                                break;
                        }
                        //
                        referrerClient.endConnection();
                    } catch (Throwable e) {
                        TrackManager.sendTrack(TrackManager.installrefer, "onInstallReferrerSetupFinished", responseCode + "=" + e.getMessage());
                        e.printStackTrace();
                    }
                }

                @Override
                public void onInstallReferrerServiceDisconnected() {
                    // Try to restart the connection on the next request to
                    // Google Play by calling the startConnection() method.
                    TrackManager.sendTrack(TrackManager.installrefer, "onInstallReferrerServiceDisconnected", "stepMillis=" + (System.currentTimeMillis() - startMillis));
                    referrerClient = null;
                }
            });
        } catch (Throwable e) {
            e.printStackTrace();
            TrackManager.sendTrack(TrackManager.installrefer, "start", e.getMessage());
        }

    }

    private void uploadInstallReferrer(ReferrerDetails referrerDetails) {
        String json = "";
        if (referrerDetails != null) {
            //
            AttributionRequest adjustMap = getAdjustMap(referrerDetails);
            json = AesGcmUtil.encrypt(GsonUtils.toJson(adjustMap));
            SPStaticUtils.put(BusinessFieldName.INSTALL_REFERRER_DETAILS, json);
        } else {
            json = SPStaticUtils.getString(BusinessFieldName.INSTALL_REFERRER_DETAILS, "");
        }
        if (TextUtils.isEmpty(json)) {
            return;
        }
        //TrackManager.sendTrack(TrackManager.installrefer, "req", "retryCount=" + retryCount);
        NetManager.post(json, new AbstractObserver<String>() {
            @Override
            public void onNext(String zeusResponse, String encryptBody) {
                TrackManager.sendTrack(TrackManager.installrefer, "req_success", "retryCount=" + retryCount);
                SPStaticUtils.put(BusinessFieldName.INSTALL_REFERRER_DETAILS, "");
                ADManager.INSTANCE().initBaseInfo();
            }

            @Override
            public void onError(Throwable e) {
                //TrackManager.sendTrack(TrackManager.installrefer, "req_fail", e.getMessage() + ", retryCount=" + retryCount);
                mHandler.sendEmptyMessageDelayed(MSG_RETRY_UPLOAD_INSTALL_REFERRER, 5_000);
            }
        });

    }

    private AttributionRequest getAdjustMap(ReferrerDetails response) {
        AttributionRequest request = new AttributionRequest();
        request.setM(ZeusConfig.J_ADJUST);
        String referrerUrl = response.getInstallReferrer();
        long referrerClickTime = response.getReferrerClickTimestampSeconds();
        request.setDeepLinkUrl(referrerUrl);
        request.setGgClickTime(referrerClickTime + "");
        return request;
    }
}
