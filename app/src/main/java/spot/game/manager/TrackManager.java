package spot.game.manager;

import android.text.TextUtils;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.TimeUtils;

import net.goal.differences.BuildConfig;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import spot.game.constant.ZeusConfig;
import spot.game.model.JLogExtBean;
import spot.game.model.callModel.ReportRequest;
import spot.game.model.callModel.TrackRequest;
import spot.game.model.callModel.ZeusEvent;

/**
 * 用户行为日志上报
 */
public class TrackManager {
    public static final int LOG_LEVEL_0 = 0, LOG_LEVEL_1 = 1, LOG_LEVEL_2 = 2, LOG_LEVEL_3 = 3, LOG_LEVEL_4 = 4, LOG_LEVEL_5 = 5;

    public static final String appopen = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE + "appopen";
    public static final String regisger = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE + "register";
    public static final String login = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE + "login";
    public static final String page_home = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE + "page_home";
    public static final String page_withdraw = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE + "page_withdraw";
    public static final String appclose = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE + "appclose";
    public static final String level = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE + "level";
    public static final String ad_user_show = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE + "ad_user_show";
    public static final String ad_show = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE + "ad_show";
    public static final String ad_error = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE + "ad_error";
    public static final String ad_loading = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE + "ad_loading";
    public static final String ad_request_success = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE + "ad_request_success";
    public static final String ad_request_failed = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE + "ad_request_failed";
    public static final String ad_click = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE + "ad_click";
    public static final String ad_reward = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE + "ad_reward";
    public static final String active_rule = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE + "active_rule";
    public static final String atr_sdk = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE + "atr_sdk";
    public static final String installrefer = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE + "installrefer";
    public static final String ad_sdkinit = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE + "ad_sdkinit";
    public static final String ad_preload = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE + "ad_preload";
    public static final String ad_pkwin = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE + "ad_pkwin";
    //业务日志
    public static final String novice_click = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE + "novice_click";
    public static final String prompt_click = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE + "prompt_click";
    public static final String iphone_click = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE + "iphone_click";
    public static final String treasure_click = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE + "treasure_click";
    public static final String level_double = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE + "level_double";

    //
    public static final List<String> logLevel0 = Arrays.asList(
            appopen, regisger, login, page_home, page_withdraw, appclose
            , level, ad_user_show, ad_show, ad_error, active_rule, ad_pkwin, ad_request_failed, ad_loading
    );
    public  static final List<String> logLevel1 = Arrays.asList(
            novice_click, prompt_click, iphone_click, treasure_click, level_double
    );

    public static final List<String> logLevel4 = Arrays.asList(
            atr_sdk, installrefer, ad_sdkinit, ad_preload
            , ad_request_success, ad_click, ad_reward
    );
   public static final List<String> blockCategoryOne = Arrays.asList(
            "debug_reward_show"
            , "debug_reward_get"
            , "preload_ban"
            , "debug_game_start"
            , "level_time"
            , "debug_reward_ad_show"
            , "debug_reward_click"
    );

    public static void health(String health) {
        if (!TextUtils.isEmpty(ConfigManager.getUserId())) {
            HashMap<String, Object> extendParam = new HashMap<>();
            extendParam.put("event", health);
            extendParam.put("logTime", TimeUtils.getNowString(TimeUtils.getSafeDateFormat("yyyy-MM-dd HH:mm:ss.SSS")));
            extendParam.put("ab_tags", ConfigManager.getABTags());
            extendParam.put("mobile_region", GameManager.getCountryByLanguage());
            TrackRequest request = new TrackRequest();
            request.setM(ZeusConfig.RP_REPORT);
            request.setExtendParam(extendParam);
            //
            NetManager.post(request, null);
        }
        //无网计数上报
        GameManager.sendNetErrorCountTrack();
    }

    public static void sendTrack(String categoryOne, String categoryTwo, String logMessage) {
        String logMsg = "";
        if (!TextUtils.isEmpty(categoryTwo)) {
            logMsg += "Two=" + categoryTwo;
        }
        if (!TextUtils.isEmpty(logMessage)) {
            logMsg += ", logMsg=" + logMessage;
        }
        sendTrack(categoryOne, logMsg);
    }


    public static void sendTrack(String event, String logMsg) {
        sendTrack(JLogExtBean.event(event).setLogmsg(logMsg));
    }

    public static void sendTrack(JLogExtBean.Builder builder) {
        if (builder == null) {
            return;
        }
        NetManager.sendTrack(builder.build());
    }



    public static void sendZeusEvent(ZeusEvent zeusEvent) {
        HashMap<String, Object> extendParam = new HashMap<>();
        extendParam.put("event", zeusEvent.getEvent());
        extendParam.put("money", zeusEvent.getMoney());
        extendParam.put("logTime", TimeUtils.getNowString(TimeUtils.getSafeDateFormat("yyyy-MM-dd HH:mm:ss.SSS")));
        extendParam.put("ab_tags", ConfigManager.getABTags());
        extendParam.put("mobile_region", GameManager.getCountryByLanguage());
        TrackRequest request = new TrackRequest();
        request.setM(ZeusConfig.RP_REPORT);
        request.setExtendParam(extendParam);
        NetManager.post(request, null);
    }

    public static void sendTrack(ReportRequest request) {
        try {
            request.setM(ZeusConfig.RP_REPORT);
            NetManager.post(request, null);
        } catch (Exception ex) {
            LogUtils.e(ex.getMessage());
        }
    }


}
