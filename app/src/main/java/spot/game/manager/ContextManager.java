package spot.game.manager;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.blankj.utilcode.util.ActivityUtils;

import java.util.concurrent.atomic.AtomicReference;

import spot.game.application.GameApplication;
import spot.game.event.EventConstant;
import spot.game.event.EventMsg;
import spot.game.event.RxBus;

public class ContextManager implements Application.ActivityLifecycleCallbacks {
    private static AtomicReference<Activity> mActivity;
    private static AtomicReference<Activity> mActivityList = new AtomicReference<>();

    public static void setActivity(Activity activity) {
        if (activity == null || activity.isFinishing() || activity.isDestroyed()) {
            return;
        }
        if (mActivityList == null){
            mActivityList = new AtomicReference<>();
        }
        if (mActivity == null) {
            mActivity = new AtomicReference<>();
        }
        mActivityList.set(activity);
        mActivity.set(activity);
    }

    public static void removeActivityAll(Activity activity) {
        if (mActivityList == null) {
            mActivityList = new AtomicReference<>();
        }
        if (mActivityList.get() == null || activity == null) {
            return;
        }
        if (activity.hashCode() == mActivityList.get().hashCode() && mActivityList.get().equals(activity)) {
            mActivityList.set(null);
        }
    }

    public static void removeActivity(Activity activity) {
        if (mActivity == null) {
            mActivity = new AtomicReference<>();
        }
        if (mActivity.get() == null || activity == null) {
            return;
        }
        if (activity.hashCode() == mActivity.get().hashCode() && mActivity.get().equals(activity)) {
            mActivity.set(null);
        }
    }

    public static Application getApplication() {
        return GameApplication.getIns();
    }

    public static Context getContext() {
        return GameApplication.getIns();
    }

    public static Activity getActivity() throws Exception {
        if (mActivity == null) {
            mActivity = new AtomicReference<>();
        }
        if (ActivityUtils.isActivityAlive(mActivity.get())) {
            return mActivity.get();
        }
        Activity topActivity = ActivityUtils.getTopActivity();
        if (ActivityUtils.isActivityAlive(topActivity)) {
            mActivity.set(topActivity);
            return topActivity;
        }
        RxBus.getInstance().post(EventMsg.event(EventConstant.CODE_EVENT_RESET_ACTIVITY).build());
        throw new Exception("currentActivity is not alive");
    }


    @Override
    public void onActivityCreated(@NonNull Activity activity, @Nullable Bundle savedInstanceState) {
        if (GameManager.isAppActivity(activity)) {
            setActivity(activity);
        }
    }

    @Override
    public void onActivityStarted(@NonNull Activity activity) {

    }

    @Override
    public void onActivityResumed(@NonNull Activity activity) {
        if (GameManager.isAppActivity(activity)) {
            setActivity(activity);
        }
    }

    @Override
    public void onActivityPaused(@NonNull Activity activity) {
        if (activity == null || activity.isFinishing() || activity.isDestroyed()) {
            removeActivity(activity);
            removeActivityAll(activity);
        }
    }

    @Override
    public void onActivityStopped(@NonNull Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(@NonNull Activity activity, @NonNull Bundle outState) {
    }

    @Override
    public void onActivityDestroyed(@NonNull Activity activity) {
        removeActivity(activity);
        removeActivityAll(activity);
    }
}
