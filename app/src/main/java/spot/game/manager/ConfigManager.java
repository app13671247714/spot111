package spot.game.manager;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;

import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.RomUtils;
import com.blankj.utilcode.util.SPStaticUtils;
import com.blankj.utilcode.util.TimeUtils;
import com.orhanobut.hawk.Hawk;

import net.goal.differences.BuildConfig;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import spot.game.constant.BusinessFieldName;
import spot.game.constant.GameConfig;
import spot.game.model.CollectPieceBean;
import spot.game.model.backModel.JGCResponse;
import spot.game.util.AesGcmUtil;
import spot.game.util.DeviceUtils;

/**
 * 配置管理
 */
public class ConfigManager {


    public static void initDeviceConfig() {
        Context ctx = ContextManager.getApplication();
        SPStaticUtils.put(BusinessFieldName.operator, DeviceUtils.mobileSimOperator(ctx));
        SPStaticUtils.put(BusinessFieldName.osversion, BusinessFieldName.OS + Build.VERSION.RELEASE);
        SPStaticUtils.put(BusinessFieldName.androidid, DeviceUtils.getAndroidId(ctx));
        SPStaticUtils.put(BusinessFieldName.appinstalltime, DeviceUtils.getInstallTime(ctx));
        SPStaticUtils.put(BusinessFieldName.appupdatetime, DeviceUtils.getLastUpdateTime(ctx));
        SPStaticUtils.put(BusinessFieldName.boottime, DeviceUtils.getBootTime());
        SPStaticUtils.put(BusinessFieldName.dedensity, DeviceUtils.getDeviceDensity(ctx));
        SPStaticUtils.put(BusinessFieldName.deheight, DeviceUtils.getDeviceInfo(ctx, 2));
        SPStaticUtils.put(BusinessFieldName.dewidth, DeviceUtils.getDeviceInfo(ctx, 1));
        SPStaticUtils.put(BusinessFieldName.electric, DeviceUtils.getBatteryLevel(ctx));
        SPStaticUtils.put(BusinessFieldName.enableadb, DeviceUtils.mobileOpenADBDebugger(ctx) == 1);
        SPStaticUtils.put(BusinessFieldName.ischarging, DeviceUtils.mobileIsCharge(ctx) == 1);
        SPStaticUtils.put(BusinessFieldName.mac, DeviceUtils.getMac(ctx));
        SPStaticUtils.put(BusinessFieldName.vpn_use, DeviceUtils.vpn());
        SPStaticUtils.put(BusinessFieldName.country, DeviceUtils.getCountryDialCode());
        if (BuildConfig.PRODUCT_FLAVORS.equalsIgnoreCase("anyThink")) {
            SPStaticUtils.put(BusinessFieldName.country, SPStaticUtils.getString(BusinessFieldName.test_country, DeviceUtils.getCountryDialCode()));
        }
        SPStaticUtils.put(BusinessFieldName.timeZone, DeviceUtils.getTimeZone());
        SPStaticUtils.put(BusinessFieldName.language, GameManager.getLanguage());
        RomUtils.RomInfo romInfo = RomUtils.getRomInfo();
        String name = "";
        String version = "";
        if (romInfo != null) {
            name = romInfo.getName();
            version = romInfo.getVersion();
        }
        String osRom = String.format("%s%s", name, version);
        SPStaticUtils.put(BusinessFieldName.osrom, osRom);
        SPStaticUtils.put(BusinessFieldName.serial, DeviceUtils.getSerialNumber());
        SPStaticUtils.put(BusinessFieldName.sim, DeviceUtils.getSim(ctx));
        SPStaticUtils.put(BusinessFieldName.simstatus, DeviceUtils.mobileSimState(ctx));
        SPStaticUtils.put(BusinessFieldName.wifimac, DeviceUtils.getWifiInfo(ctx, 1));
        SPStaticUtils.put(BusinessFieldName.wifiname, DeviceUtils.getWifiInfo(ctx, 2));
        SPStaticUtils.put(BusinessFieldName.wifiproxy, DeviceUtils.mobileNetProxyUsed(ctx) == 1);
    }


    public static String getCurrency() {
        String currency = SPStaticUtils.getString(GameConfig.CURRENCY_UNIT, "");
        if (!TextUtils.isEmpty(currency)) {
            return currency;
        }
        String country = ConfigManager.getCountry();
        if (country.equalsIgnoreCase("br")) {
            return "R$";
        } else if (country.equalsIgnoreCase("id")) {
            return "Rp";
        } else if (country.equalsIgnoreCase("pk")) {
            return "P.Rs";
        } else if (country.equalsIgnoreCase("ru")) {
            return "₽";
        } else {
            return "$";
        }
    }

    public static void setCurrency(String currency) {
        SPStaticUtils.put(GameConfig.CURRENCY_UNIT, currency);
    }

    public static void setHealthIntervalTime(int intervalTime) {
        SPStaticUtils.put(GameConfig.HEALTH_TIME, intervalTime);
    }

    public static int getHealthIntervalTime() {
        int intervalTime = SPStaticUtils.getInt(GameConfig.HEALTH_TIME, 0);
        return intervalTime;
    }

    public static boolean AdJustEnable() {
        boolean enAdjust = SPStaticUtils.getBoolean(GameConfig.EN_ADJUST, false);
        return enAdjust;
    }

    public static void setAdJustEnable(boolean isEnable) {
        SPStaticUtils.put(GameConfig.EN_ADJUST, isEnable);
    }

    public static int openTimes() {
        int openTimes = SPStaticUtils.getInt(GameConfig.OPEN_TIMES, 1);
        return openTimes;
    }

    public static void addOpenTimes() {
        int open = SPStaticUtils.getInt(GameConfig.OPEN_TIMES, 0);
        SPStaticUtils.put(GameConfig.OPEN_TIMES, open + 1);
    }


    public static void setChannel(String flag) {
        SPStaticUtils.put(GameConfig.CHANNEL, flag);
    }

    public static String getChannel() {
        String channel = SPStaticUtils.getString(GameConfig.CHANNEL, "");
        return channel;
    }

    public static void setAppCode(String flag) {
        SPStaticUtils.put(GameConfig.APPCODE, flag);
    }

    public static String getAppCode() {
        return BuildConfig.APP_CODE;
    }

    public static void setVc(int flag) {
        SPStaticUtils.put(GameConfig.APP_VERSION_CODE, flag);
    }

    public static int getVc() {
        return BuildConfig.VERSION_CODE;
    }

    public static void setVn(String flag) {
        SPStaticUtils.put(GameConfig.APP_VERSION_NAME, flag);
    }

    public static String getVn() {
        return BuildConfig.VERSION_NAME;
    }


    public static String getUserId() {
        String userId = SPStaticUtils.getString(GameConfig.USER_ID, "");
        return userId;
    }

    public static void setUserId(String userId) {
        if (!TextUtils.isEmpty(userId)) {
            SPStaticUtils.put(GameConfig.USER_ID, userId);
        }
    }

    public static void setRegisterTime(long registerTime) {
        if (registerTime != 0L) {
            SPStaticUtils.put(GameConfig.REG_TIME, registerTime);

            String registerDateStr = TimeUtils.millis2String(registerTime, "yyyyMMdd");
            long registerDateLong = TimeUtils.string2Millis(registerDateStr, "yyyyMMdd");
            setRegisterDate(registerDateLong);
        }
    }

    public static void setRegisterDate(long registerDate) {
        SPStaticUtils.put(GameConfig.REG_DATE, registerDate);
    }

    public static long getRegisterDate() {
        long date = SPStaticUtils.getLong(GameConfig.REG_DATE, 0L);
        return date;
    }


    public static void setIsAudit(boolean isAudit) {
        SPStaticUtils.put(GameConfig.IS_AUDIT, isAudit);
    }

    public static boolean isAudit() {
        return SPStaticUtils.getBoolean(GameConfig.IS_AUDIT, false);
    }


    public static void setBalance(float dollars) {
        SPStaticUtils.put(GameConfig.BALANCE, dollars);
    }

    public static float getBalance() {
        return SPStaticUtils.getFloat(GameConfig.BALANCE, 0f);
    }

    public static void addBalance(double rewardDollars) {
        double accountDollars = rewardDollars + getBalance();
        setBalance((float) accountDollars);
    }


    public static String getSessionId() {
        String sessionId = SPStaticUtils.getString(GameConfig.SESSION_ID, "");
        if (TextUtils.isEmpty(sessionId)) {
            sessionId = UUID.randomUUID().toString();
            SPStaticUtils.put(GameConfig.SESSION_ID, sessionId);
        }
        return sessionId;
    }

    public static void setSessionId() {
        SPStaticUtils.put(GameConfig.SESSION_ID, UUID.randomUUID().toString());
    }

    public static String getCountry() {
        return SPStaticUtils.getString(BusinessFieldName.country, "unknown");
    }

    public static String getTimeZone() {
        return SPStaticUtils.getString(BusinessFieldName.timeZone, "unknown");
    }


    public static void setNetErrorShowCountThisTime(int count) {
        SPStaticUtils.put(GameConfig.NET_ERROR_COUNT_THIS_TIME_SHOW, count);
    }

    public static void addNetErrorShowCountThisTime() {
        SPStaticUtils.put(GameConfig.NET_ERROR_COUNT_THIS_TIME_SHOW, getNetErrorShowCountThisTime() + 1);
    }

    public static int getNetErrorShowCountThisTime() {
        return SPStaticUtils.getInt(GameConfig.NET_ERROR_COUNT_THIS_TIME_SHOW, 0);
    }

    public static void setNetErrorClickCountThisTime(int count) {
        SPStaticUtils.put(GameConfig.NET_ERROR_COUNT_THIS_TIME_CLICK, count);
    }

    public static void addNetErrorClickCountThisTime() {
        SPStaticUtils.put(GameConfig.NET_ERROR_COUNT_THIS_TIME_CLICK, getNetErrorClickCountThisTime() + 1);
    }

    public static int getNetErrorClickCountThisTime() {
        return SPStaticUtils.getInt(GameConfig.NET_ERROR_COUNT_THIS_TIME_CLICK, 0);
    }

    public static void setAdChannel(String flag) {
        if (!TextUtils.isEmpty(flag)) {
            SPStaticUtils.put(GameConfig.AD_CHANNEL, flag);
        }
    }

    public static String getAdChannel() {
        return SPStaticUtils.getString(GameConfig.AD_CHANNEL, "");
    }

    public static void setAdGroupId(String flag) {
        if (!TextUtils.isEmpty(flag)) {
            SPStaticUtils.put(GameConfig.AD_GROUP_ID, flag);
        }
    }

    public static String getAdGroupId() {
        return SPStaticUtils.getString(GameConfig.AD_GROUP_ID, "");
    }


    public static String getABTags() {
        return SPStaticUtils.getString(BusinessFieldName.AB_TEST_TAGS, "");
    }

    public static void setABTags(String flag) {
        SPStaticUtils.put(BusinessFieldName.AB_TEST_TAGS, flag);
    }

    public static String getMaxSDKKey() {
        return SPStaticUtils.getString(BusinessFieldName.MAX_SDK_KEY, BusinessFieldName.APP_KEY);
    }

    public static void setMaxSDKKey(String flag) {
        SPStaticUtils.put(BusinessFieldName.MAX_SDK_KEY, flag);
    }

    public static void setIsNew(boolean aNew) {
        SPStaticUtils.put(BusinessFieldName.IS_NEW_USER, aNew ? 1 : 0);
    }

    public static boolean getIsNew() {
        return SPStaticUtils.getInt(BusinessFieldName.IS_NEW_USER, 1) == 1;
    }


    public static void setSoundSetting(boolean b) {
        SPStaticUtils.put(BusinessFieldName.SOUND_SETTING_ENBLE, b ? 1 : 0);
    }

    public static boolean getSoundSetting() {
        return SPStaticUtils.getInt(BusinessFieldName.SOUND_SETTING_ENBLE, 1) == 1;
    }


    public static void setNowLevelCount(int count) {
        SPStaticUtils.put(BusinessFieldName.COLLECT_PIECE_NOW_LEVEL_COUNT, count);
    }

    public static int getNowLevelCount() {
        return SPStaticUtils.getInt(BusinessFieldName.COLLECT_PIECE_NOW_LEVEL_COUNT, 0);
    }

    public static void setNowVideoCount(int count) {
        SPStaticUtils.put(BusinessFieldName.COLLECT_PIECE_NOW_VIDEO_COUNT, count);
    }

    public static int getNowVideoCount() {
        return SPStaticUtils.getInt(BusinessFieldName.COLLECT_PIECE_NOW_VIDEO_COUNT, 0);
    }

    public static void setNowLoginCount(int count) {
        SPStaticUtils.put(BusinessFieldName.COLLECT_PIECE_NOW_LOGIN_COUNT, count);
    }

    public static int getNowLoginCount() {
        return SPStaticUtils.getInt(BusinessFieldName.COLLECT_PIECE_NOW_LOGIN_COUNT, 0);
    }

    public static long getLastLoginMillis() {
        return SPStaticUtils.getLong(BusinessFieldName.LAST_LOGIN_MILLIS, 0);
    }

    public static void setLastLoginMillis(long currentTimeMillis) {
        SPStaticUtils.put(BusinessFieldName.LAST_LOGIN_MILLIS, currentTimeMillis);
    }

    public static void setLastMoneyLimit(double moneyLimit) {
        SPStaticUtils.put(BusinessFieldName.LAST_MONEY_LIMIT, (float) moneyLimit);
    }

    public static float getLastMoneyLimit() {
        return SPStaticUtils.getFloat(BusinessFieldName.LAST_MONEY_LIMIT, -1);
    }


    public static void setLocalLevel(int level) {
        setNowLevelCount(level);
        SPStaticUtils.put(BusinessFieldName.LEVEL, level);
    }

    public static int getLocalLevel() {
        return SPStaticUtils.getInt(BusinessFieldName.LEVEL, 0);
    }

    public static void setNeedLevel(int level) {
        SPStaticUtils.put(BusinessFieldName.LEVEL_NEED, level);
    }

    public static int getNeedLevel() {
        return SPStaticUtils.getInt(BusinessFieldName.LEVEL_NEED, 30);
    }


    public static void setLogLevelSwitch(int llv) {
        SPStaticUtils.put(BusinessFieldName.LOG_LEVEL_SWITCH, llv);
    }

    public static int getLogLevelSwitch() {
        return SPStaticUtils.getInt(BusinessFieldName.LOG_LEVEL_SWITCH, 5);
    }

    public static String getClueId() {
        String clueId = SPStaticUtils.getString(BusinessFieldName.CLUE_ID, "");
        if (TextUtils.isEmpty(clueId)) {
            clueId = UUID.randomUUID().toString();
            SPStaticUtils.put(BusinessFieldName.CLUE_ID, clueId);
        }
        return clueId;
    }

    public static boolean isFirstOpen() {
        boolean b = SPStaticUtils.getInt(BusinessFieldName.IS_FIRST_OPEN, 0) == 0;
        SPStaticUtils.put(BusinessFieldName.IS_FIRST_OPEN, 1);
        return b;
    }

    public static void setIsWhite(String isWhite) {
        boolean before = getIsWhite();
        if (before) {
            // 如果之前是白包
            if ("1".equalsIgnoreCase(isWhite)) {
                // 本次设置是正常包：从白包->正常包：则保存并通知 cocos。
                SPStaticUtils.put(BusinessFieldName.IS_WHITE_USER, 1);
                //GameManager.updateWhiteBaoToCocos();
            }
        } else {
            // 如果之前已经是正常包了，不要再切换成白包了。不做处理
        }
    }

    public static boolean getIsWhite() {
        return SPStaticUtils.getInt(BusinessFieldName.IS_WHITE_USER, 2) != 1;
    }

    public static int getFindDiffTipsGuideCount() {
        return SPStaticUtils.getInt(BusinessFieldName.FIND_DIFF_TIPS_GUIDE_COUNT, 3);
    }

    public static void setFindDiffTipsGuideCount(int count) {
        SPStaticUtils.put(BusinessFieldName.FIND_DIFF_TIPS_GUIDE_COUNT, count);
    }

    public static void setCollectPieceTotalCount(int count) {
        SPStaticUtils.put(BusinessFieldName.COLLECT_PIECE_TASK_TOTAL_COUNT, count);
    }

    public static int getCollectPieceTotalCount() {
        return SPStaticUtils.getInt(BusinessFieldName.COLLECT_PIECE_TASK_TOTAL_COUNT, 30);
    }

    public static void setCollectPieceCompleteCount(int count) {
        SPStaticUtils.put(BusinessFieldName.COLLECT_PIECE_TASK_COMPLETE_COUNT, count);
    }

    public static int getCollectPieceCompleteCount() {
        return SPStaticUtils.getInt(BusinessFieldName.COLLECT_PIECE_TASK_COMPLETE_COUNT, 0);
    }


    public static void setBalanceRate(float rate) {
        SPStaticUtils.put(BusinessFieldName.BALANCE_RATE, rate);
    }

    public static float getBalanceRate() {
        float aFloat = SPStaticUtils.getFloat(BusinessFieldName.BALANCE_RATE, -1);
        if (aFloat > 0) {
            return aFloat;
        }
        String country = ConfigManager.getCountry();
        if (country.equalsIgnoreCase("id")) {
            return 15000;
        } else if (country.equalsIgnoreCase("br")) {
            return 5;
        } else if (country.equalsIgnoreCase("ru")) {
            return 90;
        } else {
            return 1;
        }
    }

    public static void setServerGameConfigEncryptBody(String encryptBody) {
        if (TextUtils.isEmpty(encryptBody)) {
            return;
        }
        SPStaticUtils.put(BusinessFieldName.SERVER_GAME_CONFIG_ENCRYPT_BODY, encryptBody);
    }

    public static JGCResponse getServerGameConfigEncryptBody() {
        try {
            String encryptBody = SPStaticUtils.getString(BusinessFieldName.SERVER_GAME_CONFIG_ENCRYPT_BODY, BusinessFieldName.SEVER_CONFIG_ENCRYPT);
            String decrypt = AesGcmUtil.decrypt(encryptBody);
            LogUtils.e("getServerGameConfigEncryptBody=" + decrypt);
            return GsonUtils.fromJson(decrypt, JGCResponse.class);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return null;
    }

    public static List<CollectPieceBean> readCollectPieceList() {
        return Hawk.get(BusinessFieldName.DB_COLLECT_PIECE_BEAN_LIST, new ArrayList<CollectPieceBean>());
    }

    public static void saveCollectPieceList(List<CollectPieceBean> list) {
        if (list == null || list.isEmpty()) {
            return;
        }
        Hawk.put(BusinessFieldName.DB_COLLECT_PIECE_BEAN_LIST, list);
    }


    public static void setTreasureCompleteCount(int count) {
        SPStaticUtils.put(BusinessFieldName.TREASURE_COMPLETE_COUNT, count);
    }

    public static int getTreasureCompleteCount() {
        return SPStaticUtils.getInt(BusinessFieldName.TREASURE_COMPLETE_COUNT, 0);
    }

    public static int getTreasureTotalCount() {
        return SPStaticUtils.getInt(BusinessFieldName.Treasure_TASK_TOTAL_COUNT, 36);
    }

    public static void setTreasureTotalCount(int totalCount) {
        SPStaticUtils.put(BusinessFieldName.Treasure_TASK_TOTAL_COUNT, totalCount);
    }


}
