package spot.game.manager;

import android.animation.Animator;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.multidex.MultiDex;

import com.adjust.sdk.AdjustAttribution;
import com.airbnb.lottie.LottieAnimationView;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinSdkSettings;
import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.EncryptUtils;
import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.LanguageUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.NotificationUtils;
import com.blankj.utilcode.util.SPStaticUtils;
import com.blankj.utilcode.util.ThreadUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.FutureTarget;
import com.orhanobut.hawk.Hawk;
import com.orhanobut.hawk.HawkBuilder;
import com.orhanobut.hawk.LogLevel;

import net.goal.differences.BuildConfig;
import net.goal.differences.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.WeakHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import spot.differences.gola.GolaActivitySS3;
import spot.differences.gola.GolaActivitySS5;
import spot.differences.gola.GolaActivitySSMine;
import spot.differences.gola.GolaLoadingWindowSSSof;
import spot.differences.gola.GolaUserNetErrorWindow;
import spot.game.constant.BusinessFieldName;
import spot.game.constant.GameConfig;
import spot.game.constant.ZeusConfig;
import spot.game.event.EventConstant;
import spot.game.event.EventMsg;
import spot.game.event.RxBus;
import spot.game.manager.othersdk.AdJustManager;
import spot.game.manager.othersdk.FaceBookManager;
import spot.game.media.SoundUtils;
import spot.game.media.VoicePlayUtils;
import spot.game.model.DefaultConfigBean;
import spot.game.model.JLogExtBean;
import spot.game.model.backModel.JGCResponse;
import spot.game.model.callModel.AttributionRequest;
import spot.game.model.callModel.ParamRequest;
import spot.game.model.callModel.ZeusEvent;
import spot.game.net.AbstractObserver;
import spot.game.util.AesGcmUtil;
import spot.game.util.DeviceUtils;
import spot.game.util.GameTaskUtils;
import spot.game.util.NotifyUtil;
import spot.game.util.ViewUtils;

/**
 * 基础功能
 */
public class GameManager {
    private static final int ACTIVE_USER_OPEN_MINUTE_SPAN = 300;
    private static final int MILLIS_TO_MINUTE = 60_000;
    static double mCurrentPassReward = 0d;//缓存，当前关卡奖励数值，使预期与实际一致
    static ConcurrentLinkedQueue<JGCResponse.PassRewardConfig> mPassRewardConfigList;//过关奖励配置
    static ConcurrentLinkedQueue<JGCResponse.LevelADConfig> mLevelAdConfigList;//关卡强弹配置
    static ConcurrentLinkedQueue<JGCResponse.LevelConfig> mLevelStarConfigList;//关卡难度配置
    static AtomicBoolean isDecodeSeverGameConfig;

    private static Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
            }
        }
    };

    /**
     * 可以在隐私同意之前初始化的
     *
     * @param application
     * @param config
     */
    public static void preInit(Application application, DefaultConfigBean config) {
        try {
            ConfigManager.setChannel(config.getChannel());
            ConfigManager.setAppCode(config.getAppCode());
            SPStaticUtils.put(BusinessFieldName.APP_NAME, config.getAppName());
            SPStaticUtils.put(BusinessFieldName.DEBUG_MODE, config.isDebug());
            //
            LogUtils.getConfig().setLogSwitch(BuildConfig.DEBUG);
            LogUtils.getConfig().setGlobalTag("GameManager");
            LogUtils.getConfig().setStackDeep(3);
            //
            ToastUtils.getDefaultMaker().setGravity(Gravity.CENTER, 0, 0);
            ToastUtils.getDefaultMaker().setBgColor(application.getResources().getColor(R.color.black));
            ToastUtils.getDefaultMaker().setTextSize(15);
            ToastUtils.getDefaultMaker().setTextColor(application.getResources().getColor(R.color.white));
            //
            ConfigManager.setSessionId();
            ConfigManager.addOpenTimes();
            NotifyUtil.getInstance().updateNotify();
            //
            Hawk.init(application)
                    .setEncryptionMethod(HawkBuilder.EncryptionMethod.MEDIUM)
                    .setStorage(HawkBuilder.newSqliteStorage(application))
                    .setLogLevel(LogLevel.FULL)
                    .build();
        } catch (Throwable throwable) {

        } finally {
            TrackManager.health(ZeusConfig.health);
            long time = SPStaticUtils.getLong(BusinessFieldName.FIRST_OPEN_MILLIS, 0);
            if (time == 0) {
                SPStaticUtils.put(BusinessFieldName.FIRST_OPEN_MILLIS, System.currentTimeMillis());
            }
        }
    }

    public static void activeRule(double ecpm) {
        try {
            if (SPStaticUtils.getBoolean(BusinessFieldName.ACTIVE_USER_DONE, false)) {
                return;
            }
            //SPAN TIME
            long startTime = SPStaticUtils.getLong(BusinessFieldName.FIRST_OPEN_MILLIS, 0);
            startTime = (System.currentTimeMillis() - startTime) / MILLIS_TO_MINUTE;
            if (startTime > ACTIVE_USER_OPEN_MINUTE_SPAN) {
                return;
            }
            //CPM
            double lastEcpm = ecpm * 1000;
            double ruleValue = SPStaticUtils.getFloat(BusinessFieldName.ACTIVE_USER_ECPM_VALUE, 1f);
            if (lastEcpm < ruleValue) {
                return;
            }
            //IPU
            int lastIPU = SPStaticUtils.getInt(BusinessFieldName.EFFECTIVE_USER_IPU_COUNT, 0) + 1;
            SPStaticUtils.put(BusinessFieldName.EFFECTIVE_USER_IPU_COUNT, lastIPU);
            //
            double ruleIPU = SPStaticUtils.getFloat(BusinessFieldName.ACTIVE_USER_MM_IPU, 1);
            if (lastIPU < ruleIPU) {
                TrackManager.sendTrack(BusinessFieldName.debug_adjust_init_start
                        , "activeEcpm=" + lastEcpm + ",ruleEcpm=" + ruleValue
                                + ", ruleIPU=" + ruleIPU + ", lastIPU=" + lastIPU
                        , "gap=" + startTime);
                return;
            }
            //ACTIVE
            AdJustManager.INSTANCE().init(ContextManager.getApplication());
            FaceBookManager.initFacebookSdk();
            ConfigManager.setAdJustEnable(true);
            TrackManager.sendTrack(JLogExtBean.event(TrackManager.atr_sdk, "activeEcpm=" + lastEcpm + ",ruleEcpm=" + ruleValue + ",gap=" + startTime)
                    .setDublevalue(lastEcpm));
        } catch (Throwable throwable) {

        }
    }




    public static String getAppSecret(long requestTime, String uuid) {
        String secret = "";
        try {
            StringBuilder sb = new StringBuilder();
            String userId = ConfigManager.getUserId();
            if (!TextUtils.isEmpty(userId)) {
                sb.append(userId);
            }
            sb.append(ConfigManager.getAppCode());
            sb.append(requestTime);
            sb.append(uuid);
            secret = EncryptUtils.encryptMD5ToString(sb.toString().toUpperCase());
        } catch (Exception e) {

        }
        return secret;
    }

    public static Map<String, Object> buildCommonInfo() {
        Map<String, Object> commonMap = new HashMap<>();
        commonMap.put(BusinessFieldName.APPCODE_HEAD, ConfigManager.getAppCode());
        long requestTime = System.currentTimeMillis();
        commonMap.put(BusinessFieldName.requestTime, requestTime);
        String uuid = UUID.randomUUID().toString();
        commonMap.put(BusinessFieldName.requestId, uuid);
        commonMap.put(BusinessFieldName.userId, ConfigManager.getUserId());
        commonMap.put(BusinessFieldName.secret, getAppSecret(requestTime, uuid));
        commonMap.put(BusinessFieldName.country, ConfigManager.getCountry());
        commonMap.put(BusinessFieldName.chour, DeviceUtils.getChour());
        commonMap.put(BusinessFieldName.timeZone, ConfigManager.getTimeZone());
        commonMap.put(BusinessFieldName.sessionId, ConfigManager.getSessionId());
        commonMap.put(BusinessFieldName.language, GameManager.getLanguage());
        commonMap.put(BusinessFieldName.displayCountry, ConfigManager.getCountry());
        commonMap.put(BusinessFieldName.vc, ConfigManager.getVc());
        commonMap.put(BusinessFieldName.vn, ConfigManager.getVn());
        commonMap.put(BusinessFieldName.googleId, SPStaticUtils.getString(BusinessFieldName.GID, ""));
        commonMap.put(BusinessFieldName.registerDate, ConfigManager.getRegisterDate());
        commonMap.put(BusinessFieldName.channel, ConfigManager.getChannel());
        commonMap.put(BusinessFieldName.model, Build.MODEL);
        commonMap.put(BusinessFieldName.brand, Build.BRAND);
        commonMap.put(BusinessFieldName.lv, -1);
        commonMap.put(BusinessFieldName.androidId, SPStaticUtils.getString(BusinessFieldName.androidid, ""));
        commonMap.put(BusinessFieldName.deVersion, "Android" + Build.VERSION.RELEASE);
        return commonMap;
    }

    public static void init(Context context) {
        try {
            if (TextUtils.isEmpty(ConfigManager.getUserId())) {
                ConfigManager.initDeviceConfig();
            }
            SPStaticUtils.put(BusinessFieldName.network, DeviceUtils.getNetWorkType(context));
        } catch (Throwable throwable) {
        } finally {
            ADManager.INSTANCE().init(context);
        }
    }

    public static Map<String, Object> buildMobileCommonInfo() {
        Map<String, Object> commonMap = new HashMap<>();
        commonMap.put(BusinessFieldName.APPCODE_HEAD, ConfigManager.getAppCode());
        long requestTime = System.currentTimeMillis();
        commonMap.put(BusinessFieldName.requestTime, requestTime);
        String uuid = UUID.randomUUID().toString();
        commonMap.put(BusinessFieldName.requestId, uuid);
        commonMap.put(BusinessFieldName.userId, ConfigManager.getUserId());
        commonMap.put(BusinessFieldName.secret, getAppSecret(requestTime, uuid));
        commonMap.put(BusinessFieldName.country, ConfigManager.getCountry());
        commonMap.put(BusinessFieldName.chour, DeviceUtils.getChour());
        commonMap.put(BusinessFieldName.timeZone, SPStaticUtils.getString(BusinessFieldName.timeZone, ""));
        commonMap.put(BusinessFieldName.sessionId, ConfigManager.getSessionId());
        commonMap.put(BusinessFieldName.vc, ConfigManager.getVc());
        commonMap.put(BusinessFieldName.vn, ConfigManager.getVn());
        commonMap.put(BusinessFieldName.googleId, SPStaticUtils.getString(BusinessFieldName.GID, ""));
        commonMap.put(BusinessFieldName.registerDate, ConfigManager.getRegisterDate());
        return commonMap;
    }


    public static void attachBaseContext(Application base) {
        MultiDex.install(base);
        FaceBookManager.rejectInitFacebook();
    }


    public static void sendAttribution(AdjustAttribution attribution) {
        try {
            if (attribution != null) {
                boolean adjustAttribution = SPStaticUtils.getBoolean("AdjustAttribution", false);
                if (!adjustAttribution) {
                    final AttributionRequest request = new AttributionRequest();
                    request.setM(ZeusConfig.J_ADJUST);
                    request.setFbInstallReferrer(attribution.fbInstallReferrer);
                    request.setAdgroup(attribution.adgroup);
                    request.setAdid(attribution.adid);
                    request.setCampaign(attribution.campaign);
                    request.setClickLabel(attribution.clickLabel);
                    if (Double.isNaN(attribution.costAmount)) {
                        request.setCostAmount(0);
                    } else {
                        request.setCostAmount(attribution.costAmount);
                    }
                    request.setCostCurrency(attribution.costCurrency);
                    request.setCostType(attribution.costType);
                    request.setCreative(attribution.creative);
                    request.setEvent("app_home");
                    request.setNetwork(attribution.network);
                    request.setTrackerName(attribution.trackerName);
                    request.setTrackerToken(attribution.trackerToken);
                    request.setDeepLinkUrl("");
                    request.setGgClickTime("");
                    //
                    ConfigManager.setAdChannel(attribution.network);
                    ConfigManager.setAdGroupId(attribution.adgroup);
                    //
                    NetManager.post(request, new AbstractObserver<String>() {

                        @Override
                        public void onNext(String s, String encryptBody) {
                            SPStaticUtils.put("AdjustAttribution", true);
                            ADManager.INSTANCE().initBaseInfo();
                            TrackManager.sendTrack(BusinessFieldName.debug_adjust_report, s, "");
                        }
                    });

                }
            }
        } catch (Throwable throwable) {

        }
    }

    public static void release() {
        ADManager.INSTANCE().onDestroy();
    }


    public static Map<String, String> getDefaultHeaderMap() {
        WeakHashMap<String, String> header = new WeakHashMap<>();
        header.put(BusinessFieldName.APPCODE_HEAD, ConfigManager.getAppCode());
        return header;
    }


    /**
     * 无网计数上报
     */
    public static synchronized void sendNetErrorCountTrack() {
        int netErrorShowCountThisTime = ConfigManager.getNetErrorShowCountThisTime();
        int netErrorClickCountThisTime = ConfigManager.getNetErrorClickCountThisTime();
        if (netErrorShowCountThisTime > 0) {
            //
            ConfigManager.setNetErrorShowCountThisTime(0);
            ConfigManager.setNetErrorClickCountThisTime(0);
            //
            TrackManager.sendTrack("Internet_Popup_Show", "" + netErrorShowCountThisTime, "");
            TrackManager.sendTrack("Internet_Popup_Click", "" + netErrorClickCountThisTime, "");
        }
    }

    /**
     * 数据预加载
     */
    public static void loadData() {
        loadServerConfig();
        ADManager.INSTANCE().initBaseInfo();
    }

    /**
     * @param str 需要过滤的字符串
     * @return
     * @Description:过滤数字以外的字符
     */
    public static String filterUnNumber(String str) {
        // 只允数字
        String regEx = "[^0-9]";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(str);
        //替换与模式匹配的所有字符（即非数字的字符将被””替换）
        return m.replaceAll("").trim();

    }

    public static String getLanguage() {
        if (GameConfig.isWhite) {
            return "en";
        }
        if (BuildConfig.PRODUCT_FLAVORS.equalsIgnoreCase("anythink")) {
            return SPStaticUtils.getString(BusinessFieldName.test_language, LanguageUtils.getSystemLanguage().getLanguage().toLowerCase(Locale.ROOT));
        }
        return LanguageUtils.getSystemLanguage().getLanguage().toLowerCase(Locale.ROOT);
    }


    public static AppLovinSdk getAppLovinSdk() {
        AppLovinSdkSettings appLovinSdkSettings = new AppLovinSdkSettings(ContextManager.getContext());
        appLovinSdkSettings.setMuted(false);
        appLovinSdkSettings.setVerboseLogging(true);
        appLovinSdkSettings.setExceptionHandlerEnabled(true);
        appLovinSdkSettings.setLocationCollectionEnabled(true);
        return AppLovinSdk.getInstance(ConfigManager.getMaxSDKKey(), appLovinSdkSettings, ContextManager.getContext());
    }

    public static void showLoadingWindow() {
        GolaLoadingWindowSSSof.showLoadingWindow();
    }

    public static void dismissLoadingWindow() {
        GolaLoadingWindowSSSof.dismissLoadingWindow();
    }

    public static void showNetErrorWindow(GolaUserNetErrorWindow.OnNetErrorListener onNetErrorListener) {
        GolaUserNetErrorWindow.showNetErrorWindow(onNetErrorListener);
    }

    public static void dismissNetErrorWindow() {
        GolaUserNetErrorWindow.dismissNetErrorWindow();
    }


    public static void toHome() {
        RxBus.getInstance().post(EventMsg.event(EventConstant.CODE_EVENT_MAIN_TO_TAB).setArg1(0).build());
    }

    public static void intentToTreasurePage(Context context) {
        RxBus.getInstance().post(EventMsg.event(EventConstant.CODE_EVENT_MAIN_TO_TAB).setArg1(1).build());
    }

    //切换到提现页面
    public static void intentToMine(Context context) {
        GolaActivitySSMine.startTheActivity(context);
    }

    public static void intentToRecord(Context context) {
        GolaActivitySS5.startTheActivity(context);
    }

    public static void newClick() {
        if (!ConfigManager.getSoundSetting()) {
            return;
        }
        SoundUtils.getInstance().newClick(1, R.raw.game_spot_click_btn);
    }

    public static void newClick(int resID) {
        if (!ConfigManager.getSoundSetting()) {
            return;
        }
        SoundUtils.getInstance().newClick(1, resID);
    }

    private static void showRewardToast(double reward) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                try {
                    View mToastView = LayoutInflater.from(ContextManager.getContext()).inflate(R.layout.game_spot_layout_toast_reward_get, null);
                    TextView tvTitle = mToastView.findViewById(R.id.tv_game_spot_toast_reward_get_reward_title);
                    ViewUtils.autoResizeText(tvTitle, ContextManager.getContext().getResources().getString(R.string.game_spot_congratuiations), 22, 20, 42);
                    //
                    ImageView ivIcon = mToastView.findViewById(R.id.iv_game_spot_toast_reward_get_reward_icon);
                    ivIcon.setImageResource(IconSelectManager.getManyRewardIcon());
                    TextView tvAmount = mToastView.findViewById(R.id.tv_game_spot_toast_reward_get_reward_amount);
                    tvAmount.setText("+" + ConfigManager.getCurrency() + BalanceManager.getFormatBalance(reward));
                    ToastUtils.getDefaultMaker().show(mToastView);
                    //
                    if (mToastView != null && NotificationUtils.areNotificationsEnabled()) {
                        playRewardAnim(mToastView.findViewById(R.id.la_game_spot_toast_reward_get_reward_anim));
                    }
                    refreshAccount(reward + ConfigManager.getBalance());
                    //
                    newClick(R.raw.game_spot_bg_reward);
                } catch (Throwable e) {
                    LogUtils.e("showRewardToast=" + e.getMessage());
                }
            }
        });
    }

    /**
     * 通知未授权时ToastUtils能正常显示，但lottie动画无法播放，故通过在顶层ViewGroup上add AnimView进行动画展示
     *
     * @param reward
     * @param activity
     */
    public static void showRewardToast(double reward, Activity activity) {
        showRewardToast(reward, activity, null);
    }

    public static void showRewardToast(double reward, final Activity activity, ViewGroup viewGroup) {
        ZeusEvent zeusEvent = new ZeusEvent();
        zeusEvent.setEvent(ZeusConfig.ext_reward);
        zeusEvent.setMoney(reward + "");
        TrackManager.sendZeusEvent(zeusEvent);
        //
        if (NotificationUtils.areNotificationsEnabled()) {
            showRewardToast(reward);
            return;
        }
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                try {
                    View mAnimView = null;
                    //
                    showRewardToast(reward);
                    //
                    if (!ActivityUtils.isActivityAlive(activity)) {
                        try {
                            mAnimView = LayoutInflater.from(ContextManager.getActivity()).inflate(R.layout.game_spot_layout_spin_anim, null);
                        } catch (Throwable e) {
                            e.printStackTrace();
                            mAnimView = LayoutInflater.from(ContextManager.getContext()).inflate(R.layout.game_spot_layout_spin_anim, null);
                        }
                    } else {
                        mAnimView = LayoutInflater.from(activity).inflate(R.layout.game_spot_layout_spin_anim, null);
                    }
                    ViewGroup decorView = viewGroup;
                    if (decorView == null) {
                        if (!ActivityUtils.isActivityAlive(activity)) {
                            try {
                                decorView = (ViewGroup) ContextManager.getActivity().getWindow().getDecorView();
                            } catch (Throwable e) {
                                e.printStackTrace();
                            }
                        } else {
                            decorView = (ViewGroup) activity.getWindow().getDecorView();
                        }
                    }
                    try {
                        if (mAnimView != null) {
                            ViewGroup parent = (ViewGroup) mAnimView.getParent();
                            if (parent != null) {
                                parent.removeView(mAnimView);
                            }
                        }
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                    if (decorView != null) {
                        decorView.addView(mAnimView);
                    }
                    //
                    LottieAnimationView mToastLottieView = mAnimView.findViewById(R.id.la_game_spot_toast_reward_get_reward_anim);
                    //
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                playRewardAnim(mToastLottieView);
                            } catch (Throwable e) {
                                e.printStackTrace();
                            }
                        }
                    }, 2_000);
                } catch (Throwable e) {
                    LogUtils.e("showRewardToast=" + e.getMessage());
                }
            }
        });
    }

    public static void refreshAccount(double userNewBalance) {
        RxBus.getInstance().post(EventMsg.event(EventConstant.CODE_EVENT_REWARD_ACCOUNT).setArg2(userNewBalance).build());
    }

    public static void refreshWholeThemeBg() {
        RxBus.getInstance().post(EventMsg.event(EventConstant.CODE_EVENT_REFRESH_WHOLE_THEME_BG).build());
    }

    //public static void refreshViewBgFromAssets(Context context, View bgWhole) {
    //    refreshViewBgFromAssets(context, bgWhole, "file:///android_asset/game_spot_theme_bg/bg_game_spot_theme_bg_" + ConfigManager.getThemeSelectedIndex() + ".jpg", R.mipmap.bg_game_spot_question_whole);
    //}

    public static void refreshViewBgFromAssets(Context context, View bgWhole, String assetPath, int defaultResID) {
        try {
            ThreadUtils.executeByIo(new ThreadUtils.SimpleTask<Object>() {
                @Override
                public Object doInBackground() throws Throwable {
                    try {
                        FutureTarget<Drawable> submit = Glide.with(context).load(assetPath).submit();
                        Drawable themeSelectedRes = submit.get();
                        ThreadUtils.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    if (themeSelectedRes != null) {
                                        bgWhole.setBackground(themeSelectedRes);
                                    } else {
                                        bgWhole.setBackgroundResource(defaultResID);
                                    }
                                } catch (Throwable e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                    return null;
                }

                @Override
                public void onSuccess(Object result) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 返回String类型的Json
     *
     * @param context
     * @param fileName
     * @return
     */
    public static String loadJSONFromAsset(Context context, String fileName) {
        String json = null;
        try {
            InputStream inputStream = context.getAssets().open(fileName);
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public static void initBGMMedia() {
        if (!ConfigManager.getSoundSetting()) {
            return;
        }
        VoicePlayUtils.initBGMMedia(R.raw.game_spot_bg_music);
    }

    public static void stopMedia() {
        VoicePlayUtils.stopMedia();
    }

    public static void playRewardAnim(LottieAnimationView mRewardPkgAnim) {
        //
        String country = ConfigManager.getCountry();
        String animFolder = "spot_gola_images_cash_en";
        if (country.equalsIgnoreCase("br")) {
            animFolder = "spot_gola_images_cash_br";
        } else if (country.equalsIgnoreCase("id")) {
            animFolder = "spot_gola_images_cash_id";
        } else if (country.equalsIgnoreCase("ru")) {
            animFolder = "spot_gola_images_cash_ru";
        } else if (country.equalsIgnoreCase("pk")) {
            animFolder = "spot_gola_images_cash_pk";
        }
        playRewardAnim(mRewardPkgAnim, "spot_gola_data_cash_reward.json", animFolder);
    }

    public static void playRewardAnim(LottieAnimationView mRewardPkgAnim, String animJson, String animFolder) {
        if (mRewardPkgAnim == null || TextUtils.isEmpty(animJson) || TextUtils.isEmpty(animFolder)) {
            return;
        }
        mRewardPkgAnim.setAnimation(animJson);//在assets目录下的动画json文件名。
        mRewardPkgAnim.setImageAssetsFolder(animFolder);//assets目录下的子目录，存放动画所需的图片
//        mRewardPkgAnim.setSpeed(0.6f);
        mRewardPkgAnim.addAnimatorListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                //LogUtils.e("onAnimationStart");
                newClick(R.raw.game_spot_bg_reward_animate);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
//                mRewardPkgAnim.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                //LogUtils.e("onAnimationCancel");
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
                //LogUtils.e("onAnimationRepeat");
            }
        });
        mRewardPkgAnim.playAnimation();
    }

    /**
     * Return the country by system language.
     *
     * @return the country
     */
    public static String getCountryByLanguage() {
        try {
            return Resources.getSystem().getConfiguration().locale.getCountry();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return "unknown";
    }




    //public static void countAdShow() {
    //    ParamRequest request = new ParamRequest();
    //    request.setM(BusinessFieldName.M_COUNT_AD_SHOW);
    //    NetManager.post(request, null);
    //}

    public static void intentToSplash(Context context) {
        try {
            Intent intent = new Intent(context, GolaActivitySS3.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);//在Android 7.0以下非activity场景启动须加
            context.startActivity(intent);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public static void restartApp(Context context) {
        try {
            Intent intent = new Intent(context, GolaActivitySS3.class);
            intent.putExtra("forceKill", "forceKill");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);//在Android 7.0以下非activity场景启动须加
            context.startActivity(intent);
            //
            if (context instanceof Activity) {
                ((Activity) context).finish();
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public static boolean isAppActivity(Activity activity) {
        String packageName = ContextManager.getContext().getPackageName();
        if (BuildConfig.PRODUCT_FLAVORS.equalsIgnoreCase("anythink")) {
            packageName = "spot.differences.gola";
        }
        return (activity != null && activity.getClass().getName().contains(packageName));
    }





    /**
     * 读取本地json，转换成String
     *
     * @param context
     * @param fileName
     * @return
     */
    public static String getAssetInfoStr(Context context, String fileName) {
        StringBuilder stringBuilder = new StringBuilder();
        InputStream inputStream = null;
        try {
            inputStream = context.getAssets().open(fileName);
            InputStreamReader inputReader = new InputStreamReader(inputStream);
            BufferedReader reader = new BufferedReader(inputReader);
            String jsonLine;
            while ((jsonLine = reader.readLine()) != null) {
                stringBuilder.append(jsonLine);
            }

            reader.close();
            inputReader.close();
            inputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return stringBuilder.toString();
    }


    /**
     * 配置拉取
     */
    public static void loadServerConfig() {
        decodeServerConfig("");
        //
        NetManager.post(new ParamRequest(BusinessFieldName.M_LOAD_ALL_CONFIG), new AbstractObserver<JGCResponse>() {
            @Override
            public void onNext(JGCResponse jgcResponse, String encryptBody) {
                LogUtils.e("M_LOAD_ALL_CONFIG=" + encryptBody);
                LogUtils.e("M_LOAD_ALL_CONFIG=" + AesGcmUtil.decrypt(encryptBody));
                //
                if (isDecodeSeverGameConfig == null) {
                    isDecodeSeverGameConfig = new AtomicBoolean();
                }
                isDecodeSeverGameConfig.set(false);
                decodeServerConfig(encryptBody);
            }

            @Override
            public void onError(Throwable e) {
            }
        });
    }

    /**
     * 配置解析
     *
     * @param encryptBody 空代表默认配置
     */
    private static void decodeServerConfig(String encryptBody) {
        if (isDecodeSeverGameConfig == null) {
            isDecodeSeverGameConfig = new AtomicBoolean();
            isDecodeSeverGameConfig.set(false);
        }
        if (isDecodeSeverGameConfig.get()) {
            return;
        }
        isDecodeSeverGameConfig.set(true);
        //
        ThreadUtils.executeByIo(new ThreadUtils.SimpleTask<Object>() {

            @Override
            public Object doInBackground() throws Throwable {
                try {
                    JGCResponse jgcResponse;
                    if (TextUtils.isEmpty(encryptBody)) {
                        jgcResponse = ConfigManager.getServerGameConfigEncryptBody();
                    } else {
                        jgcResponse = GsonUtils.fromJson(AesGcmUtil.decrypt(encryptBody), JGCResponse.class);
                    }
                    /**
                     * 货币配置
                     */
                    decodeRMConfig(jgcResponse.getRm());
                    /**
                     * 关卡奖励配置
                     */
                    decodePassRewardConfig(jgcResponse.getMc());
                    /**
                     * 提现配置
                     */
                    decodeWithdrawConfig(jgcResponse);
                    /**
                     * 关卡强弹配置
                     */
                    decodeLevelADConfig(jgcResponse.getPac());
                    /**
                     * 关卡难度配置
                     */
                    decodeLevelConfig(jgcResponse.getZcpc());
                    /**
                     * 配置更新
                     */
                    // 像广告配置一样，一个加密字符串，解密触发同一个解包逻辑
                    ConfigManager.setServerGameConfigEncryptBody(encryptBody);
                } catch (Throwable e) {
                    e.printStackTrace();
                } finally {
                    if (isDecodeSeverGameConfig == null) {
                        isDecodeSeverGameConfig = new AtomicBoolean();
                    }
                    isDecodeSeverGameConfig.set(false);
                }
                return null;
            }

            @Override
            public void onSuccess(Object result) {

            }
        });
    }

    private static void decodeLevelConfig(List<JGCResponse.LevelConfig> zcpc) {
        if (mLevelStarConfigList == null) {
            mLevelStarConfigList = new ConcurrentLinkedQueue<>();
        }
        mLevelStarConfigList.clear();
        mLevelStarConfigList.addAll(zcpc);
    }

    private static void decodeLevelADConfig(List<JGCResponse.LevelADConfig> pac) {
        if (mLevelAdConfigList == null) {
            mLevelAdConfigList = new ConcurrentLinkedQueue<>();
        }
        mLevelAdConfigList.clear();
        mLevelAdConfigList.addAll(pac);
    }

    private static void decodeWithdrawConfig(JGCResponse jgcResponse) {
        //调用假提现 传入参数是测试参数
        //最低可提现关卡数
        ConfigManager.setNeedLevel(jgcResponse.getWdc().getSl());
        //需要初始化配置的任务属性
        GameTaskUtils.setWithdrawStateModel(jgcResponse.getWdc().getR1(), jgcResponse.getWdc().getR2(), jgcResponse.getWdc().getR3());
        //
        ConfigManager.setCollectPieceTotalCount((int) jgcResponse.getPtc().getTotalReward());
    }

    private static void decodePassRewardConfig(List<JGCResponse.PassRewardConfig> mc) {
        double balanceRate = ConfigManager.getBalanceRate();
        if (mPassRewardConfigList == null) {
            mPassRewardConfigList = new ConcurrentLinkedQueue<>();
        }
        mPassRewardConfigList.clear();
        //
        int size = mc.size();
        for (int i = 0; i < size; i++) {
            JGCResponse.PassRewardConfig passRewardConfig = mc.get(i);
            if (passRewardConfig == null) {
                continue;
            }
            passRewardConfig.setStartMoney(balanceRate * passRewardConfig.getStartMoney());
            if (passRewardConfig.getEndMoney() < 0) {
                passRewardConfig.setEndMoney(Double.MAX_VALUE);
            } else {
                passRewardConfig.setEndMoney(balanceRate * passRewardConfig.getEndMoney());
            }
            passRewardConfig.setMaxMoney(balanceRate * passRewardConfig.getMaxMoney());
            passRewardConfig.setMinMoney(balanceRate * passRewardConfig.getMinMoney());
            mPassRewardConfigList.add(passRewardConfig);
        }
    }

    private static void decodeRMConfig(JGCResponse.BanlanceRateConfig rm) {
        ConfigManager.setCurrency(rm.getUnit());
        ConfigManager.setBalanceRate(Float.parseFloat(rm.getRate()));
    }

    /**
     * 获取预期关卡奖励，更新
     *
     * @param justShow 是：展示，否：结算到账
     * @return
     */
    public static double getPassReward(boolean justShow) {
        if (mPassRewardConfigList == null || mPassRewardConfigList.isEmpty()) {
            decodeServerConfig("");
            return 0;
        }
        try {
            if (justShow) {
                //使用总账户余额
                double balance = getUserAllAccountBalance();
                for (JGCResponse.PassRewardConfig passRewardConfig : mPassRewardConfigList) {
                    if (passRewardConfig == null) {
                        continue;
                    }
                    if (passRewardConfig.getStartMoney() > balance || passRewardConfig.getEndMoney() <= balance) {
                        continue;
                    }
                    double minMoney = passRewardConfig.getMinMoney();
                    double round = (passRewardConfig.getMaxMoney() - minMoney);
                    mCurrentPassReward = new Random().nextDouble() * round + minMoney;
                    LogUtils.e("round=" + round + ", mCurrentPassReward=" + mCurrentPassReward + "，" + minMoney);
                    break;
                }
            }
        } catch (Throwable e) {
            LogUtils.e(e.getMessage());
            if (justShow) {
                mCurrentPassReward = 0;
            }
        }
        return mCurrentPassReward;
    }

    /**
     * 用户总余额，包含提现部分
     */
    public static float getUserAllAccountBalance() {
        return GameTaskUtils.getWithdrawStateModel().getWithdrawCashNmu() + ConfigManager.getBalance();
    }


    /**
     * 获取关卡是否强弹
     * 从1开始
     *
     * @return
     */
    public static boolean getLevelAd(int questionNo) {
        if (mLevelAdConfigList == null || mLevelAdConfigList.isEmpty()) {
            decodeServerConfig("");
            return false;
        }
        try {
            for (JGCResponse.LevelADConfig adConfig : mLevelAdConfigList) {
                if (adConfig == null) {
                    continue;
                }
                int maxLevel = adConfig.getMaxLevel() < 0 ? Integer.MAX_VALUE : adConfig.getMaxLevel();
                int minLevel = adConfig.getMinLevel();
                if (questionNo <= minLevel || questionNo > maxLevel) {
                    continue;
                }
                return (questionNo - minLevel + 1) % adConfig.getLvNum() == 0;
            }
        } catch (Throwable e) {
            LogUtils.e(e.getMessage());
        }
        return false;
    }

    /**
     * 获取关卡难度星级
     * 从1开始
     *
     * @return
     */
    public static int getLevelStar(int questionNo) {
        if (mLevelStarConfigList == null || mLevelStarConfigList.isEmpty()) {
            decodeServerConfig("");
            return 1;
        }
        try {
            for (JGCResponse.LevelConfig adConfig : mLevelStarConfigList) {
                if (adConfig == null) {
                    continue;
                }
                int maxLevel = adConfig.getMaxLevel() < 0 ? Integer.MAX_VALUE : adConfig.getMaxLevel();
                int minLevel = adConfig.getMinLevel();
                if (questionNo < minLevel || questionNo > maxLevel) {
                    continue;
                }
                return adConfig.getLvNum();
            }
        } catch (Throwable e) {
            LogUtils.e(e.getMessage());
        }
        return 1;
    }
}
