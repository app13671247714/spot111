package spot.game.manager.othersdk;

import com.facebook.FacebookSdk;
import com.facebook.LoggingBehavior;

import net.goal.differences.BuildConfig;
import net.goal.differences.R;

import spot.game.manager.ContextManager;
import spot.game.manager.TrackManager;

public class FaceBookManager {

    public static void initFacebookSdk() {
        try {
            FacebookSdk.setApplicationId(ContextManager.getApplication().getString(R.string.game_spot_facebook_app_id));
            FacebookSdk.setClientToken(ContextManager.getApplication().getString(R.string.game_spot_facebook_client_token));
            FacebookSdk.sdkInitialize(ContextManager.getApplication(), new FacebookSdk.InitializeCallback() {
                @Override
                public void onInitialized() {
                    TrackManager.sendTrack("facebook_sdk_init", "success", "");
                }
            });
            FacebookSdk.setAutoInitEnabled(true);
            FacebookSdk.setAdvertiserIDCollectionEnabled(true);
            FacebookSdk.setIsDebugEnabled("anythink".equalsIgnoreCase(BuildConfig.PRODUCT_FLAVORS));
            FacebookSdk.setAutoLogAppEventsEnabled(true);
            FacebookSdk.addLoggingBehavior(LoggingBehavior.REQUESTS);
            FacebookSdk.addLoggingBehavior(LoggingBehavior.APP_EVENTS);
            FacebookSdk.fullyInitialize();
        } catch (Throwable e) {
            TrackManager.sendTrack("facebook_sdk_init", "failed", e.getMessage());
            e.printStackTrace();
        }
    }

    public static void rejectInitFacebook() {
        FacebookSdk.setAutoInitEnabled(false);
        FacebookSdk.setIsDebugEnabled(false);
        FacebookSdk.setAutoLogAppEventsEnabled(false);
        FacebookSdk.setAdvertiserIDCollectionEnabled(false);
    }
}
