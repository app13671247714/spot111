package spot.game.manager.othersdk;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.adjust.sdk.Adjust;
import com.adjust.sdk.AdjustAttribution;
import com.adjust.sdk.AdjustConfig;
import com.adjust.sdk.LogLevel;
import com.adjust.sdk.OnAttributionChangedListener;
import com.blankj.utilcode.util.SPStaticUtils;

import spot.game.constant.BusinessFieldName;
import spot.game.constant.GameConfig;
import spot.game.manager.GameManager;
import spot.game.manager.TrackManager;

/**
 * @Class: AdJustHelper
 * @Date: 2022/12/12
 * @Description:
 */
public class AdJustManager implements Application.ActivityLifecycleCallbacks, OnAttributionChangedListener {


    private volatile static AdJustManager helper = null;

    public static AdJustManager INSTANCE() {
        if (helper == null) {
            synchronized (AdJustManager.class) {
                if (helper == null) {
                    helper = new AdJustManager();
                }
            }
        }
        return helper;
    }

    //    (1, 2102265133, 2117832728, 1601263829, 265315211)
    public void init(Application application) {
        String appToken = GameConfig.ADJUST_APP_TOKEN;
        String environment = AdjustConfig.ENVIRONMENT_PRODUCTION;
        AdjustConfig config = new AdjustConfig(application, appToken, environment);
        config.setAppSecret(GameConfig.ADJUST_APP_SECRET_1, GameConfig.ADJUST_APP_SECRET_2, GameConfig.ADJUST_APP_SECRET_3, GameConfig.ADJUST_APP_SECRET_4, GameConfig.ADJUST_APP_SECRET_5);
        config.setSendInBackground(true);
        config.setLogLevel(LogLevel.ERROR);
        config.setOnAttributionChangedListener(this);
        Adjust.onCreate(config);
        application.registerActivityLifecycleCallbacks(this);
        TrackManager.sendTrack(BusinessFieldName.debug_adjust_init_complete, "", "");
        SPStaticUtils.put(BusinessFieldName.ACTIVE_USER_DONE, true);
    }


    @Override
    public void onActivityCreated(@NonNull Activity activity, @Nullable Bundle savedInstanceState) {

    }

    @Override
    public void onActivityStarted(@NonNull Activity activity) {

    }

    @Override
    public void onActivityResumed(@NonNull Activity activity) {
        Adjust.onResume();
    }

    @Override
    public void onActivityPaused(@NonNull Activity activity) {
        Adjust.onPause();
    }

    @Override
    public void onActivityStopped(@NonNull Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(@NonNull Activity activity, @NonNull Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(@NonNull Activity activity) {

    }

    @Override
    public void onAttributionChanged(AdjustAttribution adjustAttribution) {
        GameManager.sendAttribution(adjustAttribution);
    }
}
