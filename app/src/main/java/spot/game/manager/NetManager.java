package spot.game.manager;


import android.text.TextUtils;

import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SPStaticUtils;
import com.blankj.utilcode.util.ThreadUtils;
import com.blankj.utilcode.util.TimeUtils;

import net.goal.differences.BuildConfig;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import spot.differences.gola.GolaUserNetErrorWindow;
import spot.game.constant.BusinessFieldName;
import spot.game.constant.ZeusConfig;
import spot.game.model.JLogExtBean;
import spot.game.model.backModel.JMAResponse;
import spot.game.model.backModel.ParamResponse;
import spot.game.model.callModel.TrackRequest;
import spot.game.net.AbstractObserver;
import spot.game.net.NetUtils;
import spot.game.util.AesGcmUtil;

public class NetManager {

    /**
     * Post
     *
     * @param headers       消息头
     * @param bodyStr       消息体
     * @param observer      回调
     * @param netErrorRetry 无网重试
     * @param <T>           回跳结果装配类型
     */
    public static <T> void post(Map<String, String> headers, String bodyStr, AbstractObserver<T> observer, boolean netErrorRetry) {
        try {
            NetUtils.getInstance().post(ZeusConfig.getBaseUrl(), headers, bodyStr, new Observer<String>() {

                @Override
                public void onNext(String responseBody) {
                    if (netErrorRetry) {
                        GameManager.dismissLoadingWindow();
                        GameManager.sendNetErrorCountTrack();
                    }
                    //
                    if (observer != null) {
                        try {
                            if (TextUtils.isEmpty(responseBody)) {
                                observer.onError(new Exception("responseBody=null"));
                                return;
                            }
                            ParamResponse paramResponse = GsonUtils.fromJson(responseBody, ParamResponse.class);
                            if (paramResponse == null || TextUtils.isEmpty(paramResponse.getData())) {
                                observer.onError(new Exception("UNKOWN CLASSTTYPE=" + responseBody));
                                return;
                            }
                            int resCode = paramResponse.getResCode();
                            String errorMsg = paramResponse.getMsg();
                            if (resCode != BusinessFieldName.ZEUS_SUCCESS) {
                                observer.onError(new Exception("resCode=" + resCode + ", msg=" + errorMsg));
                                return;
                            }
                            String decrypt = AesGcmUtil.decrypt(paramResponse.getData());
                            //
                            T t = observer.genClazz(decrypt);
                            if (t == null) {
                                observer.onError(new Exception("UNKOWN CLASSTTYPE=" + decrypt));
                                return;
                            }
                            observer.onNext(t, paramResponse.getData());
                        } catch (Throwable e) {
                            e.printStackTrace();
                            try {
                                observer.onError(e);
                            } catch (Throwable ex) {
                                ex.printStackTrace();
                            }
                        }
                    }
                }

                @Override
                public void onSubscribe(Disposable disposable) {
                }

                @Override
                public void onError(Throwable e) {
                    LogUtils.e(e.getMessage());
                    if (!netErrorRetry) {
                        if (observer != null) {
                            try {
                                observer.onError(e);
                            } catch (Throwable ex) {
                                ex.printStackTrace();
                            }
                        }
                        return;
                    }
                    GameManager.dismissLoadingWindow();
                    ConfigManager.addNetErrorShowCountThisTime();
                    GameManager.showNetErrorWindow(new GolaUserNetErrorWindow.OnNetErrorListener() {
                        @Override
                        public void onConfirm() {
                            ConfigManager.addNetErrorClickCountThisTime();
                            GameManager.dismissNetErrorWindow();
                            GameManager.showLoadingWindow();
                            ThreadUtils.getMainHandler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    post(headers, bodyStr, observer, netErrorRetry);
                                }
                            }, 1000);
                        }
                    });
                }

                @Override
                public void onComplete() {
                }
            });
        } catch (Throwable e) {
            if (observer != null) {
                try {
                    observer.onError(e);
                } catch (Throwable ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    /**
     * Post，对象转json并加密
     *
     * @param headers       消息头
     * @param bodyObject    消息体
     * @param observer      回调
     * @param netErrorRetry 无网重试
     * @param <T>           回跳结果装配类型
     */
    public static <T> void post(Map<String, String> headers, Object bodyObject, AbstractObserver<T> observer, boolean netErrorRetry) {
        String privateString = "";
        if (bodyObject != null && bodyObject instanceof String) {
            privateString = (String) bodyObject;
            LogUtils.e(privateString);
        } else {
            privateString = GsonUtils.toJson(bodyObject);
            LogUtils.e(privateString);
            privateString = AesGcmUtil.encrypt(privateString);
        }
        LogUtils.e(privateString);
        post(headers, privateString, observer, netErrorRetry);
    }

    /**
     * Post，对象转json并加密
     * 默认消息头
     *
     * @param bodyObject 消息体
     * @param observer   回调
     * @param <T>        回跳结果装配类型
     */
    public static <T> void post(Object bodyObject, AbstractObserver<T> observer) {
        post(bodyObject, observer, false);
    }


    /**
     * Post，对象转json并加密
     * 默认消息头
     *
     * @param bodyObject    消息体
     * @param observer      回调
     * @param netErrorRetry 无网重试
     * @param <T>           回跳结果装配类型
     */
    public static <T> void post(Object bodyObject, AbstractObserver<T> observer, boolean netErrorRetry) {
        post(GameManager.getDefaultHeaderMap(), bodyObject, observer, netErrorRetry);
    }


    public static void sendTrack(JLogExtBean logExtBean) {
        try {
            logExtBean = filterLogMobileAction(logExtBean);
            if (logExtBean == null || TextUtils.isEmpty(logExtBean.getEvent())) {
                return;
            }
            TrackRequest request = new TrackRequest();
            Map<String, Object> extend = new HashMap<>();
            extend.put("Network", SPStaticUtils.getString(BusinessFieldName.network, "unknown"));
            extend.put("OpenTimes", ConfigManager.openTimes());
            extend.put("event", logExtBean.getEvent());//业务事件
            extend.put("intvalue", logExtBean.getIntvalue());//整型类数据-统计用，非业务必要则为空
            extend.put("strvalue", logExtBean.getStrvalue());//字符类数据-统计用，非业务必要则为空
            extend.put("dublevalue", logExtBean.getDublevalue());//浮点类数值-统计用，非业务必要则为空
            extend.put("logmsg", logExtBean.getLogmsg());//备注，内容随意
            //本地时间
            extend.put("logdate", TimeUtils.getNowString(TimeUtils.getSafeDateFormat("yyyy-MM-dd HH:mm:ss.SSS")));
            //本地时间戳,毫秒
            extend.put("logtime", System.currentTimeMillis());
            //日志级别：0：最高级  1：2、3、4： 5：日志调试级别
            extend.put("loglevel", logExtBean.getLoglevel());
            extend.put(BusinessFieldName.clueid, ConfigManager.getClueId());
            extend.put("ab_tags", ConfigManager.getABTags());
            extend.put("mobile_region", GameManager.getCountryByLanguage());
            extend.put(BusinessFieldName.adChannel, ConfigManager.getAdChannel());
            extend.put(BusinessFieldName.adGroupId, ConfigManager.getAdGroupId());
            request.setExtendParam(extend);
            request.setM(BusinessFieldName.M_LOG);
            //
            NetManager.post(request, new AbstractObserver<JMAResponse>() {
                @Override
                public void onNext(JMAResponse response, String encryptBody) {
                    LogUtils.e("Llv=" + response.getLlv());
                    ConfigManager.setLogLevelSwitch(response.getLlv());
                }
            });
        } catch (Exception ex) {
            LogUtils.e(ex.getMessage());
        }
    }


    /**
     * log_mobile_action
     * 日志过滤、中转
     *
     * @return 过滤、处理后日志，返回空代表舍弃
     */
    private static JLogExtBean filterLogMobileAction(JLogExtBean logExtBean) {
        if (logExtBean == null || TextUtils.isEmpty(logExtBean.getEvent())) {
            return logExtBean;
        }
        //舍弃
        if (TrackManager.blockCategoryOne.contains(logExtBean.getEvent())) {
            return null;
        }
        //等级调整
        if (TrackManager.logLevel0.contains(logExtBean.getEvent())) {
            logExtBean.setLoglevel(TrackManager.LOG_LEVEL_0);
        } else if (TrackManager.logLevel1.contains(logExtBean.getEvent())) {
            logExtBean.setLoglevel(TrackManager.LOG_LEVEL_1);
        } else if (TrackManager.logLevel4.contains(logExtBean.getEvent())) {
            logExtBean.setLoglevel(TrackManager.LOG_LEVEL_4);
        }
        //去除混淆字符
        logExtBean.setEvent(logExtBean.getEvent().replace(BuildConfig.APP_CODE + BuildConfig.VERSION_CODE, ""));
        //服务器控制
        if (logExtBean.getLoglevel() > ConfigManager.getLogLevelSwitch()) {
            LogUtils.e(GsonUtils.toJson(logExtBean));
            return null;
        }
        //
        return logExtBean;
    }
}
