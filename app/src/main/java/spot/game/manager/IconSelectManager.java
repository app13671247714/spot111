package spot.game.manager;

import android.text.TextUtils;

import net.goal.differences.R;

import java.util.Locale;

public class IconSelectManager {

    public static int getManyRewardIcon() {
        String country = ConfigManager.getCountry();
        if (country.equalsIgnoreCase("br")) {
            return R.mipmap.game_spot_icon_many_r_dollar;
        } else if (country.equalsIgnoreCase("id")) {
            return R.mipmap.game_spot_icon_many_rp;
        } else if (country.equalsIgnoreCase("pk")) {
            return R.mipmap.game_spot_icon_many_dollar;
        } else if (country.equalsIgnoreCase("ru")) {
            return R.mipmap.game_spot_icon_many_ru_p;
        } else {
            return R.mipmap.game_spot_icon_many_dollar;
        }
    }


    public static int getSingleRewardIcon() {
        String country = ConfigManager.getCountry();
        if (country.equalsIgnoreCase("br")) {
            return R.mipmap.game_spot_icon_r_dollar_singlel;
        } else if (country.equalsIgnoreCase("id")) {
            return R.mipmap.game_spot_icon_rp_single;
        } else if (country.equalsIgnoreCase("pk")) {
            return R.mipmap.game_spot_icon_dollar_single;
        } else if (country.equalsIgnoreCase("ru")) {
            return R.mipmap.game_spot_icon_ru_single;
        } else {
            return R.mipmap.game_spot_icon_dollar_single;
        }
    }

    public static int getChannelResId(String channel) {
        if (TextUtils.isEmpty(channel)) {
            return R.mipmap.game_spot_icon_fake_dana;
        }
        if (channel.equalsIgnoreCase("ovo")) {
            return R.mipmap.game_spot_icon_fake_ovo;
        } else if (channel.equalsIgnoreCase("shopeepay")) {
            return R.mipmap.game_spot_icon_fake_shopee;
        } else if (channel.equalsIgnoreCase("amazon")) {
            return R.mipmap.game_spot_icon_fake_amazon;
        } else if (channel.equalsIgnoreCase("Boleto Bancario") || channel.toLowerCase(Locale.ROOT).contains("boleto")) {
            return R.mipmap.game_spot_icon_fake_boleto;
        } else if (channel.equalsIgnoreCase("pix")) {
            return R.mipmap.game_spot_icon_fake_pix;
        } else if (channel.equalsIgnoreCase("dana")) {
            return R.mipmap.game_spot_icon_fake_dana;
        } else if (channel.equalsIgnoreCase("paypal")) {
            return R.mipmap.game_spot_icon_fake_paypal;
        } else if (channel.equalsIgnoreCase("pagbank")) {
            return R.mipmap.game_spot_icon_fake_pagbank;
        } else if (channel.equalsIgnoreCase("master") || channel.toLowerCase(Locale.ROOT).contains("mastercard")) {
            return R.mipmap.game_spot_icon_fake_master;
        } else if (channel.equalsIgnoreCase("qiwi")) {
            return R.mipmap.game_spot_icon_fake_qiwi;
        } else if (channel.equalsIgnoreCase("webmoney")) {
            return R.mipmap.game_spot_icon_fake_webmoney;
        } else if (channel.equalsIgnoreCase("yoomoney")) {
            return R.mipmap.game_spot_icon_fake_yoomoney;
        } else {
            return R.mipmap.game_spot_icon_fake_dana;
        }
    }

    /**
     * 假提现 切换背景
     *
     * @param channel
     * @return
     */
    public static int getFakeWithdrawBig(String channel) {
        if (TextUtils.isEmpty(channel)) {
            return R.mipmap.game_spot_icon_withdraw_channel_dana;
        }
        if (channel.equalsIgnoreCase("ovo")) {
            return R.mipmap.game_spot_icon_withdraw_channel_ovo;
        } else if (channel.equalsIgnoreCase("shopeepay")) {
            return R.mipmap.game_spot_icon_withdraw_channel_shappe;
        } else if (channel.equalsIgnoreCase("amazon")) {
            return R.mipmap.game_spot_icon_withdraw_channel_amazon;
        } else if (channel.equalsIgnoreCase("Boleto Bancario") || channel.toLowerCase(Locale.ROOT).contains("boleto")) {
            return R.mipmap.game_spot_icon_withdraw_channel_boleto;
        } else if (channel.equalsIgnoreCase("pix")) {
            return R.mipmap.game_spot_icon_withdraw_channel_pix;
        } else if (channel.equalsIgnoreCase("dana")) {
            return R.mipmap.game_spot_icon_withdraw_channel_dana;
        } else if (channel.equalsIgnoreCase("paypal")) {
            return R.mipmap.game_spot_icon_withdraw_channel_paypal;
        } else if (channel.equalsIgnoreCase("pagbank")) {
            return R.mipmap.game_spot_icon_withdraw_channel_paybank;
        } else if (channel.equalsIgnoreCase("master") || channel.toLowerCase(Locale.ROOT).contains("mastercard")) {
            return R.mipmap.game_spot_icon_withdraw_channel_master;
        } else if (channel.equalsIgnoreCase("qiwi")) {
            return R.mipmap.game_spot_icon_withdraw_channel_qiwi;
        } else if (channel.equalsIgnoreCase("webmoney")) {
            return R.mipmap.game_spot_icon_withdraw_channel_webmoney;
        } else if (channel.equalsIgnoreCase("yoomoney")) {
            return R.mipmap.game_spot_icon_withdraw_channel_yoomoney;
        } else {
            return R.mipmap.game_spot_icon_withdraw_channel_dana;
        }
    }

    public static int getChannelResIdNew(String bankName) {
        if (TextUtils.isEmpty(bankName)) {
            return R.mipmap.game_spot_ic_fake_dana_new;
        }
        if (bankName.equalsIgnoreCase("ovo")) {
            return R.mipmap.game_spot_ic_fake_ovo_new;
        } else if (bankName.equalsIgnoreCase("shopeepay")) {
            return R.mipmap.game_spot_ic__fake_shopee_new;
        } else if (bankName.equalsIgnoreCase("amazon")) {
            return R.mipmap.game_spot_ic_fake_amazon_new;
        } else if (bankName.equalsIgnoreCase("Boleto Bancario") || bankName.toLowerCase(Locale.ROOT).contains("boleto")) {
            return R.mipmap.game_spot_ic_fake_boleto_new;
        } else if (bankName.equalsIgnoreCase("pix")) {
            return R.mipmap.game_spot_ic_fake_pix_new;
        } else if (bankName.equalsIgnoreCase("dana")) {
            return R.mipmap.game_spot_ic_fake_dana_new;
        } else if (bankName.equalsIgnoreCase("paypal")) {
            return R.mipmap.game_spot_ic_fake_paypal_new;
        } else if (bankName.equalsIgnoreCase("pagbank")) {
            return R.mipmap.game_spot_ic_fake_pagbank_new;
        } else if (bankName.equalsIgnoreCase("master") || bankName.toLowerCase(Locale.ROOT).contains("mastercard")) {
            return R.mipmap.game_spot_ic_game_spot_fake_master_new;
        } else if (bankName.equalsIgnoreCase("qiwi")) {
            return R.mipmap.game_spot_ic_fake_qiwi_new;
        } else if (bankName.equalsIgnoreCase("webmoney")) {
            return R.mipmap.game_spot_ic_fake_webmoney_new;
        } else if (bankName.equalsIgnoreCase("yoomoney")) {
            return R.mipmap.game_spot_ic_fake_yomoney_new;
        } else {
            return R.mipmap.game_spot_ic_fake_dana_new;
        }
    }
}
