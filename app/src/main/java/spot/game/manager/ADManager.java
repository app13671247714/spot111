package spot.game.manager;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxAdWaterfallInfo;
import com.applovin.mediation.MaxError;
import com.applovin.mediation.MaxNetworkResponseInfo;
import com.applovin.mediation.MaxReward;
import com.applovin.mediation.MaxRewardedAdListener;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinSdkConfiguration;
import com.applovin.sdk.AppLovinSdkSettings;
import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SPStaticUtils;
import com.blankj.utilcode.util.ThreadUtils;
import com.blankj.utilcode.util.TimeUtils;

import net.goal.differences.BuildConfig;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import spot.game.ad.InterstitialAd;
import spot.game.ad.RewardAd;
import spot.game.ad.strategy.SimpleLoadStrategy;
import spot.game.constant.BusinessFieldName;
import spot.game.constant.ZeusConfig;
import spot.game.model.BILocation;
import spot.game.model.BIPlatform;
import spot.game.model.JLogExtBean;
import spot.game.model.TopOnPlacement;
import spot.game.model.callModel.AdSouceRequest;
import spot.game.model.callModel.BaseInfoRequest;
import spot.game.model.callModel.ReportRequest;
import spot.game.model.callModel.ReqestObject;
import spot.game.model.backModel.AdSouceResponse;
import spot.game.model.backModel.BaseInfoResponse;
import spot.game.model.backModel.SourceInfo;
import spot.game.net.AbstractObserver;
import spot.game.util.AesGcmUtil;
import spot.game.util.GameTaskUtils;

/**
 * 广告处理使用策略
 */
public class ADManager {
    //
    //protected static final String[] InterLocationCodeList = new String[]{BusinessFieldName.Interstitial};
    //protected static final String[] RewardLocationCodeList = new String[]{BusinessFieldName.REWARD};
    protected static final String[] LocationCodeList = new String[]{BusinessFieldName.REWARD, BusinessFieldName.Interstitial};
    //protected static final String[] HighPriceLocationCodeList = new String[]{BusinessFieldName.REWARD_2, BusinessFieldName.Interstitial_2};

    private ConcurrentHashMap<String, AtomicReference<RewardAd>> mRewardAdMap = new ConcurrentHashMap<>();//激励广告位对应广告自定义类
    private ConcurrentHashMap<String, AtomicReference<InterstitialAd>> mInterAdMap = new ConcurrentHashMap<>();//插屏广告位对应广告自定义类
    //
    protected static AtomicBoolean mSDKInit = new AtomicBoolean(false);
    private static BaseInfoResponse baseInfoResponse = null;
    protected static ConcurrentHashMap<String, TopOnPlacement> localADSourceInfo = new ConcurrentHashMap<>(8);
    protected static final int MSG_ADSOURCE = 0x100 + BuildConfig.VERSION_CODE;
    protected static final int MSG_VIDEO_PRELOAD = MSG_ADSOURCE + 1;
    protected static final int MSG_INTERSTITIAL_PRELOAD = MSG_VIDEO_PRELOAD + 1;
    static ADManager INSTANCE;

    public static ADManager INSTANCE() {
        if (INSTANCE == null) {
            synchronized (ADManager.class) {
                if (INSTANCE == null) {
                    INSTANCE = new ADManager();
                }
            }
        }
        return INSTANCE;
    }


    public void initBaseInfo() {
        BaseInfoRequest request = new BaseInfoRequest();
        request.setM(ZeusConfig.B_BASEINFO);
        //
        NetManager.post(request, new AbstractObserver<String>() {

            @Override
            public void onNext(String zeusResponse, String encryptBody) {
                initLocalBaseInfo(encryptBody);
                SPStaticUtils.put(BusinessFieldName.BASEINFO, encryptBody);
            }

            @Override
            public void onError(Throwable e) {
                String res = SPStaticUtils.getString(BusinessFieldName.BASEINFO, "");
                LogUtils.e("B_BASEINFO=" + res);
                initLocalBaseInfo(res);
            }
        });
    }


    protected Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            try {
                switch (msg.what) {
                    case MSG_ADSOURCE:
                        try {
                            List<BILocation> biLocations = (List<BILocation>) msg.obj;
                            initAdSouce(biLocations);
                        } catch (Throwable throwable) {

                        }
                        break;
                    case MSG_VIDEO_PRELOAD:
                        if (msg == null || msg.obj == null || !(msg.obj instanceof String)) {
                            break;
                        }
                        loadRewardVideo((String) msg.obj);
                        break;
                    case MSG_INTERSTITIAL_PRELOAD:
                        if (msg == null || msg.obj == null || !(msg.obj instanceof String)) {
                            break;
                        }
                        loadInterstitial((String) msg.obj);
                        break;
                }
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    };

    protected void initAdSouce(List<BILocation> biLocationList) {
        if (biLocationList == null || biLocationList.isEmpty()) {
            biLocationList = new ArrayList<>();
            //
            for (String LocationCode : LocationCodeList) {
                BILocation biLocation = new BILocation();
                biLocation.setAdLocationCode(LocationCode);
                biLocationList.add(biLocation);
            }
        }
        List<BILocation> finalBiLocationList = biLocationList;
        ThreadUtils.executeByCached(new ThreadUtils.SimpleTask<Object>() {
            @Override
            public Object doInBackground() throws Throwable {
                try {
                    for (BILocation biLocation : finalBiLocationList) {
                        String locationCode = biLocation.getAdLocationCode();
                        AdSouceRequest request = new AdSouceRequest();
                        request.setAdLocationCode(locationCode);
                        request.setM(ZeusConfig.S_ADSOUCE);
                        //
                        NetManager.post(request, new AbstractObserver<String>() {
                            @Override
                            public void onNext(String zeusResponse, String encryptBody) {
                                try {
                                    SPStaticUtils.put(locationCode, encryptBody);
                                    initLocalAdSouce(locationCode, encryptBody);
                                } catch (Throwable throwable) {

                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                try {
                                    String res = SPStaticUtils.getString(locationCode, "");
                                    initLocalAdSouce(locationCode, res);
                                } catch (Throwable throwable) {

                                }
                            }
                        });
                    }
                } catch (Throwable throwable) {

                }
                return null;
            }

            @Override
            public void onSuccess(Object result) {

            }
        });
    }


    public synchronized TopOnPlacement getTopOnPlacement(String code) {
        if (localADSourceInfo == null) {
            localADSourceInfo = new ConcurrentHashMap<>();
        }
        code = code == null ? "" : code;
        TopOnPlacement topOnPlacement = localADSourceInfo.get(code);
        if (topOnPlacement == null) {
            initLocalPlacementConfig();
            topOnPlacement = localADSourceInfo.get(code);
        }
        if (topOnPlacement == null) {
            TrackManager.sendTrack(TrackManager.ad_error, code, code + " TopOnPlacement = null");
        }
        return topOnPlacement;
    }


    public void showMaxVideoAd(ADShowListener listener) {
        //if (BuildConfig.PRODUCT_FLAVORS.equalsIgnoreCase("anythink")) {
        //    if (listener != null) {
        //        listener.onAdDisplayed();
        //        listener.onUserRewarded();
        //        listener.onAdHidden();
        //        return;
        //    }
        //}
        boolean showRtn = false;
        String code = getMaxEcpm(BusinessFieldName.REWARD, BusinessFieldName.Interstitial);
        if (!TextUtils.isEmpty(code)) {
            if (code.equals(BusinessFieldName.Interstitial)) {
                showRtn = showInterstitial(code, listener);
            } else {
                showRtn = showReward(code, listener);
            }
        } else {
            showRtn = showInterstitial(BusinessFieldName.Interstitial, listener);
        }
        //
        if (!showRtn) {
            if (listener != null) {
                listener.onAdDisplayFailed();
            }
        }
    }

    public void onDestroy() {
        try {
            mHandler.removeCallbacksAndMessages(null);
        } catch (Throwable throwable) {

        }
    }


    private void reportRewardAdShow() {
        //
        ConfigManager.setNowVideoCount(ConfigManager.getNowVideoCount() + 1);
        GameTaskUtils.setAdPlayComplete();
        //
        //GameManager.countAdShow();
    }


    public Map<String, Object> buildExtendParam(String event, String eventMsg, String sourceType, String sourceId, MaxAd maxAd) {
        return buildExtendParam(event, eventMsg, sourceType, sourceId, maxAd, 0);
    }

    protected Map<String, Object> buildExtendParam(String event, String eventMsg, String sourceType, String sourceId, MaxAd maxAd, long steptime) {
        Map<String, Object> commonMap = new HashMap<>();
        commonMap.put(BusinessFieldName.event, event);
        commonMap.put(BusinessFieldName.eventMsg, eventMsg);
        if (maxAd != null) {
            commonMap.put(BusinessFieldName.mediaPlatform, maxAd.getNetworkName()); //Applovin
            commonMap.put(BusinessFieldName.mediaCodeId, maxAd.getNetworkPlacement()); // inter_videoa
            commonMap.put(BusinessFieldName.networkId, maxAd.getAdUnitId()); // fd08ecc76e9427d1
            commonMap.put(BusinessFieldName.ecpm, maxAd.getRevenue() * 1000d); //
            commonMap.put(BusinessFieldName.currencyCode, maxAd.getRevenuePrecision());
        }
        commonMap.put(BusinessFieldName.sourceId, sourceId);
        commonMap.put(BusinessFieldName.platform, ZeusConfig.MAX);
        commonMap.put(BusinessFieldName.sourceType, sourceType);
        commonMap.put(BusinessFieldName.steptime, steptime);
        commonMap.put("ab_tags", ConfigManager.getABTags());
        commonMap.put("logTime", TimeUtils.getNowString(TimeUtils.getSafeDateFormat("yyyy-MM-dd HH:mm:ss.SSS")));
        commonMap.put(BusinessFieldName.mobileRegion, GameManager.getLanguage());
        return commonMap;
    }


    /**
     * @param data
     * @return
     */
    protected TopOnPlacement alyTopOnPlacement(final String data) {
        if (TextUtils.isEmpty(data)) {
            return null;
        }
        TopOnPlacement topOnPlacement = null;
        try {
            String res = AesGcmUtil.decrypt(data);
            LogUtils.e(res);
            if (!TextUtils.isEmpty(res)) {
                AdSouceResponse adSouceResponse = GsonUtils.fromJson(res, AdSouceResponse.class);
                List<SourceInfo> sourceInfoList = null;
                if (adSouceResponse != null) {
                    sourceInfoList = adSouceResponse.getSourceInfoList();
                }
                if (sourceInfoList != null && sourceInfoList.size() > 0) {
                    SourceInfo sourceInfo = sourceInfoList.get(0);
                    String placementID = sourceInfo.getCodeId();
                    String sourceType = sourceInfo.getSourceType();
                    String id = sourceInfo.getId();
                    if (!TextUtils.isEmpty(placementID)) {
                        topOnPlacement = new TopOnPlacement();
                        topOnPlacement.setPlacementId(placementID);
                        topOnPlacement.setSourceType(sourceType);
                        topOnPlacement.setId(id);
                        topOnPlacement.setTarget(adSouceResponse.getTarget());
                    }
                }
            }
        } catch (Throwable throwable) {
            TrackManager.sendTrack(TrackManager.ad_error, data, throwable.getMessage());
        }
        return topOnPlacement;
    }


    private void initLocalBaseInfo(String data) {
        if (TextUtils.isEmpty(data)) return;
        try {
            String baseInfo = AesGcmUtil.decrypt(data);
            LogUtils.d("baseinfo --> " + baseInfo);
            baseInfoResponse = GsonUtils.fromJson(baseInfo, BaseInfoResponse.class);
            List<BIPlatform> platformList = baseInfoResponse.getPlatformList();
            if (platformList != null && !platformList.isEmpty()) {
                ConfigManager.setMaxSDKKey(platformList.get(0).getAppKey());
            }
            List<BILocation> biLocations = baseInfoResponse.getLocationInfoList();
            mHandler.sendMessage(Message.obtain(mHandler, MSG_ADSOURCE, biLocations));
            if (!baseInfoResponse.getAppInfo().isEnAdjust()) {
                ConfigManager.setAdJustEnable(false);
            }
            ConfigManager.setHealthIntervalTime(baseInfoResponse.getHealthIntervalTime());
        } catch (Throwable throwable) {
            TrackManager.sendTrack(TrackManager.ad_error, "initLocalBaseInfo-->" + data, throwable.getMessage());
        }
    }


    public void init(Context context) {
        if (localADSourceInfo == null) {
            localADSourceInfo = new ConcurrentHashMap<>(8);
        }
        initLocalPlacementConfig();
    }


    /**
     * 广告展示加载、广告失败加载均已在封装类中处理，本监听无需处理
     */
    protected class MaxInterstitialListener implements MaxAdListener {
        protected boolean hasAdClick = false;
        protected boolean hasAdShow = false;
        protected TopOnPlacement topOnPlacement;
        ADShowListener mADShowListener;
        ADLoadListener mADLoadListener;
        protected long steptime;
        protected String locationCode;

        public void setADShowListener(ADShowListener mADShowListener) {
            this.mADShowListener = mADShowListener;
        }

        public void setADLoadListener(ADLoadListener mADLoadListener) {
            this.mADLoadListener = mADLoadListener;
        }

        public MaxInterstitialListener(TopOnPlacement topOnPlacement, String locationCode) {
            this.topOnPlacement = topOnPlacement;
            this.steptime = System.currentTimeMillis();
            this.locationCode = locationCode;
        }

        @Override
        public void onAdLoaded(MaxAd ad) {
            try {
                TrackManager.sendTrack(JLogExtBean.event(TrackManager.ad_request_success, topOnPlacement.getId()).setStrvalue(locationCode).setDublevalue(ad.getRevenue() * 1000));
                LogUtils.e("onAdLoaded --> MaxInterstitial --> " + ad.getRevenue() * 1000);
                ReqestObject object = new ReqestObject();
                object.setAdId(topOnPlacement.getId());
                object.setrType(ZeusConfig.RTYPE_SUCCESS);
                object.setCodeId(topOnPlacement.getPlacementId());
                object.setrMsg("Request Success");
                List<ReqestObject> objectList = new ArrayList<>();
                objectList.add(object);
                //
                ReportRequest request = new ReportRequest();
                request.setExtendParam(buildExtendParam(ZeusConfig.ad_request_success, GsonUtils.toJson(objectList), topOnPlacement.getSourceType(), topOnPlacement.getId(), ad));
                request.setTarget(topOnPlacement.getTarget());
                TrackManager.sendTrack(request);
                //
                if (mADLoadListener != null) {
                    mADLoadListener.onAdLoaded();
                }
            } catch (Throwable throwable) {
                TrackManager.sendTrack(TrackManager.ad_error, "Interstitial onAdLoaded", "" + throwable.getMessage());
            }
        }

        @Override
        public void onAdDisplayed(MaxAd ad) {
            try {
                if (ad != null) {
                    LogUtils.e("onAdDisplayed -----> ");
                    GameManager.activeRule(ad.getRevenue());
                    if (!hasAdShow) {
                        hasAdShow = true;
                        //
                        TrackManager.sendTrack(JLogExtBean.event(TrackManager.ad_show, ad.getNetworkName()).setStrvalue(locationCode)
                                .setDublevalue(ad.getRevenue() * 1000d));
                        //
                        ReportRequest request = new ReportRequest();
                        request.setExtendParam(buildExtendParam(ZeusConfig.ad_show, "ad_show", topOnPlacement.getSourceType(), topOnPlacement.getId(), ad));

                        request.setTarget(topOnPlacement.getTarget());
                        TrackManager.sendTrack(request);
                        reportRewardAdShow();
                    } else {
                        LogUtils.e("onAdDisplayed -----> null");
                    }
                } else {
                    TrackManager.sendTrack(TrackManager.ad_error, "Interstitial onAdDisplayed", "ad == null");
                }
                if (mADShowListener != null) {
                    mADShowListener.onAdDisplayed();
                    mADShowListener.onUserRewarded();
                }
            } catch (Throwable throwable) {
                TrackManager.sendTrack(TrackManager.ad_error, "Interstitial onAdDisplayed", "" + throwable.getMessage());
                LogUtils.e("onAdDisplayed -----> " + throwable.getMessage());
            }
        }

        @Override
        public void onAdHidden(MaxAd ad) {
            LogUtils.e("onAdHidden -----> " + String.valueOf(topOnPlacement.getTarget().get(ZeusConfig.locationCode)));
            if (mADShowListener != null) {
                mADShowListener.onAdHidden();
            }
        }

        @Override
        public void onAdClicked(MaxAd ad) {
            try {
                if (ad != null) {
                    if (!hasAdClick) {
                        hasAdClick = true;
                        TrackManager.sendTrack(JLogExtBean.event(TrackManager.ad_click, topOnPlacement.getId()));
                        ReportRequest request = new ReportRequest();
                        request.setExtendParam(buildExtendParam(ZeusConfig.ad_click, "ad_click", topOnPlacement.getSourceType(), topOnPlacement.getId(), ad));
                        //
                        request.setTarget(topOnPlacement.getTarget());
                        TrackManager.sendTrack(request);
                    }
                } else {
                    TrackManager.sendTrack(TrackManager.ad_error, "Interstitial onAdClicked", "ad == null");
                }
                if (mADShowListener != null) {
                    mADShowListener.onAdClicked();
                }
            } catch (Throwable throwable) {
                TrackManager.sendTrack(TrackManager.ad_error, "Interstitial onAdClicked", "" + throwable.getMessage());
            }
        }

        @Override
        public void onAdLoadFailed(String adUnitId, MaxError error) {
            try {
                TrackManager.sendTrack(JLogExtBean.event(TrackManager.ad_request_failed, topOnPlacement.getId() + "," + getMaxErrorMsg(error)).setStrvalue(locationCode));
                //
                ReqestObject object = new ReqestObject();
                object.setAdId(topOnPlacement.getId());
                object.setrType(ZeusConfig.RTYPE_FAIL);
                object.setCodeId(topOnPlacement.getPlacementId());
                object.setrMsg(getMaxErrorMsg(error));
                List<ReqestObject> objectList = new ArrayList<>();
                objectList.add(object);
                //
                ReportRequest request = new ReportRequest();
                request.setExtendParam(buildExtendParam(ZeusConfig.ad_request_success, GsonUtils.toJson(objectList), topOnPlacement.getSourceType(), topOnPlacement.getId(), null));
                //
                request.setTarget(topOnPlacement.getTarget());
                TrackManager.sendTrack(request);
                if (mADLoadListener != null) {
                    mADLoadListener.onAdLoadFailed();
                }
            } catch (Throwable throwable) {
                TrackManager.sendTrack(TrackManager.ad_error, "Interstitial onAdLoadFailed", "" + throwable.getMessage());
            }
        }

        @Override
        public void onAdDisplayFailed(MaxAd ad, MaxError error) {
            try {
                TrackManager.sendTrack(JLogExtBean.event(TrackManager.ad_error, topOnPlacement.getId() + "," + getMaxErrorMsg(error)));
                //
                ReqestObject object = new ReqestObject();
                object.setAdId(topOnPlacement.getId());
                object.setrType(ZeusConfig.RTYPE_FAIL);
                object.setCodeId(topOnPlacement.getPlacementId());
                object.setrMsg(getMaxErrorMsg(error));
                List<ReqestObject> objectList = new ArrayList<>();
                objectList.add(object);
                //
                ReportRequest request = new ReportRequest();
                request.setExtendParam(buildExtendParam(ZeusConfig.ad_error, GsonUtils.toJson(objectList), topOnPlacement.getSourceType(), topOnPlacement.getId(), ad));
                //
                request.setTarget(topOnPlacement.getTarget());
                TrackManager.sendTrack(request);
                if (mADShowListener != null) {
                    mADShowListener.onAdDisplayFailed();
                }
            } catch (Throwable throwable) {
                TrackManager.sendTrack(TrackManager.ad_error, "Interstitial onAdDisplayFailed", "" + throwable.getMessage());
            }
        }
    }

    protected String getMaxErrorMsg(MaxError error) {
        StringBuilder errorMsg = new StringBuilder("");
        try {
            errorMsg = new StringBuilder("Code=" + error.getCode()
                    + ", Message=" + error.getMessage()
                    + ", MediatedNetworkErrorCode=" + error.getMediatedNetworkErrorCode()
                    + ", MediatedNetworkErrorMessage=" + error.getMediatedNetworkErrorMessage()
                    //+ ", RequestLatencyMillis=" + error.getRequestLatencyMillis()
            );
            MaxAdWaterfallInfo waterfall = error.getWaterfall();
            if (waterfall == null) {
                return errorMsg.toString();
            }
            errorMsg.append(", waterfall_Name=").append(waterfall.getName());
            errorMsg.append(", waterfall_TestName=").append(waterfall.getTestName());
            errorMsg.append(", waterfall_LatencyMillis=").append(waterfall.getLatencyMillis());
            List<MaxNetworkResponseInfo> networkResponses = waterfall.getNetworkResponses();
            if (networkResponses == null || networkResponses.isEmpty()) {
                return errorMsg.toString();
            }
            for (MaxNetworkResponseInfo networkResponse : networkResponses) {
                errorMsg.append(", Network -> ").append(networkResponse.getMediatedNetwork())
                        .append("...latency: ").append(networkResponse.getLatencyMillis())
                        .append("...credentials: ").append(networkResponse.getCredentials())
                        .append(" milliseconds").append("...error: ").append(networkResponse.getError());
            }
        } catch (Throwable e) {
            e.printStackTrace();
            errorMsg.append(", e=").append(e.getMessage());
        }
        return errorMsg.toString();
    }


    /**
     * 广告展示加载、广告失败加载均已在封装类中处理，本监听无需处理
     */
    protected class MaxRewardVideoListener implements MaxRewardedAdListener {
        protected boolean hasAdClick = false;
        protected boolean hasAdShow = false;
        protected boolean hasReward = false;
        protected TopOnPlacement topOnPlacement;
        ADShowListener mADShowListener;
        ADLoadListener mADLoadListener;
        protected long steptime;
        protected String locationcode;

        public void setADShowListener(ADShowListener mADShowListener) {
            this.mADShowListener = mADShowListener;
        }

        public void setADLoadListener(ADLoadListener mADLoadListener) {
            this.mADLoadListener = mADLoadListener;
        }

        public MaxRewardVideoListener(TopOnPlacement topOnPlacement, String locationcode) {
            this.topOnPlacement = topOnPlacement;
            this.steptime = System.currentTimeMillis();
            this.locationcode = locationcode;
        }


        @Override
        public void onRewardedVideoStarted(MaxAd ad) {
        }

        @Override
        public void onRewardedVideoCompleted(MaxAd ad) {
        }

        @Override
        public void onUserRewarded(MaxAd ad, MaxReward reward) {
            try {
                if (ad != null) {
                    if (!hasReward) {
                        hasReward = true;
                        TrackManager.sendTrack(JLogExtBean.event(TrackManager.ad_reward, topOnPlacement.getId()));
                        //
                        ReportRequest request = new ReportRequest();
                        request.setExtendParam(buildExtendParam(ZeusConfig.ad_reward, "ad_reward", topOnPlacement.getSourceType(), topOnPlacement.getId(), ad, System.currentTimeMillis() - steptime));
                        //
                        request.setTarget(topOnPlacement.getTarget());
                        TrackManager.sendTrack(request);
                    }
                }
                if (mADShowListener != null) {
                    mADShowListener.onUserRewarded();
                }
            } catch (Throwable throwable) {
                TrackManager.sendTrack(TrackManager.ad_error, "Reward onUserRewarded", "" + throwable.getMessage());
            }
        }

        @Override
        public void onAdLoaded(MaxAd ad) {
            try {
                TrackManager.sendTrack(JLogExtBean.event(TrackManager.ad_request_success, topOnPlacement.getId()).setStrvalue(locationcode).setDublevalue(ad.getRevenue() * 1000));
                //
                ReqestObject object = new ReqestObject();
                object.setAdId(topOnPlacement.getId());
                object.setrType(ZeusConfig.RTYPE_SUCCESS);
                object.setCodeId(topOnPlacement.getPlacementId());
                object.setrMsg("Request Success");
                List<ReqestObject> objectList = new ArrayList<>();
                objectList.add(object);
                //
                ReportRequest request = new ReportRequest();
                request.setExtendParam(buildExtendParam(ZeusConfig.ad_request_success, GsonUtils.toJson(objectList), topOnPlacement.getSourceType(), topOnPlacement.getId(), ad, System.currentTimeMillis() - steptime));
                //
                request.setTarget(topOnPlacement.getTarget());
                TrackManager.sendTrack(request);
                if (mADLoadListener != null) {
                    mADLoadListener.onAdLoaded();
                }
            } catch (Throwable throwable) {
                TrackManager.sendTrack(TrackManager.ad_error, "Reward onAdLoaded", "" + throwable.getMessage());
            }
        }

        @Override
        public void onAdDisplayed(MaxAd ad) {
            try {
                if (ad != null) {
                    GameManager.activeRule(ad.getRevenue());
                    if (!hasAdShow) {
                        TrackManager.sendTrack(JLogExtBean.event(TrackManager.ad_show, ad.getNetworkName()).setStrvalue(locationcode)
                                .setDublevalue(ad.getRevenue() * 1000d));
                        //
                        hasAdShow = true;
                        ReportRequest request = new ReportRequest();
                        request.setExtendParam(buildExtendParam(ZeusConfig.ad_show, "ad_show", topOnPlacement.getSourceType(), topOnPlacement.getId(), ad, System.currentTimeMillis() - steptime));
                        //
                        request.setTarget(topOnPlacement.getTarget());
                        TrackManager.sendTrack(request);
                        reportRewardAdShow();
                    }
                    if (mADShowListener != null) {
                        mADShowListener.onAdDisplayed();
                    }
                } else {
                    TrackManager.sendTrack(TrackManager.ad_error, "Reward onAdDisplayed", "ad == null");
                }
            } catch (Throwable throwable) {
                TrackManager.sendTrack(TrackManager.ad_error, "Reward onAdDisplayed", "" + throwable.getMessage());

            }
        }

        @Override
        public void onAdHidden(MaxAd ad) {
            try {
                LogUtils.e("onAdHidden " + hasReward);
                if (mADShowListener != null) {
                    mADShowListener.onAdHidden();
                }
            } catch (Throwable throwable) {
                TrackManager.sendTrack(TrackManager.ad_error, "Reward onAdHidden", "" + throwable.getMessage());
            } finally {
            }
        }

        @Override
        public void onAdClicked(MaxAd ad) {
            try {
                if (ad != null) {
                    if (!hasAdClick) {
                        hasAdClick = true;
                        TrackManager.sendTrack(JLogExtBean.event(TrackManager.ad_click, topOnPlacement.getId()));
                        ReportRequest request = new ReportRequest();
                        request.setExtendParam(buildExtendParam(ZeusConfig.ad_click, "ad_click", topOnPlacement.getSourceType(), topOnPlacement.getId(), ad, System.currentTimeMillis() - steptime));
                        //
                        request.setTarget(topOnPlacement.getTarget());
                        TrackManager.sendTrack(request);
                    }
                } else {
                    TrackManager.sendTrack(TrackManager.ad_error, "Reward onAdClicked", "ad == null");
                }
                if (mADShowListener != null) {
                    mADShowListener.onAdClicked();
                }
            } catch (Throwable throwable) {
                TrackManager.sendTrack(TrackManager.ad_error, "Reward onAdClicked", "" + throwable.getMessage());
            }
        }

        @Override
        public void onAdLoadFailed(String adUnitId, MaxError error) {
            try {
                TrackManager.sendTrack(JLogExtBean.event(TrackManager.ad_request_failed, topOnPlacement.getId() + "," + getMaxErrorMsg(error)).setStrvalue(locationcode));
                ReqestObject object = new ReqestObject();
                object.setAdId(topOnPlacement.getId());
                object.setrType(ZeusConfig.RTYPE_FAIL);
                object.setCodeId(topOnPlacement.getPlacementId());
                object.setrMsg(getMaxErrorMsg(error));
                List<ReqestObject> list = new ArrayList<>();
                list.add(object);
                //
                ReportRequest request = new ReportRequest();
                request.setExtendParam(buildExtendParam(ZeusConfig.ad_request_success, GsonUtils.toJson(list), topOnPlacement.getSourceType(), topOnPlacement.getId(), null, System.currentTimeMillis() - steptime));
                //
                request.setTarget(topOnPlacement.getTarget());
                TrackManager.sendTrack(request);
                if (mADLoadListener != null) {
                    mADLoadListener.onAdLoadFailed();
                }
            } catch (Throwable throwable) {
                TrackManager.sendTrack(TrackManager.ad_error, "Reward onAdLoadFailed", "" + throwable.getMessage());
            }
        }

        @Override
        public void onAdDisplayFailed(MaxAd ad, MaxError error) {
            try {
                TrackManager.sendTrack(JLogExtBean.event(TrackManager.ad_error, topOnPlacement.getId() + "," + getMaxErrorMsg(error)));
                ReqestObject object = new ReqestObject();
                object.setAdId(topOnPlacement.getId());
                object.setrType(ZeusConfig.RTYPE_FAIL);
                object.setCodeId(topOnPlacement.getPlacementId());
                object.setrMsg(getMaxErrorMsg(error));
                List<ReqestObject> objectList = new ArrayList<>();
                objectList.add(object);
                //
                ReportRequest request = new ReportRequest();
                request.setExtendParam(buildExtendParam(ZeusConfig.ad_error, GsonUtils.toJson(objectList), topOnPlacement.getSourceType(), topOnPlacement.getId(), ad, System.currentTimeMillis() - steptime));
                //
                request.setTarget(topOnPlacement.getTarget());
                TrackManager.sendTrack(request);
                if (mADShowListener != null) {
                    mADShowListener.onAdDisplayFailed();
                }
            } catch (Throwable throwable) {
                TrackManager.sendTrack(TrackManager.ad_error, "Reward onAdDisplayFailed", "" + throwable.getMessage());
            }
        }

    }

    protected void loadInterstitial(String locationCode) {
        try {
            if (mInterAdMap == null) {
                mInterAdMap = new ConcurrentHashMap<>();
            }
            //
            TopOnPlacement topOnPlacement = getTopOnPlacement(locationCode);
            if (topOnPlacement == null) {
                TrackManager.sendTrack(TrackManager.ad_error, locationCode, "TopOnPlacement is empty");
                return;
            }
            //
            AtomicReference<InterstitialAd> interAdAtomicReference = null;
            if (mInterAdMap.containsKey(locationCode)) {
                interAdAtomicReference = mInterAdMap.get(locationCode);
            }
            //自定义广告变量为空则赋值
            if (interAdAtomicReference == null || interAdAtomicReference.get() == null) {
                interAdAtomicReference = new AtomicReference<>();
                InterstitialAd newValue = new InterstitialAd(locationCode, topOnPlacement);
                interAdAtomicReference.set(newValue);
            }
            //
            if (interAdAtomicReference.get() != null) {
                //策略为空则赋值
                if (topOnPlacement.getTypeStrategy() == 0) {//普通
                    //加载策略
                    if (interAdAtomicReference.get().getLoadStrategy() == null) {
                        SimpleLoadStrategy loadStrategy = new SimpleLoadStrategy(topOnPlacement);
                        interAdAtomicReference.get().setLoadStrategy(loadStrategy);
                    }
                }
                //更新mInterAdMap
                mInterAdMap.put(locationCode, interAdAtomicReference);
                //更新广告回调
                MaxInterstitialListener mMaxAdListener = new MaxInterstitialListener(topOnPlacement, locationCode);
                interAdAtomicReference.get().setMaxAdListener(mMaxAdListener);
                interAdAtomicReference.get().load();
            } else {
                TrackManager.sendTrack(TrackManager.ad_error, locationCode, "interAdAtomicReference error");
            }
        } catch (Throwable e) {
            TrackManager.sendTrack(TrackManager.ad_error, locationCode, "loadInterstitial_error=" + e.getMessage());
        }
    }

    /**
     * 自动比价
     *
     * @return
     */
    public void showInterstitial(ADShowListener listener) {
        boolean showRtn = showInterstitial(getMaxEcpm(BusinessFieldName.Interstitial), listener);
        if (!showRtn) {
            if (listener != null) {
                listener.onAdDisplayFailed();
            }
        }
    }

    private boolean showInterstitial(String locationCode, ADShowListener listener) {
        boolean result = false;
        try {
            TopOnPlacement topOnPlacement = getTopOnPlacement(locationCode);
            if (topOnPlacement == null) {
                return false;
            }
            if (mInterAdMap == null) {
                mInterAdMap = new ConcurrentHashMap<>();
            }
            int isAdReady = 1;
            try {
                isAdReady = mInterAdMap.get(locationCode).get().getMaxInterstitialAd().isReady() ? 0 : 1;
            } catch (Throwable e) {
            }
            TrackManager.sendTrack(JLogExtBean.event(TrackManager.ad_user_show, locationCode).setStrvalue(locationCode).setIntvalue(isAdReady));

            ReportRequest request = new ReportRequest();
            request.setExtendParam(buildExtendParam(ZeusConfig.ad_user_show, "ad_user_show", topOnPlacement.getSourceType(), topOnPlacement.getId(), null));
            request.setTarget(topOnPlacement.getTarget());
            TrackManager.sendTrack(request);
            if (mInterAdMap.isEmpty() || !mInterAdMap.containsKey(locationCode)) {
                TrackManager.sendTrack(TrackManager.ad_error, "showInterstitial", locationCode + ", mInterAdMap.isEmpty");
                return false;
            }
            AtomicReference<InterstitialAd> interAdAtomicReference = mInterAdMap.get(locationCode);
            if (interAdAtomicReference != null && interAdAtomicReference.get() != null) {
                MaxInterstitialListener maxAdListener = new MaxInterstitialListener(topOnPlacement, locationCode);
                maxAdListener.setADShowListener(listener);
                result = interAdAtomicReference.get().show(maxAdListener);
            } else {
                TrackManager.sendTrack(TrackManager.ad_error, "showInterstitial", locationCode + ", no cache");
            }
        } catch (Throwable e) {
            TrackManager.sendTrack(TrackManager.ad_error, "showInterstitial", "" + e.getMessage());
        }
        return result;
    }

    private String getMaxEcpm(String... locationCodeList) {
        String maxLocationCode = BusinessFieldName.Interstitial;
        if (locationCodeList == null || locationCodeList.length < 1) {
            TrackManager.sendTrack(TrackManager.ad_pkwin, maxLocationCode, "locationCodeList == null");
            return maxLocationCode;
        }
        StringBuilder msg = new StringBuilder();
        double maxECPM = 0;
        //
        if (mInterAdMap == null) {
            mInterAdMap = new ConcurrentHashMap<>();
        }
        if (mRewardAdMap == null) {
            mRewardAdMap = new ConcurrentHashMap<>();
        }
        //
        for (String locationCode : locationCodeList) {
            if (TextUtils.isEmpty(locationCode)) {
                continue;
            }
            try {
                MaxAd maxAd = null;
                if (locationCode.toLowerCase(Locale.ROOT).contains("inter")) {
                    AtomicReference<InterstitialAd> interAdAtomicReference = mInterAdMap.get(locationCode);
                    if (interAdAtomicReference == null || interAdAtomicReference.get() == null || interAdAtomicReference.get().getMaxInterstitialAd() == null) {
                        msg.append(locationCode + ",Reference == null,");
                        mHandler.sendMessage(mHandler.obtainMessage(MSG_INTERSTITIAL_PRELOAD, locationCode));
                        continue;
                    }
                    if (!interAdAtomicReference.get().couldShow()) {
                        msg.append(locationCode + "forbidden,");
                        continue;
                    }
                    maxAd = interAdAtomicReference.get().getLoadMaxAd();
                    if (!interAdAtomicReference.get().getMaxInterstitialAd().isReady() || maxAd == null) {
                        msg.append(locationCode + ",ecpm=NoCache,");
                        mHandler.sendMessage(mHandler.obtainMessage(MSG_INTERSTITIAL_PRELOAD, locationCode));
                        continue;
                    }
                } else {
                    AtomicReference<RewardAd> rewardAdAtomicReference = mRewardAdMap.get(locationCode);
                    if (rewardAdAtomicReference == null || rewardAdAtomicReference.get() == null || rewardAdAtomicReference.get().getMaxRewardedAd() == null) {
                        msg.append(locationCode + ",Reference == null,");
                        mHandler.sendMessage(mHandler.obtainMessage(MSG_VIDEO_PRELOAD, locationCode));
                        continue;
                    }
                    if (!rewardAdAtomicReference.get().couldShow()) {
                        msg.append(locationCode + "forbidden,");
                        continue;
                    }
                    maxAd = rewardAdAtomicReference.get().getLoadMaxAd();
                    if (!rewardAdAtomicReference.get().getMaxRewardedAd().isReady() || maxAd == null) {
                        msg.append(locationCode + ",ecpm=NoCache,");
                        mHandler.sendMessage(mHandler.obtainMessage(MSG_VIDEO_PRELOAD, locationCode));
                        continue;
                    }
                }
                double locationCodeEcpm = maxAd.getRevenue();
                //
                if (locationCodeEcpm >= maxECPM) {
                    maxECPM = locationCodeEcpm;
                    maxLocationCode = locationCode;
                }
                msg.append(locationCode + ",adUnitId=" + maxAd.getAdUnitId() + ",ecpm=" + (locationCodeEcpm * 1000d) + ",");
            } catch (Throwable e) {
                msg.append(locationCode + ",err=" + e.getMessage() + ", ");
            }
        }
        //
        TrackManager.sendTrack(JLogExtBean.event(TrackManager.ad_pkwin, msg.toString())
                .setStrvalue(maxLocationCode).setDublevalue(maxECPM));

        return maxLocationCode;
    }


    /**
     * 自动比价
     *
     * @return
     */
    public void showReward(ADShowListener listener) {
        boolean showReward = showReward(getMaxEcpm(BusinessFieldName.REWARD), listener);
        if (!showReward) {
            if (listener != null) {
                listener.onAdDisplayFailed();
            }
        }
    }

    private boolean showReward(String locationCode, ADShowListener listener) {
        boolean result = false;
        try {
            TopOnPlacement topOnPlacement = getTopOnPlacement(locationCode);
            if (topOnPlacement == null) {
                return false;
            }
            if (mRewardAdMap == null) {
                mRewardAdMap = new ConcurrentHashMap<>();
            }
            int isAdReady = 1;
            try {
                isAdReady = mRewardAdMap.get(locationCode).get().getMaxRewardedAd().isReady() ? 0 : 1;
            } catch (Throwable e) {
            }
            TrackManager.sendTrack(JLogExtBean.event(TrackManager.ad_user_show, locationCode).setStrvalue(locationCode).setIntvalue(isAdReady));

            ReportRequest request = new ReportRequest();
            request.setExtendParam(buildExtendParam(ZeusConfig.ad_user_show, "ad_user_show", topOnPlacement.getSourceType(), topOnPlacement.getId(), null));
            request.setTarget(topOnPlacement.getTarget());
            TrackManager.sendTrack(request);
            if (mRewardAdMap.isEmpty() || !mRewardAdMap.containsKey(locationCode)) {
                TrackManager.sendTrack(TrackManager.ad_error, "showReward", locationCode + ", mRewardAdMap.isEmpty");
                return false;
            }
            AtomicReference<RewardAd> rewardAdAtomicReference = mRewardAdMap.get(locationCode);
            if (rewardAdAtomicReference != null && rewardAdAtomicReference.get() != null) {
                MaxRewardVideoListener maxAdListener = new MaxRewardVideoListener(topOnPlacement, locationCode);
                maxAdListener.setADShowListener(listener);
                result = rewardAdAtomicReference.get().show(maxAdListener);
            } else {
                TrackManager.sendTrack(TrackManager.ad_error, "showReward", locationCode + ", no cache");
            }
        } catch (Throwable e) {
            TrackManager.sendTrack(TrackManager.ad_error, "showReward", "" + e.getMessage());
        }
        return result;
    }

    protected void loadRewardVideo(String locationCode) {
        try {
            if (mRewardAdMap == null) {
                mRewardAdMap = new ConcurrentHashMap<>();
            }
            //
            TopOnPlacement topOnPlacement = getTopOnPlacement(locationCode);
            if (topOnPlacement == null) {
                TrackManager.sendTrack(TrackManager.ad_error, locationCode, "TopOnPlacement is empty");
                return;
            }
            //
            AtomicReference<RewardAd> rewardAdAtomicReference = null;
            if (mRewardAdMap.containsKey(locationCode)) {
                rewardAdAtomicReference = mRewardAdMap.get(locationCode);
            }
            //自定义广告变量为空则赋值
            if (rewardAdAtomicReference == null || rewardAdAtomicReference.get() == null) {
                rewardAdAtomicReference = new AtomicReference<>();
                RewardAd newValue = new RewardAd(locationCode, topOnPlacement);
                rewardAdAtomicReference.set(newValue);
            }
            //
            if (rewardAdAtomicReference != null && rewardAdAtomicReference.get() != null) {
                //策略为空则赋值
                if (topOnPlacement.getTypeStrategy() == 0) {//普通
                    //加载策略
                    if (rewardAdAtomicReference.get().getLoadStrategy() == null) {
                        SimpleLoadStrategy loadStrategy = new SimpleLoadStrategy(topOnPlacement);
                        rewardAdAtomicReference.get().setLoadStrategy(loadStrategy);
                    }
                }
                //更新广告回调
                rewardAdAtomicReference.get().setMaxRewardedAdListener(new MaxRewardVideoListener(topOnPlacement, locationCode));
                rewardAdAtomicReference.get().load();
                //更新mRewardAdMap
                mRewardAdMap.put(locationCode, rewardAdAtomicReference);
            } else {
                TrackManager.sendTrack(TrackManager.ad_error, locationCode, "loadRewardVideo error");
            }
        } catch (Throwable e) {
            TrackManager.sendTrack(TrackManager.ad_error, locationCode, "loadRewardVideo=" + e.getMessage());
        }
    }


    protected void initLocalPlacementConfig() {
        if (localADSourceInfo == null) {
            localADSourceInfo = new ConcurrentHashMap<>();
        }
        //普通广告位
        String interstitialMid = BuildConfig.AD_OBTIVE_VIDEO_MID;
        String rewardMid = BuildConfig.AD_REWARDVIDEO_1_MID;
        TopOnPlacement topOnPlacement = alyTopOnPlacement(interstitialMid);
        if (topOnPlacement != null) {
            LogUtils.e(topOnPlacement.getPlacementId());
            localADSourceInfo.put(BusinessFieldName.Interstitial, topOnPlacement);
        }
        topOnPlacement = alyTopOnPlacement(rewardMid);
        if (topOnPlacement != null) {
            LogUtils.e(topOnPlacement.getPlacementId());
            localADSourceInfo.put(BusinessFieldName.REWARD, topOnPlacement);
        }
        //
        interstitialMid = SPStaticUtils.getString(BusinessFieldName.Interstitial, "");
        rewardMid = SPStaticUtils.getString(BusinessFieldName.REWARD, "");
        if (TextUtils.isEmpty(interstitialMid)) {
            initBaseInfo();
        } else {
            initLocalAdSouce(BusinessFieldName.Interstitial, interstitialMid);
            initLocalAdSouce(BusinessFieldName.REWARD, rewardMid);
        }
    }

    protected void initLocalAdSouce(String code, String data) {
        if (localADSourceInfo == null) {
            localADSourceInfo = new ConcurrentHashMap<>();
        }
        String mid = data == null ? "" : data;
        LogUtils.e("code=" + code + ", data=" + data);
        TopOnPlacement topOnPlacement = alyTopOnPlacement(mid);
        if (topOnPlacement != null) {
            localADSourceInfo.put(code, topOnPlacement);
        }
    }

    public void initTopOnSDK(String appId, String appKey) {
        if (mSDKInit == null) {
            mSDKInit = new AtomicBoolean(false);
        }
        if (mSDKInit.get()) {
            return;
        }
        TrackManager.sendTrack(TrackManager.ad_sdkinit, "adsdkinit_req", "appId=" + appId + ",appKey=" + appKey);
        //初始化成功后 不初始化第二次
        LogUtils.e("Init SDK start ");
        AppLovinSdkSettings appLovinSdkSettings = new AppLovinSdkSettings(ContextManager.getContext());
        appLovinSdkSettings.setMuted(false);
        appLovinSdkSettings.setVerboseLogging(true);
        appLovinSdkSettings.setExceptionHandlerEnabled(true);
        appLovinSdkSettings.setLocationCollectionEnabled(true);
        AppLovinSdk.getInstance(appKey, appLovinSdkSettings, ContextManager.getContext());
        AppLovinSdk.getInstance(ContextManager.getContext()).setMediationProvider("max");
        AppLovinSdk.initializeSdk(ContextManager.getContext(), new AppLovinSdk.SdkInitializationListener() {
            @Override
            public void onSdkInitialized(final AppLovinSdkConfiguration configuration) {
                // AppLovin SDK is initialized, start loading ads
                mSDKInit.set(true);
                TrackManager.sendTrack(TrackManager.ad_sdkinit, "adsdkinit_success", "appId=" + appId + ",appKey=" + appKey);
                loadAd();
            }
        });
    }

    public void loadAd() {
        mHandler.sendMessage(mHandler.obtainMessage(MSG_VIDEO_PRELOAD, BusinessFieldName.REWARD));
        mHandler.sendMessage(mHandler.obtainMessage(MSG_INTERSTITIAL_PRELOAD, BusinessFieldName.Interstitial));
    }


    public interface ADShowListener {
        default void onUserRewarded() {
        }

        default void onAdDisplayed() {
        }

        default void onAdHidden() {
        }

        default void onAdClicked() {
        }

        default void onAdDisplayFailed() {
        }
    }

    public interface ADLoadListener {
        default void onAdLoaded() {
        }

        default void onAdLoadFailed() {
        }
    }

}
