package spot.game.manager;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class BalanceManager {

    /**
     * 获取1 至 1.5之间随机数
     *
     * @return
     */
    public static String getRandomUSD() {
        double usd = Math.random() + 1;
        if (usd > 1.5d) {
            usd = usd - 0.5;
        }
        return String.format("%.2f", usd).replace(",", ".");
    }

    public static String getFormatBalance(double dollars) {
        String balance;
        String country = ConfigManager.getCountry();
        if (dollars >= 0 && (country.equalsIgnoreCase("id")
                || country.equalsIgnoreCase("ru"))) {
            balance = addComma("#,###", (int) dollars);
        } else {
            balance = addComma("#,###.##", dollars);
        }
        return balance;
    }


    public static String getFormatBalance() {
        double dollars = ConfigManager.getBalance();
        return getFormatBalance(dollars);
    }

    /**
     * 给数字每三位加一个逗号的处理
     */
    public static String addComma(String pattern, double balance) {
        DecimalFormat myFormat = new DecimalFormat();
        DecimalFormatSymbols symbols = new DecimalFormatSymbols(new Locale("en"));
        myFormat.setDecimalFormatSymbols(symbols);
        myFormat.applyPattern(pattern);
        return myFormat.format(balance);
    }
}
