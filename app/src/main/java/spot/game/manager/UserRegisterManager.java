package spot.game.manager;

import net.goal.differences.BuildConfig;

import java.util.concurrent.atomic.AtomicInteger;

public class UserRegisterManager {
    //流程类型
    public static final int TYPE_PROCESS_UNKNOWN = BuildConfig.VERSION_CODE;
    public static final int TYPE_PROCESS_START = TYPE_PROCESS_UNKNOWN + 1;
    public static final int TYPE_PROCESS_REGISTER = TYPE_PROCESS_START + 1;
    public static final int TYPE_PROCESS_LOAD = TYPE_PROCESS_REGISTER + 1;
    public static final AtomicInteger processType = new AtomicInteger(TYPE_PROCESS_UNKNOWN);//流程标识
}
