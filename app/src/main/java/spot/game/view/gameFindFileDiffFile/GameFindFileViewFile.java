package spot.game.view.gameFindFileDiffFile;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * 嵌套区域基类
 */
public class GameFindFileViewFile extends RelativeLayout {

    protected Context mContext;
    protected List<GameFindFileViewFile> childViewList;//嵌套子view
    protected LayoutParams mLayoutParams;//预期配置，用于边缘绘制
    protected TouchDrawCallback mTouchDrawCallback;//触摸回调


    private void setParam(LayoutParams layoutParams) {
        mLayoutParams = layoutParams;
    }

    protected void initContext(Context context) {
        mContext = context;
        childViewList = new ArrayList<>();
    }

    public GameFindFileViewFile(Context context) {
        super(context);
        initContext(context);
    }

    public GameFindFileViewFile(Context context, AttributeSet attrs) {
        super(context, attrs);
        initContext(context);
    }

    public GameFindFileViewFile(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initContext(context);
    }

    public GameFindFileViewFile(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initContext(context);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (!isEnabled()) {
            return true;
        } else {
            return super.dispatchTouchEvent(ev);
        }
    }


    public void setTouchDrawCallback(TouchDrawCallback touchDrawCallback) {
        mTouchDrawCallback = touchDrawCallback;
    }


    public interface TouchDrawCallback {

        /**
         * 触摸回调
         *
         * @param event
         * @param diffZoneView 当前view
         */
        void touchEvent(MotionEvent event, GameFindFileViewFile diffZoneView);
    }


    /**
     * 增加子view
     *
     * @param diffZoneView
     * @param layoutParams
     */
    public void addFindDiffZoneView(GameFindFileViewFile diffZoneView, LayoutParams layoutParams) {
        if (diffZoneView == null || layoutParams == null) {
            return;
        }
        diffZoneView.setParam(layoutParams);
        if (childViewList == null) {
            childViewList = new ArrayList<>();
        }
        childViewList.add(diffZoneView);
    }

    public void touchDownEvent(MotionEvent event) {

    }

    /**
     * 增加子view
     *
     * @param diffZoneView
     */
    public void addFindDiffZoneView(GameFindFileViewFile diffZoneView) {
        if (diffZoneView == null) {
            return;
        }
        addView(diffZoneView);
        if (childViewList == null) {
            childViewList = new ArrayList<>();
        }
        childViewList.add(diffZoneView);
    }

    /**
     * 更新子布局位置
     *
     * @param layoutParams
     */
    public void updateChildParams(List<LayoutParams> layoutParams) {
        if (childViewList == null) {
            childViewList = new ArrayList<>();
        }
        if (layoutParams == null || layoutParams.isEmpty() || childViewList.isEmpty()) {
            return;
        }
        int minLength = Math.min(childViewList.size(), layoutParams.size());
        for (int i = 0; i < minLength; i++) {
            childViewList.get(i).setParam(layoutParams.get(i));
            childViewList.get(i).restart();
        }
    }

    public List<GameFindFileViewFile> getChildFindDiffZoneView() {
        if (childViewList == null) {
            childViewList = new ArrayList<>();
        }
        return childViewList;
    }

    /**
     * 设置所有答案不可用
     */
    public void setAllChildEnableFalse() {
        if (childViewList == null || childViewList.size() == 0) {
            return;
        }
        for (GameFindFileViewFile findZoneView : childViewList) {
            findZoneView.setEnabled(false);
        }
    }

    public void removeChildFindDiffZoneView(GameFindFileViewFile diffZoneView) {
        if (diffZoneView == null) {
            return;
        }
        if (childViewList == null) {
            childViewList = new ArrayList<>();
        }
        childViewList.remove(diffZoneView);
        removeView(diffZoneView);
    }

    public void removeChildFindDiffZoneView(int index) {
        if (childViewList == null) {
            childViewList = new ArrayList<>();
        }
        if (index < 0 || index >= childViewList.size()) {
            return;
        }
        removeView(childViewList.get(index));
        childViewList.remove(index);
    }

    public void removeAllChildFindDiffZoneView() {
        if (childViewList == null || childViewList.isEmpty()) {
            return;
        }
        for (int i = childViewList.size() - 1; i > -1; i--) {
            removeChildFindDiffZoneView(i);
        }
    }

    /**
     * 重置布局与子布局
     */
    public void restart() {
        if (mLayoutParams != null) {
            setLayoutParams(mLayoutParams);
        }
    }


    /**
     * 获取不变形的bitmap
     *
     * @param context
     * @param vectorDrawableId
     * @return
     */
    protected Bitmap getBitmap(Context context, int vectorDrawableId) {
        if (context == null || vectorDrawableId < 0) {
            return null;
        }
        Bitmap bitmap = null;
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            Drawable vectorDrawable = context.getDrawable(vectorDrawableId);
            bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(),
                    vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            vectorDrawable.draw(canvas);
        } else {
            bitmap = BitmapFactory.decodeResource(context.getResources(), vectorDrawableId);
        }
        return bitmap;
    }
}
