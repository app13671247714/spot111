package spot.game.view.gameFindFileDiffFile;

import android.animation.Animator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.airbnb.lottie.LottieAnimationView;

import net.goal.differences.R;

/**
 * 区域点击正确效果
 */
public class GameFindFileCorrectViewFile extends GameFindFileViewFile {
    static  final String CORRECT_ANIMATE_PATH = "spot_gola_find_diff_correct.json";
    protected Paint correctPaint;
    protected MotionEvent mMotionEvent;
    //
    public boolean isTouched , isTouched2;
    LottieAnimationView mLottieAnimationView;

    public GameFindFileCorrectViewFile(Context context) {
        super(context);
    }

    public GameFindFileCorrectViewFile(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GameFindFileCorrectViewFile(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public GameFindFileCorrectViewFile(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public void restart() {
        super.restart();
        isTouched = false;
        isTouched2 = false;
        mLottieAnimationView.setAnimation(CORRECT_ANIMATE_PATH);//在assets目录下的动画json文件名。
        mLottieAnimationView.setVisibility(GONE);
        mMotionEvent = null;
        invalidate();
    }

    @Override
    protected void initContext(Context context) {
        super.initContext(context);
        initPaint();
        isTouched = false;
        isTouched2 = false;
        if (mLottieAnimationView == null) {
            mLottieAnimationView = new LottieAnimationView(mContext);
            mLottieAnimationView.setAnimation(CORRECT_ANIMATE_PATH);//在assets目录下的动画json文件名。
        }
        if (mLottieAnimationView.getParent() == null) {
            addView(mLottieAnimationView);
        }
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //Log.e("TAG", "onTouchEvent: "+ event.getRawX()+", "+event.getRawY());
        if (isTouched) {
            return true;
        }
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mMotionEvent = event;
                touchDownEvent(event);
                if (mTouchDrawCallback != null) {
                    mTouchDrawCallback.touchEvent(event, this);
                }
                return true;
        }
        return super.onTouchEvent(event);
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        if (isTouched && isTouched2) {//onResume清空重绘
            isTouched2 = false;
            touchDownDraw(canvas, mMotionEvent);
            return;
        }
        if (mMotionEvent != null) {
            switch (mMotionEvent.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    touchDownDraw(canvas, mMotionEvent);
                    return;
            }
        }
    }

    public void touchDownEvent(MotionEvent event) {
        invalidate();
        isTouched = true;
        isTouched2 = true;
    }


    public void touchDownDraw(Canvas canvas, MotionEvent event) {
        mLottieAnimationView.setVisibility(VISIBLE);
        mLottieAnimationView.addAnimatorListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                mLottieAnimationView.setProgress(1f);
                Bitmap mBitmap = getBitmap(mContext, R.mipmap.game_spot_icon_find_diff_correct_zc);
                mLottieAnimationView.setImageBitmap(mBitmap);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
        mLottieAnimationView.playAnimation();
    }

    private void initPaint() {
        if (correctPaint == null) {
            correctPaint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.DITHER_FLAG);
        }
    }

    @Override
    protected Bitmap getBitmap(Context context, int vectorDrawableId) {
        Bitmap mBitmap = super.getBitmap(context, vectorDrawableId);
        //放缩填充
        if (mLayoutParams != null) {//边缘不变形
            mBitmap = Bitmap.createScaledBitmap(mBitmap, mLayoutParams.width, mLayoutParams.height, true);
        } else {
            mBitmap = Bitmap.createScaledBitmap(mBitmap, getMeasuredWidth(), getMeasuredHeight(), true);
        }
        return mBitmap;
    }
}
