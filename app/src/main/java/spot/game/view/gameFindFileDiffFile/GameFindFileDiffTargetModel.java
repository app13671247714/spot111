package spot.game.view.gameFindFileDiffFile;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * 正确区域
 */
public class GameFindFileDiffTargetModel {
    @SerializedName("spot_gola_file_a")
    String file_name_A;
    @SerializedName("spot_gola_file_b")
    String file_name_B;
    @SerializedName("spot_gola_file_width")
    int file_png_width;
    @SerializedName("spot_gola_file_height")
    int file_png_height;
    @SerializedName("spot_gola_file_target_location")
    List<FileTargetLocation> target_location;


    public int getFile_png_width() {
        return file_png_width;
    }

    public void setFile_png_width(int file_png_width) {
        this.file_png_width = file_png_width;
    }

    public int getFile_png_height() {
        return file_png_height;
    }

    public void setFile_png_height(int file_png_height) {
        this.file_png_height = file_png_height;
    }

    public List<FileTargetLocation> getTarget_location() {
        return target_location;
    }

    public void setTarget_location(List<FileTargetLocation> target_location) {
        this.target_location = target_location;
    }

    public String getFile_name_A() {
        return file_name_A;
    }

    public void setFile_name_A(String file_name_A) {
        this.file_name_A = file_name_A;
    }

    public String getFile_name_B() {
        return file_name_B;
    }

    public void setFile_name_B(String file_name_B) {
        this.file_name_B = file_name_B;
    }

    public class FileTargetLocation {
        @SerializedName("file_center_x")
        int file_center_x;
        @SerializedName("file_center_y")
        int file_center_y;
        @SerializedName("file_center_radius")
        int file_center_radius;

        public int getFile_center_x() {
            return file_center_x;
        }

        public void setFile_center_x(int file_center_x) {
            this.file_center_x = file_center_x;
        }

        public int getFile_center_y() {
            return file_center_y;
        }

        public void setFile_center_y(int file_center_y) {
            this.file_center_y = file_center_y;
        }

        public int getFile_center_radius() {
            return file_center_radius;
        }

        public void setFile_center_radius(int file_center_radius) {
            this.file_center_radius = file_center_radius;
        }
    }
}
