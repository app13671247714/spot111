package spot.game.view.gameFindFileDiffFile;

/**
 * 点击坐标与记录时间戳
 */
public class GameCountDownTimePointModel {
    float x;
    float y;
    long startMills;//点击记录时间戳


    public long getStartMills() {
        return startMills;
    }

    public void setStartMills(long startMills) {
        this.startMills = startMills;
    }


    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public void set(float x, float y, long startMills) {
        this.x = x;
        this.y = y;
        this.startMills = startMills;
    }
}
