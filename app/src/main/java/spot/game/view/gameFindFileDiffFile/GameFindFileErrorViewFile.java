package spot.game.view.gameFindFileDiffFile;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.SizeUtils;

import net.goal.differences.R;

import java.util.ArrayList;
import java.util.List;


/**
 * 区域点击错误效果
 */
public class GameFindFileErrorViewFile extends GameFindFileViewFile {
    protected final int MESSAGE_REFRESH_VIEW = 0xff000010;//刷新msg标志
    //
    protected List<GameCountDownTimePointModel> clickPointList;//历史点击
    protected Paint historyPaint;
    //
    public long millisInFuture = 3 * 1_000;//消失倒计时
    public int millisRefresh = 300;//刷新间隔

    Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MESSAGE_REFRESH_VIEW:
                    invalidate();
                    //
                    if (clickPointList != null && !clickPointList.isEmpty()) {
                        mHandler.sendMessageDelayed(mHandler.obtainMessage(MESSAGE_REFRESH_VIEW), millisRefresh);
                    }
                    break;
            }
        }
    };

    public GameFindFileErrorViewFile(Context context) {
        super(context);
    }

    public GameFindFileErrorViewFile(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GameFindFileErrorViewFile(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public GameFindFileErrorViewFile(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void initContext(Context context) {
        super.initContext(context);
        initPaint();
        clickPointList = new ArrayList<>();
    }

    @Override
    public void restart() {
        super.restart();
        if (clickPointList == null) {
            clickPointList = new ArrayList<>();
        }
        clickPointList.clear();
        mHandler.removeMessages(MESSAGE_REFRESH_VIEW);
        invalidate();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //Log.e("TAG", "FindZoneErrorView: "+ event.getRawX()+", "+event.getRawY());
        boolean onTouchEvent = super.onTouchEvent(event);
        if (!isDraw) {
            postDelayed(new Runnable() {
                @Override
                public void run() {
                    isDraw = true;
                }
            }, 800);
            return onTouchEvent;
        }
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                touchDownEvent(event);
                if (mTouchDrawCallback != null) {
                    mTouchDrawCallback.touchEvent(event, this);
                }
                return onTouchEvent;
        }
        return onTouchEvent;
    }


    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        drawHistory(canvas, clickPointList);
    }

    private boolean isDraw = true;

    public void setEnableFalse() {
        isDraw = false;
    }

    protected void drawHistory(Canvas canvas, List<GameCountDownTimePointModel> clickPointList) {
        if (canvas == null || clickPointList == null || clickPointList.isEmpty()) {
            return;
        }
        for (int i = clickPointList.size() - 1; i > -1; i--) {
            GameCountDownTimePointModel downTimePoint = clickPointList.get(i);
            if (downTimePoint == null) {
                clickPointList.remove(i);
                continue;
            }
            if (millisInFuture > System.currentTimeMillis() - downTimePoint.getStartMills()) {
                touchDownDraw(canvas, downTimePoint);
            } else {
                clickPointList.remove(i);
            }
        }
    }

    protected void touchDownDraw(Canvas canvas, GameCountDownTimePointModel downTimePoint) {
        if (canvas == null || downTimePoint == null) {
            return;
        }
        int width = SizeUtils.dp2px(30);
        int height = SizeUtils.dp2px(30);
        Bitmap mBitmap = getBitmap(mContext, R.mipmap.game_spot_icon_find_diff_error_zc);
        mBitmap = Bitmap.createScaledBitmap(mBitmap, width, height, true);
        canvas.drawBitmap(mBitmap, downTimePoint.getX() - width / 2, downTimePoint.getY() - height / 2, historyPaint);
    }


    public void touchDownEvent(MotionEvent event) {
        if (clickPointList == null) {
            clickPointList = new ArrayList<>();
        }
        //
        GameCountDownTimePointModel countDownTimeCircle = new GameCountDownTimePointModel();
        countDownTimeCircle.set((int) event.getX(), (int) event.getY(), System.currentTimeMillis());
        clickPointList.add(countDownTimeCircle);
        //
        mHandler.removeMessages(MESSAGE_REFRESH_VIEW);
        mHandler.sendEmptyMessage(MESSAGE_REFRESH_VIEW);
    }

    private void initPaint() {
        if (historyPaint == null) {
            historyPaint = new Paint();
        }
    }
}
