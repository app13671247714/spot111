package spot.game.view.gameFindFileDiffFile;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.text.Html;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ScreenUtils;
import com.blankj.utilcode.util.SizeUtils;
import com.blankj.utilcode.util.ThreadUtils;
import com.bumptech.glide.Glide;

import net.goal.differences.BuildConfig;
import net.goal.differences.R;
import net.goal.differences.databinding.GameSpotLayoutFindDiffViewZcBinding;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;

import spot.game.manager.ConfigManager;
import spot.game.manager.ContextManager;
import spot.game.manager.GameManager;
import spot.game.manager.TrackManager;
import spot.game.model.JLogExtBean;
import spot.game.util.AnimationUtils;
import spot.game.util.FastClickUtil;

/**
 * 找茬view
 * <p>
 * 动画飞行轨迹是依据在屏幕位置确定，故外部布局使用需保证本次view的左顶点与屏幕左顶点重合
 * 如要调整布局位置请在本view内部布局中调整
 */
public class GameFindFileDiffViewFile extends RelativeLayout {
    static final String BASE_URL = "file:///android_asset/spot_gola_find_diff/spot_gola_img/";
    
    private GameSpotLayoutFindDiffViewZcBinding mBinding;
    //
//    RelativeLayout mBinding.rlGameSpotFindDiffLayoutBg;
//    //星星
//    ImageView ivStart1;
//    ImageView ivStart2;
//    ImageView ivStart3;
//    ImageView ivStart4;
//    ImageView ivStart5;
//    //问题区域1
//    GameFindFileErrorViewFile mBinding.fzevGameSpotFindDiffZone1;
//    RelativeLayout mBinding.rlGameSpotFindDiffZone1Bg;
//    ImageView mBinding.ivGameSpotFindDiffZone1;
//    RelativeLayout mBinding.rlGameSpotFindDiffZone1BgGone;
//    ImageView mBinding.ivGameSpotFindDiffZone1Gone;
//    //问题区域2
//    GameFindFileErrorViewFile mBinding.fzevGameSpotFindDiffZone2;
//    RelativeLayout mBinding.rlGameSpotFindDiffZone2Bg;
//    ImageView mBinding.ivGameSpotFindDiffZone2;
//    RelativeLayout mBinding.rlGameSpotFindDiffZone2BgGone;
//    ImageView mBinding.ivGameSpotFindDiffZone2Gone;
//    //
//    ImageView mBinding.ivGameSpotFindDiffGuide;
    //
    AssetManager mAssetManager;

    int diff_sum = 5;

    private int correctCount = 0;//单次答对计数
    private List<ImageView> correctStarList;//答对星星
    private List<ImageView> animStarList;//动画
    private int touchCorrectIndex = 0;

    GameFindFileCorrectViewFile findZoneCorrectView = null;

    Handler mHandler = new Handler(Looper.getMainLooper());
    AtomicBoolean isLevelFinish;//防同时答题导致重复回调finish


    List<ImageView> mCorrectStarList;

    public GameFindFileDiffViewFile(Context context) {
        this(context, null, 0);
    }

    public GameFindFileDiffViewFile(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public GameFindFileDiffViewFile(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mBinding = GameSpotLayoutFindDiffViewZcBinding.inflate(LayoutInflater.from(context));
        addView(mBinding.getRoot());
        initView();
    }

    public GameFindFileDiffViewFile(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        this(context, attrs, 0);
    }

    private void initView() {
        mCorrectStarList = Arrays.asList(mBinding.ivGameSpotFindDiffZoneStar1,
                mBinding.ivGameSpotFindDiffZoneStar2,mBinding.ivGameSpotFindDiffZoneStar3,
                mBinding.ivGameSpotFindDiffZoneStar4,mBinding.ivGameSpotFindDiffZoneStar5);

       int screen_height = ScreenUtils.getScreenHeight();
        if (screen_height > 1920){
            mBinding.topDivideLayout.setVisibility(VISIBLE);
            mBinding.middleDivideLayout.setVisibility(VISIBLE);
        }else{
            mBinding.topDivideLayout.setVisibility(GONE);
            mBinding.middleDivideLayout.setVisibility(GONE);
        }
    }

    /**
     * @param questionNo   从1开始
     * @param locationList
     */
    public void initData(int questionNo, List<GameFindFileDiffTargetModel.FileTargetLocation> locationList) {
        int levelStar = GameManager.getLevelStar(questionNo);
        TrackManager.sendTrack(JLogExtBean.event("FindDiffView", "levelStar=" + levelStar + ", questionNo=" + questionNo).setIntvalue(ConfigManager.getLocalLevel()));
        correctStarList = mCorrectStarList.subList(0, levelStar);
        //
        diff_sum = correctStarList.size();
        animStarList = new ArrayList<>(diff_sum);
        mBinding.fzevGameSpotFindDiffZone1.setEnabled(false);
        mBinding.fzevGameSpotFindDiffZone2.setEnabled(false);

        if (locationList != null) {
            initQuestionZone(getContext(), locationList.size());
        }

        mBinding.tvGameSpotFragmentFindDiffStorageTitle.setText(Html.fromHtml(ContextManager.getContext().getString(R.string.game_spot_level, " " + questionNo)));
    }

    public void update(GameFindFileDiffTargetModel newTargetZone, GameFindFileDiffTargetModel oldDiffTargetZone, boolean isAnimate) {
        goneGuide();
        if (newTargetZone == null) {
            return;
        }
        //调整比例
        int pngWidth = mBinding.ivGameSpotFindDiffZone1.getWidth();
        float pngWidthScale = pngWidth / (float) newTargetZone.file_png_width;
        int pngHeight = mBinding.ivGameSpotFindDiffZone1.getHeight();
        float pngHeightScale = pngHeight / (float) newTargetZone.file_png_height;
        //
        List<GameFindFileDiffTargetModel.FileTargetLocation> targetLocationList = newTargetZone.getTarget_location();
        if (targetLocationList == null || targetLocationList.isEmpty()) {
            return;
        }
        //隐藏多余星星
        diff_sum = Math.min(correctStarList.size(), targetLocationList.size());
        correctStarList = correctStarList.subList(0, diff_sum);
        for (int i = 0; i < mCorrectStarList.size(); i++) {
            if (i < diff_sum) {
                mCorrectStarList.get(i).setVisibility(VISIBLE);
            } else {
                mCorrectStarList.get(i).setVisibility(GONE);
            }
        }
        TrackManager.sendTrack(JLogExtBean.event("FindDiffView", "diff_sum=" + diff_sum).setIntvalue(ConfigManager.getLocalLevel()));
        //
        List<LayoutParams> layoutParamsList = new ArrayList<>();
        for (int i = 0; i < targetLocationList.size(); i++) {
            GameFindFileDiffTargetModel.FileTargetLocation targetLocation = targetLocationList.get(i);
            int radius = targetLocation.getFile_center_radius();
            if (radius < 50) {
                radius = 50;
                targetLocation.setFile_center_radius(50);
            }
            int leftMargin = (int) (targetLocation.file_center_x - radius);
            int topMargin = (int) (targetLocation.file_center_y - radius);
            int width = 2 * radius;
            leftMargin *= pngWidthScale;
            topMargin *= pngHeightScale;
            leftMargin += SizeUtils.dp2px(4);
            topMargin += SizeUtils.dp2px(4);
            width *= Math.max(pngWidthScale, pngHeightScale);
            LayoutParams layoutParams = new LayoutParams(width, width);
            layoutParams.leftMargin = leftMargin;
            layoutParams.topMargin = topMargin;
            layoutParamsList.add(layoutParams);
        }

        //String baseUrl = "file:///android_asset/game_spot_find_diff/game_spot_img/";
        String newImgA = BASE_URL + newTargetZone.file_name_A;
        String newImgB = BASE_URL + newTargetZone.file_name_B;
        String oldImgA = newImgA;
        String oldImgB = newImgB;
        if (oldDiffTargetZone != null) {
            oldImgA = BASE_URL + oldDiffTargetZone.file_name_A;
            oldImgB = BASE_URL + oldDiffTargetZone.file_name_B;
        }
        //
        updateQuestionView(oldImgA, oldImgB, newImgA, newImgB, isAnimate);
        updateQuestionZone(layoutParamsList);

    }


    public void updateQuestionZone(List<LayoutParams> layoutParamsList) {
        try {
            mBinding.fzevGameSpotFindDiffZone1.restart();
            mBinding.fzevGameSpotFindDiffZone2.restart();
            mBinding.fzevGameSpotFindDiffZone1.updateChildParams(layoutParamsList);
            mBinding.fzevGameSpotFindDiffZone2.updateChildParams(layoutParamsList);
        } catch (Throwable e) {
            LogUtils.e(e.getMessage());
        }
    }

    public void initQuestionZone(Context context, int length) {
        if (context == null || mBinding.fzevGameSpotFindDiffZone1 == null || mBinding.fzevGameSpotFindDiffZone2 == null) {
            return;
        }
        mBinding.fzevGameSpotFindDiffZone1.removeAllChildFindDiffZoneView();
        mBinding.fzevGameSpotFindDiffZone2.removeAllChildFindDiffZoneView();
        //
        for (int i = 0; i < length; i++) {
            GameFindFileViewFile diffZoneView = new GameFindFileCorrectViewFile(context);
            //diffZoneView.setBackgroundColor(Color.GREEN);
            diffZoneView.setTouchDrawCallback(new GameFindFileViewFile.TouchDrawCallback() {
                @Override
                public void touchEvent(MotionEvent event, GameFindFileViewFile diffZoneView) {
                    goneGuide();
                    if (event == null || diffZoneView == null) {
                        return;
                    }
                    if (mFindDiffListener != null) {
                        mFindDiffListener.findCorrect(event, diffZoneView);
                    }
                    findDiffCorrectAnim(event, diffZoneView);
                    //
                    MotionEvent obtain = MotionEvent.obtain(event);
                    List<GameFindFileViewFile> childFindDiffZoneView = mBinding.fzevGameSpotFindDiffZone1.getChildFindDiffZoneView();
                    if (childFindDiffZoneView == null || childFindDiffZoneView.isEmpty()) {
                        return;
                    }
                    for (int j = 0; j < childFindDiffZoneView.size(); j++) {
                        if (childFindDiffZoneView.get(j).equals(diffZoneView)) {
                            mBinding.fzevGameSpotFindDiffZone2.getChildFindDiffZoneView().get(j).touchDownEvent(obtain);
                            break;
                        }
                    }
                }
            });
            mBinding.fzevGameSpotFindDiffZone1.addFindDiffZoneView(diffZoneView);
            //
            diffZoneView = new GameFindFileCorrectViewFile(context);
            //diffZoneView.setBackgroundColor(Color.GREEN);
            diffZoneView.setTouchDrawCallback(new GameFindFileViewFile.TouchDrawCallback() {
                @Override
                public void touchEvent(MotionEvent event, GameFindFileViewFile diffZoneView) {
                    goneGuide();
                    if (event == null || diffZoneView == null) {
                        return;
                    }
                    if (mFindDiffListener != null) {
                        mFindDiffListener.findCorrect(event, diffZoneView);
                    }
                    findDiffCorrectAnim(event, diffZoneView);
                    //
                    MotionEvent obtain = MotionEvent.obtain(event);
                    List<GameFindFileViewFile> childFindDiffZoneView = mBinding.fzevGameSpotFindDiffZone2.getChildFindDiffZoneView();
                    if (childFindDiffZoneView == null || childFindDiffZoneView.isEmpty()) {
                        return;
                    }
                    for (int j = 0; j < childFindDiffZoneView.size(); j++) {
                        if (childFindDiffZoneView.get(j).equals(diffZoneView)) {
                            mBinding.fzevGameSpotFindDiffZone1.getChildFindDiffZoneView().get(j).touchDownEvent(obtain);
                            break;
                        }
                    }
                }
            });
            mBinding.fzevGameSpotFindDiffZone2.addFindDiffZoneView(diffZoneView);
            //
            mBinding.fzevGameSpotFindDiffZone1.setTouchDrawCallback(new GameFindFileViewFile.TouchDrawCallback() {
                @Override
                public void touchEvent(MotionEvent event, GameFindFileViewFile diffZoneView) {
                    if (mFindDiffListener != null) {
                        mFindDiffListener.findWrong(event, diffZoneView);
                    }
                }
            });
            mBinding.fzevGameSpotFindDiffZone2.setTouchDrawCallback(new GameFindFileViewFile.TouchDrawCallback() {
                @Override
                public void touchEvent(MotionEvent event, GameFindFileViewFile diffZoneView) {
                    if (mFindDiffListener != null) {
                        mFindDiffListener.findWrong(event, diffZoneView);
                    }
                }
            });
        }
    }


    /**
     * 选对动画
     *
     * @param event
     * @param view
     */
    public void findDiffCorrectAnim(MotionEvent event, View view) {
        ImageView targetImageView = correctStarList.get(correctCount);
        //防止答题过快，飘动星星的目标为同一个
        correctCount = (++correctCount) % diff_sum;
        TrackManager.sendTrack(JLogExtBean.event("FindDiffView", "correctCount=" + correctCount).setIntvalue(ConfigManager.getLocalLevel()));
        //
        int[] location = new int[2];
        targetImageView.getLocationOnScreen(location);
        float endX = location[0];
        float endY = location[1];
        //
        int width = SizeUtils.dp2px(30);
        //
        ImageView ivAnim = new ImageView(getContext());
        ivAnim.setImageResource(R.mipmap.game_spot_icon_find_diff_unlock_star_zc);
        location = new int[2];
        view.getLocationOnScreen(location);
        float startX = location[0] + view.getWidth() / 2;
        float startY = location[1] + view.getHeight() / 2;
        LayoutParams layoutParams = getLayoutParams(startX, startY, width, width);
        mBinding.rlGameSpotFindDiffLayoutBg.addView(ivAnim, layoutParams);
        animStarList.add(0, ivAnim);
        //
        //String  url = "file:///android_asset/quiz_img_interval_reward_fcct.gif";
        //Glide.with(ivAnim).clear(ivAnim);
        //Glide.with(ivAnim).asGif().load(url).into(ivAnim);
        if (correctCount >= diff_sum || correctCount == 0) {
            mBinding.fzevGameSpotFindDiffZone1.setAllChildEnableFalse();
            mBinding.fzevGameSpotFindDiffZone2.setAllChildEnableFalse();
        }

        ivAnim.animate()
                .translationX(endX - startX)
                .translationY(endY - startY)
                .setInterpolator(new AccelerateDecelerateInterpolator())
                .setDuration(1_000)
                .withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        if (animStarList.size() > 0) {
                            mBinding.rlGameSpotFindDiffLayoutBg.removeView(animStarList.get(animStarList.size() - 1));
                            animStarList.remove(animStarList.size() - 1);
                        }
                        targetImageView.setImageResource(R.mipmap.game_spot_icon_find_diff_unlock_star_zc);
                        AnimationUtils.playScaleAnimation(targetImageView);
                        if (isLevelFinish == null) {
                            isLevelFinish = new AtomicBoolean();
                            isLevelFinish.set(false);
                        }
                        if (!isLevelFinish.get() && (correctCount >= diff_sum || correctCount == 0)) {
                            isLevelFinish.set(true);
                            mHandler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    resetStar();
                                    isLevelFinish.set(false);
                                    if (mFindDiffListener != null) {
                                        mFindDiffListener.finish();
                                    }
                                }
                            }, 1_000);
                        }
                        //Drawable drawable = null;
                        //Glide.with(ivAnim).asGif().load(drawable).into(ivAnim);
                    }
                }).start();
    }

    /**
     * 关卡切换，直拉图片再播放切换动画
     *
     * @param beforeNormalUrl
     * @param beforeDiffUrl
     * @param normalUrl
     * @param diffUrl
     */
    public void updateQuestionView(String beforeNormalUrl, String beforeDiffUrl, String normalUrl, String diffUrl, boolean isAnimate) {
        try {
            mHandler.removeCallbacksAndMessages(null);
            //
            mBinding.fzevGameSpotFindDiffZone1.setEnabled(false);
            mBinding.fzevGameSpotFindDiffZone2.setEnabled(false);
            //先隐藏切换
            mBinding.rlGameSpotFindDiffZone1BgGone.setVisibility(View.GONE);
            mBinding.rlGameSpotFindDiffZone2BgGone.setVisibility(View.GONE);
            //
            resetStar();
            //以防直拉失败
            //Glide.with(mContext).load(normalUrl).into(mBinding.ivGameSpotFindDiffZone1);
            //Glide.with(mContext).load(diffUrl).into(mBinding.ivGameSpotFindDiffZone2);
            ////
            //Glide.with(mContext).load(beforeNormalUrl).into(mBinding.ivGameSpotFindDiffZone1Gone);
            //Glide.with(mContext).load(beforeDiffUrl).into(mBinding.ivGameSpotFindDiffZone2Gone);
            //图片直拉
            ThreadUtils.executeByIo(new ThreadUtils.SimpleTask<Object>() {
                @Override
                public Object doInBackground() throws Throwable {
                    Drawable submitNormalUrl = null;
                    Drawable submitDiffUrl = null;
                    Drawable submitBeforeNormalUrl = null;
                    Drawable submitBeforeDiffUrl = null;
                    try {
                        submitNormalUrl = Glide.with(getContext()).load(normalUrl).submit().get();
                        submitDiffUrl = Glide.with(getContext()).load(diffUrl).submit().get();
                        submitBeforeNormalUrl = Glide.with(getContext()).load(beforeNormalUrl).submit().get();
                        submitBeforeDiffUrl = Glide.with(getContext()).load(beforeDiffUrl).submit().get();
                    } catch (Throwable e) {
                        LogUtils.e(e.getMessage());
                    } finally {
                        Drawable finalSubmitNormalUrl = submitNormalUrl;
                        Drawable finalSubmitDiffUrl = submitDiffUrl;
                        Drawable finalSubmitBeforeNormalUrl = submitBeforeNormalUrl;
                        Drawable finalSubmitBeforeDiffUrl = submitBeforeDiffUrl;
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    Glide.get(getContext()).clearMemory();
                                    //
                                    mBinding.ivGameSpotFindDiffZone1.setImageDrawable(finalSubmitNormalUrl);
                                    mBinding.ivGameSpotFindDiffZone2.setImageDrawable(finalSubmitDiffUrl);
                                    mBinding.ivGameSpotFindDiffZone1Gone.setImageDrawable(finalSubmitBeforeNormalUrl);
                                    mBinding.ivGameSpotFindDiffZone2Gone.setImageDrawable(finalSubmitBeforeDiffUrl);
                                } catch (Throwable e) {
                                    LogUtils.e(e.getMessage());
                                }
                                if (isAnimate) {
                                    //切换动画
                                    updateQuestionViewAnimate();
                                } else {
                                    mBinding.fzevGameSpotFindDiffZone1.setVisibility(VISIBLE);
                                    mBinding.fzevGameSpotFindDiffZone2.setVisibility(VISIBLE);
                                    mBinding.fzevGameSpotFindDiffZone1.setEnabled(true);
                                    mBinding.fzevGameSpotFindDiffZone2.setEnabled(true);
                                }
                            }
                        });
                    }
                    return null;
                }

                @Override
                public void onSuccess(Object result) {

                }
            });
        } catch (Throwable e) {
            LogUtils.e(e.getMessage());
        }
    }

    private void updateQuestionViewAnimate() {
        try {
            mBinding.fzevGameSpotFindDiffZone1.setVisibility(VISIBLE);
            mBinding.fzevGameSpotFindDiffZone2.setVisibility(VISIBLE);
            mBinding.rlGameSpotFindDiffZone1BgGone.setVisibility(View.VISIBLE);
            mBinding.rlGameSpotFindDiffZone2BgGone.setVisibility(View.VISIBLE);
            //
            long duration = 1000;
            int translationX = 1100;
            //
            ObjectAnimator anim1 = ObjectAnimator.ofFloat(mBinding.rlGameSpotFindDiffZone1Bg, "translationX", -translationX, 0);
            anim1.setDuration(duration);
            //
            ObjectAnimator anim2 = ObjectAnimator.ofFloat(mBinding.rlGameSpotFindDiffZone2Bg, "translationX", translationX, 0);
            anim2.setDuration(duration);
            //
            ObjectAnimator anim3 = ObjectAnimator.ofFloat(mBinding.rlGameSpotFindDiffZone1BgGone, "translationX", 0, translationX);
            anim3.setDuration(duration);
            //
            ObjectAnimator anim4 = ObjectAnimator.ofFloat(mBinding.rlGameSpotFindDiffZone2BgGone, "translationX", 0, -translationX);
            anim4.setDuration(duration);
            //
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.playTogether(anim1, anim2, anim3, anim4);
            animatorSet.setInterpolator(new AccelerateInterpolator());
            animatorSet.start();
            mBinding.fzevGameSpotFindDiffZone2.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mBinding.fzevGameSpotFindDiffZone1.setEnabled(true);
                    mBinding.fzevGameSpotFindDiffZone2.setEnabled(true);
                    mBinding.rlGameSpotFindDiffZone1BgGone.setVisibility(View.GONE);
                    mBinding.rlGameSpotFindDiffZone2BgGone.setVisibility(View.GONE);
                }
            }, duration);
        } catch (Throwable e) {
            LogUtils.e(e.getMessage());

        }
    }

    private void resetStar() {
        TrackManager.sendTrack(JLogExtBean.event("FindDiffView", "resetStar").setIntvalue(ConfigManager.getLocalLevel()));
        try {
            //内存回收
            Glide.get(getContext()).clearMemory();
        } catch (Throwable e) {
            LogUtils.e(e.getMessage());
        }
        //
        correctCount = 0;
        animStarList.clear();
        mBinding.rlGameSpotFindDiffLayoutBg.removeAllViews();
        if (correctStarList == null || correctStarList.isEmpty()) {
            return;
        }
        for (int i = 0; i < correctStarList.size(); i++) {
            correctStarList.get(i).setImageResource(R.mipmap.game_spot_icon_find_diff_locked_star);
        }
    }

    /**
     * 提示引导
     *
     * @param guideListener
     */
    @SuppressLint("ClickableViewAccessibility")
    public void guide(GuideListener guideListener) {
        try {
            if (FastClickUtil.isFastClick()) {
                guideListener.guideFail("isFastClick");
                return;
            }
            //随机
            int size = mBinding.fzevGameSpotFindDiffZone1.childViewList.size();
            int startIndex = new Random().nextInt(size);
            int end = startIndex + size;
            TrackManager.sendTrack(JLogExtBean.event("FindDiffView", "guide=" + startIndex).setIntvalue(ConfigManager.getLocalLevel()));
            for (int i = startIndex; i < end; i++) {
                int index = i % size;
                GameFindFileViewFile findZoneView = mBinding.fzevGameSpotFindDiffZone1.childViewList.get(index);
                boolean isTouched = false;
                if (findZoneView instanceof GameFindFileCorrectViewFile) {
                    findZoneCorrectView = (GameFindFileCorrectViewFile) findZoneView;
                    isTouched = findZoneCorrectView.isTouched;
                }
                if (findZoneView.isEnabled() && !isTouched) {
                    mBinding.ivGameSpotFindDiffGuide.setVisibility(VISIBLE);
                    LayoutParams params = (LayoutParams) findZoneView.getLayoutParams();
                    LayoutParams params1 = (LayoutParams) mBinding.ivGameSpotFindDiffGuide.getLayoutParams();
                    params1.leftMargin = params.leftMargin + 30;
                    params1.topMargin = params.topMargin + 20;
                    mBinding.ivGameSpotFindDiffGuide.setLayoutParams(params1);
                    //
                    mBinding.fzevGameSpotFindDiffZone2.setEnableFalse();
                    mBinding.ivGameSpotFindDiffGuide.setOnTouchListener(new OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            findZoneCorrectView.onTouchEvent(event);
                            return false;
                        }
                    });

                    AnimatorSet mGuideAnim = new AnimatorSet();
                    ObjectAnimator anim1 = ObjectAnimator.ofFloat(mBinding.ivGameSpotFindDiffGuide, "rotationX", 0, -25, 0);
                    anim1.setRepeatCount(15);
                    anim1.setDuration(1500);
                    ObjectAnimator anim2 = ObjectAnimator.ofFloat(mBinding.ivGameSpotFindDiffGuide, "rotation", 0, -15, 0);
                    anim2.setRepeatCount(15);
                    anim2.setDuration(1500);
                    mGuideAnim.playTogether(anim1, anim2);
                    mGuideAnim.start();
                    //
                    if (guideListener != null) {
                        guideListener.guideSuccess();
                    }
                    return;
                }
            }
            if (guideListener != null) {
                guideListener.guideFail("no answer");
            }
            goneGuide();
        } catch (Throwable e) {
            if (guideListener != null) {
                guideListener.guideFail(e.getMessage());
            }
            LogUtils.e(e.getMessage());
        }

    }

    public LayoutParams getLayoutParams(float x, float y, float width, float height) {
        LayoutParams mFloatBallParams = new LayoutParams((int) width, (int) height);
        mFloatBallParams.leftMargin = (int) x;
        mFloatBallParams.topMargin = (int) y;
        return mFloatBallParams;
    }

    public void setFindDiffListener(FindDiffListener findDiffListener) {
        mFindDiffListener = findDiffListener;
    }


    FindDiffListener mFindDiffListener;

    public interface FindDiffListener {
        void findCorrect(MotionEvent event, GameFindFileViewFile diffZoneView);

        void findWrong(MotionEvent event, GameFindFileViewFile diffZoneView);

        void finish();

    }

    public interface GuideListener {
        void guideSuccess();

        void guideFail(String msg);
    }

    private void reSize(int width, int height, View... viewList) {
        if (viewList == null || viewList.length < 1) {
            return;
        }
        for (View view : viewList) {
            if (view == null) {
                continue;
            }
            LayoutParams params = (LayoutParams) view.getLayoutParams();
            params.width = width;//设置当前控件布局的高度
            params.height = height;//设置当前控件布局的高度
            view.setLayoutParams(params);//将设置好的布局参数应用到控件中
        }
    }

    /**
     * 附加宝藏功能
     */

    ImageView ivTreasure;//宝物图标

    public void checkTreasureView(String sourceIvUri, View targetImageView, Runnable findRunnable) {
        try {
            TrackManager.sendTrack(JLogExtBean.event("FindDiffView", "checkTreasureView").setIntvalue(ConfigManager.getLocalLevel()));
            removeTreasureView();
            //
            Random random = new Random();
            ViewGroup treasureViewParent = mBinding.fzevGameSpotFindDiffZone1;
            //
            if (random.nextInt(BuildConfig.VERSION_CODE) % 2 == 0) {
                treasureViewParent = mBinding.fzevGameSpotFindDiffZone2;
            }
            int width = SizeUtils.dp2px(40);
            //
            int[] location = new int[2];
            targetImageView.getLocationOnScreen(location);
            float endX = location[0];
            float endY = location[1];
            //
            location = new int[2];
            treasureViewParent.getLocationOnScreen(location);
            float startX = location[0] + random.nextInt(treasureViewParent.getWidth() - 2 * width);
            float startY = location[1] + random.nextInt(treasureViewParent.getHeight() - 2 * width);
            LayoutParams layoutParams = getLayoutParams(startX, startY, width, width);
            //
            ivTreasure = new ImageView(getContext());
            ivTreasure.setOnClickListener(new ClickUtils.OnDebouncingClickListener(FastClickUtil.S_SPACE_TIME) {
                @Override
                public void onDebouncingClick(View v) {
                    if (ivTreasure == null) {
                        return;
                    }
                    if (findRunnable != null) {
                        findRunnable.run();
                    }
                    ivTreasure.animate()
                            .translationX(endX - startX)
                            .translationY(endY - startY)
                            .setInterpolator(new AccelerateDecelerateInterpolator())
                            .setDuration(1_000)
                            .withEndAction(new Runnable() {
                                @Override
                                public void run() {
                                    removeTreasureView();
                                    AnimationUtils.playScaleAnimation(targetImageView);
                                }
                            }).start();
                }
            });
            Glide.with(getContext()).load(sourceIvUri).into(ivTreasure);
            mBinding.rlGameSpotFindDiffLayoutBg.addView(ivTreasure, layoutParams);
        } catch (Throwable e) {
            LogUtils.e(e.getMessage());
        }
    }

    public void removeTreasureView() {
        try {
            if (ivTreasure != null && ivTreasure.getParent() != null) {
                ((ViewGroup) ivTreasure.getParent()).removeView(ivTreasure);
            }
            ivTreasure = null;
        } catch (Throwable e) {
            LogUtils.e(e.getMessage());
        }
    }

    private void goneGuide() {
        mBinding.ivGameSpotFindDiffGuide.setVisibility(GONE);
        mBinding.ivGameSpotFindDiffGuide.clearAnimation();
    }

}
