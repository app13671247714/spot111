package spot.game.view.textView;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.Nullable;


/**
 * Created by MaXin on 2021/6/21
 */
public class SerifHeiMediumTextView extends BaseFontTextView {
    public SerifHeiMediumTextView(Context context) {
        super(context);
    }

    public SerifHeiMediumTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public SerifHeiMediumTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void initFont() {
        FontManager.bindSerifMedium(this);
    }
}