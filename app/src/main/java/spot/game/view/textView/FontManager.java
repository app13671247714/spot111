package spot.game.view.textView;

import android.app.Application;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.widget.TextView;

import androidx.annotation.StringDef;
import androidx.core.content.res.ResourcesCompat;

import net.goal.differences.R;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.HashMap;
import java.util.Map;

import spot.game.manager.ContextManager;


/**
 * Created by Maxin on 2018/5/7.
 * 字体管理工具类
 */
public class FontManager {
    private static FontManager FONT_MANAGER;
    private AssetManager mAssetManager;
    private static final Map<String, Typeface> CACHE_FONT_MAP = new HashMap<String, Typeface>();

    private FontManager(Application application) {
        mAssetManager = application.getAssets();
    }


    /**
     * 获取字体管理对象
     *
     * @return
     */
    public static FontManager getInstance() {
        if (FONT_MANAGER == null) {
            synchronized (FontManager.class) {
                if (FONT_MANAGER == null) {
                    FONT_MANAGER = new FontManager(ContextManager.getApplication());
                }
            }
        }
        return FONT_MANAGER;
    }


    /**
     * 设置TextView字体为思源黑-中
     *
     * @return
     */
    public static void bindSerifMedium(TextView textView) {
        bindSerifMedium(textView, -1);
    }

    public static void bindSerifMedium(TextView textView, int fontResId) {
        if (textView == null) {
            return;
        }
        if (fontResId < 1) {
            fontResId = R.font.game_spot_tobi_black;
        }
        textView.setTypeface(FontManager.getInstance().getSerifMedium(fontResId));
    }

    /**
     * 思源宋体
     *
     * @return
     */
    public Typeface getSerifMedium(int fontResId) {
        Typeface typeface = CACHE_FONT_MAP.get(FONT_SOURCE_HEI_MEDIUM);
        if (typeface != null) {
            return typeface;
        }
        typeface = ResourcesCompat.getFont(ContextManager.getApplication(), fontResId);
        CACHE_FONT_MAP.put(FONT_SOURCE_HEI_MEDIUM, typeface);
        return typeface;
    }


    public static final String FONT_SOURCE_HEI_MEDIUM = "font_source_hei_medium";


    @Retention(RetentionPolicy.SOURCE)
    @StringDef(value = {FONT_SOURCE_HEI_MEDIUM})
    public @interface FontType {
    }


}