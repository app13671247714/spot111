package spot.game.view.textView;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.text.Html;
import android.text.Spanned;
import android.util.AttributeSet;

import net.goal.differences.BuildConfig;
import net.goal.differences.R;

public class StrokeTextView2 extends SerifHeiMediumTextView {
    private float strokeWidth = BuildConfig.VERSION_CODE;
    private int strokeColor = Color.BLACK;
    private boolean enableStroke = true;

    public StrokeTextView2(Context context) {
        this(context, null);
    }

    public StrokeTextView2(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public StrokeTextView2(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.StrokeTextView);

        strokeColor = a.getColor(R.styleable.StrokeTextView_stroke, 0xffffff);
        strokeWidth = a.getDimension(R.styleable.StrokeTextView_strokeWidth, 3f);

        a.recycle();
    }

    /**
     * 设置描边宽度
     *
     * @param strokeWidth 描边宽度
     */
    public void setStrokeWidth(float strokeWidth) {
        this.strokeWidth = strokeWidth;
        invalidate();
    }

    /**
     * 设置描边颜色
     *
     * @param strokeColor 描边颜色
     */
    public void setStrokeColor(int strokeColor) {
        this.strokeColor = strokeColor;
        invalidate();
    }

    /**
     * 设置是否启用描边
     *
     * @param enableStroke 是否启用描边
     */
    public void setEnableStroke(boolean enableStroke) {
        this.enableStroke = enableStroke;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (enableStroke && !getText().toString().isEmpty()) {
            // 保存原始文本颜色
            int originalTextColor = getCurrentTextColor();

            // 绘制文本描边
            getPaint().setStyle(Paint.Style.STROKE);
            getPaint().setStrokeWidth(strokeWidth);
            setTextColor(strokeColor);
            super.onDraw(canvas);

            // 恢复原始文本颜色并重新绘制文本
            getPaint().setStyle(Paint.Style.FILL);
            setTextColor(originalTextColor);
        }
        super.onDraw(canvas);
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        if (text instanceof Spanned) {
            super.setText(text, type);
        } else if (text instanceof String) {
            super.setText(Html.fromHtml((String) text), type);
        } else {
            super.setText(text, type);
        }
    }
}

