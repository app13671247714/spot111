package spot.game.view.textView;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;

/**
 * Created by MaXin on 2021/6/21
 */
public abstract class BaseFontTextView extends AppCompatTextView {
    public BaseFontTextView(Context context) {
        this(context, null);
    }

    public BaseFontTextView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BaseFontTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initFont();
    }

    public abstract void initFont();
}

