package spot.game.media;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ThreadUtils;

import spot.game.manager.ContextManager;

/**
 * 播放音效
 */
public class VoicePlayUtils {

    private static MediaPlayer  mediaPlayerBGM;

    public static void initBGMMedia(int resId) {
        try {
            stopMedia();
            mediaPlayerBGM = MediaPlayer.create(ContextManager.getContext(), resId);
            if (mediaPlayerBGM != null) {
                mediaPlayerBGM.setLooping(true);
                mediaPlayerBGM.start();
            }
        } catch (Exception e) {
            LogUtils.e(e.getMessage());
        }
    }

    public static void stopMedia() {
        if (mediaPlayerBGM != null) {
            mediaPlayerBGM.stop();
            mediaPlayerBGM.release();
        }
        mediaPlayerBGM = null;
    }
}
