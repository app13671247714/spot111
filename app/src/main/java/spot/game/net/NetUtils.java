package spot.game.net;

import java.util.Map;
import java.util.WeakHashMap;

import io.reactivex.rxjava3.core.Observer;

public class NetUtils {

    public static NetUtils jhNetUtils;

    public static NetUtils getInstance() {
        if (jhNetUtils == null) {
            synchronized (NetUtils.class) {
                if (jhNetUtils == null) {
                    jhNetUtils = new NetUtils();
                }
            }
        }
        return jhNetUtils;
    }


    /**
     * get请求
     *
     * @param url
     * @param headers
     * @param params
     * @param observer
     */
    public void get(String url, Map<String, String> headers, WeakHashMap<String, Object> params, final Observer<String> observer) {
        NetUtils_.getInstance().get(url,headers,observer);
    }


    /**
     * post请求
     *
     * @param url
     * @param headers
     * @param paramsJSON
     * @param observer
     */
    public void post(String url, Map<String, String> headers, String paramsJSON, final Observer<String> observer) {
        NetUtils_.getInstance().post(url,headers,paramsJSON,observer);
    }

}
