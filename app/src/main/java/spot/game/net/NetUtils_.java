package spot.game.net;

import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ThreadUtils;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import io.reactivex.rxjava3.core.Observer;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class NetUtils_ {

    private final long connectTimeout = 10 * 1000L;
    private final long readTimeout = 10 * 1000L;
    private final long writeTimeout = 10 * 1000L;

    public static NetUtils_ jhNetUtils = new NetUtils_();

    public static NetUtils_ getInstance() {
        if (jhNetUtils == null) {
            synchronized (NetUtils_.class) {
                if (jhNetUtils == null) {
                    jhNetUtils = new NetUtils_();
                }
            }
        }
        return jhNetUtils;
    }


    /**
     * get请求
     *
     * @param url
     * @param headers
     * @param params
     * @param observer
     */
    public void get(String url, Map<String, String> headers, final Observer<String> observer) {

        try {
            // 方法未被使用
            OkHttpClient client = new OkHttpClient().newBuilder()
                    .connectTimeout(connectTimeout, TimeUnit.MILLISECONDS) //连接超时
                    .readTimeout(readTimeout, TimeUnit.MILLISECONDS) //读取超时
                    .writeTimeout(writeTimeout, TimeUnit.MILLISECONDS) //写入超时
                    .build();

            // 添加Header
            Headers.Builder builder_header = new Headers.Builder();
            if (headers != null && headers.size() > 0) {
                for (Map.Entry<String, String> entry : headers.entrySet()) {
                    builder_header.add(entry.getKey(), entry.getValue());
                }
            }
            Headers headerBuild = builder_header.build();

            Request request = new Request.Builder()
                    .headers(headerBuild)
                    .url(url)
                    .build();

            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(@NonNull Call call, @NonNull IOException e) {
                    if (observer != null) {
                        observer.onError(e);
                    }
                }

                @Override
                public void onResponse(@NonNull Call call, @NonNull Response response) {
                    try {
                        if (response == null || response.body() == null || TextUtils.isEmpty(Objects.requireNonNull(response.body()).string())) {
                            if (observer != null) {
                                observer.onError(new Exception("response == null "));
                            }
                            return;
                        }
                        String result = Objects.requireNonNull(response.body()).string();
                        if (observer != null) {
                            observer.onNext(result);
                        }
                    } catch (Throwable e) {
                        if (observer != null) {
                            observer.onError(e);
                        }
                    }

                }
            });
        } catch (Throwable e) {
            if (observer != null) {
                observer.onError(e);
            }
        }
    }


    /**
     * post请求
     *
     * @param url
     * @param headers
     * @param paramsJSON
     * @param observer
     */
    public void post(String url, Map<String, String> headers, String paramsJSON, final Observer<String> observer) {
        try {
            OkHttpClient client = new OkHttpClient().newBuilder()
                    .connectTimeout(connectTimeout, TimeUnit.MILLISECONDS) //连接超时
                    .readTimeout(readTimeout, TimeUnit.MILLISECONDS) //读取超时
                    .writeTimeout(writeTimeout, TimeUnit.MILLISECONDS) //写入超时
                    .build();

            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            RequestBody body = RequestBody.create(paramsJSON, JSON);
            // 添加Header
            Headers.Builder builder_header = new Headers.Builder();
            if (headers != null && headers.size() > 0) {
                for (Map.Entry<String, String> entry : headers.entrySet()) {
                    builder_header.add(entry.getKey(), entry.getValue());
                }
            }
            Headers headerBuild = builder_header.build();

            Request request = new Request.Builder()
                    .headers(headerBuild)
                    .url(url)
                    .post(body)
                    .build();

            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(@NonNull Call call, @NonNull IOException e) {
                    if (observer != null) {
                        observer.onError(e);
                    }
                }

                @Override
                public void onResponse(@NonNull Call call, @NonNull Response response) {
                    try {
                        if (response.code() == HttpURLConnection.HTTP_OK) {
                            String result = response.body().string();
                            if (observer != null) {
                                ThreadUtils.getMainHandler().post(new Runnable() {
                                    @Override
                                    public void run() {
                                        observer.onNext(result);
                                    }
                                });
                            }
                        } else {
                            if (observer != null) {
                                observer.onError(new Exception(response.message()));
                            }
                        }
                    } catch (Throwable e) {
                        if (observer != null) {
                            observer.onError(e);
                        }
                    }
                }
            });
        } catch (Throwable e) {
            if (observer != null) {
                observer.onError(e);
            }
        }
    }

}
