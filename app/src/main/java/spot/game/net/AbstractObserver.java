package spot.game.net;

import com.blankj.utilcode.util.GsonUtils;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import io.reactivex.rxjava3.annotations.NonNull;


public abstract class AbstractObserver<T> {

    /**
     *
     * @param t 多态变量
     * @param encryptBody 多态变量加密json
     */
    public abstract void onNext(@NonNull T t, String encryptBody);


    public void onError(@NonNull Throwable e) {

    }


    /**
     * 解析泛型类型并转成bean
     *
     * @param data 原始数据
     * @return T
     */
    public T genClazz(String data) {
        try {
            ParameterizedType parameterizedType = (ParameterizedType) getClass().getGenericSuperclass();
            if (parameterizedType != null) {
                Type[] arguments = parameterizedType.getActualTypeArguments();
                Type argument = arguments[0];
                if (argument == String.class) {
                    return (T) data;
                } else {
                    return GsonUtils.fromJson(data, argument);
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return null;
    }
}
