package spot.game.model;

import com.google.gson.annotations.SerializedName;

import net.goal.differences.BuildConfig;

/**
 * @Class: Platform
 * @Date: 2022/12/7
 * @Description:
 */
public class BIPlatform {
    @SerializedName("platformType")
    private String platformType = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE;
    @SerializedName("appId")
    private String appId = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE;
    @SerializedName("appKey")
    private String appKey = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE;
    @SerializedName("appName")
    private String appName = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE;

    public String getPlatformType() {
        return platformType;
    }

    public void setPlatformType(String platformType) {
        this.platformType = platformType;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }
}
