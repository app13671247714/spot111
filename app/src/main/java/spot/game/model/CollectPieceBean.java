package spot.game.model;

import com.google.gson.annotations.SerializedName;

import net.goal.differences.BuildConfig;

public class CollectPieceBean {
    public static final int COLLECT_PIECE_DOING = BuildConfig.VERSION_CODE
            , COLLECT_PIECE_COMPLETE = COLLECT_PIECE_DOING + 1
            , COLLECT_PIECE_REWARDED = COLLECT_PIECE_COMPLETE + 1
            , COLLECT_PIECE_UNLOCK = COLLECT_PIECE_REWARDED + 1;

    int state = BuildConfig.VERSION_CODE;//：新（已完成）：已解锁未选中、：已解锁已选中、未解锁任务中、未开启解锁任务
    int curProgress = BuildConfig.VERSION_CODE;//
    @SerializedName("taskNum")
    int totalProgress = BuildConfig.VERSION_CODE;//taskNum
    int startLevelNum = BuildConfig.VERSION_CODE;//起始
    int startVideoNum = BuildConfig.VERSION_CODE;
    int startLoginNum = BuildConfig.VERSION_CODE;
    int type = BuildConfig.VERSION_CODE;//taskType
    @SerializedName("reward")
    int rewardCount = BuildConfig.VERSION_CODE;//奖励碎片个数
    String taskDesc = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE;//任务描述
    String urlRewardIcon = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE;//奖励图标
    String btnText = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE;//按键描述


    public String getBtnText() {
        return btnText;
    }

    public void setBtnText(String btnText) {
        this.btnText = btnText;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getCurProgress() {
        return curProgress;
    }

    public void setCurProgress(int curProgress) {
        this.curProgress = curProgress;
    }

    public int getTotalProgress() {
        return totalProgress;
    }

    public void setTotalProgress(int totalProgress) {
        this.totalProgress = totalProgress;
    }

    public int getStartLevelNum() {
        return startLevelNum;
    }

    public void setStartLevelNum(int startLevelNum) {
        this.startLevelNum = startLevelNum;
    }

    public int getStartVideoNum() {
        return startVideoNum;
    }

    public void setStartVideoNum(int startVideoNum) {
        this.startVideoNum = startVideoNum;
    }

    public int getStartLoginNum() {
        return startLoginNum;
    }

    public void setStartLoginNum(int startLoginNum) {
        this.startLoginNum = startLoginNum;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getRewardCount() {
        return rewardCount;
    }

    public void setRewardCount(int rewardCount) {
        this.rewardCount = rewardCount;
    }

    public String getTaskDesc() {
        return taskDesc;
    }

    public void setTaskDesc(String taskDesc) {
        this.taskDesc = taskDesc;
    }

    public String getUrlRewardIcon() {
        return urlRewardIcon;
    }

    public void setUrlRewardIcon(String urlRewardIcon) {
        this.urlRewardIcon = urlRewardIcon;
    }
}
