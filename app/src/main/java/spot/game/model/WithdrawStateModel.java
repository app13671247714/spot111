package spot.game.model;


/**
 * 提现进度
 */
public class WithdrawStateModel {

    private boolean withdrawCashIng;//是否提现中 false = 未提取， true = 提取中
    private int withdrawCashProgress;//提现进度  1 = 第一步等待24小时， 2 = 累计3天完成看广告任务， 3 = 达到等级，4 = 提示处理中

    private float withdrawCashNmu;//提现金额
    private String withdrawCashOrder;//定单号
    private String withdrawCashBankName;//提现银行
    private String withdrawCashAccount;//提现账号
    private long withdrawCashHour;//提现时间

    private int withdrawCashPlayAdDay = 3;//提现任务  需要完成几天（当天广告20次）
    private int withdrawCashPlayAdDayCount = 20;//提现任务 每天需要观看广告次数
    private int withdrawCashLevel =  300;//最终提现需要的等级

    private int date;//日期 按照1-365获取 判断是同一天看的视频
    private int playAdDayCount;// 当天观看广告次数

    private int completeDate;//日期 按照1-365获取 判断这一天是否完成了任务
    private int completeDay;//完成了几天

    public boolean isWithdrawCashIng() {
        return withdrawCashIng;
    }

    public void setWithdrawCashIng(boolean withdrawCashIng) {
        this.withdrawCashIng = withdrawCashIng;
    }

    public float getWithdrawCashNmu() {
        return withdrawCashNmu;
    }

    public void setWithdrawCashNmu(float withdrawCashNmu) {
        this.withdrawCashNmu = withdrawCashNmu;
    }

    public String getWithdrawCashAccount() {
        return withdrawCashAccount;
    }

    public void setWithdrawCashAccount(String withdrawCashAccount) {
        this.withdrawCashAccount = withdrawCashAccount;
    }

    public long getWithdrawCashHour() {
        return withdrawCashHour;
    }

    public void setWithdrawCashHour(long withdrawCashHour) {
        this.withdrawCashHour = withdrawCashHour;
    }

    public int getWithdrawCashPlayAdDay() {
        return withdrawCashPlayAdDay;
    }

    public void setWithdrawCashPlayAdDay(int withdrawCashPlayAdDay) {
        this.withdrawCashPlayAdDay = withdrawCashPlayAdDay;
    }

    public int getWithdrawCashPlayAdDayCount() {
        return withdrawCashPlayAdDayCount;
    }

    public void setWithdrawCashPlayAdDayCount(int withdrawCashPlayAdDayCount) {
        this.withdrawCashPlayAdDayCount = withdrawCashPlayAdDayCount;
    }

    public int getWithdrawCashLevel() {
        return withdrawCashLevel;
    }

    public void setWithdrawCashLevel(int withdrawCashLevel) {
        this.withdrawCashLevel = withdrawCashLevel;
    }

    public String getWithdrawCashOrder() {
        return withdrawCashOrder;
    }

    public void setWithdrawCashOrder(String withdrawCashOrder) {
        this.withdrawCashOrder = withdrawCashOrder;
    }

    public int getWithdrawCashProgress() {
        return withdrawCashProgress;
    }

    public void setWithdrawCashProgress(int withdrawCashProgress) {
        this.withdrawCashProgress = withdrawCashProgress;
    }

    public String getWithdrawCashBankName() {
        return withdrawCashBankName;
    }

    public void setWithdrawCashBankName(String withdrawCashBankName) {
        this.withdrawCashBankName = withdrawCashBankName;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public int getPlayAdDayCount() {
        return playAdDayCount;
    }

    public void setPlayAdDayCount(int playAdDayCount) {
        this.playAdDayCount = playAdDayCount;
    }

    public int getCompleteDay() {
        return completeDay;
    }

    public void setCompleteDay(int completeDay) {
        this.completeDay = completeDay;
    }

    public int getCompleteDate() {
        return completeDate;
    }

    public void setCompleteDate(int completeDate) {
        this.completeDate = completeDate;
    }
}
