package spot.game.model.backModel;

import com.google.gson.annotations.SerializedName;

/**
 * @Class: ResponseParams
 * @Date: 2023/2/2
 * @Description:
 */
public class ResponseParams {
    @SerializedName("key")
    private String key;
    @SerializedName("value")
    private String value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
