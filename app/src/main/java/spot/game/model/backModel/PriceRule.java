package spot.game.model.backModel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * @Class: PriceRule
 * @Date: 2023/2/2
 * @Description:
 */
public class PriceRule {

    @SerializedName("responseParams")
    private List<ResponseParams> responseParams = new ArrayList<>();

    public List<ResponseParams> getResponseParams() {
        return responseParams;
    }

    public void setResponseParams(List<ResponseParams> responseParams) {
        this.responseParams = responseParams;
    }
}
