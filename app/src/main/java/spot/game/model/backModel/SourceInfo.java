package spot.game.model.backModel;

import com.google.gson.annotations.SerializedName;

/**
 * @Class: SourceInfo
 * @Date: 2022/12/7
 * @Description:
 */
public class SourceInfo {
    @SerializedName("id")
    String id;
    @SerializedName("platformType")
    String platformType;
    @SerializedName("sourceType")
    String sourceType;
    @SerializedName("codeId")
    String codeId;
    @SerializedName("appId")
    String appId;
    @SerializedName("appName")
    String appName;
    @SerializedName("interstitialType")
    String interstitialType;
    @SerializedName("preload")
    Boolean preload;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPlatformType() {
        return platformType;
    }

    public void setPlatformType(String platformType) {
        this.platformType = platformType;
    }

    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    public String getCodeId() {
        return codeId;
    }

    public void setCodeId(String codeId) {
        this.codeId = codeId;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getInterstitialType() {
        return interstitialType;
    }

    public void setInterstitialType(String interstitialType) {
        this.interstitialType = interstitialType;
    }

    public Boolean getPreload() {
        return preload;
    }

    public void setPreload(Boolean preload) {
        this.preload = preload;
    }

}
