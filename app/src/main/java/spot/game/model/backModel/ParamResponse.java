package spot.game.model.backModel;

import com.google.gson.annotations.SerializedName;

/**
 * @Class: DeviceResponse
 * @Date: 2022/12/7
 * @Description:
 */
public class ParamResponse {
    @SerializedName("resCode")
    private int resCode;
    @SerializedName("msg")
    private String msg;
    @SerializedName("data")
    private String data;


    public int getResCode() {
        return resCode;
    }

    public void setResCode(int resCode) {
        this.resCode = resCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
