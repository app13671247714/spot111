package spot.game.model.backModel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import spot.game.model.AppInfo;
import spot.game.model.BILocation;
import spot.game.model.BIPlatform;

/**
 * @Class: BaseInfoResponse
 * @Date: 2022/12/7
 * @Description:
 */
public class BaseInfoResponse {
    @SerializedName("healthIntervalTime")
    private int healthIntervalTime;
    @SerializedName("locationInfoList")
    private List<BILocation> locationInfoList;
    @SerializedName("platformList")
    private List<BIPlatform> platformList;
    @SerializedName("appInfo")
    private AppInfo appInfo;

    public int getHealthIntervalTime() {
        return healthIntervalTime;
    }

    public void setHealthIntervalTime(int healthIntervalTime) {
        this.healthIntervalTime = healthIntervalTime;
    }

    public List<BILocation> getLocationInfoList() {
        return locationInfoList;
    }

    public void setLocationInfoList(List<BILocation> locationInfoList) {
        this.locationInfoList = locationInfoList;
    }

    public List<BIPlatform> getPlatformList() {
        return platformList;
    }

    public void setPlatformList(List<BIPlatform> platformList) {
        this.platformList = platformList;
    }

    public AppInfo getAppInfo() {
        return appInfo;
    }

    public void setAppInfo(AppInfo appInfo) {
        this.appInfo = appInfo;
    }

}
