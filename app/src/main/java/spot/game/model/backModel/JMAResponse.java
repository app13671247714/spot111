package spot.game.model.backModel;

import com.google.gson.annotations.SerializedName;

import net.goal.differences.BuildConfig;

public class JMAResponse {
    @SerializedName("llv")
    int llv = BuildConfig.VERSION_CODE;

    public int getLlv() {
        return llv;
    }

    public void setLlv(int llv) {
        this.llv = llv;
    }

}