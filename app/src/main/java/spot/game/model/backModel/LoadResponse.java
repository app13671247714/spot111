package spot.game.model.backModel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @Class: LoadResponse
 * @Date: 2022/12/16
 * @Description:
 */
public class LoadResponse {
    @SerializedName("userId")
    private String userId;
    @SerializedName("registerTime")
    private long registerTime;
    @SerializedName("abTestInfo")
    AbTestInfo abTestInfo;
    @SerializedName("showSecondAD")
    private boolean showSecondAD; //当天是否展示第二组广告（默认展示2天）
    @SerializedName("showSecondADTimes")
    private int showSecondADTimes; //当天第二组广告总展示次数（默认每天展示3次）
    @SerializedName("adTimeGap")
    private long adTimeGap; // 广告间隔时间；默认120秒
    @SerializedName("isAudit")
    private boolean isAudit;
    @SerializedName("isNew")
    private boolean isNew;

    @SerializedName("ozcb")
    private String isWhite; // 开启正常包 1:正常包 2:白包

    public void setIsWhite(String isWhite) {
        this.isWhite = isWhite;
    }

    public String getIsWhite() {
        return isWhite;
    }

    public boolean isNew() {
        return isNew;
    }

    public void setNew(boolean aNew) {
        isNew = aNew;
    }

    public boolean isShowSecondAD() {
        return showSecondAD;
    }

    public void setShowSecondAD(boolean showSecondAD) {
        this.showSecondAD = showSecondAD;
    }

    public int getShowSecondADTimes() {
        return showSecondADTimes;
    }

    public void setShowSecondADTimes(int showSecondADTimes) {
        this.showSecondADTimes = showSecondADTimes;
    }

    public boolean isAudit() {
        return isAudit;
    }

    public void setAudit(boolean audit) {
        isAudit = audit;
    }

    public long getAdTimeGap() {
        return adTimeGap;
    }

    public void setAdTimeGap(long adTimeGap) {
        this.adTimeGap = adTimeGap;
    }

    public AbTestInfo getAbTestInfo() {
        return abTestInfo;
    }

    public void setAbTestInfo(AbTestInfo abTestInfo) {
        this.abTestInfo = abTestInfo;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public long getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(long registerTime) {
        this.registerTime = registerTime;
    }


    public class AbTestInfo {
        @SerializedName("ab_tags")
        String ab_tags;
        @SerializedName("ab_lists")
        List<ABList> ab_lists;

        public String getAb_tags() {
            return ab_tags;
        }

        public void setAb_tags(String ab_tags) {
            this.ab_tags = ab_tags;
        }

        public List<ABList> getAb_lists() {
            return ab_lists;
        }

        public void setAb_lists(List<ABList> ab_lists) {
            this.ab_lists = ab_lists;
        }

    }

    public class ABList {
        @SerializedName("testId")
        int testId;
        @SerializedName("groupId")
        int groupId;

        public int getTestId() {
            return testId;
        }

        public void setTestId(int testId) {
            this.testId = testId;
        }

        public int getGroupId() {
            return groupId;
        }

        public void setGroupId(int groupId) {
            this.groupId = groupId;
        }

    }


}
