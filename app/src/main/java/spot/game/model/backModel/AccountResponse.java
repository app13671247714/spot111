package spot.game.model.backModel;

/**
 * @Class: AccountResponse
 * @Date: 2022/12/20
 * @Description:
 */
public class AccountResponse {
    //@SerializedName("aclist")
    //List<AccountBean> aclist;
    //@SerializedName("moneyLimit")
    //double moneyLimit;
    //
    //public double getMoneyLimit() {
    //    return moneyLimit;
    //}
    //
    //public void setMoneyLimit(double moneyLimit) {
    //    this.moneyLimit = moneyLimit;
    //}
    //
    //public List<AccountBean> getAclist() {
    //    return aclist;
    //}
    //
    //public void setAclist(List<AccountBean> aclist) {
    //    this.aclist = aclist;
    //}

    public static class AccountBean {
        //@SerializedName("type")
        //String type;
        //@SerializedName("total")
        //double total;
        //@SerializedName("rem")
        double rem;

        //public String getType() {
        //    return type;
        //}
        //
        //public void setType(String type) {
        //    this.type = type;
        //}
        //
        //public double getTotal() {
        //    return total;
        //}
        //
        //public void setTotal(double total) {
        //    this.total = total;
        //}

        public double getRem() {
            return rem;
        }

        public void setRem(double rem) {
            this.rem = rem;
        }

    }
}
