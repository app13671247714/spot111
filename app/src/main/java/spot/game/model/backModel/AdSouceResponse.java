package spot.game.model.backModel;

import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Map;

/**
 * @Class: AdSouceResponse
 * @Date: 2022/12/7
 * @Description:
 */
public class AdSouceResponse {
    @SerializedName("sourceInfoList")
    private List<SourceInfo> sourceInfoList;
    @SerializedName("target")
    private Map<String, Object> target;

    public List<SourceInfo> getSourceInfoList() {
        return sourceInfoList;
    }

    public void setSourceInfoList(List<SourceInfo> sourceInfoList) {
        this.sourceInfoList = sourceInfoList;
    }

    public Map<String, Object> getTarget() {
        return target;
    }

    public void setTarget(Map<String, Object> target) {
        this.target = target;
    }
}
