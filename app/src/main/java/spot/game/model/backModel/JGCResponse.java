package spot.game.model.backModel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import spot.game.model.CollectPieceBean;

public class JGCResponse {
    @SerializedName("mc")
    List<PassRewardConfig> mc;//过关奖励数值
    @SerializedName("ptc")
    CollectPieceConfig ptc;//碎片收集数值
    @SerializedName("wdc")
    WithdrawAllConfig wdc;//提现配置
    @SerializedName("rm")
    BanlanceRateConfig rm;//提现配置
    @SerializedName("pac")
    List<LevelADConfig> pac;//关卡广告配置
    @SerializedName("zcpc")
    List<LevelConfig> zcpc;//关卡难度配置

    public List<LevelADConfig> getPac() {
        return pac;
    }

    public void setPac(List<LevelADConfig> pac) {
        this.pac = pac;
    }

    public List<LevelConfig> getZcpc() {
        return zcpc;
    }

    public void setZcpc(List<LevelConfig> zcpc) {
        this.zcpc = zcpc;
    }

    public WithdrawAllConfig getWdc() {
        return wdc;
    }

    public void setWdc(WithdrawAllConfig wdc) {
        this.wdc = wdc;
    }

    public BanlanceRateConfig getRm() {
        return rm;
    }

    public void setRm(BanlanceRateConfig rm) {
        this.rm = rm;
    }

    public List<PassRewardConfig> getMc() {
        return mc;
    }

    public void setMc(List<PassRewardConfig> mc) {
        this.mc = mc;
    }

    public CollectPieceConfig getPtc() {
        return ptc;
    }

    public void setPtc(CollectPieceConfig ptc) {
        this.ptc = ptc;
    }

    public static class PassRewardConfig {
        @SerializedName("startMoney")
        double startMoney;
        @SerializedName("endMoney")
        double endMoney;
        @SerializedName("minMoney")
        double minMoney;
        @SerializedName("maxMoney")
        double maxMoney;

        public double getStartMoney() {
            return startMoney;
        }

        public void setStartMoney(double startMoney) {
            this.startMoney = startMoney;
        }

        public double getEndMoney() {
            return endMoney;
        }

        public void setEndMoney(double endMoney) {
            this.endMoney = endMoney;
        }

        public double getMinMoney() {
            return minMoney;
        }

        public void setMinMoney(double minMoney) {
            this.minMoney = minMoney;
        }

        public double getMaxMoney() {
            return maxMoney;
        }

        public void setMaxMoney(double maxMoney) {
            this.maxMoney = maxMoney;
        }
    }


    public static class CollectPieceConfig {
        @SerializedName("totalReward")
        double totalReward;
        @SerializedName("tasks")
        List<CollectPieceBean> tasks;

        public double getTotalReward() {
            return totalReward;
        }

        public void setTotalReward(double totalReward) {
            this.totalReward = totalReward;
        }

        public List<CollectPieceBean> getTasks() {
            return tasks;
        }

        public void setTasks(List<CollectPieceBean> tasks) {
            this.tasks = tasks;
        }

    }

    public static class WithdrawAllConfig {
        @SerializedName("sl")
        int sl;//起始等级
        @SerializedName("r1")
        int r1;//签到天数

        @SerializedName("r2")
        int r2;//每日视频个数
        @SerializedName("r3")
        int r3;//提现成功关卡数

        public int getSl() {
            return sl;
        }

        public void setSl(int sl) {
            this.sl = sl;
        }

        public int getR1() {
            return r1;
        }

        public void setR1(int r1) {
            this.r1 = r1;
        }

        public int getR2() {
            return r2;
        }

        public void setR2(int r2) {
            this.r2 = r2;
        }

        public int getR3() {
            return r3;
        }

        public void setR3(int r3) {
            this.r3 = r3;
        }

    }

    public static class BanlanceRateConfig {
        @SerializedName("unit")
        String unit;//货币单位
        @SerializedName("rate")
        String rate;//汇率
        @SerializedName("jiliang")
        String jiliang;//计量比例
        @SerializedName("jiliang_name")
        String jiliang_name;//计量单位

        @SerializedName("scale")
        String scale;//保留小数点位数


        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }

        public String getRate() {
            return rate;
        }

        public void setRate(String rate) {
            this.rate = rate;
        }

        public String getJiliang() {
            return jiliang;
        }

        public void setJiliang(String jiliang) {
            this.jiliang = jiliang;
        }

        public String getJiliang_name() {
            return jiliang_name;
        }

        public void setJiliang_name(String jiliang_name) {
            this.jiliang_name = jiliang_name;
        }

        public String getScale() {
            return scale;
        }

        public void setScale(String scale) {
            this.scale = scale;
        }

    }

    public static class LevelADConfig {
        @SerializedName("minLevel")
        int minLevel;//
        @SerializedName("maxLevel")
        int maxLevel;//
        @SerializedName("lvNum")
        int lvNum;//

        public int getMinLevel() {
            return minLevel;
        }

        public void setMinLevel(int minLevel) {
            this.minLevel = minLevel;
        }

        public int getMaxLevel() {
            return maxLevel;
        }

        public void setMaxLevel(int maxLevel) {
            this.maxLevel = maxLevel;
        }

        public int getLvNum() {
            return lvNum;
        }

        public void setLvNum(int lvNum) {
            this.lvNum = lvNum;
        }

    }

    public static class LevelConfig {
        @SerializedName("minLv")
        int minLevel;//
        @SerializedName("maxLv")
        int maxLevel;//
        @SerializedName("r")
        int lvNum;//

        public int getMinLevel() {
            return minLevel;
        }

        public void setMinLevel(int minLevel) {
            this.minLevel = minLevel;
        }

        public int getMaxLevel() {
            return maxLevel;
        }

        public void setMaxLevel(int maxLevel) {
            this.maxLevel = maxLevel;
        }

        public int getLvNum() {
            return lvNum;
        }

        public void setLvNum(int lvNum) {
            this.lvNum = lvNum;
        }

    }

}
