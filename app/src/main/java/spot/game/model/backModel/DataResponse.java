package spot.game.model.backModel;

import com.google.gson.annotations.SerializedName;

/**
 * @Class: DataResponse
 * @Date: 2022/12/7
 * @Description:
 */
public class DataResponse {
    @SerializedName("tab")
    private int tab;
    @SerializedName("registerTime")
    private long registerTime;
    @SerializedName("needAttribution")
    private boolean needAttribution;
    @SerializedName("userId")
    private String userId;
    @SerializedName("rules")
    private PriceRule rules;

    @SerializedName("ozcb")
    private String isWhite; // 开启正常包 1:正常包 2:白包

    public void setIsWhite(String isWhite) {
        this.isWhite = isWhite;
    }

    public String getIsWhite() {
        return isWhite;
    }


    public PriceRule getRules() {
        return rules;
    }

    public void setRules(PriceRule rules) {
        this.rules = rules;
    }

    public int getTab() {
        return tab;
    }

    public void setTab(int tab) {
        this.tab = tab;
    }

    public long getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(long registerTime) {
        this.registerTime = registerTime;
    }

    public boolean isNeedAttribution() {
        return needAttribution;
    }

    public void setNeedAttribution(boolean needAttribution) {
        this.needAttribution = needAttribution;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
