package spot.game.model.backModel;

public class ResultAnswerResponse {
    //@SerializedName("awMoney")
    double awMoney;
    //@SerializedName("showAdv")
    boolean showAdv;


    public boolean isShowAdv() {
        return showAdv;
    }

    public void setShowAdv(boolean showAdv) {
        this.showAdv = showAdv;
    }

    public double getAwMoney() {
        return awMoney;
    }

    public void setAwMoney(double awMoney) {
        this.awMoney = awMoney;
    }

}
