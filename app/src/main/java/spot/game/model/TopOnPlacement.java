package spot.game.model;

import com.google.gson.annotations.SerializedName;

import net.goal.differences.BuildConfig;

import java.util.Map;

/**
 * @Class: TopOnPlacement
 * @Date: 2022/12/7
 * @Description:
 */
public class TopOnPlacement {
    @SerializedName("placementId")
    String placementId = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE; // 广告源ID
    @SerializedName("sourceType")
    String sourceType = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE; // 广告类型
    @SerializedName("id")
    String id = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE;
    @SerializedName("target")
    private Map<String, Object> target;

    int typeStrategy;//0普通，1高价，客户端自定义变量，无需指定序列化

    public int getTypeStrategy() {
        return typeStrategy;
    }

    public void setTypeStrategy(int typeStrategy) {
        this.typeStrategy = typeStrategy;
    }

    public Map<String, Object> getTarget() {
        return target;
    }

    public void setTarget(Map<String, Object> target) {
        this.target = target;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPlacementId() {
        return placementId;
    }

    public void setPlacementId(String placementId) {
        this.placementId = placementId;
    }

    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }
}

