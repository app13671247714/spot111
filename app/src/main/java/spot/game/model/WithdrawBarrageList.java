package spot.game.model;

import android.graphics.drawable.Drawable;

import com.google.gson.annotations.SerializedName;

import net.goal.differences.BuildConfig;

import java.util.List;

public class WithdrawBarrageList {
    @SerializedName("list_spot_gola")
    List<SpotGolaBarrageBean> list;

    String country = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public List<SpotGolaBarrageBean> getList() {
        return list;
    }

    public void setList(List<SpotGolaBarrageBean> list) {
        this.list = list;
    }

    public class SpotGolaBarrageBean {
        @SerializedName("index_spot_gola")
        int index =  BuildConfig.VERSION_CODE;
        @SerializedName("way_spot_gola")
        String channel = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE;
        @SerializedName("name_spot_gola")
        String name = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE;
        @SerializedName("money_spot_gola")
        String money = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE;

        Drawable headIcon;

        public Drawable getHeadIcon() {
            return headIcon;
        }

        public void setHeadIcon(Drawable headIcon) {
            this.headIcon = headIcon;
        }

        public int getIndex() {
            return index;
        }

        public void setIndex(int index) {
            this.index = index;
        }

        public String getChannel() {
            return channel;
        }

        public void setChannel(String channel) {
            this.channel = channel;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }
    }
}
