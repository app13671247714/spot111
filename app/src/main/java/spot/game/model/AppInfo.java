package spot.game.model;

import com.google.gson.annotations.SerializedName;

/**
 * @Class: AppInfo
 * @Date: 2022/12/26
 * @Description:
 */
public class AppInfo {
    @SerializedName("enAdjust")
    boolean enAdjust;

    public boolean isEnAdjust() {
        return enAdjust;
    }

    public void setEnAdjust(boolean enAdjust) {
        this.enAdjust = enAdjust;
    }
}
