package spot.game.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CollectPieceList {
    @SerializedName("totalReward")
    int totalReward;
    @SerializedName("tasks")
    List<CollectPieceBean> tasks;

    public int getTotalReward() {
        return totalReward;
    }

    public void setTotalReward(int totalReward) {
        this.totalReward = totalReward;
    }

    public List<CollectPieceBean> getTasks() {
        return tasks;
    }

    public void setTasks(List<CollectPieceBean> tasks) {
        this.tasks = tasks;
    }
}
