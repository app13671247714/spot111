package spot.game.model;

import net.goal.differences.BuildConfig;

import spot.game.manager.TrackManager;

/**
 * 日志，action表，不反射则无需加注解
 */
public class JLogExtBean {
    //@SerializedName("event")
    String event = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE;//业务事件
    //@SerializedName("intvalue")
    int intvalue = 0;//整型类数据-统计用，非业务必要则为空
    //@SerializedName("strvalue")
    String strvalue = "";//字符类数据-统计用，非业务必要则为空
    //@SerializedName("dublevalue")
    double dublevalue = 0;//浮点类数值-统计用，非业务必要则为空
    //@SerializedName("logmsg")
    String logmsg = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE;//备注，内容随意
    //@SerializedName("loglevel")
    int loglevel = BuildConfig.VERSION_CODE;//日志级别：0：最高级  1：2、3、4： 5：日志调试级别
    //@SerializedName("steptime")
    long steptime = BuildConfig.VERSION_CODE;//时长, 单位是秒，可以计入时间的，比如page_home  的时间=进入首页的时间-appopen的时间

    public JLogExtBean() {
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public int getIntvalue() {
        return intvalue;
    }

    public void setIntvalue(int intvalue) {
        this.intvalue = intvalue;
    }

    public String getStrvalue() {
        return strvalue;
    }

    public void setStrvalue(String strvalue) {
        this.strvalue = strvalue;
    }

    public double getDublevalue() {
        return dublevalue;
    }

    public void setDublevalue(double dublevalue) {
        this.dublevalue = dublevalue;
    }

    public String getLogmsg() {
        return logmsg;
    }

    public void setLogmsg(String logmsg) {
        this.logmsg = logmsg;
    }

    public int getLoglevel() {
        return loglevel;
    }

    public void setLoglevel(int loglevel) {
        this.loglevel = loglevel;
    }

    public long getSteptime() {
        return steptime;
    }

    public void setSteptime(long steptime) {
        this.steptime = steptime;
    }

    private JLogExtBean(Builder builder) {
        this.event = builder.event;
        this.intvalue = builder.intvalue;
        this.strvalue = builder.strvalue;
        this.dublevalue = builder.dublevalue;
        this.logmsg = builder.logmsg;
        this.loglevel = builder.loglevel;
        this.steptime = builder.steptime;
    }

    // 省略getter方法

    public static class Builder {
        String event = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE;//业务事件
        int intvalue = 0;//整型类数据-统计用，非业务必要则为空
        String strvalue = "";//字符类数据-统计用，非业务必要则为空
        double dublevalue = 0;//浮点类数值-统计用，非业务必要则为空
        String logmsg = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE;//备注，内容随意
        int loglevel = BuildConfig.VERSION_CODE;//日志级别：0：最高级  1：2、3、4： 5：日志调试级别
        long steptime = BuildConfig.VERSION_CODE;//时长, 单位是秒，可以计入时间的，比如page_home  的时间=进入首页的时间-appopen的时间


        private Builder setEvent(String event) {
            this.event = event;
            return this;
        }


        public Builder setIntvalue(int intvalue) {
            this.intvalue = intvalue;
            return this;
        }


        public Builder setStrvalue(String strvalue) {
            this.strvalue = strvalue;
            return this;
        }


        public Builder setDublevalue(double dublevalue) {
            this.dublevalue = dublevalue;
            return this;
        }


        public Builder setLogmsg(String logmsg) {
            this.logmsg = logmsg;
            return this;
        }


        public Builder setLoglevel(int loglevel) {
            this.loglevel = loglevel;
            return this;
        }


        public Builder setSteptime(long steptime) {
            this.steptime = steptime;
            return this;
        }


        public Builder() {
            //默认值
            setLoglevel(TrackManager.LOG_LEVEL_5);
            setLogmsg("");
            setSteptime(0);
        }


        public JLogExtBean build() {
            return new JLogExtBean(this);
        }
    }

    public static Builder event(String event) {
        return event(event, TrackManager.LOG_LEVEL_5);
    }

    public static Builder event(String event, int logLevel) {
        return new Builder().setEvent(event).setLoglevel(logLevel);
    }

    public static Builder event(String event, String logmsg) {
        return new Builder().setEvent(event).setLogmsg(logmsg);
    }
}
