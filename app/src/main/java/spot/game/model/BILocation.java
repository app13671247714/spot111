package spot.game.model;

import com.google.gson.annotations.SerializedName;

import net.goal.differences.BuildConfig;

/**
 * @Class: BILocation
 * @Date: 2022/12/7
 * @Description:
 */
public class BILocation {
    @SerializedName("adLocationCode")
    private String adLocationCode = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE;

    public String getAdLocationCode() {
        return adLocationCode;
    }

    public void setAdLocationCode(String adLocationCode) {
        this.adLocationCode = adLocationCode;
    }
}
