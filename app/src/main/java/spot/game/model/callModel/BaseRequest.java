package spot.game.model.callModel;

import com.google.gson.annotations.SerializedName;

import net.goal.differences.BuildConfig;

import java.util.HashMap;
import java.util.Map;

import spot.game.manager.GameManager;

public class BaseRequest {
    @SerializedName("m")
    protected String m = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE;
    @SerializedName("commonInfo")
    protected Map<String, Object> commonInfo;


    public BaseRequest() {
        buildDefaultCommonInfo();
    }

    public BaseRequest(String m) {
        this.m = m;
        buildDefaultCommonInfo();
    }


    public void buildDefaultCommonInfo() {
        HashMap<String, Object> objectMap = new HashMap<>();
        objectMap.putAll(GameManager.buildCommonInfo());
        addCommonInfo(objectMap);
    }


    public void addCommonInfo(Map<String, Object> otherMap) {
        if (otherMap == null) {
            return;
        }
        if (commonInfo == null) {
            if (otherMap != null && !otherMap.isEmpty()) {
                commonInfo = new HashMap<>(otherMap.size());
            } else {
                commonInfo = new HashMap<>();
            }
        }
        commonInfo.putAll(otherMap);
    }


    public void setCommonInfo(Map<String, Object> otherMap) {
        commonInfo = otherMap;
    }


    public String getM() {
        return m;
    }

    public Map<String, Object> getCommonInfo() {
        return commonInfo;
    }

    public void setM(String m) {
        this.m = m;
    }

}
