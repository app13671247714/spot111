package spot.game.model.callModel;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

/**
 * @Class: ReportRequest
 * @Date: 2022/12/7
 * @Description:
 */
public class ReportRequest extends BaseRequest {
    @SerializedName("extendParam")
    private Map<String, Object> extendParam = new HashMap<>();
    @SerializedName("target")
    private Map<String, Object> target = new HashMap<>();

    public Map<String, Object> getExtendParam() {
        return extendParam;
    }

    public void setExtendParam(Map<String, Object> extendParam) {
        this.extendParam = extendParam;
    }

    public Map<String, Object> getTarget() {
        return target;
    }

    public void setTarget(Map<String, Object> target) {
        this.target = target;
    }

}
