package spot.game.model.callModel;


import com.google.gson.annotations.SerializedName;

import java.util.UUID;

import spot.game.manager.ConfigManager;
import spot.game.manager.GameManager;
import spot.game.util.DeviceUtils;

/**
 * @Class: BaseInfoRequest
 * @Date: 2022/12/7
 * @Description:
 */
public class BaseInfoRequest {
    @SerializedName("requestTime")
    private long requestTime = System.currentTimeMillis();
    @SerializedName("country")
    private String country = ConfigManager.getCountry();
    @SerializedName("chour")
    private int chour = DeviceUtils.getChour();
    @SerializedName("requestId")
    private String requestId = UUID.randomUUID().toString();
    @SerializedName("displayCountry")
    private String displayCountry = ConfigManager.getCountry();
    @SerializedName("timeZone")
    private String timeZone = ConfigManager.getTimeZone();
    @SerializedName("appCode")
    private String appCode = ConfigManager.getAppCode();
    @SerializedName("userId")
    private String userId = ConfigManager.getUserId();
    @SerializedName("secret")
    private String secret = GameManager.getAppSecret(requestTime, requestId);
    @SerializedName("m")
    private String m;

    public void setM(String m) {
        this.m = m;
    }

}
