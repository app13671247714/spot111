package spot.game.model.callModel;

import com.google.gson.annotations.SerializedName;

/**
 * @Class: AttributionRequest
 * @Date: 2022/12/26
 * @Description:
 */
public class AttributionRequest extends BaseRequest {
    @SerializedName("adgroup")
    private String adgroup;
    @SerializedName("adid")
    private String adid;
    @SerializedName("campaign")
    private String campaign;
    @SerializedName("clickLabel")
    private String clickLabel;
    @SerializedName("costAmount")
    private double costAmount;
    @SerializedName("costCurrency")
    private String costCurrency;
    @SerializedName("costType")
    private String costType;
    @SerializedName("creative")
    private String creative;
    @SerializedName("event")
    private String event;
    @SerializedName("network")
    private String network;

    public void setAdgroup(String adgroup) {
        this.adgroup = adgroup;
    }

    public void setAdid(String adid) {
        this.adid = adid;
    }

    public void setCampaign(String campaign) {
        this.campaign = campaign;
    }

    public void setClickLabel(String clickLabel) {
        this.clickLabel = clickLabel;
    }


    public double getCostAmount() {
        return costAmount;
    }

    public void setCostAmount(double costAmount) {
        this.costAmount = costAmount;
    }

    public void setCostCurrency(String costCurrency) {
        this.costCurrency = costCurrency;
    }

    public void setCostType(String costType) {
        this.costType = costType;
    }

    public void setCreative(String creative) {
        this.creative = creative;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public void setNetwork(String network) {
        this.network = network;
    }

    public void setTrackerName(String trackerName) {
        this.trackerName = trackerName;
    }

    public void setTrackerToken(String trackerToken) {
        this.trackerToken = trackerToken;
    }

    public void setDeepLinkUrl(String deepLinkUrl) {
        this.deepLinkUrl = deepLinkUrl;
    }

    public void setGgClickTime(String ggClickTime) {
        this.ggClickTime = ggClickTime;
    }

    public void setFbInstallReferrer(String fbInstallReferrer) {
        this.fbInstallReferrer = fbInstallReferrer;
    }

    @SerializedName("trackerName")
    private String trackerName;
    @SerializedName("trackerToken")
    private String trackerToken;
    @SerializedName("deepLinkUrl")
    private String deepLinkUrl;
    @SerializedName("ggClickTime")
    private String ggClickTime;
    @SerializedName("fbInstallReferrer")
    private String fbInstallReferrer;


    public String getAdgroup() {
        return adgroup;
    }

    public String getAdid() {
        return adid;
    }

    public String getCampaign() {
        return campaign;
    }

    public String getClickLabel() {
        return clickLabel;
    }

    public String getCostCurrency() {
        return costCurrency;
    }

    public String getCostType() {
        return costType;
    }

    public String getCreative() {
        return creative;
    }

    public String getEvent() {
        return event;
    }

    public String getNetwork() {
        return network;
    }

    public String getTrackerName() {
        return trackerName;
    }

    public String getTrackerToken() {
        return trackerToken;
    }

    public String getDeepLinkUrl() {
        return deepLinkUrl;
    }

    public String getGgClickTime() {
        return ggClickTime;
    }

    public String getFbInstallReferrer() {
        return fbInstallReferrer;
    }


}
