package spot.game.model.callModel;

import com.google.gson.annotations.SerializedName;

import net.goal.differences.BuildConfig;

/**
 * @Class: ZeusEvent
 * @Date: 2022/12/20
 * @Description:
 */
public class ZeusEvent {
    @SerializedName("event")
    private String event = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE;
    @SerializedName("money")
    private String money = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE;

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

}
