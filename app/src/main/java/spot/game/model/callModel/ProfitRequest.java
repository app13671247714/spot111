package spot.game.model.callModel;

import com.google.gson.annotations.SerializedName;

import spot.game.constant.BusinessFieldName;

/**
 * @Class: ProfitRequest
 * @Date: 2022/12/17
 * @Description:
 */
public class ProfitRequest extends BaseRequest {
    public static final String ANSWER_REWARD = "v2_pass", DOUBLE_REWARD = "v2_passd", TASK_REWARD = "answer_task", TIPS_REWARD = "tips_reward";
    @SerializedName("biztype")
    private String biztype;//v2_pass:通关 v2_passd:通关翻倍
    @SerializedName("money")
    private double money;//钞票奖励


    public ProfitRequest() {
        m = BusinessFieldName.M_GET_PROFIT;
    }


    public String getBiztype() {
        return biztype;
    }

    public void setBiztype(String biztype) {
        this.biztype = biztype;
    }

}
