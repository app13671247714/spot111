package spot.game.model.callModel;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

/**
 * @Class: TrackRequest
 * @Date: 2022/12/16
 * @Description:
 */
public class TrackRequest extends BaseRequest {
    @SerializedName("extendParam")
    private Map<String, Object> extendParam = new HashMap<>();


    public Map<String, Object> getExtendParam() {
        return extendParam;
    }

    public void setExtendParam(Map<String, Object> extendParam) {
        this.extendParam = extendParam;
    }

}
