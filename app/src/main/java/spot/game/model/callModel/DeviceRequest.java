package spot.game.model.callModel;

import android.os.Build;

import com.blankj.utilcode.util.SPStaticUtils;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

import spot.game.constant.BusinessFieldName;
import spot.game.manager.ConfigManager;
import spot.game.manager.GameManager;
import spot.game.util.DeviceUtils;

/**
 * @Class: DeviceRequest
 * @Date: 2022/12/7
 * @Description:
 */
public class DeviceRequest {
    @SerializedName("googleId")
    private String googleId = SPStaticUtils.getString(BusinessFieldName.GID, "");
    @SerializedName("country")
    private String country = ConfigManager.getCountry();
    @SerializedName("isRoot")
    private boolean isRoot = false;
    @SerializedName("enableAdb")
    private boolean enableAdb = SPStaticUtils.getBoolean(BusinessFieldName.enableadb, false);
    @SerializedName("dewidth")
    private int dewidth = SPStaticUtils.getInt(BusinessFieldName.dewidth, 0);
    @SerializedName("channel")
    private String channel = ConfigManager.getChannel();
    @SerializedName("language")
    private String language = GameManager.getLanguage();
    @SerializedName("lv")
    private String lv = "-1";
    @SerializedName("appCode")
    private String appCode = ConfigManager.getAppCode();
    @SerializedName("secret")
    private String secret;
    @SerializedName("mac")
    private String mac = SPStaticUtils.getString(BusinessFieldName.mac, "");
    @SerializedName("lanList")
    private List<String> lanList;
    @SerializedName("network")
    private String network = SPStaticUtils.getString(BusinessFieldName.network, "wifi");
    @SerializedName("adSdk")
    private int adSdk = 2;
    @SerializedName("requestId")
    private String requestId = UUID.randomUUID().toString();
    @SerializedName("displayCountry")
    private String displayCountry = ConfigManager.getCountry();
    @SerializedName("model")
    private String model = Build.MODEL;
    @SerializedName("brand")
    private String brand = Build.BRAND;
    @SerializedName("osRom")
    private String osRom = SPStaticUtils.getString(BusinessFieldName.osrom, "");
    @SerializedName("androidId")
    private String androidId = SPStaticUtils.getString(BusinessFieldName.androidid, "");
    @SerializedName("smId")
    private String smId = "";
    @SerializedName("deheight")
    private int deheight = SPStaticUtils.getInt(BusinessFieldName.deheight, 0);
    @SerializedName("chour")
    private int chour = DeviceUtils.getChour();
    @SerializedName("deVersion")
    private String deVersion = "Android" + Build.VERSION.RELEASE;
    @SerializedName("wifiProxy")
    private boolean wifiProxy = SPStaticUtils.getBoolean(BusinessFieldName.wifiproxy, false);
    @SerializedName("timeZone")
    private String timeZone = SPStaticUtils.getString(BusinessFieldName.timeZone, "");
    @SerializedName("isCharging")
    private boolean isCharging = SPStaticUtils.getBoolean(BusinessFieldName.ischarging, false);
    @SerializedName("userId")
    private String userId = ConfigManager.getUserId();
    @SerializedName("vc")
    private int vc = ConfigManager.getVc();
    @SerializedName("deDensity")
    private String deDensity = SPStaticUtils.getString(BusinessFieldName.dedensity, "");
    @SerializedName("requestTime")
    private long requestTime = System.currentTimeMillis();
    @SerializedName("deviationX")
    private String deviationX = "";
    @SerializedName("vpn")
    private boolean vpn = SPStaticUtils.getInt(BusinessFieldName.vpn_use, 0) == 1 ? true : false;
    @SerializedName("clientIp")
    private String clientIp = "";
    @SerializedName("vn")
    private String vn = ConfigManager.getVn();
    @SerializedName("simStatus")
    private boolean simStatus = (SPStaticUtils.getInt(BusinessFieldName.simstatus, 0) == 1 ? true : false);
    @SerializedName("deviationZ")
    private String deviationZ = "";
    @SerializedName("deviationY")
    private String deviationY = "";
    @SerializedName("m")
    private String m = "R";

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public int getChour() {
        return chour;
    }

    public void setChour(int chour) {
        this.chour = chour;
    }

    public long getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(long requestTime) {
        this.requestTime = requestTime;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }
}
