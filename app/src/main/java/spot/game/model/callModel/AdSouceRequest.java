package spot.game.model.callModel;


import com.google.gson.annotations.SerializedName;

import net.goal.differences.BuildConfig;

/**
 * @Class: AdSouceRequest
 * @Date: 2022/12/7
 * @Description:
 */
public class AdSouceRequest extends BaseRequest {
    @SerializedName("adLocationCode")
    private String adLocationCode = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE;

    public void setAdLocationCode(String adLocationCode) {
        this.adLocationCode = adLocationCode;
    }
}
