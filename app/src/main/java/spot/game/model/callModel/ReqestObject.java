package spot.game.model.callModel;

import com.google.gson.annotations.SerializedName;

/**
 * @Class: ReqestObject
 * @Date: 2022/12/26
 * @Description:
 */
public class ReqestObject {
    @SerializedName("adId")
    private String adId;
    @SerializedName("rType")
    private int rType;
    @SerializedName("codeId")
    private String codeId;
    @SerializedName("rMsg")
    private String rMsg;

    public String getAdId() {
        return adId;
    }

    public void setAdId(String adId) {
        this.adId = adId;
    }

    public int getrType() {
        return rType;
    }

    public void setrType(int rType) {
        this.rType = rType;
    }

    public String getCodeId() {
        return codeId;
    }

    public void setCodeId(String codeId) {
        this.codeId = codeId;
    }

    public String getrMsg() {
        return rMsg;
    }

    public void setrMsg(String rMsg) {
        this.rMsg = rMsg;
    }


}
