package spot.game.model;


import net.goal.differences.BuildConfig;

/**
 * @Class: DefaultConfigBean
 * @Date: 2022/11/8
 * @Description:
 */
public class DefaultConfigBean {
    // 应用唯一编码
    //@SerializedName("appCode")
    private String appCode = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE;

    // 是否启用debug模式，debug tag:JKSDK
    //@SerializedName("debug")
    private boolean debug;
    // APP名字
    //@SerializedName("appName")
    private String appName = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE;
    // APP版本号
    //@SerializedName("appVersionCode")
    private int appVersionCode = BuildConfig.VERSION_CODE;
    // APP版本名
    //@SerializedName("appVersionName")
    private String appVersionName = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE;
    // 渠道名
    //@SerializedName("channel")
    private String channel = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE;


    public String getChannel() {
        return channel;
    }

    public String getAppCode() {
        return appCode;
    }


    public boolean isDebug() {
        return debug;
    }

    public String getAppName() {
        return appName;
    }

    public int getAppVersionCode() {
        return appVersionCode;
    }

    public String getAppVersionName() {
        return appVersionName;
    }

    private DefaultConfigBean(Builder builder) {
        this.appCode = builder.appCode;
        this.debug = builder.debug;
        this.appName = builder.appName;
        this.appVersionCode = builder.appVersionCode;
        this.appVersionName = builder.appVersionName;
        this.channel = builder.channel;
    }

    public static class Builder {
        //@SerializedName("appCode")
        private String appCode = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE;
        //@SerializedName("debug")
        private boolean debug;
        //@SerializedName("appName")
        private String appName = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE;
        //@SerializedName("appVersionCode")
        private int appVersionCode = BuildConfig.VERSION_CODE;
        //@SerializedName("appVersionName")
        private String appVersionName = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE;
        //@SerializedName("channel")
        private String channel = BuildConfig.APP_CODE + BuildConfig.VERSION_CODE;


        public Builder channel(String channel) {
            this.channel = channel;
            return this;
        }

        public Builder appCode(String appCode) {
            this.appCode = appCode;
            return this;
        }


        public Builder debug(boolean debug) {
            this.debug = debug;
            return this;
        }

        public Builder appName(String appName) {
            this.appName = appName;
            return this;
        }

        public Builder appVersionCode(int appVersionCode) {
            this.appVersionCode = appVersionCode;
            return this;
        }

        public Builder appVersionName(String appVersionName) {
            this.appVersionName = appVersionName;
            return this;
        }

        public DefaultConfigBean build() {
            return new DefaultConfigBean(this);
        }

    }
}
