package spot.game.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ClickUtils;
import com.bumptech.glide.Glide;

import net.goal.differences.R;

import java.util.ArrayList;
import java.util.List;

import spot.game.model.TreasureBean;
import spot.game.util.FastClickUtil;

/**
 * @Class: WithdrawAdapter
 * @Date: 2022/12/16
 * @Description:
 */
public class TreasureAdapter extends RecyclerView.Adapter<TreasureAdapter.ViewHolder> {

    private List<TreasureBean> amountList = new ArrayList<>();
    protected OnItemClickListener mClickListener;


    public void setData(List<TreasureBean> list) {
        if (amountList == null) {
            amountList = new ArrayList<>();
        }
        amountList.clear();
        //
        if (list == null || list.isEmpty()) {
            return;
        }
        amountList.addAll(list);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.game_spot_item_treasure, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        TreasureBean amount = amountList.get(position);
        try {
            Glide.with(holder.itemView.getContext()).load(amount.getIconUrl()).into(holder.ivIcon);
        } catch (Throwable e){
            e.printStackTrace();
        }
        //设置点击事件
        holder.itemView.setOnClickListener(new ClickUtils.OnDebouncingClickListener(FastClickUtil.S_SPACE_TIME) {
            @Override
            public void onDebouncingClick(View view) {
                if (mClickListener != null) {
                    mClickListener.onItemClick(view, amount, position);
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return amountList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView ivIcon;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ivIcon = itemView.findViewById(R.id.iv_game_spot_item_treasure_icon);
        }
    }

    public void setOnItemClickListener(OnItemClickListener mListener) {
        this.mClickListener = mListener;
    }

    /***************************inner class area***********************************/
    public interface OnItemClickListener {
        void onItemClick(View view, TreasureBean pos, int position);
    }

}
