package spot.game.adapter;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import spot.game.fragment.BaseFragment;

/**
 * Created by MaXin on 2019-09-10.
 */
public class BaseFragmentAdapter extends FragmentPagerAdapter {

    private List<BaseFragment> mFragmentList = new ArrayList<BaseFragment>();

    public BaseFragmentAdapter setList(List<BaseFragment> mFragment) {
        mFragmentList.clear();
        mFragmentList.addAll(mFragment);
        return this;
    }

    public BaseFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

}
