package spot.game.adapter;

import android.graphics.Color;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ClickUtils;

import net.goal.differences.R;

import java.util.ArrayList;
import java.util.List;

import spot.differences.gola.GolaViewTVS;
import spot.game.model.CollectPieceBean;
import spot.game.util.FastClickUtil;
import spot.game.util.ViewUtils;

/**
 * @Class: WithdrawAdapter
 * @Date: 2022/12/16
 * @Description:
 */
public class CollectPieceAdapter extends RecyclerView.Adapter<CollectPieceAdapter.ViewHolder> {

    private List<CollectPieceBean> amountList = new ArrayList<>();
    protected OnItemClickListener mClickListener;


    public void setData(List<CollectPieceBean> list) {
        if (amountList == null) {
            amountList = new ArrayList<>();
        }
        amountList.clear();
        //
        if (list == null || list.isEmpty()) {
            return;
        }
        amountList.addAll(list);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.game_spot_item_collect_piece, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CollectPieceBean amount = amountList.get(position);
        int taskNum = amount.getTotalProgress();
        int userNum = amount.getCurProgress();
        holder.tvProgress.setText(userNum + "/" + taskNum);
        //
        //Drawable drawableLeft = ContextCompat.getDrawable(holder.itemView.getContext(), GameManager.getSingleRewardIconSmall());
        //drawableLeft.setBounds(0, 0, drawableLeft.getMinimumWidth(), drawableLeft.getMinimumHeight());
        //holder.tvReward.setCompoundDrawables(drawableLeft, null, null, null);
        ViewUtils.autoResizeText(holder.tvReward, "x" + amount.getRewardCount(), 12, 8, 20);
        //
        holder.tvTaskDesc.setText(Html.fromHtml(amount.getTaskDesc()));
        if (userNum > taskNum) {
            userNum = 100;
        } else {
            userNum = userNum * 100 / taskNum;
        }
        if (userNum > 0 && userNum < 10) {
            userNum = 10;
        }
        holder.progressBar.setProgress(userNum);
        holder.progressBar.setMax(100);
        switch (amount.getState()) {
            case CollectPieceBean.COLLECT_PIECE_COMPLETE:
                holder.itemView.setEnabled(true);
                holder.tvBtn.setStrokeColor(Color.parseColor("#09782B"));
                holder.tvBtn.setText(amount.getBtnText());
                holder.tvBtn.setBackgroundResource(R.mipmap.game_spot_bg_btn_find_diff_tips);
                break;
            case CollectPieceBean.COLLECT_PIECE_DOING:
                holder.itemView.setEnabled(false);
                holder.tvBtn.setStrokeColor(Color.parseColor("#6B430A"));
                holder.tvBtn.setText(amount.getBtnText());
                holder.tvBtn.setBackgroundResource(R.mipmap.game_spot_bg_collect_peice_task_receive_doing);
                break;
            default:
                holder.itemView.setEnabled(false);
                holder.tvBtn.setStrokeColor(Color.parseColor("#595755"));
                holder.tvBtn.setText(amount.getBtnText());
                holder.tvBtn.setBackgroundResource(R.mipmap.game_spot_bg_game_spot_collect_peice_task_receive_lock);
                break;
        }
        //设置点击事件
        holder.itemView.setOnClickListener(new ClickUtils.OnDebouncingClickListener(FastClickUtil.S_SPACE_TIME) {
            @Override
            public void onDebouncingClick(View view) {
                if (mClickListener != null) {
                    mClickListener.onItemClick(view, amount, position);
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return amountList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTaskDesc, tvReward, tvProgress;
        GolaViewTVS tvBtn;
        ImageView ivIcon;
        public ProgressBar progressBar;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ivIcon = itemView.findViewById(R.id.iv_game_spot_item_collect_piece_icon);
            tvReward = itemView.findViewById(R.id.tv_game_spot_item_collect_piece_desc);
            tvTaskDesc = itemView.findViewById(R.id.tv_game_spot_item_collect_piece_task_desc);
            progressBar = itemView.findViewById(R.id.pb_game_spot_item_collect_piece_item_progress);
            tvProgress = itemView.findViewById(R.id.tv_game_spot_item_collect_piece_item_progress_content);
            tvBtn = itemView.findViewById(R.id.tv_game_spot_item_collect_piece_btn);
        }
    }

    public void setOnItemClickListener(OnItemClickListener mListener) {
        this.mClickListener = mListener;
    }

    /***************************inner class area***********************************/
    public interface OnItemClickListener {
        void onItemClick(View view, CollectPieceBean pos, int position);
    }

}
