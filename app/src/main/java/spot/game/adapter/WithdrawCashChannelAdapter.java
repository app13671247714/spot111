package spot.game.adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ClickUtils;

import net.goal.differences.BuildConfig;
import net.goal.differences.R;

import java.util.ArrayList;
import java.util.List;

import spot.game.manager.GameManager;
import spot.game.manager.IconSelectManager;
import spot.game.util.FastClickUtil;

public class WithdrawCashChannelAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<String> amountList = new ArrayList<>();
    private int mCurrentPos = BuildConfig.VERSION_CODE;
    protected OnItemClickListener mClickListener;

    public void setData(List<String> list) {
        if (list == null || list.isEmpty()) {
            return;
        }
        if (amountList == null) {
            amountList = new ArrayList<>();
        }
        amountList.clear();
        amountList.addAll(list);
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.game_spot_item_withdraw_cash_channel, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        String platform = amountList.get(position);
            ViewHolder holder1 = (ViewHolder) holder;
            if (mCurrentPos == position) {
                holder1.ivSelectMask.setVisibility(View.VISIBLE);
                holder1.ivChecked.setVisibility(View.VISIBLE);
            } else {
                holder1.ivSelectMask.setVisibility(View.INVISIBLE);
                holder1.ivChecked.setVisibility(View.INVISIBLE);
            }
            holder1.ivChannel.setImageResource(IconSelectManager.getChannelResId(platform));
            //设置点击事件
            holder1.itemView.setOnClickListener(new ClickUtils.OnDebouncingClickListener(FastClickUtil.S_SPACE_TIME) {
                @Override
                public void onDebouncingClick(View view) {
                    if (mClickListener != null) {
                        mClickListener.onItemClick(view, position);
                    }
                }
            });
    }

    public void setSelectItem(int pos) {
        mCurrentPos = pos;
        notifyDataSetChanged();
    }

    public String getSelectItem() {
        return amountList.get(mCurrentPos);
    }

    public int getCurrentSelectItem() {
        return mCurrentPos;
    }

    @Override
    public int getItemCount() {
        return amountList.size();
    }

    @Override
    public int getItemViewType(int position) {
//        WithdrawAllResponse.Platform platform = amountList.get(position);
//        if (platform.getName().equalsIgnoreCase("ovo") ||
//                platform.getName().equalsIgnoreCase("shopeepay") ||
//                platform.getName().equalsIgnoreCase("dana")) {
//            return 1;
//        } else {
//            return 2;
//        }
        return 1;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView ivChannel, ivSelectMask , ivChecked;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ivChannel = itemView.findViewById(R.id.iv_game_spot_item_withdraw_all_channel);
            ivSelectMask =  itemView.findViewById(R.id.iv_game_spot_item_withdraw_all_channel_select);
            ivChecked = itemView.findViewById(R.id.iv_game_spot_item_withdraw_all_channel_checked);
        }
    }


    public void setOnItemClickListener(OnItemClickListener mListener) {
        this.mClickListener = mListener;
    }

    /***************************inner class area***********************************/
    public interface OnItemClickListener {
        void onItemClick(View view, int pos);
    }

}