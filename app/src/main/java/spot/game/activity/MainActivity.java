package spot.game.activity;

import android.animation.ValueAnimator;
import android.os.Bundle;
import android.view.View;

import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;

import com.blankj.utilcode.util.BarUtils;
import com.blankj.utilcode.util.ClickUtils;

import net.goal.differences.R;
import net.goal.differences.databinding.GameSpotActivityMainBinding;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.Disposable;
import spot.differences.gola.GolaSSUserSettingWindow;
import spot.game.adapter.BaseFragmentAdapter;
import spot.game.event.EventConstant;
import spot.game.event.EventMsg;
import spot.game.event.RxBus;
import spot.game.fragment.BaseFragment;
import spot.game.fragment.MainFindDiffFragment;
import spot.game.fragment.MainTreasureFragment;
import spot.game.manager.ADManager;
import spot.game.manager.BalanceManager;
import spot.game.manager.ConfigManager;
import spot.game.manager.ContextManager;
import spot.game.manager.GameManager;
import spot.game.manager.IconSelectManager;
import spot.game.manager.TrackManager;
import spot.game.model.JLogExtBean;
import spot.game.model.backModel.AccountResponse;
import spot.game.util.NotifyUtil;


public class MainActivity extends BaseActivity {
    private GameSpotActivityMainBinding mBinding;

    private MainFindDiffFragment mMainFindDiffFragment; // 找茬
    private MainTreasureFragment mTreasureFragment; // 宝藏图鉴

    private List<BaseFragment> mFragment = new ArrayList<>();
    private ValueAnimator mAmountGrowthAnim;

    private float mLastPrice = 0.0F; // 上一次余额
    private boolean isAnalyze; // 是否是审核版本

    @Override
    protected View getLayoutView() {
        if (mBinding!= null){
            return mBinding.getRoot();
        }
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mBinding = GameSpotActivityMainBinding.inflate(getLayoutInflater());
        super.onCreate(savedInstanceState);

        BarUtils.setStatusBarVisibility(this, false);
        BarUtils.setNavBarVisibility(this, false);
        //
        ADManager.INSTANCE().initTopOnSDK("", ConfigManager.getMaxSDKKey());
        //
        mBinding.ivGameSpotActivityMainMoneyCurrency.setImageResource(IconSelectManager.getSingleRewardIcon());
        //
        mLastPrice = ConfigManager.getBalance();
        //
        isAnalyze = ConfigManager.isAudit();
        if (isAnalyze) {
            mBinding.rlGameSpotActivityMainTitleBar.setVisibility(View.GONE);
            mBinding.tvGameSpotActivityMainMoneyTxTips.setVisibility(View.GONE);
            //mTab.setVisibility(View.GONE);
        } else {
            mBinding.rlGameSpotActivityMainTitleBar.setVisibility(View.VISIBLE);
            //mTab.setVisibility(View.VISIBLE);
        }
        //
        initView(savedInstanceState);
        initTabNav();
        initUserAmountMsg();
        initVP();
        //
        mBinding.tvGameSpotActivityMainTotalPrice.setText(ConfigManager.getCurrency() + BalanceManager.getFormatBalance());
        //
        //    startService(new Intent(this, game_spotService.class));
    }

    private void initView(Bundle savedInstanceState) {

        if (savedInstanceState != null) {
            FragmentManager manager = getSupportFragmentManager();
            mMainFindDiffFragment = (MainFindDiffFragment) manager.getFragment(savedInstanceState, "mMainFindDiffFragment");
            //mMineFragment = (MineFragment) manager.getFragment(savedInstanceState, "mMineFragment");
            mTreasureFragment = (MainTreasureFragment) manager.getFragment(savedInstanceState, "mTreasureFragment");
            //mWithdrawRecordFragment = (WithdrawRecordFragment) manager.getFragment(savedInstanceState, "mWithdrawRecordFragment");
        }
        if (mMainFindDiffFragment == null) {
            mMainFindDiffFragment = MainFindDiffFragment.getInstance();
        }
        //if (mMineFragment == null) {
        //    mMineFragment = MineFragment.getInstance();
        //}
        if (mTreasureFragment == null) {
            mTreasureFragment = MainTreasureFragment.getInstance();
        }
        //if (mWithdrawRecordFragment == null) {
        //    mWithdrawRecordFragment = WithdrawRecordFragment.getInstance();
        //}

        mFragment.clear();
        //if (isAnalyze) {
        //    mFragment.add(mQuizFragment);
        //    mFragment.add(mMainSettingFragment);
        //} else {
        mFragment.add(mMainFindDiffFragment);
        //mFragment.add(mMineFragment);
        mFragment.add(mTreasureFragment);
        //mFragment.add(mWithdrawRecordFragment);
        //}

        mBinding.vpGameSpotActivityMain.setAdapter(new BaseFragmentAdapter(getSupportFragmentManager()).setList(mFragment));
        mBinding.vpGameSpotActivityMain.setOffscreenPageLimit(mFragment.size());

        View.OnClickListener onClickListener = new ClickUtils.OnDebouncingClickListener(500) {
            @Override
            public void onDebouncingClick(View v) {
                GameManager.newClick();
                switch (v.getId()) {
                    case R.id.tv_game_spot_activity_main_money_tx_tips:
                    case R.id.rl_game_spot_activity_main_money_title_bar:
//                // 切换到我的页面
//                TrackManager.sendTrack("page", "page_mine", "");
                        amountClick(1);
                        break;
                    case R.id.iv_game_spot_activity_main_money_setting:
                        TrackManager.sendTrack("setting", "setting_click", "");
                        GolaSSUserSettingWindow.showSettingWindow(MainActivity.this);
                        break;
                }
            }
        };

        mBinding.rlGameSpotActivityMainMoneyTitleBar.setOnClickListener(onClickListener);
        mBinding.ivGameSpotActivityMainMoneySetting.setOnClickListener(onClickListener);
        mBinding.tvGameSpotActivityMainMoneyTxTips.setOnClickListener(onClickListener);
    }

    public void onClick(View view) {

    }

    /**
     * 点击余额
     *
     * @param amountType 1：红包余额  2：元宝余额
     */
    private void amountClick(int amountType) {
        //if (amountType == 1) {
        //MineFragment.setPageType(1);
        //mineTxSelect = 1;
        //if (mVp != null) mVp.setCurrentItem(2, false);
        //} else {
        //MineFragment.setPageType(0);
        //mineTxSelect = 2;
        //if (mVp != null) mVp.setCurrentItem(2, false);
        //}
        GameManager.intentToMine(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("android:support:fragments", null);
        //
        FragmentManager manager = getSupportFragmentManager();

        if (mMainFindDiffFragment != null && mMainFindDiffFragment.isAdded()) {
            manager.putFragment(outState, "mMainFindDiffFragment", mMainFindDiffFragment);
        }
        //if (mMineFragment != null && mMineFragment.isAdded()) {
        //    manager.putFragment(outState, "mMineFragment", mMineFragment);
        //}
        if (mTreasureFragment != null && mTreasureFragment.isAdded()) {
            manager.putFragment(outState, "mTreasureFragment", mTreasureFragment);
        }
        //if (mWithdrawRecordFragment != null && mWithdrawRecordFragment.isAdded()) {
        //    manager.putFragment(outState, "mWithdrawRecordFragment", mWithdrawRecordFragment);
        //}
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mAmountGrowthAnim != null) {
            mAmountGrowthAnim.cancel();
            mAmountGrowthAnim = null;
        }
    }


    /**
     * 初始化底部导航栏
     */
    private void initTabNav() {}

    private void initVP() {
        mBinding.vpGameSpotActivityMain.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int page) {
                if (page > 0) {
                    mBinding.rlGameSpotActivityMainTitleBar.setVisibility(View.GONE);
                    mBinding.tvGameSpotActivityMainMoneyTxTips.setVisibility(View.GONE);
                } else {
                    if (isAnalyze) {
                        mBinding.tvGameSpotActivityMainMoneyTxTips.setVisibility(View.GONE);
                    } else {
                        mBinding.rlGameSpotActivityMainTitleBar.setVisibility(View.VISIBLE);
                        changeTipVisible();
                    }
                }

                String categoryThree = "tab_one";
                if (page == 0) {
                    categoryThree = "tab_one";
                } else if (page == 1) {
                    categoryThree = "tab_two";
                } else if (page == 2) {
                    categoryThree = "tab_three";
                } else if (page == 3) {
                    categoryThree = "tab_four";
                }

                TrackManager.sendTrack("page", categoryThree, "");

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
        //
        //GameManager.refreshViewBgFromAssets(this, bgWhole);
    }

    /**
     * 初始化账户信息-监听
     */
    private void initUserAmountMsg() {
        Disposable dispPreviewMsg = RxBus.getInstance()
                .toObservable(EventMsg.class)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        (msg) -> {
                            if (msg.getCode() == EventConstant.CODE_EVENT_MAIN_TO_TAB) {
                                scollToTab(msg.getArg1());
                            } else if (msg.getCode() == EventConstant.CODE_EVENT_REWARD_ACCOUNT) {
                                refreshAccount(msg.getArg2());
                            } else if (msg.getCode() == EventConstant.CODE_EVENT_REFRESH_WHOLE_THEME_BG) {
                                //GameManager.refreshViewBgFromAssets(this, bgWhole);
                            } else if (msg.getCode() == EventConstant.CODE_EVENT_WINDOW_ALL_DISMISS) {
                                //if (mQuizFragment != null) {
                                //    mQuizFragment.startAllSound();
                                //}
                            } else if (msg.getCode() == EventConstant.CODE_EVENT_WINDOW_HAS_SHOWING) {
                                //if (mQuizFragment != null) {
                                //    mQuizFragment.stopAllSound();
                                //}
                            }
                            //else if (msg.getCode() == EventConstant.CODE_EVENT_RESTART_GAME) {
                            //    if (mMainFindDiffFragment != null) {
                            //        mMainFindDiffFragment.updateQuestionInfo();
                            //    }
                            //}
                        }
                );
        addDisposable(dispPreviewMsg);
    }


    /**
     * 获取答题、余额信息
     */
    private void refreshAccount(double userNewBalance) {
        AccountResponse.AccountBean accountInfoResponse = new AccountResponse.AccountBean();
        accountInfoResponse.setRem(userNewBalance);
        updateAmount(accountInfoResponse);
        //ParamRequest requestData = new ParamRequest();
        //requestData.setM(BusinessFieldName.M_ACCOUNT);
        //NetManager.post(requestData, new AbstractObserver<AccountResponse>() {
        //    @Override
        //    public void onNext(AccountResponse accountInfoResponse, String encryptBody) {
        //        String data = AesGcmUtil.decrypt(encryptBody);
        //        LogUtils.e("M_ACCOUNT=" + data);
        //
        //        ConfigManager.setLastMoneyLimit(accountInfoResponse.getMoneyLimit());
        //        if (!accountInfoResponse.getAclist().isEmpty()) {
        //            updateAmount(accountInfoResponse.getAclist().get(0));
        //        }
        //    }
        //});
    }

    /**
     * 统一此处更新余额
     *
     * @param
     * @param accountInfoResponse
     */
    private void updateAmount(AccountResponse.AccountBean accountInfoResponse) {
        //
        mBinding.ivGameSpotActivityMainMoneyCurrency.setImageResource(IconSelectManager.getSingleRewardIcon());
        //
        float accountAmount = (float) accountInfoResponse.getRem();
        if (accountAmount < 0) {
            return;
        }
        ConfigManager.setBalance(accountAmount);

        if (!isAnalyze) {
            NotifyUtil.getInstance().updateNotify();
        }
        //金额增长Anim
        try {
            if (mLastPrice != accountAmount) {
                mAmountGrowthAnim = ValueAnimator.ofFloat(mLastPrice, accountAmount);
                mAmountGrowthAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    //                    FloatEvaluator mFloatEvaluator = new FloatEvaluator();
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        float fraction = animation.getAnimatedFraction();
//                        float currentValue = mFloatEvaluator.evaluate(fraction, mLastPrice, Float.parseFloat(accountAmount));
                        float currentValue = Float.parseFloat(animation.getAnimatedValue().toString());
                        if (mBinding.tvGameSpotActivityMainTotalPrice != null)
                            mBinding.tvGameSpotActivityMainTotalPrice.setText(ConfigManager.getCurrency() + BalanceManager.getFormatBalance(currentValue));

                        mLastPrice = (float) accountAmount;
                    }
                });
                mAmountGrowthAnim.setDuration(2500);
                mAmountGrowthAnim.start();
            } else {
                if (mBinding.tvGameSpotActivityMainTotalPrice != null)
                    mBinding.tvGameSpotActivityMainTotalPrice.setText(ConfigManager.getCurrency() + BalanceManager.getFormatBalance(accountAmount));
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
        changeTipVisible();
    }

    private void changeTipVisible() {
        mBinding.tvGameSpotActivityMainMoneyTxTips.setVisibility(View.VISIBLE);
        mBinding.tvGameSpotActivityMainMoneyTxTips.setText(ContextManager.getContext().getString(R.string.game_spot_anytime_withdraw));
    }


    /**
     * 切换到主页
     */
    public void scollToTab(int tabNo) {
        if (mBinding.vpGameSpotActivityMain != null) mBinding.vpGameSpotActivityMain.setCurrentItem(tabNo, false);
    }


    ///**
    // * 第三种方法
    // */
    //@Override
    //public void onBackPressed() {
    //long secondTime = System.currentTimeMillis();
    //if (secondTime - firstTime > 2000) {
    //    ToastUtils.showShort(getResources().getString(R.string.game_spot_quit_app_reminder));
    //    firstTime = secondTime;
    //} else {
    //    finish();
    //}
    //}


    @Override
    protected void onResume() {
        super.onResume();
        TrackManager.sendTrack(JLogExtBean.event(TrackManager.page_home));
        GameManager.refreshAccount(ConfigManager.getBalance());
    }

    /**
     * 检测Activity是否正常存活
     * * @return
     */
    private boolean checkActivityLive() {
        if (!isFinishing() && !isDestroyed()) {
            return true;
        }
        return false;
    }
}
