package spot.game.activity;

import android.animation.ValueAnimator;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.BarUtils;
import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.LanguageUtils;
import com.blankj.utilcode.util.SPStaticUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.orhanobut.hawk.Hawk;

import net.goal.differences.BuildConfig;
import net.goal.differences.R;
import net.goal.differences.databinding.GameSpotActivityWithdrawAll2Binding;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.Disposable;
import spot.game.adapter.WithdrawCashChannelAdapter;
import spot.game.constant.BusinessFieldName;
import spot.game.manager.BalanceManager;
import spot.game.manager.IconSelectManager;
import spot.game.popupwindow.DialogButtonClickListener;
import spot.game.popupwindow.WithdrawalCancellationDialog;
import spot.game.popupwindow.WithdrawalCashDialog;
import spot.game.popupwindow.WithdrawalCashDoubleCheckDialog;
import spot.game.popupwindow.WithdrawalProgressDialog;
import spot.game.event.EventConstant;
import spot.game.event.EventMsg;
import spot.game.event.RxBus;
import spot.game.manager.ADManager;
import spot.game.manager.ConfigManager;
import spot.game.manager.ContextManager;
import spot.game.manager.GameManager;
import spot.game.manager.TrackManager;
import spot.game.model.JLogExtBean;
import spot.game.model.WithdrawStateModel;
import spot.game.util.FastClickUtil;
import spot.game.util.GameTaskUtils;
import spot.game.util.RandomStringGenerator;
import spot.game.util.ViewUtils;

/**
 * 新版假提现
 */
public class WithdrawCashActivity extends BaseActivity implements View.OnClickListener {

    private GameSpotActivityWithdrawAll2Binding mBinding;
    private WithdrawCashChannelAdapter mWithdrawCashChannelAdapter;
    
    float mLastPrice;

    @Override
    protected View getLayoutView() {
        if (mBinding != null){
            return mBinding.getRoot();
        }
        return null;
    }

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        mBinding = GameSpotActivityWithdrawAll2Binding.inflate(getLayoutInflater());
        super.onCreate(savedInstanceState);
        BarUtils.transparentStatusBar(this);
        initUserAmountMsg();
        initView();
        initWithdrawalView();
        if (BuildConfig.PRODUCT_FLAVORS.equalsIgnoreCase("anyThink")) {
            mBinding.llWithdrawTestCountry.setVisibility(View.VISIBLE);
        }
        mWithdrawCashChannelAdapter = new WithdrawCashChannelAdapter();
        mBinding.rvListWithdrawChannel.setAdapter(mWithdrawCashChannelAdapter);

        initClicks();
        withdrawPage();

        TrackManager.sendTrack(JLogExtBean.event(TrackManager.page_withdraw, TrackManager.LOG_LEVEL_0)
                .setLogmsg("WITHDRAW_PAGE_MONEY").setStrvalue("money"));
    }

    @Override
    public void onClick(View v) {
        if (FastClickUtil.isFastClick()) return;
        int id = v.getId();
        switch (id) {
            case R.id.iv_game_spot_close_btn:
                GameManager.newClick();
                finish();
                break;
            case R.id.iv_game_spot_record_btn:
                GameManager.newClick();
                GameManager.intentToRecord(this);
                break;
//            case R.id.iv_withdraw_record:
//                GameManager.newClick();
//                GameManager.intentToRecord(this);
//                break;
            case R.id.tv_withdraw_btn:
                GameManager.newClick();

                int user_need_level = ConfigManager.getNeedLevel();
                int local_level = ConfigManager.getLocalLevel();
                //满足等级级才能提现
                if (user_need_level > local_level) {
                    Spanned withdrawNeeds = Html.fromHtml(ContextManager.getContext()
                            .getString(R.string.game_spot_withdraw_needs_lv, user_need_level + ""));
                    ToastUtils.showShort(withdrawNeeds);
                    return;
                }

                WithdrawStateModel model = GameTaskUtils.getWithdrawStateModel();

                if (model.isWithdrawCashIng() && model.getWithdrawCashProgress() == 4) {
                    //提示即将审核完成，请耐心等待
//                    ToastUtils.showShort(getString(R.string.review_will_be_completed_shortly_please_be_patient));

                    //这里点击显示比价广告
                    ADManager.INSTANCE().showMaxVideoAd(null);
                    return;
                } else if (model.isWithdrawCashIng() && model.getWithdrawCashProgress() > 1) {
                    showWithdrawalProgressDialog();
                    return;
                } else {

                    //提现最少金额
                    double cashAccount = ConfigManager.getBalance();
                    if (cashAccount < 2) {
                        String cashRemRuleChange = BalanceManager.getFormatBalance(2);
                        Spanned withdrawNeeds = Html.fromHtml(ContextManager.getContext()
                                .getString(R.string.game_spot_withdraw_needs, ConfigManager.getCurrency() + cashRemRuleChange + ""));
                        ToastUtils.showShort(withdrawNeeds);
                        return;
                    }

                    showWithdrawalDialog(model);
                }
                break;
            case R.id.tv_withdraw_test_country_submit_s:
                GameManager.newClick();
                dfgrg(mBinding.etWithdrawTestCountry.getText().toString().trim());
                break;
            case R.id.add_ad_play:
                GameTaskUtils.setAdPlayComplete();
                break;
            case R.id.add_add_money:
                ConfigManager.addBalance(2D);
                break;
            case R.id.tv_withdraw_ing_btn:
                GameManager.newClick();
                //去掉提现弹窗
                WithdrawalCancellationDialog dialog = new WithdrawalCancellationDialog(this, new DialogButtonClickListener() {
                    @Override
                    public void onClose() {

                    }

                    @Override
                    public void onConfirm() {
                        initWithdrawalView();
                    }
                });
                Window window = dialog.getWindow();
                //设置弹出动画
                window.setWindowAnimations(R.style.game_spot_STDialogAnim);
                dialog.show();
                break;
        }
    }

    private void showWithdrawalProgressDialog() {
        WithdrawalProgressDialog dialog = new WithdrawalProgressDialog(this);
        Window window = dialog.getWindow();
        //设置弹出位置
        window.setGravity(Gravity.CENTER);
        //设置弹出动画
        window.setWindowAnimations(R.style.game_spot_STDialogAnim);
        //设置对话框大小
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.show();
    }

    private void showWithdrawalDialog(WithdrawStateModel model) {
        if (TextUtils.isEmpty(mBinding.etUserAccount.getText().toString())) {
            ToastUtils.showShort(getString(R.string.game_spot_please_enter_your_receipt_account));
            return;
        }

        model.setWithdrawCashAccount(mBinding.etUserAccount.getText().toString());
        model.setWithdrawCashBankName(mWithdrawCashChannelAdapter.getSelectItem());
        model.setWithdrawCashHour(System.currentTimeMillis());
        model.setWithdrawCashNmu(ConfigManager.getBalance());
        model.setWithdrawCashOrder(RandomStringGenerator.generateRandomString(8));
        GameTaskUtils.setWithdrawStateModel(model);

        WithdrawalCashDialog mWithdrawFakeDialog = new WithdrawalCashDialog(this, new DialogButtonClickListener() {
            @Override
            public void onClose() {

            }

            @Override
            public void onConfirm() {
                GameTaskUtils.startWithdrawalCash();
                new WithdrawalCashDoubleCheckDialog(WithdrawCashActivity.this, null).show();
                initWithdrawalView();
            }
        });
        Window window = mWithdrawFakeDialog.getWindow();
        //设置弹出位置
        window.setGravity(Gravity.CENTER);
        //设置弹出动画
        window.setWindowAnimations(R.style.game_spot_STDialogAnim);
        //设置对话框大小
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        mWithdrawFakeDialog.show();
    }

    private void initView() {
        ViewGroup.LayoutParams mBarParam = mBinding.systemBar.getLayoutParams();
        mBarParam.height = BarUtils.getStatusBarHeight();
        mBinding.ivBalanceImg.setImageResource(IconSelectManager.getSingleRewardIcon());
    }

    private void initWithdrawalView() {
        GameTaskUtils.initWithdrawStateModel();
        long timeCountdown = GameTaskUtils.getHourCountdown();
        WithdrawStateModel model = GameTaskUtils.getWithdrawStateModel();

        if (model.isWithdrawCashIng()) {
            if (model.getWithdrawCashProgress() == 1) {
                //提现中24小时内展示UI
                mBinding.llWithdrawalIngLayout.setVisibility(View.VISIBLE);
                mBinding.tvWithdrawIngBtn.setVisibility(View.VISIBLE);
                mBinding.tvWithdrawBtn.setVisibility(View.GONE);
                String withdrawalStr = ConfigManager.getCurrency() + BalanceManager.getFormatBalance(model.getWithdrawCashNmu());
                ViewUtils.autoResizeText(mBinding.mineWithdrawalIngBalance, withdrawalStr, 22, 10, 29);
                //mBinding.mineWithdrawalIngBalance.setText(withdrawalStr);

                //启动倒计时
                mHandler.sendEmptyMessageDelayed(MSG_COUNTDOWN_TIME, 1000);
                mBinding.tvWithdrawIngBtn.setText(getString(R.string.game_spot_review_in_progress) + GameTaskUtils.formatSecondsToHMS(timeCountdown));
            } else {
                //提现中每日任务和等级限制任务
                mBinding.llWithdrawalIngLayout.setVisibility(View.VISIBLE);
                mBinding.tvWithdrawIngBtn.setVisibility(View.GONE);
                mBinding.tvWithdrawBtn.setVisibility(View.VISIBLE);
                String withdrawalStr = ConfigManager.getCurrency() + BalanceManager.getFormatBalance(model.getWithdrawCashNmu());
                ViewUtils.autoResizeText(mBinding.mineWithdrawalIngBalance, withdrawalStr, 22, 10, 29);
                //mBinding.mineWithdrawalIngBalance.setText(withdrawalStr);
            }
        } else {
            //未提现状态
            mBinding.llWithdrawalIngLayout.setVisibility(View.GONE);
            mBinding.tvWithdrawIngBtn.setVisibility(View.GONE);
            mBinding.tvWithdrawBtn.setVisibility(View.VISIBLE);
        }

        mBinding.mineTotalBalance.setText(ConfigManager.getCurrency() + BalanceManager.getFormatBalance());
        mBinding.etUserAccount.setText(model.getWithdrawCashAccount());
    }

    private void initClicks() {
        mWithdrawCashChannelAdapter.setOnItemClickListener(new WithdrawCashChannelAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int pos) {
                GameManager.newClick();

                mWithdrawCashChannelAdapter.setSelectItem(pos);

                changeWithDrawBG(mWithdrawCashChannelAdapter.getSelectItem());
            }
        });

        mBinding.ivGameSpotCloseBtn.setOnClickListener(this);
        mBinding.ivGameSpotRecordBtn.setOnClickListener(this);
        mBinding.tvWithdrawBtn.setOnClickListener(this);
        mBinding.tvWithdrawIngBtn.setOnClickListener(this);
        mBinding.tvWithdrawTestCountrySubmitS.setOnClickListener(this);
        mBinding.addAddMoney.setOnClickListener(this);
        mBinding.addAdPlay.setOnClickListener(this);

        ClickUtils.applyPressedViewScale(mBinding.tvWithdrawBtn, mBinding.tvWithdrawIngBtn);
        KeyboardUtils.fixAndroidBug5497(this);

        KeyboardUtils.registerSoftInputChangedListener(this, new KeyboardUtils.OnSoftInputChangedListener() {
            @Override
            public void onSoftInputChanged(int height) {
                if (height == 0 ){
                    WithdrawStateModel model = GameTaskUtils.getWithdrawStateModel();
                    model.setWithdrawCashAccount(mBinding.etUserAccount.getText().toString());
                    GameTaskUtils.setWithdrawStateModel(model);
                }
            }
        });
    }

    /**
     * 修改提现相关背景图片
     */
    private void changeWithDrawBG(String platform) {
        mBinding.llChannelLayout.setBackgroundResource(IconSelectManager.getFakeWithdrawBig(platform));
    }

    private static final int MSG_COUNTDOWN_TIME = 0x100;

    private final Handler mHandler = new Handler(Looper.getMainLooper()) {

        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_COUNTDOWN_TIME:
                    try {
                        long timeCountdown = GameTaskUtils.getHourCountdown();
                        if (timeCountdown > 0) {
                            mHandler.sendEmptyMessageDelayed(MSG_COUNTDOWN_TIME, 1000);
                            mBinding.tvWithdrawIngBtn.setText(getString(R.string.game_spot_review_in_progress) + GameTaskUtils.formatSecondsToHMS(timeCountdown));
                        } else {
                            initWithdrawalView();
                        }
                    } catch (Throwable throwable) {

                    }
                    break;
            }
        }
    };

    List<String> mPlatformList;

    private void withdrawPage() {
        if (mPlatformList == null || mPlatformList.isEmpty()) {
            mPlatformList = new ArrayList<String>();
            String country = ConfigManager.getCountry();
            if ("id".equalsIgnoreCase(country)) {
                mPlatformList.add("dana");
                mPlatformList.add("shopeepay");
                mPlatformList.add("ovo");
            } else if ("br".equalsIgnoreCase(country)) {
                mPlatformList.add("pix");
                mPlatformList.add("boleto");
                mPlatformList.add("pagbank");
            } else if ("ru".equalsIgnoreCase(country)) {
                mPlatformList.add("qiwi");
                mPlatformList.add("webmoney");
                mPlatformList.add("yoomoney");
            } else {
                mPlatformList.add("paypal");
                mPlatformList.add("amazon");
                mPlatformList.add("mastercard");
            }
        }

        List<String> platforms = mPlatformList;
        if (platforms != null && !platforms.isEmpty()) {
            mBinding.rvListWithdrawChannel.setVisibility(View.VISIBLE);
            mWithdrawCashChannelAdapter.setData(platforms);
            mWithdrawCashChannelAdapter.setSelectItem(0);

            changeWithDrawBG(mWithdrawCashChannelAdapter.getSelectItem());
        } else {
            mBinding.rvListWithdrawChannel.setVisibility(View.GONE);
        }
    }

    private void dfgrg(String testCountry) {
        if (!TextUtils.isEmpty(testCountry) && BuildConfig.PRODUCT_FLAVORS.equalsIgnoreCase("anyThink")) {
            SPStaticUtils.clear(true);
//            ConfigManager.setIsResetAccount(false);
            SPStaticUtils.put(BusinessFieldName.test_country, testCountry, true);
            if (testCountry.equalsIgnoreCase("id")) {
                LanguageUtils.applyLanguage(new Locale("in"));
            } else if (testCountry.equalsIgnoreCase("br")) {
                LanguageUtils.applyLanguage(new Locale("pt"));
            } else if (testCountry.equalsIgnoreCase("pk")) {
                LanguageUtils.applyLanguage(new Locale("ur"));
            } else if (testCountry.equalsIgnoreCase("ru")) {
                LanguageUtils.applyLanguage(new Locale("ru"));
            } else {
                LanguageUtils.applyLanguage(new Locale("en"));
            }
            Hawk.clear();
            AppUtils.relaunchApp(true);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        SoundUtils.getInstance().playBgMusic(true);
    }

    @Override
    protected void onPause() {
        super.onPause();
//        SoundUtils.getInstance().playBgMusic(false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        SoundUtils.getInstance().playBgMusic(false);
        mHandler.removeMessages(MSG_COUNTDOWN_TIME);
    }

    /**
     * 初始化账户信息-监听
     */
    private void initUserAmountMsg() {
        Disposable dispPreviewMsg = RxBus.getInstance()
                .toObservable(EventMsg.class)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        (msg) -> {
                            if (msg.getCode() == EventConstant.CODE_EVENT_REWARD_ACCOUNT) {
                                updateAmount((float) msg.getArg2());
                            }
                        }
                );
        addDisposable(dispPreviewMsg);
    }

    private void updateAmount(float accountAmount) {
        if (accountAmount < 0) {
            return;
        }
        ConfigManager.setBalance(accountAmount);
        //金额增长Anim
        try {
            if (mLastPrice != accountAmount) {
                ValueAnimator mAmountGrowthAnim = ValueAnimator.ofFloat(mLastPrice, accountAmount);
                mAmountGrowthAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        float currentValue = Float.parseFloat(animation.getAnimatedValue().toString());
                        if (mBinding.mineTotalBalance != null)
                            mBinding.mineTotalBalance.setText(ConfigManager.getCurrency() + BalanceManager.getFormatBalance(currentValue));

                        mLastPrice = (float) accountAmount;
                    }
                });
                mAmountGrowthAnim.setDuration(2500);
                mAmountGrowthAnim.start();
            } else {
                if (mBinding.mineTotalBalance != null)
                    mBinding.mineTotalBalance.setText(ConfigManager.getCurrency() + BalanceManager.getFormatBalance(accountAmount));
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
