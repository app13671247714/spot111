package spot.game.activity;


import static spot.game.manager.UserRegisterManager.TYPE_PROCESS_LOAD;
import static spot.game.manager.UserRegisterManager.TYPE_PROCESS_REGISTER;
import static spot.game.manager.UserRegisterManager.TYPE_PROCESS_START;
import static spot.game.manager.UserRegisterManager.TYPE_PROCESS_UNKNOWN;
import static spot.game.manager.UserRegisterManager.processType;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;

import com.adjust.sdk.Adjust;
import com.adjust.sdk.OnDeviceIdsRead;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SPStaticUtils;
import com.blankj.utilcode.util.TimeUtils;

import net.goal.differences.BuildConfig;
import net.goal.differences.databinding.GameSpotActivitySplashBinding;

import java.util.List;

import in.xiandan.countdowntimer.CountDownTimerSupport;
import in.xiandan.countdowntimer.SimpleOnCountDownTimerListener;
import io.reactivex.rxjava3.annotations.NonNull;
import spot.differences.gola.GolaActivitySS1;
import spot.differences.gola.GolaUserNetErrorWindow;
import spot.game.constant.BusinessFieldName;
import spot.game.constant.ZeusConfig;
import spot.game.manager.ConfigManager;
import spot.game.manager.ContextManager;
import spot.game.manager.GameManager;
import spot.game.manager.InstallReferrerManager;
import spot.game.manager.NetManager;
import spot.game.manager.TrackManager;
import spot.game.model.JLogExtBean;
import spot.game.model.callModel.DeviceRequest;
import spot.game.model.callModel.ParamRequest;
import spot.game.model.backModel.DataResponse;
import spot.game.model.backModel.LoadResponse;
import spot.game.model.backModel.PriceRule;
import spot.game.model.backModel.ResponseParams;
import spot.game.net.AbstractObserver;
import spot.game.util.AesGcmUtil;
import spot.game.util.AppUtils;
import spot.game.util.DeviceUtils;


/**
 * @Class: SplashActivity
 * @Date: 2022/12/7
 * @Description:
 */
public class SplashActivity extends BaseActivity {

    private static final int MSG_LOAD = 0x100 + BuildConfig.VERSION_CODE;
    private static final int MSG_ERROR = MSG_LOAD + 1;
    private static final int MSG_MAIN = MSG_ERROR + 1;
    private static final int MSG_REGISTER = MSG_MAIN + 1;
    private static final int MSG_WAIT_REGISTER = MSG_REGISTER + 1;

    private static boolean process_done = false;
    private static boolean process_permission = false;

    protected GameSpotActivitySplashBinding mBinding;
    CountDownTimerSupport pbCountDownTimerSupport;

    @Override
    protected View getLayoutView() {
        if (mBinding!= null){
            return mBinding.getRoot();
        }
        return null;
    }

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        mBinding = GameSpotActivitySplashBinding.inflate(getLayoutInflater());
        super.onCreate(savedInstanceState);
        TrackManager.sendTrack("page_splash", "");
        //检查资源回收则重启应用
        AppUtils.getInstance().setAppStatus(AppUtils.STATUS_NORMAL);
        //避免执行2次onCreate
        if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) {
            finish();
            return;
        }
        //打开不保留活动时，广告播放失败
        String logMsg = "";
        if (DeviceUtils.isFinishActivitiesOptionEnabled(getApplication())) {
            logMsg += "FinishActivitiesOptionOpened,";
        }
        //
        String forceKill = getIntent().getStringExtra("forceKill");
        String openType = "op_normal";
        if (!TextUtils.isEmpty(forceKill)) {//变量回收，强杀重启
            openType = "op_force_restart";
        } else if (getIntent().getBooleanExtra("NoticeLaunch", false)) {//通知栏
            openType = "op_notice";
        }
        TrackManager.sendTrack(JLogExtBean.event(TrackManager.appopen, logMsg).setStrvalue(openType).setIntvalue(ConfigManager.openTimes()));

        initView();
        init();
        checkNotifyPermission();
    }

    private void checkNotifyPermission() {
        boolean isOpened = false;
        try {
            try {
                NotificationManagerCompat manager = NotificationManagerCompat.from(ContextManager.getApplication().getApplicationContext());
                isOpened = manager.areNotificationsEnabled();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (ContextCompat.checkSelfPermission(
                    this, Manifest.permission.POST_NOTIFICATIONS) ==
                    PackageManager.PERMISSION_GRANTED ||isOpened) {
                process_permission = true;
                toMain();
            } else {
                // You can directly ask for the permission.
                // The registered ActivityResultCallback gets the result of this request.
                requestPermissionLauncher.launch(
                        Manifest.permission.POST_NOTIFICATIONS);
            }
        } catch (Throwable e) {
            LogUtils.e(e.getMessage());
        }
    }


    private ActivityResultLauncher<String> requestPermissionLauncher =
            registerForActivityResult(new ActivityResultContracts.RequestPermission(), isGranted -> {
                process_permission = true;
                toMain();
            });

    private void initView() {
        mBinding.pbGameSpotSplash.setProgress(0);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mBinding.pbGameSpotSplash.setMin(0);
        }
        mBinding.pbGameSpotSplash.setMax(100);
        pbCountDownTimerSupport = new CountDownTimerSupport(5000, 500);
        pbCountDownTimerSupport.setOnCountDownTimerListener(new SimpleOnCountDownTimerListener() {
            @Override
            public void onTick(long millisUntilFinished) {
                int progress = mBinding.pbGameSpotSplash.getProgress() + 1;
                if (progress >= 80) {
                    stopCountDownTimer();
                    return;
                }
                mBinding.pbGameSpotSplash.setProgress(progress);
            }

            @Override
            public void onFinish() {
                mBinding.pbGameSpotSplash.setProgress(100);
            }
        });
        pbCountDownTimerSupport.start();
    }

    private void init() {
        TrackManager.sendTrack("page_splash_init", "");
        if (processType.get() == TYPE_PROCESS_START) {
            return;
        }
        mBinding.pbGameSpotSplash.setProgress(0);
        processType.set(TYPE_PROCESS_START);
        GameManager.init(this);
        if (TextUtils.isEmpty(ConfigManager.getUserId())) {
            //register无网时
            String googleAdId = SPStaticUtils.getString(BusinessFieldName.GID, "");
            if (!TextUtils.isEmpty(googleAdId)) {
                mHandler.sendEmptyMessage(MSG_REGISTER);
                return;
            }
            Adjust.getGoogleAdId(this, new OnDeviceIdsRead() {
                @Override
                public void onGoogleAdIdRead(String googleAdId) {
                    SPStaticUtils.put(BusinessFieldName.GID, googleAdId);
                    mHandler.sendEmptyMessage(MSG_REGISTER);
                }
            });
            mHandler.sendEmptyMessageDelayed(MSG_WAIT_REGISTER, 3_000);
        } else {
            mHandler.sendEmptyMessage(MSG_LOAD);
        }
    }

    private final Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_LOAD:
                    if (processType.get() == TYPE_PROCESS_LOAD) {
                        return;
                    }
                    processType.set(TYPE_PROCESS_LOAD);
                    GameManager.loadData();
                    load();
                    TrackManager.health(ZeusConfig.health_app);
                    break;
                case MSG_MAIN:
                    process_done = true;
                    toMain();
                    break;
                case MSG_REGISTER:
                    mHandler.removeMessages(MSG_WAIT_REGISTER);
                    mHandler.removeMessages(MSG_REGISTER);
                    if (processType.get() == TYPE_PROCESS_REGISTER) {
                        return;
                    }
                    processType.set(TYPE_PROCESS_REGISTER);
                    register();
                    break;
                case MSG_ERROR:
                    showNetErrorWindow();
                    break;
                case MSG_WAIT_REGISTER:
                    mHandler.sendEmptyMessage(MSG_REGISTER);
                    break;
            }
        }
    };

    private void showNetErrorWindow() {
        stopCountDownTimer();
        ConfigManager.addNetErrorShowCountThisTime();
        GameManager.showNetErrorWindow(new GolaUserNetErrorWindow.OnNetErrorListener() {
            @Override
            public void onConfirm() {
                ConfigManager.addNetErrorClickCountThisTime();
                GameManager.dismissNetErrorWindow();
                processType.set(TYPE_PROCESS_UNKNOWN);
                init();
                if (pbCountDownTimerSupport != null) {
                    pbCountDownTimerSupport.reset();
                    pbCountDownTimerSupport.start();
                }
            }
        });
    }

    private void stopCountDownTimer() {
        if (pbCountDownTimerSupport != null) {
            try {
                pbCountDownTimerSupport.stop();
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }

    private void toMain() {
        TrackManager.sendTrack("page_splash_toMain", "process_done=" + process_done + ", process_permission=" + process_permission);
        if (!process_done || !process_permission) {
            return;
        }
        mBinding.pbGameSpotSplash.setProgress(100);
        if (pbCountDownTimerSupport != null) {
            stopCountDownTimer();
            pbCountDownTimerSupport = null;
        }
        //
        Intent intent = new Intent(this, GolaActivitySS1.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }


    private void load() {
        TrackManager.sendTrack("page_splash_load", "req");
        mBinding.pbGameSpotSplash.setProgress(50);
        ParamRequest request = new ParamRequest();
        request.setM(BusinessFieldName.M_LOGIN_LOAD);
        //
        NetManager.post(request, new AbstractObserver<LoadResponse>() {
            @Override
            public void onNext(LoadResponse loadResponse, String encryptBody) {
                ConfigManager.setIsWhite(loadResponse.getIsWhite());
                TrackManager.sendTrack(JLogExtBean.event(TrackManager.login, "login_success")
                        .setStrvalue(ConfigManager.getIsWhite() ? "beta" : "muse"));
                mBinding.pbGameSpotSplash.setProgress(70);
                LogUtils.e(AesGcmUtil.decrypt(encryptBody));
                ConfigManager.setIsAudit(loadResponse.isAudit());
                //ConfigManager.setIsNew(loadResponse.isNew());
                checkLoginCount();
                try {
                    LoadResponse.AbTestInfo abTestInfo = loadResponse.getAbTestInfo();
                    if (abTestInfo != null) {
                        ConfigManager.setABTags(abTestInfo.getAb_tags());
                        proceedABList(abTestInfo.getAb_lists());
                    }
                } catch (Throwable e) {
                    e.printStackTrace();
                }
                mHandler.sendEmptyMessage(MSG_MAIN);
            }

            @Override
            public void onError(Throwable e) {
                //if (BuildConfig.DEBUG) {
                //    mHandler.sendEmptyMessage(MSG_MAIN);
                //} else {
                mHandler.sendEmptyMessage(MSG_ERROR);
                //}
                TrackManager.sendTrack(JLogExtBean.event(TrackManager.login, "login_fail=" + e.getMessage())
                        .setStrvalue(ConfigManager.getIsWhite() ? "beta" : "muse"));
            }
        });
    }

    private void checkLoginCount() {
        long lastLoginMillis = ConfigManager.getLastLoginMillis();
        if (!TimeUtils.isToday(lastLoginMillis) && System.currentTimeMillis() - lastLoginMillis > 0) {
            ConfigManager.setLastLoginMillis(System.currentTimeMillis());
            ConfigManager.setNowLoginCount(ConfigManager.getNowLoginCount() + 1);
        }
    }

    private void register() {
        TrackManager.sendTrack("page_splash_register", "req");
        DeviceRequest request = new DeviceRequest();
        mBinding.pbGameSpotSplash.setProgress(20);
        request.setSecret(GameManager.getAppSecret(request.getRequestTime(), request.getRequestId()));
        //
        NetManager.post(request, new AbstractObserver<DataResponse>() {
            @Override
            public void onNext(DataResponse dataResponse, String encryptBody) {
                mBinding.pbGameSpotSplash.setProgress(30);
                String res = AesGcmUtil.decrypt(encryptBody);
                LogUtils.e(res);
                ConfigManager.setUserId(dataResponse.getUserId());
                ConfigManager.setRegisterTime(dataResponse.getRegisterTime());
                ConfigManager.setIsWhite(dataResponse.getIsWhite());
                TrackManager.sendTrack(JLogExtBean.event(TrackManager.regisger, "register_success")
                        .setStrvalue(ConfigManager.getIsWhite() ? "beta" : "muse"));
                //
                InstallReferrerManager.INSTANCE().uploadInstallReferrer();
                //
                decodePriceRule(dataResponse.getRules());
                mHandler.sendEmptyMessage(MSG_LOAD);
            }

            @Override
            public void onError(Throwable e) {
                LogUtils.e(e.getMessage());
                //if (BuildConfig.DEBUG) {
                //    mHandler.sendEmptyMessage(MSG_LOAD);
                //} else {
                mHandler.sendEmptyMessage(MSG_ERROR);
                //}
                TrackManager.sendTrack(JLogExtBean.event(TrackManager.regisger, "register_fail=" + e.getMessage())
                        .setStrvalue(ConfigManager.getIsWhite() ? "beta" : "muse"));
            }
        });
    }

    private void decodePriceRule(PriceRule rules) {
        try {
            if (rules == null) {
                return;
            }
            List<ResponseParams> responseParamList = rules.getResponseParams();
            if (responseParamList == null || responseParamList.isEmpty()) {
                return;
            }
            StringBuilder logMsg = new StringBuilder();
            double activeEcpm = 0;
            for (ResponseParams responseParams : responseParamList) {
                if (responseParams == null) {
                    continue;
                }
                try {
                    logMsg.append(responseParams.getKey()).append("=").append(responseParams.getValue()).append(", ");
                } catch (Throwable e) {
                    e.printStackTrace();
                }
                if ("ad_cpm_min".equalsIgnoreCase(responseParams.getKey())) {
                    try {
                        float value = Float.parseFloat(responseParams.getValue());
                        SPStaticUtils.put(BusinessFieldName.ACTIVE_USER_ECPM_VALUE, value);
                        activeEcpm = value;
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                } else if ("mm_ipu".equalsIgnoreCase(responseParams.getKey())) {
                    try {
                        float value = Float.parseFloat(responseParams.getValue());
                        SPStaticUtils.put(BusinessFieldName.ACTIVE_USER_MM_IPU, value);
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
            }
            TrackManager.sendTrack(JLogExtBean.event(TrackManager.active_rule, logMsg.toString())
                    .setDublevalue(activeEcpm));
        } catch (Throwable throwable) {
            LogUtils.e(throwable.getMessage());
        }
    }


    private void proceedABList(List<LoadResponse.ABList> ab_lists) {
        if (ab_lists == null || ab_lists.isEmpty()) {
            return;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (pbCountDownTimerSupport != null) {
            pbCountDownTimerSupport.pause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (pbCountDownTimerSupport != null) {
            pbCountDownTimerSupport.resume();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (pbCountDownTimerSupport != null) {
            stopCountDownTimer();
            pbCountDownTimerSupport = null;
        }
    }
}
