package spot.game.activity;

import android.os.Bundle;
import android.view.View;

import com.blankj.utilcode.util.BarUtils;

import net.goal.differences.R;
import net.goal.differences.databinding.GameSpotActivityWithdrawRecordBinding;

import spot.game.manager.GameManager;
import spot.game.manager.TrackManager;

public class WithdrawRecordActivity extends BaseActivity {

    private GameSpotActivityWithdrawRecordBinding mBinding;
    private View backBtn;

    @Override
    protected View getLayoutView() {
        if (mBinding!= null){
            return mBinding.getRoot();
        }
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mBinding = GameSpotActivityWithdrawRecordBinding.inflate(getLayoutInflater());
        super.onCreate(savedInstanceState);
        TrackManager.sendTrack("page", "record", "");
        BarUtils.setStatusBarVisibility(this, false);
        BarUtils.setNavBarVisibility(this, false);
        backBtn = findViewById(R.id.iv_game_spot_activity_withdraw_record_back_btn);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TrackManager.sendTrack("page_record", "close", "left");
                GameManager.newClick();
                finish();
            }
        });
    }


}
