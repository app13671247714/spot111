package spot.game.activity;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.blankj.utilcode.util.BarUtils;
import com.blankj.utilcode.util.KeyboardUtils;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import spot.game.event.EventConstant;
import spot.game.event.EventMsg;
import spot.game.event.RxBus;
import spot.game.manager.ContextManager;


/**
 * @Class: BaseActivity
 * @Date: 2022/11/8
 * @Description:
 */
public abstract class BaseActivity extends AppCompatActivity {

    protected int getLayoutId(){
        return 0;
    };
    protected View getLayoutView(){
        return null;
    };

    ImageView focusView;

    protected CompositeDisposable mDisposable;
    Disposable mBaseDispPreviewMsg;

    protected void addDisposable(Disposable d) {
        if (mDisposable == null) {
            mDisposable = new CompositeDisposable();
        }
        mDisposable.add(d);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getLayoutView() != null) {
            setContentView(getLayoutView());
        }else if(getLayoutId() != 0){
            setContentView(getLayoutId());
        }
        mBaseDispPreviewMsg = RxBus.getInstance()
                .toObservable(EventMsg.class)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        (msg) -> {
                            if (msg.getCode() == EventConstant.CODE_EVENT_RESET_ACTIVITY) {
                                ContextManager.setActivity(this);
                            }
                        }
                );
        addDisposable(mBaseDispPreviewMsg);
    }

    @Override
    protected void onResume() {
        BarUtils.setStatusBarLightMode(this, true);
        BarUtils.setNavBarVisibility(this, false);
        clearFocus();
        KeyboardUtils.hideSoftInput(this);
        super.onResume();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mDisposable != null) {
            mDisposable.dispose();
            mDisposable = null;
        }
        if (mBaseDispPreviewMsg != null) {
            mBaseDispPreviewMsg.dispose();
            mBaseDispPreviewMsg = null;
        }
    }

    private void clearFocus() {
        //
        addFocusView();
        //清空焦点
        getWindow().getDecorView().clearFocus();
    }


    //添加抢占焦点view
    private void addFocusView() {
        if (focusView == null) {
            focusView = new ImageView(this);
            focusView.setFocusable(true);
            focusView.setFocusableInTouchMode(true);
        }
        //
        ViewGroup viewGroup = (ViewGroup) getWindow().getDecorView();
        if (viewGroup.getChildAt(0) != focusView) {
            if (focusView.getParent() != null) {
                ((ViewGroup) focusView.getParent()).removeView(focusView);
            }
            viewGroup.addView(focusView, 0);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                BarUtils.setStatusBarLightMode(this, true);
                BarUtils.setNavBarVisibility(this, false);
                break;
            case MotionEvent.ACTION_UP:
                KeyboardUtils.hideSoftInput(this);
                //
                clearFocus();
                break;
        }
        return super.onTouchEvent(event);
    }
}
