package spot.game.constant;


import net.goal.differences.BuildConfig;

/**
 * @Class: ZeusConfig
 * @Date: 2022/12/7
 * @Description:
 */
public class ZeusConfig {

    public static final String ad_loading = "ext_ad_loading";
    public static final String ad_user_show = "ext_ad_user_show";
    public static final String ad_request_success = "ad_request_success";
    public static final String ad_show = "ad_show";
    public static final String ad_click = "ad_click";
    public static final String ad_error = "ad_error";
    public static final String ad_reward = "ext_ad_reward";
    public static final String health = "health";
    public static final String health_app = "health_app";
    public static final String MAX = "max";
    public static final String ext_pass = "ext_pass";
    public static final String ext_reward = "ext_reward";


    public static int RTYPE_FAIL = 0;
    public static int RTYPE_SUCCESS = 1;

    public static final String locationCode = "locationCode";

    public static final String B_BASEINFO = "B";
    public static final String S_ADSOUCE = "S";
    public static final String RP_REPORT = "RP";
    public static final String J_ADJUST = "J";
    public static final String AR_ADJUST_RULE = "AR";

    public static String getBaseUrl() {
        return BuildConfig.BASE_URL;
    }

}
