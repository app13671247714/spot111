package spot.game.constant;


import net.goal.differences.BuildConfig;

public class GameConfig {
    public static boolean isWhite = false;
    public static final String PRIVACY_POLICY = BuildConfig.PRIVACY_POLICY;
    public static final String USER_POLICY = BuildConfig.USER_POLICY;
    /**
     * ADJUST
     */
    public static final String ADJUST_APP_TOKEN = BuildConfig.ADJUST_APP_TOKEN;
    public static final long ADJUST_APP_SECRET_1 = Long.parseLong(BuildConfig.ADJUST_APP_SECRET_1);
    public static final long ADJUST_APP_SECRET_2 = Long.parseLong(BuildConfig.ADJUST_APP_SECRET_2);
    public static final long ADJUST_APP_SECRET_3 = Long.parseLong(BuildConfig.ADJUST_APP_SECRET_3);
    public static final long ADJUST_APP_SECRET_4 = Long.parseLong(BuildConfig.ADJUST_APP_SECRET_4);
    public static final long ADJUST_APP_SECRET_5 = Long.parseLong(BuildConfig.ADJUST_APP_SECRET_5);
    //本地货币单位
    public static final String CURRENCY_UNIT = BuildConfig.APP_CODE + "CURRENCY_UNIT";
    public static final String HEALTH_TIME = BuildConfig.APP_CODE + "HEALTH_TIME";
    public static final String EN_ADJUST = BuildConfig.APP_CODE + "EN_ADJUST";
    public static final String OPEN_TIMES = BuildConfig.APP_CODE + "OPEN_TIMES";
    public static final String CHANNEL = BuildConfig.APP_CODE + "CHANNEL";
    public static final String APPCODE = BuildConfig.APP_CODE + "APPCODE";
    public static final String APP_VERSION_CODE = BuildConfig.APP_CODE + "APP_VERSION_CODE";
    public static final String APP_VERSION_NAME = BuildConfig.APP_CODE + "APP_VERSION_NAME";
    public static final String USER_ID = BuildConfig.APP_CODE + "USER_ID";
    public static final String REG_TIME = BuildConfig.APP_CODE + "REG_TIME";
    public static final String REG_DATE = BuildConfig.APP_CODE + "REG_DATE";
    public static final String BALANCE = BuildConfig.APP_CODE + "BALANCE";
    public static final String IS_AUDIT = BuildConfig.APP_CODE + "IS_AUDIT";
    public static final String USER_LEVEL = BuildConfig.APP_CODE + "USER_LEVEL";
    public static final String SESSION_ID = BuildConfig.APP_CODE + "SESSION_ID";
    public static final String COUNTRY = BuildConfig.APP_CODE + "COUNTRY";
    public static final String TIME_ZONE = BuildConfig.APP_CODE + "TIME_ZONE";
    public static final String LEVEL_HEADER = BuildConfig.APP_CODE + "LEVEL_HEADER";
    //本次无网展示、点击计数
    public static final String NET_ERROR_COUNT_THIS_TIME_SHOW = BuildConfig.APP_CODE + "NET_ERROR_COUNT_THIS_TIME_SHOW";
    public static final String NET_ERROR_COUNT_THIS_TIME_CLICK = BuildConfig.APP_CODE + "NET_ERROR_COUNT_THIS_TIME_CLICK";
    //
    public static final String AD_CHANNEL = BuildConfig.APP_CODE + "AD_CHANNEL";//活动
    public static final String AD_GROUP_ID = BuildConfig.APP_CODE + "AD_GROUP_ID";//group

}
