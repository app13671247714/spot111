package spot.game.constant;


import net.goal.differences.BuildConfig;

public class BusinessFieldName {
    public static final String REWARD = BuildConfig.AD_REWARDVIDEO_1;
    public static final String Interstitial = BuildConfig.AD_OBTIVE_VIDEO;
    public static final String SEVER_CONFIG_ENCRYPT = "Ttq6yhZo/GWovuAAE0HARdFVakV7npAugBl5kCjTPP4CtIr8iWmQ84yUpo3KoGnpu/8m66+RWnmbKa7Pg7qNjeBoJjggKy09YBWTd+tOHk49kx+IO2pmuoYroJoKgd7XUr+6BLpuXDIGF9K1HISoYx4nhgYY5yOVaVFfh7NU6QiII6dutBoo+qw5iD0f51jRw/LOQFeX9j6z4W3y+q978wnETzMhwfHJN0hFxYtmMccHep59B2Cd5b00ZYh5azPYY6agG+0nrBqX93zHTuDPeYhuTezALPbiDghm0xVrJqxZhOnbzvOgXrPLsahR9IhQGUu+jqmQVAdIw+Frmf0PeKxiIbtBsQNHmqK2skPuC975FU+pbOkK37KhBLTj0ensR48nF9ag181QJeQHLk7zp9G8btyRfRMHUJKbJsFt2uFpX7L0J/wd4xuPDI7ZjlEjlEYMrPwhzvEY0B6O6C7ebq0mTzyu1iYW57FEavbQMsYPN/MJEGJVQPJBs6ym7jYNxsGRewixVdzLC0qzlBwOQqyrTPDMv7e+gtfNmDR6b763QJSTVBD8qGqcOoAbG+DJGmbAfM/GltBfP6jaLW/nulMztffs+RIgxyDjMS0eogw6ScqZ8xZvtNAyLyERWIrPc4Bn2FEreGrxNtp2KGkSQs+0Qopw3p56yMiXFChMYwZ32CO9FzmftQFfAXhQPdrvKSa4Hf96gnTLheGyaGbfspJSsFTPZRXN/nuW4nggmuFglEnsMjDQQU+fac652lIRepqyKtzG0xsZ+q7o7yt4vfqoi4GjmcwcIsAX/yv3Ar0NXDZ8m5ONpcqdGIU8NEv4YWye4uj/Q5qjhtYMou6Kvm9ZNNGJ/HDfxxtdCdiBvMHm4NOcNiDX9NNS7XTHuNl08py+SXK4NV+mQLTagq8A3SFS+n8oWWyfrVv1JaHZlp8/T+jh2fTsL4NM9WagozHklyJxzmkNFkH8irwMwjgCHrbzPlPWZH49mJZiDJ8N9Ngky8qxJxgv3NVG3tjUSpdorqIVDdYt4/Mddlejnl47dCisB5rBOkzX9lQvTkTdAP2WVKrsz9mKpt2dzq21fIqSilR2m81p03mZzvO8s/Z4t5vIbM521A1NgDYsPPxDVYpjDvpWPwGnLFx/MJtzcQEeEItQgKBXw3RzPSiyzylr5pMvAGFWUmJCRYFxxs1Rk45TdfQ/WG6t3U6+Lh6LtJnVVdq8G+W+V4lHWDW10k9hI0udgDlhW6Kd+grTyCxI+/bfuFC/wHT35SaEhP079FjXn+ckjCuNruYj/pBwXcPAP3lXqHVNNZXiqlzk9gSKPi+cLJWF//Ytnun7K5M56v9KA+qRQSkyK8crz096M9znEmey96j99FgSWEmFxcA1p3b6+Cbyt6+VK2y+1kSDq4fI9kApI0/dH2DoNDcNrzvMgniu+soxwQ+WFm596RslrrJydVvc86SVFjr01KKU9JeJ+TPiaKgdxA+Be6bTE8xUw2yLwEwVMO905dcI+BxUnmKupRyNnROZg002adjaO0tDctATmW3NPsQt95aBKsDxB30EGWB8JOlevxiE+2scbD5BN7VTIU+KfLT9kWQo7dCoaSftjZC7UnoWdJGoit6Gobi6+xucoECWhw92Cm99u2PueEU3WQSpypE=";
    //public static final String REWARD_2 = BuildConfig.AD_REWARDVIDEO_2;
    //public static final String Interstitial_2 = BuildConfig.AD_OBTIVE_VIDEO;
    //
    public static final String APP_KEY = "RFkBXDqxGvsL4Gn9IoMe2eTr5GuGgKGExIOPrleIzkHlac6kOhFmc-9dGeFinxOAwz3fwsnneyNiFaTIWMgPc-";
    //登录加载
    public static final String M_LOGIN_LOAD = "J_L";

//    public static final String M_ACCOUNT = "J_AW_ACI";
    //游戏配置
    public static final String M_LOAD_ALL_CONFIG = "J_ZC_GC";
    // 奖励弹窗
//    public static final String M_PROFIT = "J_PP";
    // 领取奖励
    public static final String M_GET_PROFIT = "J_ZC_PR";
    //提现页面
//    public static final String M_WITHDRAW = "J_AW_WDI";
    //全部提现页面
//    public static final String M_ALL_WITHDRAW = "J_ZC_WDI";
    //日志上报
    public static final String M_LOG = "J_MA";
    //题目列表
//    public static final String M_QUESTION_LIST = "J_AW_LIST";
    //问题回答
//    public static final String M_RESULT_ANSWER = "J_AW_QT";
    //任务列表
//    public static final String M_TASK_LIST = "J_AW_T_LIST";
    //广告观看上报计数（业务）
    //public static final String M_COUNT_AD_SHOW = "J_AW_U_VR";

    public static final String OS = "Android";
    public static final String APPCODE_HEAD = "appCode";

    public static final int ZEUS_SUCCESS = 0;


    public static final String debug_adjust_init_start = "debug_adjust_init_start";
    public static final String debug_adjust_init_complete = "debug_adjust_init_complete";
    public static final String debug_adjust_report = "debug_adjust_report";
    public static final String preload_wait = "preload_wait";
    public static final String preload_ban = "preload_ban";
    public static final String show_wait = "show_wait";

    /**
     *
     */

    public static final String GID = BuildConfig.APP_CODE + "GID";
//    public static final String OAID = BuildConfig.APP_CODE + "OAID";
//    public static final String SMID = BuildConfig.APP_CODE + "SMID";
//    public static final String USER_ID = BuildConfig.APP_CODE + "USER_ID";
//    public static final String REG_DATE = BuildConfig.APP_CODE + "REG_DATE";
//    public static final String REG_TIME = BuildConfig.APP_CODE + "REG_TIME";
//    public static final String USER_PIC = BuildConfig.APP_CODE + "USER_PIC";
//    public static final String AD_CHANNEL = BuildConfig.APP_CODE + "AD_CHANNEL";
//    public static final String CLIENT_IP = BuildConfig.APP_CODE + "CLIENT_IP";
//    public static final String ANSKY_DEVICEID = BuildConfig.APP_CODE + "ANSKY_DEVICEID";
//    public static final String SECURITY = BuildConfig.APP_CODE + "SECURITY";
//    public static final String QR_FILE_NAME = BuildConfig.APP_CODE + "QR_FILE_NAME";
//    public static final String BANNER_HEIGHT = BuildConfig.APP_CODE + "BANNER_HEIGHT";
//    public static final String NATIVE_HEIGHT = BuildConfig.APP_CODE + "NATIVE_HEIGHT";
//    public static final String TOKEN = BuildConfig.APP_CODE + "TOKEN";
//    public static final String VOLC_BD_DID = BuildConfig.APP_CODE + "VOLC_BD_DID";
//    public static final String ALI_DEVICE_TOKEN = BuildConfig.APP_CODE + "ALI_DEVICE_TOKEN";
    public static final String APP_NAME = BuildConfig.APP_CODE + "APP_NAME";
//    public static final String INIT_FLAG = BuildConfig.APP_CODE + "INIT_FLAG";
    //是否同意隐私政策
//    public static final String IS_AGREE_PRIVACY = BuildConfig.APP_CODE + "IS_AGREE_PRIVACY";
    //身份证认证通过 isIDCardPassed
//    public static final String IS_IDCARD_PASSED = BuildConfig.APP_CODE + "IS_IDCARD_PASSED";
    public static final String DEBUG_MODE = BuildConfig.APP_CODE + "DEBUG_MODE";

//    public static final String IS_REAL_NAME = "IS_REAL_NAME";
//    public static final String SAFS_DATA = "SAFS_DATA";
//    public final static String register = "register";//服务-注册
//    public final static String login = "login";//服务-登录
//    public final static String withdrawal = "withdrawal";//服务-提现
    public final static String activity = "activity";//服务-获利行为
//    public final static String READ_CLIP = "READ_CLIP";
//    public final static String CHN_HUAWEI = "huawei";
//    public final static String CHN_OPPO = "oppo";
//    public final static String CHN_VIVO = "vivo";
//    public final static String CHN_XIAOMI = "xiaomi";
//    public final static String CHN_KUAISHOU = "kuaishou";
//    public final static String CHN_TOUTIAO = "toutiao";
//    public final static String CHN_SUBCHANNEL = "subchannel";
//    public final static String WX_HEAD = "WX_HEAD";
//    public final static String WX_NAME = "WX_NAME";
//    public final static String randomid = "randomid";
//    public static final String wxbind = "wxbind";
    public static final String operator = "operator";
    public static final String os = "os"; // 系统
    public static final String osversion = "osversion";
    public final static String androidid = "androidid";//AndroidId
    public final static String appinstalltime = "appinstalltime";  // App安装时间
    public final static String appupdatetime = "appupdatetime"; // App修改时间
//    public final static String backstage = "backstage"; // 后台弹出权限
    public final static String boottime = "boottime";  // 开机时间 单位秒
    public final static String dedensity = "dedensity"; // 屏幕密度
    public final static String deheight = "deheight"; // 屏幕高度
    public final static String electric = "electric"; // 电量
//    public final static String deviationx = "deviationx";
//    public final static String deviationy = "deviationy";


    public final static String deviceid = "deviceid";
    public final static String dewidth = "dewidth";
    public final static String enableadb = "enableadb";// 是否开启调试模式 0为否 1为是

    public final static String ischarging = "ischarging"; //是否在充电中
    public final static String islocation = "islocation"; // 是否开启定位权限

    public final static String language = "language"; //当前语言
    public final static String latitude = "latitude";

    public final static String longitude = "longitude";
    public final static String lv = "V3";
    public final static String mac = "mac"; // MAC地址
    public final static String network = "network";
    public final static String vpn_use = "vpnuse";
    public final static String osrom = "osrom";

    public final static String serial = "serial";
    public final static String sim = "sim";
    public final static String simstatus = "simstatus";
    public final static String wifimac = "wifimac";
    public final static String wifiname = "wifiname";
    public final static String wifiproxy = "wifiproxy"; //
    public final static String channel = "channel";
    public final static String timeZone = "timeZone";

    public final static String secret = "secret";
    public final static String sessionId = "sessionId";
    public final static String vc = "vc";
    public final static String vn = "vn";
    public final static String googleId = "googleId";
    public final static String registerDate = "registerDate";
    public static final String requestTime = "requestTime";
    public static final String country = "country";
    public static final String chour = "chour";
    public static final String requestId = "requestId";
    public static final String userId = "userId";

    public static final String event = "event";
    public static final String eventMsg = "eventMsg";
    public static final String mediaPlatform = "mediaPlatform";
    public static final String mediaCodeId = "mediaCodeId";
    public static final String networkId = "networkId";
    public static final String ecpm = "ecpm";
    public static final String currencyCode = "currencyCode";
    public static final String platform = "platform";
    public static final String sourceId = "sourceId";
    public static final String sourceType = "sourceType";

    public final static String BASEINFO = "BASEINFO";

    public static final String LEVEL = BuildConfig.APP_CODE + "LEVEL";
    /**
     * 用户当前等级
     */
//    public static final String LEVEL = "LEVEL";
    /**
     * 提现 起始的等级
     */
    public static final String LEVEL_NEED = "NEED_LEVEL";


    public static final String FIRST_OPEN_MILLIS = BuildConfig.APP_CODE + "FIRST_OPEN_MILLIS";
    public static final String ACTIVE_USER_ECPM_VALUE = BuildConfig.APP_CODE + "ACTIVE_USER_ECPM_VALUE";
    public static final String ACTIVE_USER_DONE = BuildConfig.APP_CODE + "ACTIVE_USER_DONE";
    public static final String ACTIVE_USER_MM_IPU = BuildConfig.APP_CODE + "ACTIVE_USER_MM_IPU";
    public static final String EFFECTIVE_USER_IPU_COUNT = BuildConfig.APP_CODE + "EFFECTIVE_USER_IPU_COUNT";
    public static final String AB_TEST_TAGS = BuildConfig.APP_CODE + "AB_TEST_TAGS";
    public static final String MAX_SDK_KEY = BuildConfig.APP_CODE + "MAX_SDK_KEY";
    public static final String IS_NEW_USER = BuildConfig.APP_CODE + "IS_NEW_USER";
    public static final String SOUND_SETTING_ENBLE = BuildConfig.APP_CODE + "SOUND_SETTING_ENBLE";
    public static final String COLLECT_PIECE_NOW_LOGIN_COUNT = BuildConfig.APP_CODE + "COLLECT_PIECE_NOW_LOGIN_COUNT";
    public static final String COLLECT_PIECE_NOW_VIDEO_COUNT = BuildConfig.APP_CODE + "COLLECT_PIECE_NOW_VIDEO_COUNT";
    public static final String COLLECT_PIECE_NOW_LEVEL_COUNT = BuildConfig.APP_CODE + "COLLECT_PIECE_NOW_LEVEL_COUNT";
    public static final String LAST_LOGIN_MILLIS = BuildConfig.APP_CODE + "LAST_LOGIN_MILLIS";
    public static final String LAST_MONEY_LIMIT = BuildConfig.APP_CODE + "LAST_MONEY_LIMIT";
    public static final String INSTALL_REFERRER_DETAILS = BuildConfig.APP_CODE + "INSTALL_REFERRER_DETAILS";
    public static String adChannel = "adChannel";
    public static String adGroupId = "adGroupId";
    public static String test_country = BuildConfig.APP_CODE + "test_country";
    public static String steptime = "steptime";
    public static String displayCountry = "displayCountry";
    public static String model = "model";
    public static String brand = "brand";
    public static String androidId = "androidId";
    public static String deVersion = "deVersion";
    public static String mobileRegion = "mobile_region";
    public static String test_language = "test_language";
    public static String clueid = "clueid";
    public static String LOG_LEVEL_SWITCH = BuildConfig.APP_CODE + "LOG_LEVEL_SWITCH";
    public static String CLUE_ID = BuildConfig.APP_CODE + "CLUE_ID";
    public static String IS_FIRST_OPEN = BuildConfig.APP_CODE + "IS_FIRST_OPEN";
    public static String IS_WHITE_USER = BuildConfig.APP_CODE + "IS_WHITE_USER";
    public static String FIND_DIFF_TIPS_GUIDE_COUNT = BuildConfig.APP_CODE + "FIND_DIFF_TIPS_GUIDE_COUNT";
    public static String COLLECT_PIECE_TASK_TOTAL_COUNT = BuildConfig.APP_CODE + "COLLECT_PIECE_TASK_TOTAL_COUNT";
    public static String COLLECT_PIECE_TASK_COMPLETE_COUNT = BuildConfig.APP_CODE + "COLLECT_PIECE_TASK_COMPLETE_COUNT";
    public static String SERVER_GAME_CONFIG_ENCRYPT_BODY = BuildConfig.APP_CODE + "SERVER_GAME_CONFIG_ENCRYPT_BODY";
    public static String DB_COLLECT_PIECE_BEAN_LIST = BuildConfig.APP_CODE + "DB_COLLECT_PIECE_BEAN_LIST";
    public static String TREASURE_COMPLETE_COUNT = BuildConfig.APP_CODE + "TREASURE_COMPLETE_COUNT";
    public static String Treasure_TASK_TOTAL_COUNT = BuildConfig.APP_CODE + "Treasure_TASK_TOTAL_COUNT";
    public static String BALANCE_RATE = BuildConfig.APP_CODE + "BALANCE_RATE";

    public static final String WITHDRAW_FULL_CASH_STATUS = BuildConfig.APP_CODE + "withdraw_full_cash_status";//提现状态

}
