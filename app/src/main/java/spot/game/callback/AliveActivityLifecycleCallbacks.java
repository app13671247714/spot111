package spot.game.callback;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 活跃activity计数
 */
public class AliveActivityLifecycleCallbacks implements Application.ActivityLifecycleCallbacks {
    //
    //保存处于活跃状态的 Activity 个数
    private AtomicInteger mActivityCount = new AtomicInteger(0);

    ConcurrentLinkedQueue<ActivityAllPauseListener> callbacksList;


    public void register(ActivityAllPauseListener listener) {
        try {
            if (listener == null) {
                return;
            }
            if (callbacksList == null) {
                callbacksList = new ConcurrentLinkedQueue<>();
            }
            callbacksList.add(listener);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public void unregister(ActivityAllPauseListener listener) {
        try {
            if (listener == null) {
                return;
            }
            if (callbacksList == null) {
                callbacksList = new ConcurrentLinkedQueue<>();
            }
            callbacksList.remove(listener);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onActivityCreated(@NonNull Activity activity, @Nullable Bundle savedInstanceState) {

    }

    @Override
    public void onActivityStarted(@NonNull Activity activity) {
        //+1
        mActivityCount.getAndAdd(1);
        try {
            if (callbacksList != null && !callbacksList.isEmpty()) {
                for (ActivityAllPauseListener listener : callbacksList) {
                    if (listener == null) {
                        continue;
                    }
                    listener.hadAliveActivity(mActivityCount.get(), activity);
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResumed(@NonNull Activity activity) {
    }

    @Override
    public void onActivityPaused(@NonNull Activity activity) {
    }

    @Override
    public void onActivityStopped(@NonNull Activity activity) {
        mActivityCount.getAndDecrement();
        //退到后台，记录时间
        if (mActivityCount.get() == 0) {
            try {
                if (callbacksList != null && !callbacksList.isEmpty()) {
                    for (ActivityAllPauseListener listener : callbacksList) {
                        if (listener == null) {
                            continue;
                        }
                        listener.allActivityPaused();
                    }
                }
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public void onActivitySaveInstanceState(@NonNull Activity activity, @NonNull Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(@NonNull Activity activity) {

    }


    public interface ActivityAllPauseListener {
//        default void onActivityResumed() {
//
//        }

        default void allActivityPaused() {
        }

        default void hadAliveActivity(int count, Activity activity) {
        }
    }
}
