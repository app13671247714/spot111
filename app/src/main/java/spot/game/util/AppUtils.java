package spot.game.util;


import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import spot.game.manager.ContextManager;
import spot.game.manager.GameManager;

/**
 * //检查资源回收则重启应用
 */
public class AppUtils implements Application.ActivityLifecycleCallbacks {

    public static final int STATUS_FORCE_KILLED = -1;//应用在后台被强杀了
    public static final int STATUS_NORMAL = 2; //APP正常态

    private int appStatus = STATUS_FORCE_KILLED; //默认为被后台回收了
    private static AppUtils appStatusManager;

    public static AppUtils getInstance() {
        if (appStatusManager == null) {
            appStatusManager = new AppUtils();
        }
        return appStatusManager;
    }

    public int getAppStatus() {
        return appStatus;
    }

    public void setAppStatus(int appStatus) {
        ContextManager.getApplication().registerActivityLifecycleCallbacks(this);
        this.appStatus = appStatus;
    }

    @Override
    public void onActivityCreated(@NonNull Activity activity, @Nullable Bundle savedInstanceState) {

    }

    @Override
    public void onActivityStarted(@NonNull Activity activity) {
        if (appStatus == STATUS_FORCE_KILLED) {
            GameManager.restartApp(activity);
        }
    }

    @Override
    public void onActivityResumed(@NonNull Activity activity) {

    }

    @Override
    public void onActivityPaused(@NonNull Activity activity) {

    }

    @Override
    public void onActivityStopped(@NonNull Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(@NonNull Activity activity, @NonNull Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(@NonNull Activity activity) {

    }
}
