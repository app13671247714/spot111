package spot.game.util;

import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import spot.game.manager.ContextManager;


public class ViewUtils {

    public static void setOnClickListener(View.OnClickListener onClickListener, View... vList) {
        if (onClickListener == null || vList == null || vList.length < 1) {
            return;
        }
        for (View view : vList) {
            if (view == null) {
                continue;
            }
            view.setOnClickListener(onClickListener);
        }
    }

    public static void setOnClickListener(View.OnClickListener onClickListener, Integer... resList) {
        if (onClickListener == null || resList == null || resList.length < 1) {
            return;
        }
        for (Integer resId : resList) {
            if (resId == null) {
                continue;
            }
            try {
                ContextManager.getActivity().findViewById(resId).setOnClickListener(onClickListener);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void autoResizeText(TextView tv, CharSequence text, float defaultSp, int startChangLength, float maxSP) {
        autoResizeText(tv, text, defaultSp, startChangLength, maxSP, 11, 1);

    }

    public static void autoResizeText(TextView tv, CharSequence text, float defaultSp, int startChangLength, float maxSP, float minSP, float stepRatio) {
        try {
            if (tv == null) {
                return;
            }
            if (!TextUtils.isEmpty(text) && text.length() > startChangLength) {
                tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, Math.max(maxSP - text.length() * stepRatio, minSP));
            } else {
                tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, defaultSp);
            }
            tv.setText(text);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
