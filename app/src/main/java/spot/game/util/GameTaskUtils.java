package spot.game.util;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.SPStaticUtils;
import com.blankj.utilcode.util.TimeUtils;

import java.util.Calendar;
import java.util.Date;

import spot.game.constant.BusinessFieldName;
import spot.game.manager.ConfigManager;
import spot.game.manager.GameManager;
import spot.game.model.WithdrawStateModel;

public class GameTaskUtils {


    public static @NonNull
    WithdrawStateModel getWithdrawStateModel() {
        try {
            String json = SPStaticUtils.getString(BusinessFieldName.WITHDRAW_FULL_CASH_STATUS);
            WithdrawStateModel model = GsonUtils.fromJson(json, WithdrawStateModel.class);
            if (model == null) {
                model = new WithdrawStateModel();
            }
            return model;
        } catch (Exception e) {
            return new WithdrawStateModel();
        }
    }

    public static void setWithdrawStateModel(WithdrawStateModel model) {
        try {
            SPStaticUtils.put(BusinessFieldName.WITHDRAW_FULL_CASH_STATUS, GsonUtils.toJson(model));
        } catch (Exception e) {
        }
    }

    /**
     * 提现的限制
     *
     * @param withdrawCashPlayAdDay   需要完成看广告天数
     * @param withdrawCashPlayAdCount 需要完成每天看广告次数
     * @param withdrawCashLevel       最终提现的等级限制
     */
    public static void setWithdrawStateModel(int withdrawCashPlayAdDay, int withdrawCashPlayAdCount, int withdrawCashLevel) {
        try {
            WithdrawStateModel model = getWithdrawStateModel();
            model.setWithdrawCashPlayAdDay(withdrawCashPlayAdDay);
            model.setWithdrawCashPlayAdDayCount(withdrawCashPlayAdCount);
            model.setWithdrawCashLevel(withdrawCashLevel);
            setWithdrawStateModel(model);
        } catch (Exception e) {
        }
    }

    /**
     * 不是同一天重置广告观看量
     * 判断通关状态
     */
    public static void initWithdrawStateModel() {
        WithdrawStateModel model = getWithdrawStateModel();
        int day = TimeUtils.getValueByCalendarField(new Date(), Calendar.DAY_OF_YEAR);
        if (model.getDate() != day) {
            model.setDate(day);
            model.setPlayAdDayCount(0);
        }

        long timeCountdown = getHourCountdown();
        if (timeCountdown <= 0 && model.getWithdrawCashProgress() == 1) {
            model.setWithdrawCashProgress(2);
        }

        if (model.getCompleteDay() >= model.getWithdrawCashPlayAdDay() && model.getWithdrawCashProgress() == 2) {
            model.setWithdrawCashProgress(3);
        }

        if (ConfigManager.getLocalLevel() > model.getWithdrawCashLevel() && model.getWithdrawCashProgress() == 3) {
            model.setWithdrawCashProgress(4);
        }

        setWithdrawStateModel(model);
    }

    /**
     * 统计广告观看次数
     */
    public static void setAdPlayComplete() {
        WithdrawStateModel model = getWithdrawStateModel();
        if (model.isWithdrawCashIng() && model.getWithdrawCashProgress() == 2) {
            int day = TimeUtils.getValueByCalendarField(new Date(), Calendar.DAY_OF_YEAR);
            if (model.getDate() == day) {
                //同一天观看的广告增加
                model.setPlayAdDayCount(model.getPlayAdDayCount() + 1);
            } else {
                model.setDate(day);
                model.setPlayAdDayCount(1);
            }

            //判断当天是否满足任务
            if (model.getPlayAdDayCount() >= model.getWithdrawCashPlayAdDayCount()) {
                //判断今天是否完成过
                if (day != model.getCompleteDate()) {
                    //完成了几天的任务
                    model.setCompleteDay(model.getCompleteDay() + 1);
                    //记录完成任务的日期
                    model.setCompleteDate(day);
                }

                //任务达标进入下一阶段
                if (model.getCompleteDay() >= model.getWithdrawCashPlayAdDay()) {
                    model.setWithdrawCashProgress(3);
                }
            }
            setWithdrawStateModel(model);
        }
    }

    private static long dayTime = 3600 * 24;
    private static long dayTimeDebug = 60;

    /**
     * 获取提现时间
     *
     * @return 0说明进入下一阶段
     */
    public static long getHourCountdown() {
        WithdrawStateModel model = getWithdrawStateModel();
        long hour = model.getWithdrawCashHour();
        long timeCount = System.currentTimeMillis() - hour;
        timeCount = timeCount / 1000;

        //TODO 测试代码
        //if (BuildConfig.PRODUCT_FLAVORS.equalsIgnoreCase("anyThink")) {
        //    if (timeCount > dayTimeDebug) {
        //        return 0L;
        //    }
        //} else
        {
            if (timeCount > dayTime) {
                return 0L;
            }
        }

        return dayTime - timeCount;
    }

    /**
     * 秒转时间
     *
     * @param seconds
     * @return
     */
    public static String formatSecondsToHMS(long seconds) {
        long hours = seconds / 3600;
        long minutes = (seconds % 3600) / 60;
        long remainingSeconds = seconds % 60;

        String formattedTime = String.format("%02d:%02d:%02d", hours, minutes, remainingSeconds);

        return formattedTime;
    }

    /**
     * 开始提现
     */
    public static void startWithdrawalCash() {
        WithdrawStateModel model = getWithdrawStateModel();
        model.setWithdrawCashIng(true);
        model.setWithdrawCashProgress(1);
        setWithdrawStateModel(model);
//        ConfigManager.addBalance(Double.parseDouble("-"+model.getWithdrawCashNmu()));
        //TODO 这里对余额进行了操作 减去全部
//        ConfigManager.setBalance(0.0F);
        GameManager.refreshAccount(0.0F);

    }

    /**
     * 取消提现
     */
    public static void resetTaskState() {
        WithdrawStateModel model = getWithdrawStateModel();
        //TODO 这里对余额进行了操作 增加提现中部分
//        ConfigManager.addBalance(model.getWithdrawCashNmu());
//        ConfigManager.setBalance(Double.parseDouble(ConfigManager.getBalance()) + Double.parseDouble(modxel.getWithdrawCashNmu())+"");
        GameManager.refreshAccount(model.getWithdrawCashNmu() + ConfigManager.getBalance());

        model.setWithdrawCashIng(false);
        model.setWithdrawCashProgress(0);
        model.setWithdrawCashNmu(0F);
        model.setWithdrawCashHour(0L);
        model.setCompleteDay(0);
        setWithdrawStateModel(model);
    }

}
