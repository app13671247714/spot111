package spot.game.util;

import static android.telephony.TelephonyManager.SIM_STATE_READY;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.BatteryManager;
import android.os.Build;
import android.os.SystemClock;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import com.blankj.utilcode.util.LogUtils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.Reader;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.TimeZone;

import spot.game.manager.ContextManager;

/**
 * @Class: DeviceUtils
 * @Date: 2022/11/8
 * @Description:
 */
public class DeviceUtils {


    public static int getChour() {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.HOUR_OF_DAY);
    }

    public static String getTimeZone() {
        return TimeZone.getDefault().getDisplayName(false, TimeZone.SHORT);
    }

    public static String getCountryDialCode() {
        try {
            TelephonyManager telephonyMngr = (TelephonyManager) ContextManager.getApplication().getSystemService(Context.TELEPHONY_SERVICE);
            String networkCountryIso = telephonyMngr.getNetworkCountryIso();
            return networkCountryIso;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * 是否正在使用VPN
     */
    public static int vpn() {
        try {
            Enumeration niList = NetworkInterface.getNetworkInterfaces();
            if (niList != null) {
                for (Object intf : java.util.Collections.list(niList)) {
                    if (intf instanceof NetworkInterface) {
                        NetworkInterface networkInterface = (NetworkInterface) intf;
                        if (!networkInterface.isUp() || networkInterface.getInterfaceAddresses().size() == 0) {
                            continue;
                        }
                        if ("tun0".equalsIgnoreCase(networkInterface.getName())) {
                            return 1;
                        }
                        if ("ppp0".equalsIgnoreCase(networkInterface.getName())) {
                            return 1;
                        }
                    } else {
                        return 0;
                    }
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * 手机是否开启网络代理
     *
     * @param context ctx
     * @return 1使用代理 0 没有使用代理
     */
    public static int mobileNetProxyUsed(Context context) {
        try {
            final boolean IS_ICS_OR_LATER = Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH;
            String proxyAddress;
            int proxyPort;
            if (IS_ICS_OR_LATER) {
                proxyAddress = System.getProperty("http.proxyHost");
                String portStr = System.getProperty("http.proxyPort");
                proxyPort = Integer.parseInt((portStr != null ? portStr : "-1"));
            } else {
                proxyAddress = android.net.Proxy.getHost(context);
                proxyPort = android.net.Proxy.getPort(context);
            }
            boolean isProxy = (!TextUtils.isEmpty(proxyAddress)) && (proxyPort != -1);
            return isProxy ? 1 : 0;
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * 获取运行商code
     *
     * @param context ctx
     * @return 46000...
     */
    public static String mobileSimOperator(Context context) {
        String simOp = getSimOperator(context);
        if (simOp != null && !"".equals(simOp)) {
            return simOp;
        }
        return "-1";
    }


    public static String getWifiInfo(Context context, int type) {
        String bssid = "";
        String ssid = "";
        try {
            WifiManager wifiManager = (WifiManager) context
                    .getSystemService(Context.WIFI_SERVICE);
            if (wifiManager != null) {
                WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                if (wifiInfo != null) {
                    bssid = wifiInfo.getBSSID();
                    ssid = wifiInfo.getSSID().replaceAll("\"", "");
                }
            }
        } catch (Throwable e) {

        } finally {
            if (TextUtils.isEmpty(bssid) || "02:00:00:00:00:00".equals(bssid)) {
                bssid = "-1";
            }
            if (TextUtils.isEmpty(ssid) || ssid.contains("unknown")) {
                ssid = "-1";
            }
        }
        if (type == 1) {
            return bssid;
        }
        return ssid;
    }

    public static int mobileSimState(Context context) {
        try {
            TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);//取得相关系统服务
            if (tm != null) {
                if (tm.getSimState() != SIM_STATE_READY) {
                    return 0;
                } else {
                    return 1;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    /**
     * 获取手机sim卡运营商
     *
     * @param context context
     * @return
     */
    private static String getSimOperator(Context context) {
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (null != tm) {
            return tm.getSimOperator();
        }
        return null;
    }


    private static String getSimOperatorName(Context context) {
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (null != tm) {
            return tm.getSimOperatorName();
        }
        return null;
    }

    @SuppressLint("MissingPermission")
    public static String getSim(Context context) {
        return "-1";
    }

    public static String getSerialNumber() {
        return "-1";
    }

    private static final String NETWORK_UNKNOWN = "unknown";
    private static final String NETWORK_WIFI = "wifi";
    private static final String NETWORK_2G = "2G";
    private static final String NETWORK_3G = "3G";
    private static final String NETWORK_4G = "4G";
    private static final String NETWORK_5G = "5G";

    /**
     * 网络类型
     *
     * @param context
     * @return
     */
    public static String getNetWorkType(Context context) {
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE); // 获取网络服务
        if (null == connManager) { // 为空则认为无网络
            return NETWORK_UNKNOWN;
        }
        try {
            // 获取网络类型，如果为空，返回无网络
            NetworkInfo activeNetInfo = connManager.getActiveNetworkInfo();
            if (activeNetInfo == null || !activeNetInfo.isAvailable()) {
                return NETWORK_UNKNOWN;
            }
            // 判断是否为WIFI
            NetworkInfo wifiInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (null != wifiInfo) {
                NetworkInfo.State state = wifiInfo.getState();
                if (null != state) {
                    if (state == NetworkInfo.State.CONNECTED || state == NetworkInfo.State.CONNECTING) {
                        return NETWORK_WIFI;
                    }
                }
            }
            // 若不是WIFI，则去判断是2G、3G、4G网
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (telephonyManager == null) return NETWORK_WIFI;
            @SuppressLint("MissingPermission")
            int networkType = telephonyManager.getNetworkType();
            switch (networkType) {

                // 2G网络
                case TelephonyManager.NETWORK_TYPE_GPRS:
                case TelephonyManager.NETWORK_TYPE_CDMA:
                case TelephonyManager.NETWORK_TYPE_EDGE:
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                case TelephonyManager.NETWORK_TYPE_IDEN:
                    return NETWORK_2G;
                // 3G网络
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                case TelephonyManager.NETWORK_TYPE_UMTS:
                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                case TelephonyManager.NETWORK_TYPE_HSDPA:
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                case TelephonyManager.NETWORK_TYPE_HSPA:
                case TelephonyManager.NETWORK_TYPE_EVDO_B:
                case TelephonyManager.NETWORK_TYPE_EHRPD:
                case TelephonyManager.NETWORK_TYPE_HSPAP:
                    return NETWORK_3G;
                // 4G网络
                case TelephonyManager.NETWORK_TYPE_LTE:
                case 19:// 聚波载合 4G+
                    return NETWORK_4G;
                // 5G
// case TelephonyManager.NETWORK_TYPE_NR:// 需要 SdkVersion>=29
                case 20:// 当 SdkVersion<=28 直接写20
                    return NETWORK_5G;
                default:
                    return NETWORK_UNKNOWN;
            }
        } catch (Throwable e) {

        }
        return NETWORK_UNKNOWN;
    }

    /**
     * 各个版本 MAC
     *
     * @return
     */
    public static String getMac(Context context) {
        String strMac = null;

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {

            strMac = getLocalMacAddressFromWifiInfo(context);
            return strMac;
        } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N
                && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            strMac = getMacAddress(context);
            return strMac;
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

            if (!TextUtils.isEmpty(getMacAddress(context))) {

                strMac = getMacAddress(context);
                return strMac;
            } else if (!TextUtils.isEmpty(getMachineHardwareAddress())) {

                strMac = getMachineHardwareAddress();
                return strMac;
            } else {

                strMac = getLocalMacAddressFromBusybox();
                return strMac;
            }
        }

        return "02:00:00:00:00:00";
    }

    /**
     * 根据wifi信息获取本地mac
     *
     * @param context
     * @return
     */
    public static String getLocalMacAddressFromWifiInfo(Context context) {
        WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo winfo = wifi.getConnectionInfo();
        String mac = winfo.getMacAddress();
        return mac;
    }

    /**
     * android 6.0及以上、7.0以下 获取mac地址
     *
     * @param context
     * @return
     */
    public static String getMacAddress(Context context) {

        // 如果是6.0以下，直接通过wifimanager获取
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            String macAddress0 = getMacAddress0(context);
            if (!TextUtils.isEmpty(macAddress0)) {
                return macAddress0;
            }
        }
        String str = "";
        String macSerial = "";
        try {
            Process pp = Runtime.getRuntime().exec(
                    "cat /sys/class/net/wlan0/address");
            InputStreamReader ir = new InputStreamReader(pp.getInputStream());
            LineNumberReader input = new LineNumberReader(ir);
            for (; null != str; ) {
                str = input.readLine();
                if (str != null) {
                    macSerial = str.trim();// 去空格
                    break;
                }
            }
        } catch (Exception ex) {

        }
        if (macSerial == null || "".equals(macSerial)) {
            try {
                return loadFileAsString("/sys/class/net/eth0/address")
                        .toUpperCase().substring(0, 17);
            } catch (Exception e) {


            }

        }
        return macSerial;
    }

    private static String getMacAddress0(Context context) {
        if (isAccessWifiStateAuthorized(context)) {
            WifiManager wifiMgr = (WifiManager) context
                    .getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = null;
            try {
                wifiInfo = wifiMgr.getConnectionInfo();
                return wifiInfo.getMacAddress();
            } catch (Exception e) {

            }

        }
        return "";
    }

    /**
     * Check whether accessing wifi state is permitted
     *
     * @param context
     * @return
     */
    private static boolean isAccessWifiStateAuthorized(Context context) {
        if (PackageManager.PERMISSION_GRANTED == context
                .checkCallingOrSelfPermission("android.permission.ACCESS_WIFI_STATE")) {

            return true;
        } else
            return false;
    }

    private static String loadFileAsString(String fileName) throws Exception {
        FileReader reader = new FileReader(fileName);
        String text = loadReaderAsString(reader);
        reader.close();
        return text;
    }

    private static String loadReaderAsString(Reader reader) throws Exception {
        StringBuilder builder = new StringBuilder();
        char[] buffer = new char[4096];
        int readLength = reader.read(buffer);
        while (readLength >= 0) {
            builder.append(buffer, 0, readLength);
            readLength = reader.read(buffer);
        }
        return builder.toString();
    }

    /**
     * android 7.0及以上 （2）扫描各个网络接口获取mac地址
     *
     */
    /**
     * 获取设备HardwareAddress地址
     *
     * @return
     */
    public static String getMachineHardwareAddress() {
        Enumeration<NetworkInterface> interfaces = null;
        try {
            interfaces = NetworkInterface.getNetworkInterfaces();
        } catch (SocketException e) {
            e.printStackTrace();
        }
        String hardWareAddress = null;
        NetworkInterface iF = null;
        if (interfaces == null) {
            return null;
        }
        while (interfaces.hasMoreElements()) {
            iF = interfaces.nextElement();
            try {
                hardWareAddress = bytesToString(iF.getHardwareAddress());
                if (hardWareAddress != null)
                    break;
            } catch (SocketException e) {
                e.printStackTrace();
            }
        }
        return hardWareAddress;
    }

    /**
     * android 7.0及以上 （3）通过busybox获取本地存储的mac地址
     *
     */

    /**
     * 根据busybox获取本地Mac
     *
     * @return
     */
    public static String getLocalMacAddressFromBusybox() {
        String result = "";
        String Mac = "";
        result = callCmd("busybox ifconfig", "HWaddr");
        // 如果返回的result == null，则说明网络不可取
        if (result == null) {
            return "网络异常";
        }
        // 对该行数据进行解析
        // 例如：eth0 Link encap:Ethernet HWaddr 00:16:E8:3E:DF:67
        if (result.length() > 0 && result.contains("HWaddr") == true) {
            Mac = result.substring(result.indexOf("HWaddr") + 6,
                    result.length() - 1);
            result = Mac;
        }
        return result;
    }

    private static String callCmd(String cmd, String filter) {
        String result = "";
        String line = "";
        try {
            Process proc = Runtime.getRuntime().exec(cmd);
            InputStreamReader is = new InputStreamReader(proc.getInputStream());
            BufferedReader br = new BufferedReader(is);

            while ((line = br.readLine()) != null
                    && line.contains(filter) == false) {
                result += line;
            }

            result = line;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /***
     * byte转为String
     *
     * @param bytes
     * @return
     */
    private static String bytesToString(byte[] bytes) {
        if (bytes == null || bytes.length == 0) {
            return null;
        }
        StringBuilder buf = new StringBuilder();
        for (byte b : bytes) {
            buf.append(String.format("%02X:", b));
        }
        if (buf.length() > 0) {
            buf.deleteCharAt(buf.length() - 1);
        }
        return buf.toString();
    }


    /**
     * 手机是否充电中
     *
     * @param context ctx
     * @return 1充电 0未充电
     */
    public static int mobileIsCharge(Context context) {
        Intent batteryBroadcast = context.registerReceiver(null,
                new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        if (batteryBroadcast != null) {
            boolean isCharging = batteryBroadcast.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1) != 0;
            if (isCharging) {
                return 1;
            } else {
                return 0;
            }
        }
        return -1;
    }

    /**
     * 是否开启开发者模式
     *
     * @param context ctx
     * @return 1开启 0没开启
     */
    public static int mobileOpenADBDebugger(Context context) {
        try {
            boolean debuggerOpen = (Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.ADB_ENABLED, 0) > 0);
            if (debuggerOpen) {
                return 1;
            } else {
                return 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    /**
     * 获取电池电量
     *
     * @param context ctx
     * @return 1-100
     */
    public static int getBatteryLevel(Context context) {
        try {
            IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
            Intent batteryStatus = context.registerReceiver(null, ifilter);

            if (null != batteryStatus) {
                int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
                if (level != 0) {
                    return level;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }


    /**
     * 获取设备信息
     *
     * @param context
     * @param type    1:with  2:height
     * @return
     */
    public static int getDeviceInfo(Context context, int type) {

        int withPixels = context.getResources().getDisplayMetrics().widthPixels;
        int deviceHeight = context.getResources().getDisplayMetrics().heightPixels;

        if (type == 1) {
            if (withPixels <= 0) {
                return -1;
            } else {
                return withPixels;
            }
        } else {
            if (deviceHeight <= 0) {
                return -1;
            } else {
                return deviceHeight;
            }
        }
    }

    /**
     * 获取屏幕密度
     *
     * @param context
     * @return
     */
    public static String getDeviceDensity(Context context) {
        try {
            float density = context.getResources().getDisplayMetrics().density;

            if (density <= 0) {
                return "-1";
            } else {
                return String.valueOf(density);
            }
        } catch (Exception e) {
            return "-1";
        }
    }

    /**
     * 获取系统开机时间  毫秒
     *
     * @return
     */
    public static long getBootTime() {
        long realTime = SystemClock.elapsedRealtimeNanos();
        if (realTime == 0) {
            return -1L;
        } else {
            return SystemClock.elapsedRealtimeNanos() / 1000000;
        }
    }

    /**
     * 获取最后升级时间
     *
     * @param context
     * @return
     */
    public static long getLastUpdateTime(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);

            if (info != null) {
                return info.lastUpdateTime;
            } else {
                return -1;
            }
        } catch (Exception e) {

            return -1;
        }
    }

    /**
     * 获取安装时间
     *
     * @param context
     * @return
     */
    public static long getInstallTime(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);

            if (info != null) {
                return info.firstInstallTime;
            } else {
                return -1;
            }
        } catch (Exception e) {

            return -1;
        }
    }

    /**
     * 获取AndroidId
     *
     * @param context
     * @return
     */
    public static String getAndroidId(Context context) {
        if (context == null) {
            return "-1";
        }
        String androidId = Settings.System.getString(context.getContentResolver(), Settings.System.ANDROID_ID);
        if (TextUtils.isEmpty(androidId)) {
            return "-1";
        } else {
            return androidId;
        }
    }

    public static boolean isFinishActivitiesOptionEnabled(Context context) {
        int result = 0;
        try {
            result = Settings.Global.getInt(context.getContentResolver(), Settings.Global.ALWAYS_FINISH_ACTIVITIES, 0);
            if (result == 1) {
                return true;
            }
            result = Settings.System.getInt(context.getContentResolver(), Settings.Global.ALWAYS_FINISH_ACTIVITIES, 0);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return result == 1;
    }

    /**
     * shows Settings -> Developer options screen
     */
    public static void showDeveloperOptionsScreen(Activity activity) {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DEVELOPMENT_SETTINGS);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        activity.startActivity(intent);
    }

    public static boolean ping(String ip) {
        String result = null;
        try {
            Process p = Runtime.getRuntime().exec("ping -c 3 -w 100 " + ip);//ping3次
// 读取ping的内容，可不加。
            InputStream input = p.getInputStream();
            BufferedReader in = new BufferedReader(new InputStreamReader(input));
            StringBuffer stringBuffer = new StringBuffer();
            String content = "";
            while ((content = in.readLine()) != null) {
                stringBuffer.append(content);
            }
            LogUtils.e("result content : " + stringBuffer.toString());
// PING的状态
            int status = p.waitFor();
            if (status == 0) {
                result = "successful~";
                return true;
            } else {
                result = "failed~ cannot reach the IP address";
            }
        } catch (Throwable e) {
            result = "failed~ " + e.getMessage();
        } finally {
            LogUtils.e("result = " + result);
        }
        return false;

    }

}
