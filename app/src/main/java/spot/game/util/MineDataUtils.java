package spot.game.util;

import android.content.Context;
import android.os.Handler;

import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ThreadUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import spot.game.manager.GameManager;
import spot.game.view.gameFindFileDiffFile.GameFindFileDiffTargetModel;

public class MineDataUtils {

    private static MineDataUtils instance;

    public static MineDataUtils getInstance() {
        if (instance == null) {
            instance = new MineDataUtils();
        }
        return instance;
    }

    private MineDataUtils() {
    }


    //宝藏出现关卡
    public static final List<Integer> treasureConfigList = Arrays.asList(
            3, 7, 10, 15, 18
            , 22, 25, 30, 36, 42
            , 50, 57, 65, 78, 90
            , 100, 112, 125, 136, 148
            , 160, 180, 200, 220, 242
            , 267, 290, 325, 350, 378
            , 400, 430, 460, 500, 800
            , 1000
    );
    //宝藏图标顺序
    public static final List<Integer> treasureItemConfigList = Arrays.asList(
            1, 5, 7, 4, 2
            , 6, 3, 11, 9, 15
            , 8, 14, 13, 12, 10
            , 23, 21, 18, 19, 16
            , 17, 25, 20, 24, 22
            , 30, 27, 31, 28, 26
            , 29, 33, 32, 34, 35
            , 36
    );

    private List<GameFindFileDiffTargetModel> questionLists;


    public void loadCorrectZone(Handler mHandler, Context context, int what) {
        if (questionLists != null && !questionLists.isEmpty()) {
            mHandler.sendEmptyMessage(what);
            return;
        }
        ThreadUtils.executeByIo(new ThreadUtils.SimpleTask<Object>() {
            @Override
            public Object doInBackground() throws Throwable {
                try {
                    if (questionLists == null) {
                        questionLists = new ArrayList<>();
                    }
                    String assetInfoStr = GameManager.getAssetInfoStr(context, "spot_gola_find_diff/spot_gola_target.json");
                    JSONObject jsonObject = new JSONObject(assetInfoStr);
                    JSONArray jsonArray = jsonObject.getJSONArray("file_name_a_and_b");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        String json = jsonObject1.toString();
                        //LogUtils.e("loadCorrectZone=" + json);
                        questionLists.add(GsonUtils.fromJson(json, GameFindFileDiffTargetModel.class));
                    }
                    LogUtils.e("loadCorrectZone=" + questionLists.size());
                } catch (Throwable e) {
                    LogUtils.e("loadCorrectZone=" + e.getMessage());
                }
                mHandler.sendEmptyMessage(what);
                return null;
            }

            @Override
            public void onSuccess(Object result) {
            }
        });
    }

    public List<GameFindFileDiffTargetModel> getQuestionLists() {
        return questionLists;
    }
}

