package spot.game.util;

import java.security.SecureRandom;

public class RandomStringGenerator {
    private static final String CHARACTERS012 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    private static final String CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static SecureRandom random = new SecureRandom();

    public static String generateRandomString(int length) {
        StringBuilder stringBuilder = new StringBuilder();

        int randomIndex = random.nextInt(CHARACTERS.length());
        char randomChar = CHARACTERS.charAt(randomIndex);
        stringBuilder.append(randomChar).append("-");

        for (int i = 0; i < length; i++) {
            randomIndex = random.nextInt(CHARACTERS012.length());
            randomChar = CHARACTERS012.charAt(randomIndex);
            stringBuilder.append(randomChar);
        }
        return stringBuilder.toString();
    }
}
