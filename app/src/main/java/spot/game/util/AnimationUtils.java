package spot.game.util;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.view.View;

/**
 * 播放动画
 */
public class AnimationUtils {

    public static AnimatorSet playScaleAnimation(View view) {
        return playScaleAnimation(view, 0, 2_000);
    }

    public static AnimatorSet playScaleAnimation(View view, int repeatCount, long duration) {
        AnimatorSet animatorSet = new AnimatorSet();

        ObjectAnimator anim1 = ObjectAnimator.ofFloat(view, "scaleX", 1, 1.05F, 0.95F, 1.1F, 0.9F, 1);
        anim1.setRepeatCount(repeatCount);
        anim1.setDuration(duration);
        ObjectAnimator anim2 = ObjectAnimator.ofFloat(view, "scaleY", 1, 1.05F, 0.95F, 1.1F, 0.9F, 1);
        anim2.setRepeatCount(repeatCount);
        anim2.setDuration(duration);
        animatorSet.playTogether(anim1, anim2);
        animatorSet.start();
        return animatorSet;
    }

    public static void playTranslateAnimation(View view) {
        playTranslateAnimation(view, 0);
    }

    public static void playTranslateAnimation(View view, int repeatCount) {
        ObjectAnimator anim1 = ObjectAnimator.ofFloat(view, "translationX", 0f, -(float) (0.1 * view.getWidth()), 0);
        anim1.setRepeatCount(repeatCount);
        anim1.setDuration(2000);
        anim1.start();
    }

    public static void playRotationAnimation(View view,long duration ) {
        ObjectAnimator anim1 = ObjectAnimator.ofFloat(view, "rotation", 0f, 360f);
        anim1.setRepeatCount(ObjectAnimator.INFINITE);
        anim1.setDuration(duration);
        anim1.setInterpolator(null);
        anim1.start();
    }

}
