package spot.game.util;

import android.text.TextUtils;
import android.util.Base64;

import androidx.annotation.Nullable;

import java.security.SecureRandom;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * 不指定密码进行加密、解密
 */
public class AesGcmUtil {

    private static final String TRANSFORMATION = "AES/GCM/NoPadding";
    private static final String UTF_8 = "UTF-8";

    private final static int GCM_IV_LENGTH = 12;
    private final static int GCM_TAG_LENGTH = 16;

    public static String encrypt(String privateString) {
        return encrypt(getEncryptIv(), privateString);
    }

    @Nullable
    private static String encrypt(byte[] iv, String privateString) {
        try {
            SecretKey skey = generateKey(); // key is 16 zero bytes
            (new SecureRandom()).nextBytes(iv);

            Cipher cipher = Cipher.getInstance(TRANSFORMATION);
            GCMParameterSpec ivSpec = new GCMParameterSpec(GCM_TAG_LENGTH * Byte.SIZE, iv);
            cipher.init(Cipher.ENCRYPT_MODE, skey, ivSpec);

            byte[] ciphertext = cipher.doFinal(privateString.getBytes("UTF8"));
            byte[] encrypted = new byte[iv.length + ciphertext.length];
            System.arraycopy(iv, 0, encrypted, 0, iv.length);
            System.arraycopy(ciphertext, 0, encrypted, iv.length, ciphertext.length);

            String encoded = Base64.encodeToString(encrypted, Base64.NO_WRAP);

            return encoded;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String decrypt(String encrypted) {
        if (TextUtils.isEmpty(encrypted)) {
            return "";
        }
        byte[] decoded = Base64.decode(encrypted, Base64.NO_WRAP);

        byte[] iv = Arrays.copyOfRange(decoded, 0, GCM_IV_LENGTH);

        return decrypt(iv, decoded);
    }

    @Nullable
    private static String decrypt(byte[] iv, byte[] decoded) {
        try {
            SecretKey skey = generateKey(); // key is 16 zero bytes

            Cipher cipher = Cipher.getInstance(TRANSFORMATION);
            GCMParameterSpec ivSpec = new GCMParameterSpec(GCM_TAG_LENGTH * Byte.SIZE, iv);
            cipher.init(Cipher.DECRYPT_MODE, skey, ivSpec);

            byte[] ciphertext = cipher.doFinal(decoded, GCM_IV_LENGTH, decoded.length - GCM_IV_LENGTH);

            String result = new String(ciphertext, "UTF8");

            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    private static SecretKey generateKey() {
        return new SecretKeySpec(getKey(), TRANSFORMATION);
    }

    private static byte[] getKey() {
        return new byte[16];
    }

    private static byte[] getEncryptIv() {
        return new byte[GCM_IV_LENGTH];
    }
}
