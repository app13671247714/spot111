package spot.game.util;

import static android.content.Context.NOTIFICATION_SERVICE;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.widget.RemoteViews;

import androidx.core.app.NotificationCompat;

import net.goal.differences.BuildConfig;
import net.goal.differences.R;

import spot.differences.gola.GolaActivitySS3;
import spot.game.constant.GameConfig;
import spot.game.manager.BalanceManager;
import spot.game.manager.ConfigManager;
import spot.game.manager.ContextManager;
import spot.game.manager.GameManager;


/**
 * @Class: NotifyUtil
 * @Date: 2022/12/20
 * @Description:
 */
public class NotifyUtil {
    private static NotifyUtil sInstance;
    protected int NOTIFI_PLAYER_ID = 20221222 + BuildConfig.VERSION_CODE;

    private NotifyUtil() {
        initNoticePlayer();
    }


    private NotificationManager mNotifiManager;
    private RemoteViews mNotifiPlayerMiniView;
    private Notification mNotifiPlayer;

    private void initNoticePlayer() {
        String id = BuildConfig.APP_CODE + "";
        String name = BuildConfig.APP_CODE + "";
        mNotifiManager = (NotificationManager) ContextManager.getApplication().getSystemService(NOTIFICATION_SERVICE);
        mNotifiPlayerMiniView = new RemoteViews(ContextManager.getApplication().getPackageName(), R.layout.game_spot_notice_layout);
        mNotifiPlayer = null;
        Intent resultIntent = new Intent(Intent.ACTION_MAIN); // 启动栈顶的activity
        resultIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        resultIntent.putExtra("NoticeLaunch", true);
        Activity activity = null;
        try {
            activity = ContextManager.getActivity();
            resultIntent.setClass(activity, activity.getClass());
        } catch (Exception e) {
            resultIntent.setClass(ContextManager.getApplication(), GolaActivitySS3.class);
        }
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
        PendingIntent contentIntent = PendingIntent.getActivity(ContextManager.getApplication(), 0, resultIntent, PendingIntent.FLAG_IMMUTABLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(id, name, NotificationManager.IMPORTANCE_DEFAULT);
            mChannel.setSound(null, null);
            mChannel.enableVibration(false);
            mChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            mNotifiManager.createNotificationChannel(mChannel);
            mNotifiPlayer = new Notification.Builder(ContextManager.getApplication(), id)
                    .setChannelId(id)
                    .setSmallIcon(R.mipmap.game_spot_ic_launcher)
                    .setCustomContentView(mNotifiPlayerMiniView)
                    .setVisibility(Notification.VISIBILITY_PUBLIC)
                    .setContentIntent(contentIntent)
                    .setOngoing(true)
                    .build();
        } else {
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(ContextManager.getApplication(), id)
                    .setChannelId(id)
                    .setSmallIcon(R.mipmap.game_spot_ic_launcher)
                    .setContent(mNotifiPlayerMiniView)
                    .setPriority(Notification.PRIORITY_MAX)
                    .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                    .setDefaults(NotificationCompat.FLAG_ONLY_ALERT_ONCE)
                    .setContentIntent(contentIntent)
                    .setOngoing(true);
            mNotifiPlayer = notificationBuilder.build();
        }
    }

    public void cancel() {
        if (mNotifiManager != null) mNotifiManager.cancel(NOTIFI_PLAYER_ID);
    }

    public static NotifyUtil getInstance() {
        if (sInstance == null) {
            synchronized (NotifyUtil.class) {
                if (sInstance == null) {
                    sInstance = new NotifyUtil();
                }
            }
        }
        return sInstance;
    }


    public void updateNotify() {
        //100默认为白包
        if (GameConfig.isWhite) {
            return;
        }
        if (mNotifiManager != null) mNotifiManager.notify(NOTIFI_PLAYER_ID, mNotifiPlayer);
        if (mNotifiPlayerMiniView == null) return;
        String temp = ContextManager.getApplication().getResources().getString(R.string.game_spot_you_earned);
        String balance = ConfigManager.getCurrency() + BalanceManager.getFormatBalance();
        SpannableStringBuilder builder = new SpannableStringBuilder(temp + balance + ";");
        ForegroundColorSpan colorSpan = new ForegroundColorSpan(ContextManager.getApplication().getResources().getColor(R.color.color_FFEB524D));
        builder.setSpan(colorSpan, temp.length(), temp.length() + balance.length(), Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        mNotifiPlayerMiniView.setTextViewText(R.id.notice_desc, builder);
        if (mNotifiManager != null) mNotifiManager.notify(NOTIFI_PLAYER_ID, mNotifiPlayer);
    }


}
