package spot.game.util;

/**
 * @Class: FastClickUtil
 * @Date: 2022/11/8
 * @Description:
 */
public class FastClickUtil {
    public final static int S_SPACE_TIME = 400;//时间间隔


    private static long lastClickTime = 0;//上次点击的时间
    private static long lastMultiClickTime = 0;//上次点击的时间
    private static int spaceTime = 400;//时间间隔

    public static boolean isFastClick() {
        long currentTime = System.currentTimeMillis();//当前系统时间
        boolean isFirstClick;
        if (currentTime - lastClickTime > spaceTime) {
            isFirstClick = false;
        } else {
            isFirstClick = true;
        }
        lastClickTime = currentTime;
        return isFirstClick;
    }

    public static boolean isMultiClick() {
        long curClickTime = System.currentTimeMillis();
        if ((curClickTime - lastMultiClickTime) <= spaceTime) {
            return true;
        } else {
            lastMultiClickTime = curClickTime;
        }
        return false;
    }
}
