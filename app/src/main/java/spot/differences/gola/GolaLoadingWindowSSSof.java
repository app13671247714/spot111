package spot.differences.gola;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ThreadUtils;

import net.goal.differences.BuildConfig;
import net.goal.differences.R;
import net.goal.differences.databinding.GameSpotDialogLoadingBinding;

import java.util.concurrent.atomic.AtomicReference;

import spot.game.manager.ContextManager;

/**
 * @Class: LoadingWindow
 */
public class GolaLoadingWindowSSSof extends GolaBaseWindows {
    private GameSpotDialogLoadingBinding mBinding;
    private static AtomicReference<GolaLoadingWindowSSSof> mLoadingWindow;
    private static final int MSG_RETRY_SHOW = BuildConfig.VERSION_CODE;

    private static final Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_RETRY_SHOW:
                    showLoadingWindow();
                    break;
            }
        }
    };


    public GolaLoadingWindowSSSof(Context context) {
        super(context);
        mBinding = GameSpotDialogLoadingBinding.inflate(LayoutInflater.from(context));
        setContentView(mBinding.getRoot());
        setOutSideDismiss(false);
        ImageView imageView = new ImageView(getContext());
        imageView.setImageResource(R.color.bg_window_color);
        setBackgroundView(imageView);
        setPopupGravity(Gravity.CENTER);
    }

    /**
     * 展示
     */
    public static void showLoadingWindow() {
        showLoadingWindow(0);
    }

    /**
     * 展示
     */
    public static void showLoadingWindow(long waitMillis) {
        Activity activity = null;
        try {
            activity = ContextManager.getActivity();
        } catch (Exception e) {
            e.printStackTrace();
            mHandler.sendEmptyMessageDelayed(MSG_RETRY_SHOW, 1000L);
            return;
        }
        if (waitMillis < 0) {
            waitMillis = 0;
        }
        Activity finalActivity = activity;
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    if (mLoadingWindow == null) {
                        mLoadingWindow = new AtomicReference<>();
                    }
                    if (mLoadingWindow.get() != null) {
                        mLoadingWindow.get().dismiss();
                    }
                    mLoadingWindow.set(new GolaLoadingWindowSSSof(finalActivity));
                    mLoadingWindow.get().showPopupWindow();
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            }
        }, waitMillis);
    }

    public static void dismissLoadingWindow() {
        mHandler.removeMessages(MSG_RETRY_SHOW);
        ThreadUtils.getMainHandler().post(new Runnable() {
            @Override
            public void run() {
                try {
                    if (mLoadingWindow == null) {
                        mLoadingWindow = new AtomicReference<>();
                    }
                    if (mLoadingWindow.get() != null) {
                        mLoadingWindow.get().dismiss();
                        mLoadingWindow.set(null);
                    }
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
