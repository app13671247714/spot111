package spot.differences.gola;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.ThreadUtils;

import net.goal.differences.BuildConfig;
import net.goal.differences.R;
import net.goal.differences.databinding.GameSpotWindowNewUserBinding;

import java.util.concurrent.atomic.AtomicReference;

import spot.game.manager.ConfigManager;
import spot.game.manager.ContextManager;
import spot.game.manager.GameManager;
import spot.game.manager.TrackManager;
import spot.game.model.JLogExtBean;

/**
 * 新人
 */
public class GolaNewUserWindowSSSof extends GolaBaseWindows {

    private GameSpotWindowNewUserBinding mBinding;
    private static AtomicReference<GolaNewUserWindowSSSof> mNewUserWindow;
    private static final int MSG_RETRY_SHOW = BuildConfig.VERSION_CODE;

    private static final Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_RETRY_SHOW:
                    showNewUserWindow(null);
                    break;
            }
        }
    };


    public GolaNewUserWindowSSSof(Context context) {
        super(context);
        mBinding = GameSpotWindowNewUserBinding.inflate(LayoutInflater.from(context));
        setContentView(mBinding.getRoot());
        setOutSideDismiss(true);
        ImageView imageView = new ImageView(getContext());
        imageView.setImageResource(R.color.bg_window_color);
        setBackgroundView(imageView);
        setPopupGravity(Gravity.CENTER);
        mBinding.tvGameSpotWindowNewUserReward.setText(Html.fromHtml(getContext().getResources().getString(R.string.game_spot_answer_5_questions_correctly_to_withdraw, ""+ConfigManager.getNeedLevel())));
        mBinding.tvGameSpotWindowNewUserRewardConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TrackManager.sendTrack(JLogExtBean.event(TrackManager.novice_click));
                TrackManager.sendTrack("NewUserWindow", "close", "");
                GameManager.newClick();
                dismiss();
            }
        });


    }

    /**
     * 展示
     */
    public static void showNewUserWindow(Activity activity) {
        try {
            if (!ActivityUtils.isActivityAlive(activity)) {
                activity = ContextManager.getActivity();
            }
        } catch (Exception e) {
            e.printStackTrace();
            mHandler.sendEmptyMessageDelayed(MSG_RETRY_SHOW, 1000L);
            return;
        }
        Activity finalActivity = activity;
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                try {
                    if (mNewUserWindow == null) {
                        mNewUserWindow = new AtomicReference<>();
                    }
                    if (mNewUserWindow.get() != null) {
                        mNewUserWindow.get().dismiss();
                    }
                    TrackManager.sendTrack("NewUserWindow", "show", "");
                    mNewUserWindow.set(new GolaNewUserWindowSSSof(finalActivity));
                    mNewUserWindow.get().showPopupWindow();
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static void dismissNewUserWindow() {
        mHandler.removeMessages(MSG_RETRY_SHOW);
        ThreadUtils.getMainHandler().post(new Runnable() {
            @Override
            public void run() {
                try {
                    if (mNewUserWindow == null) {
                        mNewUserWindow = new AtomicReference<>();
                    }
                    if (mNewUserWindow.get() != null) {
                        mNewUserWindow.get().dismiss();
                        mNewUserWindow.set(null);
                    }
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            }
        });
    }


    @Override
    public boolean onBeforeDismiss() {
        return super.onBeforeDismiss();
    }
}
