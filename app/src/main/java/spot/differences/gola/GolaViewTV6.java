package spot.differences.gola;

import android.content.Context;
import android.util.AttributeSet;

import spot.game.view.gameFindFileDiffFile.GameFindFileErrorViewFile;

public class GolaViewTV6 extends GameFindFileErrorViewFile {
    public GolaViewTV6(Context context) {
        super(context);
    }

    public GolaViewTV6(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GolaViewTV6(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public GolaViewTV6(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
}
