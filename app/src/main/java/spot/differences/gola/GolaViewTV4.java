package spot.differences.gola;

import android.content.Context;
import android.util.AttributeSet;

import spot.game.view.gameFindFileDiffFile.GameFindFileDiffViewFile;

public class GolaViewTV4 extends GameFindFileDiffViewFile {
    public GolaViewTV4(Context context) {
        super(context);
    }

    public GolaViewTV4(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
}
