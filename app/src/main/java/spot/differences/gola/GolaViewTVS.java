package spot.differences.gola;

import android.content.Context;
import android.util.AttributeSet;

import spot.game.view.textView.StrokeTextView2;

public class GolaViewTVS extends StrokeTextView2 {
    public GolaViewTVS(Context context) {
        super(context);
    }

    public GolaViewTVS(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GolaViewTVS(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
}
