package spot.differences.gola;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.ThreadUtils;

import net.goal.differences.BuildConfig;
import net.goal.differences.R;
import net.goal.differences.databinding.GameSpotDialogNetBinding;

import java.util.concurrent.atomic.AtomicReference;

import spot.game.manager.ContextManager;
import spot.game.manager.GameManager;
import spot.game.util.FastClickUtil;

/**
 * @Class: NetErrorWindow
 */
public class GolaUserNetErrorWindow extends GolaBaseWindows {
    private GameSpotDialogNetBinding mBinding;
    private static final int MSG_RETRY_SHOW = BuildConfig.VERSION_CODE;

    private static AtomicReference<GolaUserNetErrorWindow> mNetErrorWindow;

    public static void dismissNetErrorWindow() {
        mHandler.removeMessages(MSG_RETRY_SHOW);
        ThreadUtils.getMainHandler().post(new Runnable() {
            @Override
            public void run() {
                try {
                    if (mNetErrorWindow == null) {
                        mNetErrorWindow = new AtomicReference<>();
                    }
                    if (mNetErrorWindow.get() != null) {
                        mNetErrorWindow.get().dismiss();
                        mNetErrorWindow.set(null);
                    }
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public GolaUserNetErrorWindow(Context context, OnNetErrorListener onNetErrorListener) {
        super(context);
        mBinding = GameSpotDialogNetBinding.inflate(LayoutInflater.from(context));
        setContentView(mBinding.getRoot());
        setOutSideDismiss(false);
        ImageView imageView = new ImageView(getContext());
        imageView.setImageResource(R.color.bg_window_color);
        setBackgroundView(imageView);
        setPopupGravity(Gravity.CENTER);
        mBinding.tvGameSpotInfoProtectConfirm.setOnClickListener(new ClickUtils.OnDebouncingClickListener(FastClickUtil.S_SPACE_TIME) {
            @Override
            public void onDebouncingClick(View v) {
                GameManager.newClick();
                if (onNetErrorListener != null) {
                    onNetErrorListener.onConfirm();
                }
            }
        });
    }


    public interface OnNetErrorListener {
        void onConfirm();
    }

    private static final Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            try {
                switch (msg.what) {
                    case MSG_RETRY_SHOW:
                        showNetErrorWindow((OnNetErrorListener) msg.obj);
                        break;
                }
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * 展示
     *
     * @param onNetErrorListener
     */
    public static void showNetErrorWindow(OnNetErrorListener onNetErrorListener) {
        Activity activity = null;
        try {
            activity = ContextManager.getActivity();
        } catch (Exception e) {
            e.printStackTrace();
            mHandler.sendMessageDelayed(Message.obtain(mHandler, MSG_RETRY_SHOW, onNetErrorListener), 1000L);
            return;
        }
        Activity finalActivity = activity;
        ThreadUtils.getMainHandler().post(new Runnable() {
            @Override
            public void run() {
                try {
                    if (mNetErrorWindow == null) {
                        mNetErrorWindow = new AtomicReference<>();
                    }
                    if (mNetErrorWindow.get() != null) {
                        mNetErrorWindow.get().dismiss();
                    }
                    mNetErrorWindow.set(new GolaUserNetErrorWindow(finalActivity, onNetErrorListener));
                    mNetErrorWindow.get().showPopupWindow();
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            }
        });
    }

}
