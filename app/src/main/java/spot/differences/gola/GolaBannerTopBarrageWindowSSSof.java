package spot.differences.gola;

import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.view.Gravity;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import net.goal.differences.BuildConfig;
import net.goal.differences.R;

import spot.game.model.WithdrawBarrageList;
import razerdp.basepopup.BasePopupWindow;
import razerdp.util.animation.AnimationHelper;
import razerdp.util.animation.TranslationConfig;
/**
 * WithdrawBarrageWindow
 * 滚动弹幕窗-全局
 */
public class GolaBannerTopBarrageWindowSSSof extends BasePopupWindow {
    ImageView ivUserIcon, ivChannel;
    TextView tvSuccessHint;

    public GolaBannerTopBarrageWindowSSSof(Context context) {
        super(context);
        setContentView(R.layout.game_spot_view_barrage_withdraw_tips);
        setOutSideDismiss(false);
        setTouchable(false);
        setOutSideTouchable(true);
        setBackgroundColor(Color.TRANSPARENT);
        setPopupGravity(Gravity.TOP);
        setPriority(Priority.HIGH);
        initView();
    }



    private void initView() {
        ivUserIcon = findViewById(R.id.iv_game_spot_view_barrage_withdraw_tips_head_icon);
        ivChannel = findViewById(R.id.iv_game_spot_view_barrage_withdraw_tips_withdraw_channel);
        tvSuccessHint = findViewById(R.id.iv_game_spot_view_barrage_withdraw_tips_content);
    }


    public void start(WithdrawBarrageList.SpotGolaBarrageBean bean) {
        if (bean == null || bean.getHeadIcon() == null) {
            return;
        }
        try {
            //
            Glide.with(getContentView()).load(bean.getHeadIcon()).into(ivUserIcon);
            int resId = BuildConfig.VERSION_CODE;
            if ("Pagbank".equalsIgnoreCase(bean.getChannel())) {
                resId = R.mipmap.game_spot_barrage_pagbank;
            } else if ("PIX".equalsIgnoreCase(bean.getChannel())) {
                resId = R.mipmap.game_spot_barrage_pix;
            } else if ("Boleto".equalsIgnoreCase(bean.getChannel())) {
                resId = R.mipmap.game_spot_barrge_boleto;
            } else if ("dana".equalsIgnoreCase(bean.getChannel())) {
                resId = R.mipmap.game_spot_barrage_dana;
            } else if ("ovo".equalsIgnoreCase(bean.getChannel())) {
                resId = R.mipmap.game_spot_barrage_ovo;
            } else if ("ShopeePay".equalsIgnoreCase(bean.getChannel())) {
                resId = R.mipmap.game_spot_barrage_shopeepay;
            } else if ("Amazon".equalsIgnoreCase(bean.getChannel())) {
                resId = R.mipmap.game_spot_barrage_amazon;
            } else if ("MasterCard".equalsIgnoreCase(bean.getChannel())) {
                resId = R.mipmap.game_spot_barrage_mastercard;
            } else {
                resId = R.mipmap.game_spot_barrage_paypal;
            }
            Glide.with(getContentView()).load(resId).into(ivChannel);
            //
            tvSuccessHint.setText(Html.fromHtml(getContext().getResources().getString(R.string.game_spot_withdaw_barrage_content, bean.getName(), bean.getMoney())));
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }


    @Override
    protected Animation onCreateShowAnimation() {
        Animation animation = AnimationHelper.asAnimation()
                .withTranslation(TranslationConfig.FROM_RIGHT)
                .toShow();
        animation.setDuration(4_000);
        return animation;
    }

    @Override
    protected Animation onCreateDismissAnimation() {
        Animation animation = AnimationHelper.asAnimation()
                .withTranslation(TranslationConfig.TO_LEFT)
                .toDismiss();
        animation.setDuration(4_000);
        return animation;
    }

    //@Override
    //public void onShowing() {
    //    super.onShowing();
    //    this.dismiss();
    //}
}
