package spot.differences.gola;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.annotation.NonNull;

import net.goal.differences.BuildConfig;
import net.goal.differences.R;

import java.util.concurrent.atomic.AtomicInteger;

import spot.game.event.EventConstant;
import spot.game.event.EventMsg;
import spot.game.event.RxBus;
import razerdp.basepopup.BasePopupWindow;

/**
 * BaseWindow
 */
public class GolaBaseWindows extends BasePopupWindow {

    static AtomicInteger mShowingCount = new AtomicInteger();
    public static final int MSG_WINDOW_HAS_SHOWING = BuildConfig.VERSION_CODE;
    public static final int MSG_WINDOW_ALL_DISMISS = MSG_WINDOW_HAS_SHOWING + 1;
    Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_WINDOW_HAS_SHOWING:
                    mHandler.removeMessages(MSG_WINDOW_HAS_SHOWING);
                    mHandler.removeMessages(MSG_WINDOW_ALL_DISMISS);
                    //
                    RxBus.getInstance().post(EventMsg.event(EventConstant.CODE_EVENT_WINDOW_HAS_SHOWING).build());
                    break;
                case MSG_WINDOW_ALL_DISMISS:
                    mHandler.removeMessages(MSG_WINDOW_HAS_SHOWING);
                    mHandler.removeMessages(MSG_WINDOW_ALL_DISMISS);
                    //
                    RxBus.getInstance().post(EventMsg.event(EventConstant.CODE_EVENT_WINDOW_ALL_DISMISS).build());
                    break;
            }
        }
    };

    public GolaBaseWindows(Context context) {
        super(context);
    }

    @Override
    public boolean onBeforeShow() {
        if (mShowingCount == null) {
            mShowingCount = new AtomicInteger();
        }
        int count = mShowingCount.incrementAndGet();
        //LogUtils.e("count=" + count);
        if (count > 0) {
            mHandler.removeMessages(MSG_WINDOW_ALL_DISMISS);
            mHandler.sendEmptyMessageDelayed(MSG_WINDOW_HAS_SHOWING, 500);
        }
        return super.onBeforeShow();
    }

    @Override
    public boolean onBeforeDismiss() {
        if (mShowingCount == null) {
            mShowingCount = new AtomicInteger();
        }
        int count = mShowingCount.decrementAndGet();
        //LogUtils.e("count=" + count);
        if (count < 1) {
            mHandler.removeMessages(MSG_WINDOW_HAS_SHOWING);
            mHandler.sendEmptyMessageDelayed(MSG_WINDOW_ALL_DISMISS, 500);
        }
        return super.onBeforeDismiss();
    }



    @Override
    protected Animation onCreateShowAnimation() {
        return AnimationUtils.loadAnimation(getContext(), R.anim.game_spot_st_dialog_in);
    }

    @Override
    protected Animation onCreateDismissAnimation() {
        return AnimationUtils.loadAnimation(getContext(), R.anim.game_spot_st_dialog_out);
    }

    @Override
    public boolean onBackPressed() {
        //return super.onBackPressed();
        return false;
    }
}
