package spot.differences.gola;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.ClickUtils;

import net.goal.differences.R;

import spot.game.manager.BalanceManager;
import spot.game.manager.ConfigManager;
import spot.game.manager.ContextManager;
import spot.game.manager.GameManager;
import spot.game.manager.IconSelectManager;
import spot.game.model.backModel.ResultAnswerResponse;
import spot.game.util.FastClickUtil;
import spot.game.util.ViewUtils;

/**
 * CorrectAnswerWindow
 */
public class GolaAThisCorrectAnswerWindowSSSof extends GolaBaseWindows {
    ImageView ivRewardIcon;
    TextView tvReward, tvNormalReward, tvTitle, tvShowAd;
    LinearLayout llMoreReward;

    public GolaAThisCorrectAnswerWindowSSSof(Context context) {
        super(context);
        setContentView(R.layout.game_spot_window_correct_answer);
        setOutSideDismiss(false);
        ImageView imageView = new ImageView(getContext());
        imageView.setImageResource(R.color.bg_window_color);
        setBackgroundView(imageView);
        setPopupGravity(Gravity.CENTER);
        initView();
    }

    private void initView() {
        //TextView viewById = findViewById(R.id.tv_game_spot_window_correct_answer_reward_random);
        //viewById.setText(Html.fromHtml(getContext().getResources().getString(R.string.random3_5)));
        //
        //TextView tvAmount = findViewById(R.id.tv_game_spot_window_correct_answer_total_price);
        //tvAmount.setText(ConfigManager.getCurrency() + GameManager.getFormatBalance());
        //ImageView ivCurrency = findViewById(R.id.iv_game_spot_window_correct_answer_money_currency);
        //ivCurrency.setImageResource(GameManager.getSingleRewardIcon());
        //
        tvTitle = findViewById(R.id.tv_game_spot_window_correct_answer_title);
        ViewUtils.autoResizeText(tvTitle, ContextManager.getContext().getString(R.string.game_spot_congratuiations), 24, 18, 42, 8, 1);
        //
        ivRewardIcon = findViewById(R.id.iv_game_spot_window_correct_answer_reward_icon);
        ivRewardIcon.setImageResource(IconSelectManager.getManyRewardIcon());
        tvReward = findViewById(R.id.tv_game_spot_window_correct_answer_reward);
        //
        tvShowAd = findViewById(R.id.tv_game_spot_window_correct_answer_reward_random_start);
        //
        llMoreReward = findViewById(R.id.ll_game_spot_window_correct_answer_reward_random);
        tvNormalReward = findViewById(R.id.tv_game_spot_window_correct_answer_reward_next);
        llMoreReward.setOnClickListener(new ClickUtils.OnDebouncingClickListener(FastClickUtil.S_SPACE_TIME) {
            @Override
            public void onDebouncingClick(View v) {
                GameManager.newClick();
                if (mCorrectListener != null) {
                    mCorrectListener.showAd();
                }
            }
        });
        tvNormalReward.setOnClickListener(new ClickUtils.OnDebouncingClickListener(FastClickUtil.S_SPACE_TIME) {
            @Override
            public void onDebouncingClick(View v) {
                GameManager.newClick();
                if (mCorrectListener != null) {
                    mCorrectListener.close();
                }
            }
        });
    }

    public void setParam(ResultAnswerResponse response, CorrectListener correctListener) {
        if (response != null) {
            tvReward.setText("+ " + ConfigManager.getCurrency() + BalanceManager.getFormatBalance(response.getAwMoney()));
            ViewUtils.autoResizeText(tvShowAd, ConfigManager.getCurrency() + BalanceManager.getFormatBalance(2 * response.getAwMoney()), 30, 13, 36, 11, 1);
        }
        mCorrectListener = correctListener;
    }


    CorrectListener mCorrectListener;

    public interface CorrectListener {
        void showAd();

        void close();
    }

    @Override
    public void showPopupWindow() {
        GameManager.newClick(R.raw.game_spot_correct_answer);
        super.showPopupWindow();
    }

    @Override
    public boolean onBeforeDismiss() {
        return super.onBeforeDismiss();
    }
}

