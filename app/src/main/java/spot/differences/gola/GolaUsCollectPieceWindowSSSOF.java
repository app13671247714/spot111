package spot.differences.gola;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ThreadUtils;
import com.blankj.utilcode.util.ToastUtils;

import net.goal.differences.BuildConfig;
import net.goal.differences.R;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import spot.game.adapter.CollectPieceAdapter;
import spot.game.manager.ConfigManager;
import spot.game.manager.ContextManager;
import spot.game.manager.GameManager;
import spot.game.manager.TrackManager;
import spot.game.model.CollectPieceBean;
import spot.game.model.JLogExtBean;
import spot.game.model.backModel.JGCResponse;
import spot.game.util.AnimationUtils;
import spot.game.util.ViewUtils;

/**
 * 碎片收集
 */
public class GolaUsCollectPieceWindowSSSOF extends GolaBaseWindows implements View.OnClickListener {
    CollectPieceAdapter mCollectPieceAdapter;
    ProgressBar pbPhoneProgress;
    TextView tvPhoneProgress, tvPhoneConfirm, tvTitleDesc;
    List<CollectPieceBean> mCollectPieceBeanList;
    ImageView ivRotateAnimate;

    static AtomicBoolean isRequest = new AtomicBoolean();
    AtomicBoolean isDone = new AtomicBoolean();

    ThreadUtils.SimpleTask<Object> mLoadDataTask, mRefreshViewTask;
    private static final int MSG_RETRY_SHOW = BuildConfig.VERSION_CODE;
    private static final int MSG_SET_VIEW = MSG_RETRY_SHOW + 1;

    Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_RETRY_SHOW:
                    mHandler.removeMessages(MSG_RETRY_SHOW);
                    mHandler.removeCallbacksAndMessages(null);
                    showPopupWindow();
                    break;
                case MSG_SET_VIEW:
                    mHandler.removeMessages(MSG_SET_VIEW);
                    mHandler.removeCallbacksAndMessages(null);
                    setView();
                    break;
            }
        }
    };

    public GolaUsCollectPieceWindowSSSOF(Context context) {
        super(context);
        setContentView(R.layout.game_spot_window_collect_piece);
        setOutSideDismiss(true);
        ImageView imageView = new ImageView(getContext());
        imageView.setImageResource(R.color.bg_window_color);
        setBackgroundView(imageView);
        setPopupGravity(Gravity.CENTER);
        //
        initView();
    }

    private void initView() {
        pbPhoneProgress = findViewById(R.id.pb_game_spot_window_collect_piece_item_progress);
        tvPhoneProgress = findViewById(R.id.tv_game_spot_window_collect_piece_item_progress_content);
        tvPhoneConfirm = findViewById(R.id.tv_game_spot_window_collect_piece_item_progress_confirm);
        ivRotateAnimate = findViewById(R.id.iv_game_spot_window_collect_piece_phone_bg_anim_2);
        tvTitleDesc = findViewById(R.id.tv_game_spot_window_collect_piece_header_tips);
        ViewUtils.autoResizeText(tvTitleDesc, ContextManager.getContext().getString(R.string.game_spot_collect_get_iphone), 13, 40, 53, 10, 1);
        //
        AnimationUtils.playRotationAnimation(ivRotateAnimate, 3_000);
        //
        ImageView ivClose = findViewById(R.id.iv_game_spot_window_collect_piece_close);
        ivClose.setOnClickListener(this::onClick);
        tvPhoneConfirm.setOnClickListener(this::onClick);
        //
        mCollectPieceAdapter = new CollectPieceAdapter();
        mCollectPieceAdapter.setOnItemClickListener(new CollectPieceAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, CollectPieceBean pos, int position) {
                try {
                    TrackManager.sendTrack(JLogExtBean.event("CollectPiece", "position=" + position).setIntvalue(ConfigManager.getLocalLevel()));
                    if (pos == null) {
                        return;
                    }
                    if (pos.getState() != CollectPieceBean.COLLECT_PIECE_COMPLETE) {
                        return;
                    }
                    GameManager.newClick();
                    ConfigManager.setCollectPieceCompleteCount(ConfigManager.getCollectPieceCompleteCount() + pos.getRewardCount());
                    mCollectPieceBeanList.get(position).setState(CollectPieceBean.COLLECT_PIECE_REWARDED);
                    //
                    ThreadUtils.executeByIo(getRefreshViewTask());
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            }
        });
        RecyclerView recyclerView = findViewById(R.id.rv_game_spot_window_collect_piece_content);
        LinearLayoutManager layout = new LinearLayoutManager(getContext());
        layout.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layout);
        recyclerView.setAdapter(mCollectPieceAdapter);
    }

    @Override
    public void showPopupWindow() {
        if (isRequest == null) {
            isRequest = new AtomicBoolean();
            isRequest.set(false);
        }
        if (isRequest.get()) {
            mHandler.sendEmptyMessageDelayed(MSG_RETRY_SHOW, 1000);
            return;
        }
        if (isDone == null) {
            isDone = new AtomicBoolean();
            isDone.set(false);
        }
        if (isDone.get()) {
            mHandler.removeCallbacksAndMessages(null);
            mHandler.removeMessages(MSG_RETRY_SHOW);
            setView();
            super.showPopupWindow();
            return;
        }
        //
        isRequest.set(true);
        isDone.set(false);
        initTaskList();
        //
        mHandler.sendEmptyMessageDelayed(MSG_RETRY_SHOW, 1000);
    }

    private void setView() {
        //
        int collectPieceCompleteCount = ConfigManager.getCollectPieceCompleteCount();
        int collectPieceTotalCount = ConfigManager.getCollectPieceTotalCount();
        pbPhoneProgress.setMax(collectPieceTotalCount);
        tvPhoneProgress.setText(collectPieceCompleteCount + "/" + collectPieceTotalCount);
        //
        if (collectPieceCompleteCount > 0 && collectPieceCompleteCount < collectPieceTotalCount * 0.1) {
            collectPieceCompleteCount = (int) (collectPieceTotalCount * 0.1);
        }
        pbPhoneProgress.setProgress(collectPieceCompleteCount);
        //
        if (mCollectPieceBeanList == null) {
            mCollectPieceBeanList = new ArrayList<>();
        }
        mCollectPieceAdapter.setData(mCollectPieceBeanList);
        mCollectPieceAdapter.notifyDataSetChanged();
    }

    private void initTaskList() {
        ThreadUtils.executeByIo(getLoadDataTask());
    }

    private void taskDone() {
        if (isDone == null) {
            isDone = new AtomicBoolean();
        }
        isDone.set(true);
        //
        if (isRequest == null) {
            isRequest = new AtomicBoolean();
        }
        isRequest.set(false);
    }

    //需运行在子线程，更新hawk数据库
    private void checkThemeTaskState() {
        if (mCollectPieceBeanList == null || mCollectPieceBeanList.isEmpty()) {
            return;
        }
        //
        int themeTaskDoingCount = mCollectPieceBeanList.size();
        //
        int nowLevelNum = ConfigManager.getNowLevelCount();//起始
        int nowVideoNum = ConfigManager.getNowVideoCount();
        int nowLoginNum = ConfigManager.getNowLoginCount();
        //
        for (int i = 0; i < mCollectPieceBeanList.size(); i++) {
            CollectPieceBean collectPieceBean = mCollectPieceBeanList.get(i);
            if (collectPieceBean == null) {
                continue;
            }
            //
            int state = collectPieceBean.getState();
            //
            int needVideoNum = 0;
            int needLoginNum = 0;
            int needLevelNum = 0;
            if (collectPieceBean.getType() == 1) {
                needLevelNum = collectPieceBean.getTotalProgress();
            } else if (collectPieceBean.getType() == 2) {
                needVideoNum = collectPieceBean.getTotalProgress();
            } else {
                needLoginNum = collectPieceBean.getTotalProgress();
            }
            //已完成或已领取则不再处理
            if (state == CollectPieceBean.COLLECT_PIECE_REWARDED || state == CollectPieceBean.COLLECT_PIECE_COMPLETE) {
                collectPieceBean.setCurProgress(collectPieceBean.getTotalProgress());
                continue;
            }
            //任务完成
            if (nowVideoNum >= needVideoNum && nowLoginNum >= needLoginNum && nowLevelNum >= needLevelNum) {
                themeTaskDoingCount = i + 1;
                collectPieceBean.setCurProgress(collectPieceBean.getTotalProgress());
                collectPieceBean.setState(CollectPieceBean.COLLECT_PIECE_COMPLETE);
                continue;
            }
            //
            if (state == CollectPieceBean.COLLECT_PIECE_DOING) {
                themeTaskDoingCount = i;
            }
            //任务完成，下一任务解锁
            if (i == themeTaskDoingCount) {
                collectPieceBean.setState(CollectPieceBean.COLLECT_PIECE_DOING);
                //
                if (collectPieceBean.getType() == 1) {
                    collectPieceBean.setCurProgress(nowLevelNum);
                } else if (collectPieceBean.getType() == 2) {
                    collectPieceBean.setCurProgress(nowVideoNum);
                } else {
                    collectPieceBean.setCurProgress(nowLoginNum);
                }
                continue;
            }
            collectPieceBean.setState(CollectPieceBean.COLLECT_PIECE_UNLOCK);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_game_spot_window_collect_piece_close:
                GameManager.newClick();
                dismiss();
                break;
            case R.id.tv_game_spot_window_collect_piece_item_progress_confirm:
                TrackManager.sendTrack(JLogExtBean.event("CollectPiece", "confirm").setIntvalue(ConfigManager.getLocalLevel()));
                GameManager.newClick();
                ToastUtils.showShort(getContext().getResources().getString(R.string.game_spot_theme_task_not_complete));
                break;
        }
    }

    @Override
    public boolean onBeforeDismiss() {
        taskDone();
        mCollectPieceBeanList = null;
        ivRotateAnimate = null;
        mHandler.removeMessages(MSG_RETRY_SHOW);
        mHandler.removeMessages(MSG_SET_VIEW);
        mHandler.removeCallbacksAndMessages(null);
        //
        if (mLoadDataTask != null) {
            mLoadDataTask.onCancel();
        }
        mLoadDataTask = null;
        //
        if (mRefreshViewTask != null) {
            mRefreshViewTask.onCancel();
        }
        mRefreshViewTask = null;
        return super.onBeforeDismiss();
    }


    /**
     * ThreadUtils: Task can only be executed once.
     *
     * @return
     */
    private ThreadUtils.SimpleTask<Object> getRefreshViewTask() {
        if (mRefreshViewTask != null) {
            mRefreshViewTask.onCancel();
            mRefreshViewTask = null;
        }
        mRefreshViewTask = new ThreadUtils.SimpleTask<Object>() {
            @Override
            public Object doInBackground() throws Throwable {
                try {
                    checkThemeTaskState();
                    ConfigManager.saveCollectPieceList(mCollectPieceBeanList);
                    mHandler.sendEmptyMessage(MSG_SET_VIEW);
                } catch (Throwable e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            public void onSuccess(Object result) {

            }
        };
        return mRefreshViewTask;
    }

    private ThreadUtils.SimpleTask<Object> getLoadDataTask() {
        if (mLoadDataTask != null) {
            mLoadDataTask.onCancel();
            mLoadDataTask = null;
        }
        mLoadDataTask = new ThreadUtils.SimpleTask<Object>() {
            @Override
            public Object doInBackground() throws Throwable {
                try {
                    //已有缓存
                    if (mCollectPieceBeanList != null && !mCollectPieceBeanList.isEmpty()) {
                        checkThemeTaskState();
                        return null;
                    }
                    if (mCollectPieceBeanList == null) {
                        mCollectPieceBeanList = new ArrayList<>();
                    }
                    //
                    List<CollectPieceBean> cpList = ConfigManager.readCollectPieceList();
                    if (cpList == null || cpList.isEmpty()) {
                        JGCResponse response = ConfigManager.getServerGameConfigEncryptBody();
                        if (response == null || response.getPtc() == null) {
                            return null;
                        }
                        //本地配置
                        cpList = response.getPtc().getTasks();
                        if (cpList == null || cpList.isEmpty()) {
                            return null;
                        }
                        int size = cpList.size();
                        for (int i = 0; i < size; i++) {
                            CollectPieceBean cp = cpList.get(i);
                            if (cp == null) {
                                continue;
                            }
                            //默认第一个进行中
                            if (i == 0) {
                                cp.setState(CollectPieceBean.COLLECT_PIECE_DOING);
                            } else {
                                cp.setState(CollectPieceBean.COLLECT_PIECE_UNLOCK);
                            }
                            cp.setCurProgress(0);
                            cp.setStartLevelNum(0);
                            cp.setStartLoginNum(0);
                            cp.setStartVideoNum(0);
                            cp.setType(1);//过关
                            cp.setUrlRewardIcon("");
                            cp.setTaskDesc(ContextManager.getContext().getResources().getString(R.string.game_spot_reach_level, " " + cp.getTotalProgress()));
                            cp.setBtnText(ContextManager.getContext().getResources().getString(R.string.game_spot_receive));
                            mCollectPieceBeanList.add(cp);
                        }
                    } else {
                        mCollectPieceBeanList.addAll(cpList);
                    }
                    checkThemeTaskState();
                } catch (Throwable e) {
                    e.printStackTrace();
                } finally {
                    ConfigManager.saveCollectPieceList(mCollectPieceBeanList);
                    taskDone();
                    if (mHandler != null) {
                        mHandler.sendEmptyMessage(MSG_RETRY_SHOW);
                    }
                }
                return null;
            }

            @Override
            public void onSuccess(Object result) {

            }
        };
        return mLoadDataTask;
    }
}

