package spot.differences.gola;

import android.content.Context;
import android.util.AttributeSet;

import spot.game.view.NoScrollViewPager;

public class GolaViewTV3 extends NoScrollViewPager {
    public GolaViewTV3(Context context) {
        super(context);
    }

    public GolaViewTV3(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
}
