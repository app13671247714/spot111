package spot.differences.gola;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.ThreadUtils;

import net.goal.differences.BuildConfig;
import net.goal.differences.R;

import java.util.concurrent.atomic.AtomicReference;

import spot.game.constant.GameConfig;
import spot.game.manager.ConfigManager;
import spot.game.manager.ContextManager;
import spot.game.manager.GameManager;
import spot.game.manager.TrackManager;
import spot.game.util.ViewUtils;
/**
 * SettingWindow
 */
public class GolaSSUserSettingWindow extends GolaBaseWindows {
    TextView tvUserTerm, tvPolicy;
    CheckBox cbSound;
    private static AtomicReference<GolaSSUserSettingWindow> mSettingWindow;
    private static final int MSG_RETRY_SHOW = BuildConfig.VERSION_CODE;

    private static final Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_RETRY_SHOW:
                    showSettingWindow(null);
                    break;
            }
        }
    };

    public GolaSSUserSettingWindow(Context context) {
        super(context);
        setContentView(R.layout.game_spot_window_setting);
        setOutSideDismiss(true);
        ImageView imageView = new ImageView(getContext());
        imageView.setImageResource(R.color.bg_window_color);
        setBackgroundView(imageView);
        setPopupGravity(Gravity.CENTER);
        //tvRestart = findViewById(R.id.tv_game_spot_window_setting_restart);
        tvUserTerm = findViewById(R.id.tv_game_spot_window_setting_help);
        tvPolicy = findViewById(R.id.tv_game_spot_window_setting_document);
        cbSound = findViewById(R.id.iv_game_spot_window_setting_sound);
        //
        ViewUtils.autoResizeText(tvUserTerm, Html.fromHtml(getContext().getResources().getString(R.string.game_spot_term_of_use)), 17, 19, 13);
        tvUserTerm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TrackManager.sendTrack("SettingWindow", "UserTerm", "");
                GameManager.newClick();
                Uri uri = Uri.parse(GameConfig.PRIVACY_POLICY);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ActivityUtils.startActivity(intent);
            }
        });
        tvPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TrackManager.sendTrack("SettingWindow", "Policy", "");
                GameManager.newClick();
                Uri uri = Uri.parse(GameConfig.USER_POLICY);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ActivityUtils.startActivity(intent);
            }
        });
        cbSound.setChecked(ConfigManager.getSoundSetting());
        cbSound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TrackManager.sendTrack("SettingWindow", "Sound", "" + cbSound.isChecked());
                GameManager.newClick();
                ConfigManager.setSoundSetting(cbSound.isChecked());
                if (cbSound.isChecked()) {
                    GameManager.initBGMMedia();
                } else {
                    GameManager.stopMedia();
                }
            }
        });

        ImageView ivClose = findViewById(R.id.iv_game_spot_window_setting_close);
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GameManager.newClick();
                dismiss();
            }
        });

        //tvRestart.setOnClickListener(new View.OnClickListener() {
        //    @Override
        //    public void onClick(View v) {
        //        RxBus.getInstance().post(EventMsg.event(EventConstant.CODE_EVENT_RESTART_GAME).build());
        //        dismiss();
        //    }
        //});
    }

    public static void showSettingWindow(Activity activity) {
        try {
            if (!ActivityUtils.isActivityAlive(activity)) {
                activity = ContextManager.getActivity();
            }
        } catch (Exception e) {
            e.printStackTrace();
            mHandler.sendEmptyMessageDelayed(MSG_RETRY_SHOW, 1000L);
            return;
        }
        Activity finalActivity = activity;
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                try {
                    if (mSettingWindow == null) {
                        mSettingWindow = new AtomicReference<>();
                    }
                    if (mSettingWindow.get() != null) {
                        mSettingWindow.get().dismiss();
                    }
                    TrackManager.sendTrack("SettingWindow", "show", "");
                    mSettingWindow.set(new GolaSSUserSettingWindow(finalActivity));
                    mSettingWindow.get().showPopupWindow();
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static void dismissSettingWindow() {
        mHandler.removeMessages(MSG_RETRY_SHOW);
        ThreadUtils.getMainHandler().post(new Runnable() {
            @Override
            public void run() {
                try {
                    if (mSettingWindow == null) {
                        mSettingWindow = new AtomicReference<>();
                    }
                    if (mSettingWindow.get() != null) {
                        mSettingWindow.get().dismiss();
                        mSettingWindow.set(null);
                    }
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            }
        });
    }
}

