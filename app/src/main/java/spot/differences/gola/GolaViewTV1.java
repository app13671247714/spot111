package spot.differences.gola;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.Nullable;

import spot.game.view.textView.SerifHeiMediumTextView;

public class GolaViewTV1 extends SerifHeiMediumTextView {
    public GolaViewTV1(Context context) {
        super(context);
    }

    public GolaViewTV1(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public GolaViewTV1(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
