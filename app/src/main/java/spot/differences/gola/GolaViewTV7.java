package spot.differences.gola;

import android.content.Context;
import android.util.AttributeSet;

import net.lucode.hackware.magicindicator.MagicIndicator;


public class GolaViewTV7 extends MagicIndicator {
    public GolaViewTV7(Context context) {
        super(context);
    }

    public GolaViewTV7(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
}
